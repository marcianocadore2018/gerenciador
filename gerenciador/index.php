<?php
include ( 'protected/libs/Email.php' );                             # Include da lib e-mail sent
require_once ('./protected/conexao.php');

# LOAD DINAMIC MODELS
foreach (glob("./protected/model/*.php") as $filename) {
    include $filename;
}

require_once ( './protected/libs/controlador.php' );                # Include Default controller
require_once ( './protected/libs/slimFramework/Slim/Slim.php' );    # Include SlimFramework
require_once ( './controlelogin/conectadosession.php');             # Include session controller
require_once ( './protected/libs/verificadiretorio.php' );
include ( './protected/libs/verificaambiente.php' );

include ( './ws/index.php' );                                       # Include WS (WebService) start 
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $title;?></title>
        <link href="includes/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="includes/css/datatables.min.css">
        <link rel="stylesheet" href="includes/css/bootstrap-select.min.css">
        <link rel="shortcut icon" href="http://atlantajeans.com.br/gerenciador/includes/imagens/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskMoney.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="includes/js/validarcampos.js" type="text/javascript"></script>
        <script src="includes/js/vanilla-masker.min.js" type="text/javascript"></script>
        <script src="includes/js/bootstrap-select.min.js" type="text/javascript"></script>

        <!-- Excluir Registro - Mensagem-->
        <script src="includes/js/jquery-1.11.3.js"></script>
        <script src="includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="includes/css/jquery-ui.css" type="text/css" />

        <!-- Chosen -->
        <script src="includes/js/plugins/chosen/chosen.jquery.js"></script>
        <!-- Chosen -->
        <link href="includes/css/plugins/chosen/chosen.css" rel="stylesheet">
        <script type="text/javascript">

            function excluir(acao, controle, id) {

                $("#dialog").dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        "Cancelar": function () {
                            $(this).dialog("close");
                        },
                        "Confirmar": function () {
                            window.location.href = 'index.php?controle=' + controle + '&acao=' + acao + '&id=' + id;
                        }
                    }
                });
                $("#dialog").dialog("open");
            };
            
            //Abrir Modal do Relatório de Vendas
            function abrir(URL) {
                var width = 590;
                var height = 350;
                var left = 380;
                var top = 200;
                window.open(URL, 'janela', 'width=' + width +
                        ', height=' + height +
                        ', top=' + top +
                        ', left=' + left +
                        ', scrollbars=yes, \n\
                                 status=no, \n\
                                 toolbar=no, \n\
                                 location=no, \n\
                                 directories=no, \n\
                                 menubar=no, \n\
                                 resizable=no, \n\
                                 fullscreen=no');
                }
        </script>
        <script src="includes/js/jquery.easy-confirm-dialog.js"></script>
        <style type="text/css">
            .error{
                color: red;
                font-size: 12px;
            }
            hr {
                color: blue;
                background-color: blue;
                height: 1px;
            }
            .ui-dialog { font-size: 11px; }
            #question {
                width: 300px!important;
                height: 60px!important;
                padding: 10px 0 0 10px;
            }
            #question img {
                float: left;
            }
            #question span {
                float: left;
                margin: 20px 0 0 10px;
            }
            .ui-dialog 
            .ui-dialog-titlebar-close{
                background-image: url(includes/img/remove.png);
                background-color: white;
            }
            .ui-draggable 
            .ui-dialog-titlebar{
                background-color: #A44160;
            }
            .navbar-default 
            .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{background-color: #FFB5C5}
            #menutitle{
                color: white;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #A44160;
            }
            body {
                padding-top: 105px;
            }

            /*AtlantaJeans theme added*/
            .panel-primary>.panel-heading {
              color: #fff;
              background-color: #923a56;
              border-color: #f0dbe1;
            }
            .panel-primary{
              border-color: #f0dbe1;
            }
            a {
                color: #5b2436;
                text-decoration: none;
            }
            .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
                z-index: 2;
                color: #fff;
                cursor: default;
                background-color: #d0889f;
                border-color: #f0dbe1;
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button {
                box-sizing: border-box;
                display: inline-block;
                min-width: 1.5em;
                padding: 0.5em 1em;
                margin-left: 2px;
                text-align: center;
                text-decoration: none !important;
                cursor: pointer;
                color: #333 !important;
                border: 1px solid transparent;
                border-radius: 2px;
            }
            .btn-primary {
                color: #fff;
                background-color: #5b2436;
                border-color: #f0dbe1;
            }
            hr {
                color: #f0dbe1;
                background-color: #f0dbe1;
                height: 1px;
            }
            #box{
                width: 100px; 
                height: 35px;
                border-radius: 10px;
            }
            #valordisponivel{
                margin-top: 7px; 
                margin-bottom: 8px; 
                text-align: center; 
                font-weight:normal;
                color: white;
            }
            #mostrardata{
                display: none;
            }

            /*Rodapé*/
            .footer {
                position:absolute;
                bottom:0;
                left: 0px;
                width:100%;
                height: 50px;
                background: #A44160
            }
            p {
                max-width: 91%;
                text-align: right;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #A44160;
            }
            div.ui-datepicker{
             font-size:12px;
            }
        </style>
    </head>
</head>
<body>
    
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #A44160">
        <div class="container" style="padding-left: 0px">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style=""  id="menutitle"><?php echo $navtitle;?></a>
                <!-- ICONE REFERENTE A CLIENTE INADIMPLENTE -->
                <?php
                    date_default_timezone_set('America/Sao_Paulo');
                    $data = date('d/m/Y');
                    $hora = date('H:i');

                    $sqlclienteinadimplente   = pg_query("SELECT count(*) qtdclienteinadimplente
                                                            FROM parcelas pa
                                                            LEFT JOIN cliente cli
                                                              ON pa.idcliente = cli.id
                                                            LEFT JOIN registropessoa reg
                                                              ON cli.idregistropessoa = reg.id
                                                            LEFT JOIN estado e
                                                              ON reg.idestado = e.id
                                                            LEFT JOIN venda v
                                                              ON pa.idvenda = v.id
                                                            LEFT JOIN tipopagamento tipopag
                                                              ON v.formapgto = tipopag.id
                                                           WHERE (((CURRENT_DATE - v.datavenda) >= 30) or (CURRENT_DATE - pa.datavencimentoparcela) > 0)
                                                             AND pa.statusparcela = 'PE'
                                                             AND (CURRENT_DATE > pa.datavencimentoparcela)
                                                             AND (tipopag.tipo = 'R' and tipopag.parcelar = 'S')
                                                             AND v.qtdparcelas > 0;");
                    $resclienteinadimplente   = pg_fetch_array ($sqlclienteinadimplente);
                    $clienteinadimplentequantidade = $resclienteinadimplente['qtdclienteinadimplente'];
                ?>
                               
            </div>
            <p><lable style="color: white;"><b style="font-size: 12px;">Logado como:</b> <?php echo $_SESSION['login'];?>&nbsp;&nbsp;&nbsp;<b style="font-size: 12px;">Data/Hora:</b>&nbsp; <?php echo $data . ' - ' . $hora;?>
<a class="glyphicon glyphicon-user" href="index.php?controle=clienteinadimplenteController&acao=listar"  id="menutitle" style="left: 30px; font-size: 18px" title="Clientes Inadimplentes"> 
                    <b><?php echo $clienteinadimplentequantidade; ?></b>
                </a>
</lable>
</p>
            
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.php"  id="menutitle">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Clientes<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php if ($colaboradortipo == 'C' || $colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D') { ?>
                                <li><a href="index.php?controle=clienteController&acao=listar">Cadastro de Clientes</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=clienteautorizadoController&acao=listar">Cliente Autorizado</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=clienterapidoController&acao=listar">Cliente Rápido</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=clienteinadimplenteController&acao=listar">Relação de Clientes Inadimplentes</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=rendaController&acao=listar">Renda</a></li>
                            <?php } ?>
                            <?php if (($colaboradortipo == 'M') || ($colaboradortipo == 'G') || ($colaboradortipo == 'D')) { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=porcentagemcrediarioclienteController&acao=listar">Porcentagem Crediário</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=emailaniversarianteController&acao=listar">E-mails Aniversariantes</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php if ($colaboradortipo == 'M' || $colaboradortipo == 'G' || $colaboradortipo == 'D') { ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Colaboradores<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?controle=colaboradoresController&acao=listar">Cadastro de Colaboradores</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=documentacaoController&acao=listar">Documentação</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=cargoController&acao=listar">Cargo</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=setorController&acao=listar">Setor</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=informacaoadicionalcolaboradorController&acao=listar">Informação Adicional Colaborador</a></li>
                                <?php if ($colaboradortipo == 'D') { ?>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="index.php?controle=manutencaocolaboradorController&acao=listar">Manutenção de Colaboradores</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                        <?php if ($colaboradortipo == 'M' || $colaboradortipo == 'G' || $colaboradortipo == 'D') { ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Produtos<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?controle=produtoController&acao=listar">Cadastro de Produtos</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=trocaprodutoController&acao=novo">Troca de Produtos</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=fotoprodutoController&acao=listar">Foto Produto</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=grupoController&acao=listar">Grupo Produtos</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=retiradaController&acao=listar">Retirada Produto</a></li>
                                
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=permissaocolaboradorProdutosController&acao=listar">Permissão Colaboradores - Produtos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Pagamentos<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controle=pagamentoController&acao=novo">Pagamentos</a></li>
                            <?php if ($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D') { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=pagamentotrocaController&acao=listar">Pagamentos Diferenças Trocas</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=diferencabonustrocaController&acao=listar">Diferenças Bônus Trocas</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=parcelamentoController&acao=listar">Parcelamento</a></li>
				<li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=tipopagamentoController&acao=listar">Tipo Pagamento</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=descontomaximoController&acao=listar">Desconto Máximo</a></li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Vendas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controle=vendaController&acao=listar">Cadastro de Vendas</a></li>
                            <?php if ($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D') { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=descontotipopagamentoController&acao=listar">Desconto Tipo de Pagamento - Vendas</a></li>
                            <?php } ?>
                            <?php if ($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'C' || $colaboradortipo == 'D') { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=estoquecontroleController&acao=listar">Controle de Estoque</a></li>
                            <?php } ?>
                            <?php if ($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D') { ?>
                                  <li role="separator" class="divider"></li>
                                  <li><a href="index.php?controle=descontovendaController&acao=listar">Desconto Venda</a></li>
                            <?php } ?>
                            <?php if ($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D') { ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=ajusteestoqueController&acao=novo">Ajuste Estoque</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=ajusteexclusaovendaController&acao=listar">Ajuste Exclusão Venda</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=relacaoexclusaovendaController&acao=listar">Relação de Exclusão Venda</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php if (($colaboradortipo == 'M') || ($colaboradortipo == 'G') || ($colaboradortipo == 'D')) { ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  id="menutitle" aria-haspopup="true" aria-expanded="false">Gerenciamento Financeiro<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?controle=gerenciamentofinanceiroentradaController&acao=listar">Gerenciamento Financeiro Entrada</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="index.php?controle=gerenciamentofinanceirosaidaController&acao=listar">Gerenciamento Financeiro Saída</a></li>
                            </ul>
                        </li>
                        <li><a href="index.php?controle=lojaController&acao=listar"  id="menutitle">Cadastro de Lojas</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  id="menutitle" aria-haspopup="true" aria-expanded="false">Relatórios<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" data-toggle="modal" data-target="#modalVendas">Vendas</a></li>
				<li role="separator" class="divider"></li>
                                <li><a href="#" data-toggle="modal" data-target="#modalProdutos">Produtos</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#" data-toggle="modal" data-target="#modalPagamento">Pagamentos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li><a href="<?php echo $sairaplicacao;?>"  id="menutitle">Sair</a></li>

                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container">
        
        <?php
        $app = new Controlador();
        ?>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="includes/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="includes/js/jquery.dataTables.min.js"></script>
    <script src="includes/js/dataTables.bootstrap.min.js"></script>

    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable({
               "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Pesquisar",
                "oPaginate": {
                   "sFirst":    "Primeiro",
                   "sPrevious": "Anterior",
                   "sNext":     "Próximo",
                   "sLast":     "Último"
                }  
              } 
            });
            
        });
        $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                        "sProcessing":   "Aguarde enquanto os dados são carregados ...",
                        "sLengthMenu":   "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords":  "Nenhum registro correspondente ao criterio encontrado",
                        "sInfoEmtpy":    "Exibindo 0 a 0 de 0 registros",
                        "sInfo":         "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                        "sInfoFiltered": "",
                        "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sFirst":    "Primeiro",
                                "sPrevious": "Anterior",
                                "sNext":     "Próximo",
                                "sLast":     "Último"
                        }  
                } 
            });
            $(function () {
            $("#examplevenda").DataTable({
               "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Pesquisar",
                "oPaginate": {
                   "sFirst":    "Primeiro",
                   "sPrevious": "Anterior",
                   "sNext":     "Próximo",
                   "sLast":     "Último"
                }  
              } 
            });
            
        });
    </script>
    <div style="display: none">
        <div id="dialog" title="Exclusão do Registro"/><img src="includes/imagens/question.png" alt="" /><span>Tem certeza que deseja excluir o registro?</span></div>
    </div>

</body>
</html>

<?php 
    
    //Funcionalidade para enviar e-mails:::::::::
    $email        = new CI_Email();
    $clienteModel = new ClienteModel();
    $resultado    = $clienteModel->buscaAniversariantes("false"); // false indica que deve buscar por aniversariantes pendentes

    if ($resultado->rowCount() > 0) {

        foreach ($resultado as $rs) {

            $id_cliente    = $rs['id'];
            $nome_cliente  = $rs['nome'];
            $email_cliente = $rs['email'];
            $nomeformatado = ucwords(strtolower($nome_cliente));
            if($email_cliente){
                 $email->from('atlanta@atlantajeans.com.br', 'Atlanta Jeans');
                 $email->to($email_cliente);

                 $email->subject('Parabéns ' . $nomeformatado);
                 $mensagememail= "<html>
                                    <body>
                                        <div>
                                            <p>Neste mês tão especial Nós da Atlanta Jeans lhe desejamos um Feliz Aniversário!</p>
                                            <p>Para que este mês seja ainda mais especial a Loja Atlanta Jeans gostaria de lhe presentear com um desconto de <b>10%</b> </br> em cada compra realizada na loja durante todo este mês.</br></br>
                                               Venha nos visitar.
                                            </p>
                                            <p style='font-size: 14px'><b>Obs:&nbsp Favor mostrar algum documento para o vendedor comprovando sua data de aniversário no momento da compra.</b></p>
                                            <p>&nbsp;</p>
                                                <p><img style='background: #111111 none repeat scroll 0% 0%; width: 127px;' src='http://atlantajeans.com.br//media/logomarca.jpg' alt='Atlanta Jeans' width='127px' height='auto' /></p>
                                                <p style='color: #555; font-size: 10px;'><span style='font-size: 10pt;'>Atenciosamente,</span><br /><span style='font-size: 10pt;'>Atlanta Jeans</span></p>
                                                <p style='color: #555; font-size: 10px;'><span style='font-size: 10pt;'><strong>Site:</strong>&nbsp;<a href='http://www.atlantajeans.com.br'>www.atlantajeans.com.br</a></span><br /><span style='font-size: 10pt;'><a><strong>E-mail:</strong> atlanta@atlantajeans.com.br &nbsp; &nbsp;</a><br /><strong>Telefone:</strong> (54)3632-8458 &nbsp; &nbsp;</span></p>
                                        </div>
                                    </body>
                                </html>";
                 $email->message($nomeformatado . $mensagememail);


                 if($email->send() == TRUE){
                    $clienteModel->atualizaListaEmail($id_cliente);
                 }
            }
        }
    }
?>