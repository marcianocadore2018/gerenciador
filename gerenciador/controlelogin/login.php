<?php require '../protected/libs/verificadiretorio.php'; ?>
<html>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="http://atlantajeans.com.br/gerenciador/includes/imagens/favicon.png" type="image/x-icon"/>
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Gerenciador Atlanta Jeans</title>
        <style>
            /* CSS used here will be applied after bootstrap.css */

            body { 
             background: url('/assets/example/bg_suburb.jpg') no-repeat center center fixed; 
             -webkit-background-size: cover;
             -moz-background-size: cover;
             -o-background-size: cover;
             background-size: cover;
            }

            .panel-default {
             opacity: 0.9;
             margin-top:30px;
            }
            .form-group.last {
             margin-bottom:0px;
            }
            .btn.primary {border-color: #3498db; color: #3498db;}
            .btn.success {border-color: #A44160; color: #A44160;}
            .form-login {max-width: 450px; max-height: 350px; margin-left: auto; margin-right: auto; padding: 10px; border-radius: 10px; margin-top: 10%;}
            .btn {display: inline-block;border: 2px solid #ddd; color: #ddd; text-decoration: none; font-weight: 700;}
            #formlogin .mensagemerro{color: red; height: 34px;}
            .error{color: red; font-size: 12px;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #A44160"> 
                            <strong class="" style="font-size: 16px; color: white">Login do Sistema</strong>
                        </div>
                        <div class="panel-body">
                            <?php
                                if($_GET['erro'] == 01){?>
                                    <b><div id="result" class="alert alert-danger col-sm-12">
                                        Dados incorretos :( <br/> Favor verificar seu Login ou sua Senha! 
                                    </div></b>
                                <?php }
                            ?>
                            <form class="form-horizontal" action="conectado.php" method="post" id="formlogin" name="formLogin" role="form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input type="text" id="login" name="login" id="login" autocomplete="false" class="form-control" placeholder="Informe seu Login" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input type="password" id="senha" name="senha" autocomplete="false" class="form-control" placeholder="Informe sua Senha" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label class="">
                                                <input type="checkbox" class="">Lembrar-me</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn primary btn">Acessar</button>
                                        <button type="reset" class="btn success btn">Limpar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="../includes/js/jquery.validate.min.js" type="text/javascript"></script>

        <script>
            $("#formlogin").validate({
                rules: {
                    login: {
                        required: true,
                        minlength: 3
                    },
                    senha: {
                        required: true,
                        minlength: 7
                    }
                },
                messages: {
                    login: {
                        required: "Por favor, informe seu Login",
                        minlength: "O Login deve ter pelo menos 3 caracteres"
                    },
                    senha: {
                        required: "Por favor, informe sua Senha",
                        minlength: "A Senha deve ter pelo menos 7 caracteres"
                    }
                }
            });
        </script>
    </body>
</html>