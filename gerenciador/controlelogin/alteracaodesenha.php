<?php include("../config/confloginrel.php"); ?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Gerenciador</title>
        <link href="../includes/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="../includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../includes/js/validarcampos.js" type="text/javascript"></script>

        <style type="text/css">
            .error{
                color: red;
                font-size: 12px;
            }
            body {
                min-height: 2000px;
                padding-top: 85px;
            }
            .navbar-default 
                .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{background-color: #FFB5C5}
                #menutitle{
                    color: white;
                }
        	#imagesituationcerto{
                background-image: url(../includes/img/certo.png);
                display: none;
                background-repeat: no-repeat;
            }
            #imagesituationerro{
                background-image: url(../includes/img/erro.png);
                display: none;
                background-repeat: no-repeat;
            }
            #mensagemlabelerro{
            	display: none;
            	color: red;
            }
            #mensagemredirecionamento{
            	color: #A44160;
            	display: none;
            }
        </style>
    </head>
</head>
<body>
    
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #A44160">
        <div class="container" style="padding-left: 0px">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Gerenciador Atlanta Jeans</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" style="width: 120%;">
                <ul class="nav navbar-nav">
                    <li><a href="alteracaodesenha.php"  id="menutitle">Alteração de Senha</a></li>
                    <li><a href="/gerenciador/controlelogin/sair.php"  id="menutitle">Voltar</a></li>

                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div class="col-md-6 col-offset-2" style="margin-left: 25%; margin-right: 25%;">
        <div class="panel panel-primary">
            <div class="panel-heading">Alteração de Senha do Colaborador</div>
            <div class="panel-body">
                <form action="alteracaodesenha.php" name="formAlteracaosenha" id="formAlteracaosenha" method="POST" class="form" role="form">
                    <?php
                        if(isset($_GET['login'])){ 
                            $camposdesabilitados = "";
                            $loginget = $_GET['login']; ?>
                            <input type="hidden" id="loginhidden" name="loginhidden" value="<?php echo $loginget;?>">
                            <div class="row" id="mensagemredirecionamento">
                                <div class="col-md-10">
                                    <label>Parabéns sua senha foi alterada com sucesso! <br/>
                                           Estamos redirecionando você para o Login<br/>
                                    </label>
                                    <img src="../includes/img/loading.gif" style="width: 50px;">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10" id="definasenha">
                                    <label style="color: #A44160;">Defina aqui sua nova senha!</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="login" name="loginpost" value="<?php echo $loginget;?>" minlength="6" disabled="">
                                </div>
                            </div>
                        <?php } else {
                            $camposdesabilitados = "disabled=''";
                        }
                    ?>
                    <script>
                    	function verificarSenhas(){
						if (document.formAlteracaosenha.senha.value == document.formAlteracaosenha.confirmasenha.value){
							$('#imagesituationerro').hide();
							$('#mensagemlabelerro').hide();
							$('#imagesituationcerto').show();
                            $('#definasenha').fadeOut(1000);
						}else{
							document.formAlteracaosenha.confirmasenha.focus();
							$('#imagesituationcerto').hide();
							$('#imagesituationerro').show();
							$('#mensagemlabelerro').show();
                            $('#definasenha').fadeIn(1000);
							}
						};

						function redirecionarLogin(){
						if((document.formAlteracaosenha.senha.value != "") && (document.formAlteracaosenha.confirmasenha.value != "")){
							$("#mensagemredirecionamento").show();
                            setTimeout(
								function(){ 
									var formulario = document.getElementById('formAlteracaosenha');
									formulario.submit();
										
									}, 5000);
							}
						}
                    </script>
                    <div class="row">
                        <div class="col-md-10">
                            <label for="senha">Nova Senha</label>
                            <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite a Senha" 
                             <?php echo $camposdesabilitados; ?> required minlength="6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <label for="confirmasenha">Confirma Nova Senha</label>
                            <input type="password" class="form-control" id="confirmasenha" name="confirmasenha" placeholder="Digite novamente a Senha" required minlength="6" onchange="verificarSenhas()" <?php echo $camposdesabilitados; ?> >
                        </div>
                        <div id="imagesituationcerto" class="col-md-1" style="padding-left: -0px; top: 28px; padding-right: 0px; height: 30px;">
                        </div>
                        <div id="imagesituationerro" class="col-md-1" style="padding-left: -0px; top: 28px; padding-right: 0px; height: 30px;">
                        </div>
                    </div>
            		<div id="mensagemlabelerro">
						<label style="font-size: 12px">As senhas não conferem :( Por favor informe as  senhas novamente!</label>
					</div>
					
                    <br/>
                    <button type="button" id="btn" class="btn btn-success" onclick="redirecionarLogin()" value="Validate">Gravar</button>
                    <button type="reset" class="btn btn-primary">Limpar</button>
                </form>
            </div>
        </div>
    </div>
<script src="../includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="../includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        $("#formAlteracaosenha").validate({
            rules: {
                senha: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                confirmasenha: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                }
            },
            messages: {
                senha: {
                    required: "Por favor, informe a Nova Senha",
                    minlength: "A Nova Senha deve ter pelo menos 6 caracteres",
                    maxlength: "A Nova Senha deve ter no máximo 20 caracteres"
                },
                confirmasenha: {
                    required: "Por favor, informe o campo Confirma Nova Senha",
                    minlength: "O Campo Confirma Nova Senha deve ter pelo menos 6 caracteres",
                    maxlength: "O Campo Confirma Nova Senha deve ter no máximo 20 caracteres"
                }
            }
        })
    $('#btn').click(function() {
        $("#formAlteracaosenha").valid();
    });
});
</script>
</body>
<!-- Recebe os posts -->
<?php
	//Seção
    if((isset($_POST['senha'])) && (isset($_POST['confirmasenha'])) && (isset($_POST['loginhidden']))){
    	$confirmasenha = $_POST['confirmasenha'];
        $loginhidden = $_POST['loginhidden'];

        $queryalteracaosenha = "update colaboradores set senha= '$confirmasenha', quantidadeacesso = 1 where login = '$loginhidden'";
        $result = pg_query($queryalteracaosenha);
        if($result != null){ ?>
        	<!--Redireciona para página de Login-->
        	<script type="text/javascript">
        		window.location="../index.php";
        	</script>
        <?php }
    }
?>
</html>