<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Colaborador</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formColaborador" id="formColaborador" method="POST" class="form" role="form">

                <?php
                    if($acao != 'novo'){
                        ?>
                            <input type="hidden" id="idloja" name="idloja" value="<?php if(isset($colaboradores['idloja'])) echo $colaboradores['idloja']; ?>"/>
                        <?php
                    }
                ?>

                <div class="tab-content">
                    <div id="dadosPessoais" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-1">
                                <label for="id">Id</label>
                                <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                       value="<?php if(isset($colaboradores['id'])) echo $colaboradores['id']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome" 
                                       value="<?php if(isset($colaboradores['nome'])) echo $colaboradores['nome']; ?>" required  
                                       onkeypress="return Onlychars(event)">
                            </div>
                        </div>
                        
                       <?php 
                            if($_GET['acao'] == 'novo' || $_GET['acao'] == 'inserir'){

                                //Busca seção verifica se o usuário que está na seção é master
                                //Se não for master não mostra o tipo de colaborador
                                $verificacolaboradortipo = $_SESSION['login'];
                                $colaboradortipomaster   = pg_query("select tipocolaborador from colaboradores where login = '$verificacolaboradortipo';");
                                $rstipocolaborador       = pg_fetch_array ($colaboradortipomaster);
                                $colaboradormastertipo   = $rstipocolaborador['tipocolaborador'];

                                if($colaboradormastertipo == 'M'){ ?>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="tipocolaborador">Tipo Colaborador</label>
                                            <input type="radio" name="tipocolaborador" value="G" 
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'G'){echo 'checked';}else{$colaboradores == null;} ?>> Gerente
                                            <input type="radio" name="tipocolaborador" value="C" checked=""
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'C'){echo 'checked';}else{$colaboradores == null;}?>> Colaborador
                                            <input type="radio" name="tipocolaborador" value="M" 
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'M'){echo 'checked';}else{$colaboradores == null;}?>> Master
                                        </div>
                                    </div>
                               <?php }
                                ?>
                                    
                            <?php }
                            if($_GET['acao'] == 'buscar'){ 

                                $verificacolaboradortipo = $_SESSION['login'];
                                $colaboradortipomaster   = pg_query("select tipocolaborador from colaboradores where login = '$verificacolaboradortipo';");
                                $rstipocolaborador       = pg_fetch_array ($colaboradortipomaster);
                                $colaboradormastertipo   = $rstipocolaborador['tipocolaborador'];

                                if($colaboradormastertipo == 'M'){ ?>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="tipocolaborador">Tipo Colaborador</label>
                                            <input type="radio" name="tipocolaborador" value="G" 
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'G'){echo 'checked';}else{$colaboradores == null;} ?>> Gerente
                                            <input type="radio" name="tipocolaborador" value="C" 
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'C'){echo 'checked';}else{$colaboradores == null;}?>> Colaborador
                                            <input type="radio" name="tipocolaborador" value="M" 
                                                <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'M'){echo 'checked';}else{$colaboradores == null;}?>> Master
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label for="idloja">Loja</label>
                                            <select class="form-control" name="idloja" id="idloja" required data-errormessage-value-missing="Selecione a Loja">
                                                <option value="">Selecione a Loja</option>>
                                                <?php
                                                foreach ($listaLojas as $lojas) {
                                                    $selected = (isset($colaboradores) && $colaboradores['idloja'] == $lojas['id']) ? 'selected' : '';
                                                    ?>
                                                    <option value='<?php echo $lojas['id']; ?>'
                                                            <?php echo $selected; ?>> 
                                                                <?php echo $lojas['nomefantasia']; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php }else{
                                  
                                    //Busca o tipo do registro do colaborador no banco e deixa por padrão
                                    $idcolaboradordefault   = $_GET['id'];

                                    $colaboradortipodefault = pg_query("select idloja, login, senha, tipocolaborador from colaboradores where id = $idcolaboradordefault");
                                    $rscolaboradortipodefault 
                                                            = pg_fetch_array ($colaboradortipodefault);
                                    $resultcolaboradortipodefault 
                                                            = $rscolaboradortipodefault['tipocolaborador'];
                                    $resultidlojafault      = $rscolaboradortipodefault['idloja'];
                                    $resultloginfault       = $rscolaboradortipodefault['login'];
                                    $resultsenhafault       = $rscolaboradortipodefault['senha'];


                                    if($resultcolaboradortipodefault != null){?>
                                        <input type="radio" name="tipocolaborador" value="G" style="width: 0px;" 
                                            <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'G'){echo 'checked';}else{$colaboradores == null;} ?>>
                                        <input type="radio" name="tipocolaborador" value="C" style="width: 0px;" 
                                            <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'C'){echo 'checked';}else{$colaboradores == null;}?>>
                                        <input type="radio" name="tipocolaborador" value="M" style="width: 0px;" 
                                            <?php if(isset($colaboradores)) if ($colaboradores['tipocolaborador'] == 'M'){echo 'checked';}else{$colaboradores == null;}?>>
                                        <!-- Informação da Loja -->
                                        <input type="hidden" name="idloja" value="<?php echo $resultidlojafault; ?>">
                                        <!-- Informação Login e Senha -->
                                        <input type="hidden" name="login" value="<?php echo $resultloginfault; ?>">
                                        <input type="hidden" name="senha" value="<?php echo $resultsenhafault; ?>">
                                    <?php }
                                    }
                                }
                                
                                if($_GET['acao'] == 'novo' || $_GET['acao'] == 'inserir'){?>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label for="idloja">Loja</label>
                                            <select class="form-control" name="idloja" id="idloja" required data-errormessage-value-missing="Selecione a Loja">
                                                <option value="">Selecione a Loja</option>>
                                                <?php
                                                foreach ($listaLojas as $lojas) {
                                                    $selected = (isset($colaboradores) && $colaboradores['idloja'] == $lojas['id']) ? 'selected' : '';
                                                    ?>
                                                    <option value='<?php echo $lojas['id']; ?>'
                                                            <?php echo $selected; ?>> 
                                                                <?php echo $lojas['nomefantasia']; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php }
                            ?>
                        
                        
                        <div class="row">
                            <div class="col-md-8">
                                <label for="idsetor">Setor</label>
                                <select class="form-control" name="idsetor" id="idsetor" required data-errormessage-value-missing="Selecione o Setor">
                                    <option value="">Selecione o Setor</option>>
                                    <?php
                                    foreach ($listaSetores as $setores) {
                                        $selected = (isset($colaboradores) && $colaboradores['idsetor'] == $setores['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $setores['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $setores['descricao']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="idcargo">Cargo</label>
                                <select class="form-control" name="idcargo" id="idcargo" required data-errormessage-value-missing="Selecione o Cargo">
                                    <option value="">Selecione o Cargo</option>>
                                    <?php
                                    foreach ($listaCargos as $cargos) {
                                        $selected = (isset($colaboradores) && $colaboradores['idcargo'] == $cargos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $cargos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $cargos['descricao']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        
                        <?php
                            if($_GET['acao'] == 'buscar'){ 

                                $verificacolaboradortipo = $_SESSION['login'];
                                $colaboradortipomaster   = pg_query("select login, senha, tipocolaborador from colaboradores where login = '$verificacolaboradortipo';");
                                $rstipocolaborador       = pg_fetch_array ($colaboradortipomaster);
                                $colaboradormastertipo   = $rstipocolaborador['tipocolaborador'];

                                if($colaboradormastertipo == 'M'){ ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="login">Login</label>
                                            <input type="text" class="form-control" id="login" name="login" placeholder="Digite o Login" 
                                                   value="<?php if (isset($colaboradores)) echo $colaboradores['login']; ?>" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="senha">Senha</label>
                                            <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite a Senha" 
                                                   value="<?php if (isset($colaboradores)) echo $colaboradores['senha']; ?>" required>
                                        </div>
                                    </div>
                                <?php }
                            }
                            
                            if($_GET['acao'] == 'novo' || $_GET['acao'] == 'inserir'){?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="login">Login</label>
                                            <input type="text" class="form-control" id="login" name="login" placeholder="Digite o Login" 
                                                   value="<?php if (isset($colaboradores)) echo $colaboradores['login']; ?>" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="senha">Senha</label>
                                            <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite a Senha" 
                                                   value="<?php if (isset($colaboradores)) echo $colaboradores['senha']; ?>" required>
                                        </div>
                                    </div>
                                <?php }
                            ?>
                                        
                                        
                        <div class="row">
                            <div class="col-md-4">
                                <label for="rg">RG</label>
                                <input type="text" class="form-control" id="rg" name="rg" placeholder="Digite o RG" 
                                           value="<?php if (isset($colaboradores)) echo $colaboradores['rg']; ?>" required>
                            </div>
                            <div class="col-md-4">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Digite o CPF" 
                                       onkeypress="return Onlynumbers(event)" value="<?php if (isset($colaboradores)) echo $colaboradores['cpf']; ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="datanascimento">Data Nascimento</label>
                                <input type="text" class="form-control" id="data" name="datanascimento" placeholder="Digite a Data Nascimento" 
                                           value="<?php if (isset($colaboradores)) echo $colaboradores['datanascimento']; ?>" required>
                            </div>
                        </div>
                        <div>
                        <label>Dados Carteira de Trabalho</label>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                                <label for="pispasep">PIS/PASEP</label>
                                <input type="text" class="form-control" id="pispasep" name="pispasep" 
                                           value="<?php if (isset($colaboradores)) echo $colaboradores['pispasep']; ?>">
                        </div>
                        <div class="col-md-2">
                                <label for="numerocarteiratrabalho">Número</label>
                                <input type="text" class="form-control" id="numerocarteiratrabalho" name="numerocarteiratrabalho" maxlength="10"
                                           value="<?php if (isset($colaboradores)) echo $colaboradores['numerocarteiratrabalho']; ?>" onkeypress="return Onlynumbers(event)">
                        </div>
                        <div class="col-md-2">
                                <label for="serie">Série</label>
                                <input type="text" class="form-control" id="serie" name="serie" 
                                           value="<?php if (isset($colaboradores)) echo $colaboradores['serie']; ?>">
                        </div>
                        <div class="col-md-2">
                                <label for="uf">UF</label>
                                <select class="form-control" name="idestadocarteiratrabalho" id="idestadocarteiratrabalho">
                                        <option value="">Selecione a UF</option>>
                                        <?php
                                        foreach ($listaEstados as $estados) {
                                                $selected = (isset($colaboradores) && $colaboradores['idestadocarteiratrabalho'] == $estados['id']) ? 'selected' : '';
                                                ?>
                                                <option value='<?php echo $estados['id']; ?>'
                                                                <?php echo $selected; ?>> 
                                                                        <?php echo $estados['uf']; ?>
                                                </option>
                                        <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="row">
                    <script>
                        $(document).ready(function(){
                            $("#btnCadastrarIA").click(function(){
                                <?php
                                    if(strpos($acao, 'atualizar') !== false){
                                        ?> 
                                            $('#cadastrarIA').modal('show');
                                        <?php
                                    } else {
                                        ?> 
                                            alert('Atenção! Deve-se salvar colaborador antes de cadastrar informações adicionais.');
                                        <?php
                                    }
                                ?>
                            });
                        });
                     </script>
                        <?php
                            if(($_GET['acao'] == "buscar") && ($_GET['id'] != "")){ ?>
                                <div class="col-md-5">
                                    <br/>
                                    <button type="button" class="btn btn-primary btn-default" data-toggle="modal" data-target="#myModal" id="btnCadastrarIA">
                                          Situação Contrato de Trabalho
                                    </button>
                                </div>
                            <?php }
                        ?>
                    </div>           
                    
                    <hr/>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['endereco']; ?>" required>
                            </div>
                            <div class="col-md-3">
                                <label for="bairro">Bairro</label>
                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o Bairro" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['bairro']; ?>" required>
                            </div>
                            <div class="col-md-3">
                                <label for="numero">Número</label>
                                <input type="text" class="form-control" id="numero" name="numero" placeholder="Digite o Número" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['numero']; ?>" maxlength="5" required onkeypress="return Onlynumbers(event)">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="cidade">Cidade</label>
                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Digite a Cidade" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['cidade']; ?>" required >
                            </div>
                            <div class="col-md-3">
                                <label for="idestado">Estado</label>
                                <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                                    <option value="">Selecione o Estado</option>>
                                    <?php
                                    foreach ($listaEstados as $estados) {
                                        $selected = (isset($colaboradores) && $colaboradores['idestado'] == $estados['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $estados['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $estados['uf']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cep">CEP</label>
                                <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o CEP" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['cep']; ?>" required>
                            </div>
                            <div style="padding-top: 30px; font-size: 12px; color: blue">
                                <a href="http://m.correios.com.br/movel/buscaCep.do" target="_blank" title="Clique aqui para pesquisar o CEP"><b>Verificar CEP Correios</b></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="complemento">Complemento</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Digite o Complemento" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['complemento']; ?>">
                            </div>
                        </div>  
                        <hr/>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Digite o E-mail" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['email']; ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" attrname="telephone1" name="telefone" placeholder="Digite o Telefone" 
                                       value="<?php if (isset($colaboradores)) echo $colaboradores['telefone']; ?>" required>
                            </div>
                        </div>
                    </div>
                </div>
            <br/>
            <button type="submit" class="btn btn-success">Gravar</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
            <form action="<?=  $acaoIAColaborador ?>" name="formIAColaborador" id="formIAColaborador" method="POST" class="form" role="form">

                <input type="hidden" value="<?= $acao ?>" id="acaoBack" name="acaoBack"/>
                <input type="hidden" 
                       value="<?php if(isset($_GET['id'])) echo $_GET['id']; else if(isset($colaboradores['id'])) echo ($colaboradores['id']); ?>" 
                       id="idcolaborador" 
                       name="idcolaborador"/>

                <input type="hidden" value="<?php if(isset($colaboradores['idsetor'])) echo $colaboradores['idsetor']; ?>" id="idsetor" name="idsetor"/>
                <input type="hidden" value="<?php if(isset($colaboradores['idcargo'])) echo $colaboradores['idcargo']; ?>" id="idcargo" name="idcargo"/>
                <input type="hidden" value="<?= base64_encode(gzdeflate(serialize($colaboradores))) ?>" id="colaborador" name="colaborador"/>

                <!-- Modal -->
                <div class="modal fade" id="cadastrarIA" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Insira o Contrato de Trabalho</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="admissao">Data Admissão</label>
                                        <input type="text" class="form-control" id="admissao" name="admissao" placeholder="Digite a Data Admissão" 
                                               value="<?php if (isset($iAColaborador)) echo $iAColaborador['admissao']; ?>" required>
                                    </div>
                                </div>
                                <!--Se o Colaborador tiver o Status de demitido é necessário mostrar o campo abaixo -->
                                <script>
                                    $(document).ready(function(){
                                        $("#jus").hide();
                                        $("#situacaodemissao").change(function() {
                                            loadSituation();
                                        });

                                        loadSituation();

                                        $("#gravarIAColaborador").click(function(){
                                            var $form = $("#formIAColaborador");
                                            if(!$form.valid()) return false;

                                            $("#formIAColaborador").submit();
                                        });
                                    });

                                    function loadSituation(){
                                        var val = $("#situacaodemissao").val();
                                        if(val==1){
                                            $("#situacao").fadeIn();
                                            $("#datademissao").attr("required", true);
                                        }else{
                                            $("#situacao").fadeOut();
                                            $("#datademissao").attr("required", false);
                                        }
                                    }
                                </script>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="situacaodemissao">Demitido?</label>
                                        <select class="form-control" name="situacaodemissao" id="situacaodemissao">
                                            <option value="0">Admitido</option>
                                            <option value="1">Demitido</option>
                                          </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="situacao">
                                        <label for="demissao">Data Demissão</label>
                                        <input type="text" class="form-control" id="datademissao" name="demissao" placeholder="Digite a Demissão" 
                                               value="<?php if (isset($iAColaborador)) echo $iAColaborador['demissao']; ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="salario">Salário</label>
                                        <input type="text" class="form-control" id="valor" name="salario" placeholder="Digite o Salário" 
                                               value="<?php if (isset($iAColaborador)) echo $iAColaborador['salario']; ?>" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="comissao">Comissao</label>
                                        <input type="text" class="form-control" id="comissao" name="comissao" placeholder="Digite a Comissão Exp: Números" 
                                               value="<?php if (isset($iAColaborador)) echo $iAColaborador['comissao']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                <button type="button" class="btn btn-primary" id="gravarIAColaborador">Gravar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    jQuery.extend(jQuery.validator.messages, {
        email:"Por favor informe um email válido."
    });
    $("#formColaborador").validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            idsetor: {
                required: true
            },
            idcargo: {
                required: true
            },
            login: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            senha: {
                required: true,
                minlength: 7,
                maxlength: 20
            },
            rg: {
                required: true,
                maxlength: 10
            },
            cpf: {
                required: true
            },
            datanascimento: {
                required: true
            },
            endereco: {
                required: true,
                maxlength: 100
            },
            bairro: {
                required: true,
                maxlength: 80
            },
            idestado: {
                required: true
            },
            cidade: {
                required: true,
                maxlength: 100
            },
            email: {
                maxlength: 100
            },
            telefone: {
                maxlength: 20
            },
            numero: {
                maxlength: 5
            },
            cep: {
                maxlength: 9
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 80 caracteres"
            },
            idsetor: {
                required: "Por favor, Selecione o Setor"
            },
            idcargo: {
                required: "Por favor, Selecione o Cargo"
            },
            login: {
                required: "Por favor, informe o Login",
                minlength: "O Login deve ter pelo menos 6 caracteres",
                maxlength: "O Login deve ter no máximo 20 caracteres"
            },
            senha: {
                required: "Por favor, informe a Senha",
                minlength: "A Senha deve ter pelo menos 6 caracteres",
                maxlength: "A Senha deve ter no máximo 20 caracteres"
            },
            rg: {
                required: "Por favor, informe O RG"
            },
            cpf: {
                required: "Por favor, informe O CPF"
            },
            datanascimento: {
                required: "Por favor, informe a Data de Nascimento"
            },
            endereco: {
                required: "Por favor, informe o Endereço",
                maxlength: "O Endereço deve ter no máximo 100 caracteres"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                maxlength: "O Bairro deve ter no máximo 80 caracteres"
            },
            idestado: {
                required: "Por favor, Selecione o Estado"
            },
            cidade: {
                required: "Por favor, informe a Cidade",
                maxlength: "A Cidade deve ter no máximo 100 caracteres"
            },
            email: {
                required: "Por favor, informe o E-mail",
                maxlength: "O E-mail deve ter no máximo 100 caracteres"
            },
            telefone: {
                required: "Por favor, informe o Telefone"
            },
            numero: {
                required: "Por favor informe o Número"
            },
            cep: {
                required: "Por favor, informe o CEP"
            }
        }
    });
</script>

<script>
    $("#formIAColaborador").validate({
        rules: {
            admissao: {
                required: true
            },
            salario: {
                required: true
            }
        },
        messages: {
            admissao: {
                required: "Por favor, informe a Data de Demissão"
            },
            salario: {
                required: "Por favor, Informe o Salário"
            }
        }
    });
</script>