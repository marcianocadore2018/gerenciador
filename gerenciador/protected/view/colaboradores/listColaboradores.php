<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
                <div class="panel-heading">Listagem de Colaboradores</div>
            <div class="panel-body">
                <?php
                    $verificacolaboradortipo = $_SESSION['login'];
                    $colaboradortipomaster = pg_query("select tipocolaborador from colaboradores where login = '$verificacolaboradortipo';");
                    $rstipocolaborador = pg_fetch_array ($colaboradortipomaster);
                    $colaboradormastertipo = $rstipocolaborador['tipocolaborador'];
                    if($colaboradormastertipo == 'M' || $colaboradormastertipo == 'D'){ ?>
                        <a href="index.php?controle=colaboradoresController&acao=novo">
                            <span class='glyphicon glyphicon-plus'> Adicionar</span>
                        </a>
                    <?php }
                ?>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1500px;">
                    <thead>
                    <th>Nome</th>
                    <th>Tipo Colaborador</th>
                    <th>RG</th>
                    <th>CPF</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th>Cargo</th>
                    <th>Cidade</th>
                    <th>Número</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nome'];
                            echo '<td>' . $item['tipocolaborador'];
                            echo '<td>' . $item['rg'];
                            echo '<td>' . $item['cpf'];
                            echo '<td>' . $item['telefone'];
                            echo '<td>' . $item['email'];
                            echo '<td>' . $item['descricaocargo'];
                            echo '<td>' . $item['descricaocidade'];
                            echo '<td style="text-align: center;">' . $item['numero'];
                            $id = $item['id'];
                            $tipocolaborador = $item['tpcolaborador'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=colaboradoresController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            
                                echo "<td> <a onclick='excluir(\"excluir\",\"colaboradoresController\",\"$idencriptografa\")' href='#'>"
                                        . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                                        . "</a> </td>";
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>