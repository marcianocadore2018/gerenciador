<?php date_default_timezone_set('America/Sao_Paulo'); ?>
<style>

.hr-line-dashed {
    border-top: 1px dashed #e7eaec;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 20px 0;
}
</style>

<!-- 
    Form pagamento-step-1
-->
<form id="form-step1">
    <div id="pgto" class="col-md-12 container">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Pagamento</h2>
                <div class="hr-line-dashed"></div>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="ibox-content m-b-sm border-bottom">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="idpagamento">ID#</label>
                        <input type="text" class="form-control" id="idpagamento" name="idpagamento" readonly="true" 
                                   value="<?php if (isset($pagamentos)) echo $pagamentos['id']; ?>">
                    </div>
                </div>
                <div class="col-sm-5">
                    <label for="idcliente">Cliente</label>
                    <?php 
                        if($pagamentos['tipopagamento'] == '0' || $pagamentos['tipopagamento'] == ''){
                        ?>
                        <select data-placeholder="Selecione um cliente" class="form-control chosen-select" tabindex="1" name="idcliente" id="idcliente">
                            <option value="0">Selecione</option>
                            <?php
                                foreach ($listaClientes as $clientes) {
                                    $selected = (isset($pagamentos) && $pagamentos['idcliente'] == $clientes['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $clientes['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $clientes['nome']; ?>
                                    </option>
                                <?php 
                                }
                                ?>
                        </select>
                        <?php 
                        }else{
                            foreach ($listaClientes as $clientes) {
                                if($pagamentos['idcliente'] == $clientes['id']){
                                    echo "<input type='text' id='clienteSel' name='clienteSel' value='".$clientes['nome']."' class='form-control' readonly>";
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label for="datapagamento">Data Pagamento</label>
                    <input type="text" class="form-control" id="datapagamento" name="datapagamento" disabled=""
                           value="<?= date('d/m/Y') ?>" style="background-color: #D3D3D3" required>
                </div>
                <div class="col-md-2">
                    <label for="total-aberto">Total em aberto</label>
                    <input type="text" class="form-control" id="total-aberto" name="total-aberto" placeholder="R$ 0,00" 
                           value="<?php if (isset($pagamentos)) echo $pagamentos['total_aberto']; ?>" required readonly>
                </div>
                <div class="col-md-2">
                    <?php 
                        if($pagamentos['tipopagamento'] == '0' || $pagamentos['tipopagamento'] == '')
                            $type = '';
                        else
                            $type = ' readonly';
                    ?>
                    <label for="valorpago">Valor à pagar</label>
                    <input type="text" class="form-control" id="valor" name="valorpago" placeholder="R$ 0,00" 
                           value="<?php if (isset($pagamentos)) echo $pagamentos['valorpago']; ?>" required <?= $type?>>
                </div>
            </div>
        </div>

        <div class="hr-line-dashed"></div>

        <div class="row">
            <div class="col-sm-7">
                <?php
                    if($pagamentos['tipopagamento'] == '0' || $pagamentos['tipopagamento'] == ''){
                        ?>
                        <button id="btn-step1" type="button" class="btn btn-success pull-right">Próximo ></button>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
</form>

<!-- 
    Form pagamento-step-2
-->
<form id="form-step2">
    <div id="order_step2" class="col-md-12 col-offset-2">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Concluir o pagamento</h2>
                <div class="hr-line-dashed"></div>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="ibox">
            <div class="ibox-title">
                Método de pagamento
            </div>
            <div class="ibox-content">
                <div class="panel-group payments-method" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <img class="text-success" src="includes/img/dinheiro.png" title="Dinheiro/À vista" style="width: 50px; height: 25px;" />
                            </div>
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAvista">Dinheiro/à vista</a>
                            </h5>
                        </div>
                        <div id="collapseAvista" class="panel-collapse collapse <?php if($pagamentos['tipopagamento']=='D') echo 'in';?>">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <strong>Total:</strong><span class="text-navy" id="money-total"></span>
                                        <br><br>
                                        <a class="btn btn-success" id="pagar-dinheiro">
                                            <i class="fa fa-cc-paypal">
                                                Utilizar esta forma de pagamento
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <img class="text-success" src="includes/img/cartaocredito.png" title="Dinheiro/À vista" style="width: 50px; height: 25px;" />
                            </div>
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseParcelado">À Prazo/Cartão de cédito</a>
                            </h5>
                        </div>
                        <div id="collapseParcelado" class="panel-collapse collapse <?php if($pagamentos['tipopagamento']=='P') echo 'in';?>">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <strong>Total parcelado:</strong><span class="text-navy" id="total-parcelado"></span>
                                        <br><br>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select class="form-control" name="idparcelamento" id="idparcelamento" 
                                                required data-errormessage-value-missing="Selecione ..." style="width: 206px;" 
                                                title="Escolha a quantidade de parcelas">

                                        </select>
                                        <br><br>
                                        <a class="btn btn-success" id="pagar-parcelado">
                                            <i class="fa fa-cc-paypal">
                                                Utilizar esta forma de pagamento
                                            </i>
                                        </a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <img class="text-success" src="includes/img/boleto.png" title="Boleto" style="width: 50px; height: 25px;" />
                            </div>
                            <h5 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseBoleto">Boleto bancário</a>
                            </h5>
                        </div>
                        <div id="collapseBoleto" class="panel-collapse collapse <?php if($pagamentos['tipopagamento']=='B') echo 'in';?>">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-10">
                                        <strong>Total:</strong><span class="text-navy" id="boleto-bancario"></span>
                                        <br><br>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label" for="datavencimento">Data Vencimento</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <input type="text" class="form-control" id="data" name="datavencimento" placeholder="" 
                                                       value="<?php if (isset($pagamentos)) echo $pagamentos['datavencimento']; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <a class="btn btn-success" id="pagar-boleto">
                                            <i class="fa fa-cc-paypal">
                                                Utilizar esta forma de pagamento
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>

                <div class="row">
                    <div class="col-sm-12">
                        <button id="btn-back" type="button" class="btn btn-default pull-left">< Voltar</button>
                    </div>
            </div>
        </div>
        <div id="container"></div>
    </div>
</form>

<script>

    var clienteSelecionado = 0;
    var qtdParcelas        = 1;

    var timeEffect = 500;
    var timeWait   = 700;

    $(document).ready(function(){


        set();

        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $("#form-step2").hide();

        $("#btn-step1").click(function(){

            var cliente     = clienteSelecionado;
            var dataPgto    = $("#datapagamento").val();
            var idpgto      = $("#idpagamento").val();
            var valor       = $("#valor").val();

            <?php 
                if($pagamentos['tipopagamento'] == '0' || $pagamentos['tipopagamento'] == ''){
            ?>
                    if(!cliente || cliente==0){
                        alert('Selecione um cliente!');
                        return;
                    }
            <?php
                }
            ?>

            var uri = encodeURI("id="+ idpgto + "&cliente="+cliente+"&data='"+dataPgto+"'&valor="+valor);

            var response = requestHTTP('salvaPgtoCapa', null, null, uri);
        
            $("#container").html( response );
        });


        $("#btn-back").click(function(){
            $("#form-step2").hide("slide", { direction: "right" }, timeEffect);
            setTimeout(function(){
                $("#form-step1").show("slide", { direction: "left" }, timeEffect);
            }, timeWait);
        });


        $("#idparcelamento").on( "change", function( event ) {
            qtdParcelas = this.value;
            $("#total-parcelado").html($('#idparcelamento option:selected').html() );
        });

        $("#idcliente").on('change', function(){
            clienteSelecionado = this.value;
            var response       = requestHTTP('totalAberto', clienteSelecionado, null, null);
            $("#container").html(response);
        });

        // Buttons pagamento events
        $("#pagar-dinheiro").click(function(){
            var response = requestHTTP('pagar_', 'D', 'null', "idpgto="+$("#idpagamento").val());
            $("#container").html(response);
            //in php code continue javascript
        });
        $("#pagar-parcelado").click(function(){
            var response = requestHTTP('pagar_', 'P', qtdParcelas, "idpgto="+$("#idpagamento").val());
            $("#container").html(response);
            //in php code continue javascript
        });
        $("#pagar-boleto").click(function(){
            var response = requestHTTP('pagar_', 'B', 'null', "idpgto="+$("#idpagamento").val()+"&datavencimento="+$("#data").val());
            $("#container").html(response); 
            //in php code continue javascript
        });
    });


    function set(){
        colaboradorSelecionado = $("#idcolaboradorvendedor").val();
        clienteSelecionado     = $("#idcliente").val();
    }

    function requestHTTP(service, param1, param2, p){  

        var hostname = 'http://'+window.location.hostname + window.location.pathname;
        var response = null;
        var params   = "";
        var uri      = null; 

        if(param1)
            params = '/' + param1;
        if(param2)
            params +='/' + param2;

        uri = hostname + '/' + service + params; 

        $.ajax({ 
            url : uri,
            type : 'POST',
            async: false,  
            data : p,
            dataType: 'text',
            timeout: 2900,    
            success: function(retorno){
                response = retorno;
            },
            error: function(erro){
                console.log("ERRO: "+erro);
            }       
        });
        return response;
    }
</script>