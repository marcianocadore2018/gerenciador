<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Exibir pagamentos</h2>
        <ol class="breadcrumb">
            <div class="m-t text-righ">
                <a href="index.php?controle=pagamentosController&acao=novo" class="btn btn-xs btn-outline btn-default">NOVO PAGAMENTO 
                    <i class="glyphicon glyphicon-plus"></i> </a>
            </div>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class=" table-responsive">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" id="example1" data-page-size="8">
                        <thead>
                            <tr>
                                <th style="width: 200px">Cliente</th>
                                <th>Data/Hora</th>
                                <th>Valor</th>
                                <th>Nº Parcela</th>
                                <th>Status</th>
                                <th>Tipo</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($listaDados as $item) {
                                echo '<tr>';

                                echo '<td>' . $item['nomecliente']      . "</td>";
                                echo '<td>' . $item['datapagamento'] ." ". $item['horapagamento'] . "</td>";

                                if($item['valorpago'] == 'R$ '){
                                    $item['valorpago'] = 'R$ 0,00';
                                }

                                echo '<td>' . $item['valorpago']        . "</td>";
                                echo '<td>' . $item['numeroparcela']    . "</td>";

                                if ($item['statuspagamento'] == 0){
                                    echo '<td><span class="label label-danger">INATIVO</span></td>';            // TIPO DO PAGAMENTO
                                }
                                if ($item['statuspagamento'] == 1){
                                    echo '<td><span class="label label-success">ATIVO</span></td>';         // TIPO DO PAGAMENTO
                                }

                                if ($item['tipopagamento'] == 'B'){
                                    echo '<td><span class="label label-default">BOLETO</span></td>';            // TIPO DO PAGAMENTO
                                }
                                if ($item['tipopagamento'] == 'P'){
                                    //echo '<td>' . $dados['qtdparcelas'].'x/('.$dados['parcelado']. ')</td>';  // DATA DO VENCIMENTO
                                    echo '<td><span class="label label-warning">PARCELADO</span></td>';         // TIPO DO PAGAMENTO
                                }
                                if ($item['tipopagamento'] == 'D')
                                    echo '<td><span class="label label-success">À VISTA</span></td>';           // TIPO DO PAGAMENTO

                                if ($item['tipopagamento'] == '0')
                                    echo '<td><span class="label label-danger">DESCONHECIDO</span></td>'; 

                                if ($item['tipopagamento'] == 'E')
                                    echo '<td><span class="label label-success">PGTO/VLR ENTRADA</span></td>';

                                $id = $item['id'];

                                // Se o pagamento ainda nao estiver concluído permite editar/excluir
                                if ($item['tipopagamento'] == '0'){
                                    echo "<td class='text-center'>";
                                    echo    "<a href='index.php?controle=pagamentosController&acao=buscar&id=$id' class='btn btn-xs btn-outline btn-default'>";
                                    echo        "<span class='glyphicon glyphicon-pencil'> </span>";
                                    echo    '</a>';
                                    echo  '&nbsp;';

                                    echo "<a onclick='excluir(\"excluir\",\"pagamentosController\",$id)' href='#' class='btn btn-xs btn-outline btn-danger'>";
                                    echo    "<span class='glyphicon glyphicon-trash customDialog'> </span>";
                                    echo '</a>';
                                    echo '</td>';
                                // Se o pagamento estiver concluido permite somente visualizar.
                                }else{
                                    echo "<td class='text-center'>";
                                    echo    "<a href='index.php?controle=pagamentosController&acao=buscar&id=$id' class='btn btn-xs btn-outline btn-default'>";
                                    echo        "<span class='glyphicon glyphicon-eye-open'> </span>";
                                    echo    '</a>';
                                    echo  '&nbsp;';
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>