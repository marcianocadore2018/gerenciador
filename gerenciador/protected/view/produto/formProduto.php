<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formProduto" id="formProduto" method="POST" class="form" role="form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($produto)) echo $produto['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="referencia">Referência</label>
                        <input type="text" class="form-control" id="referencia" name="referencia" placeholder="Digite a Referência" 
                               value="<?php if (isset($produto)) echo $produto['referencia']; ?>" required minlength="3" maxlength="50">
                    </div>

                    <div class="col-md-4">
                        <label for="nomeproduto">Nome do Produto</label>
                        <input type="text" class="form-control" id="nomeproduto" name="nomeproduto" placeholder="Digite o nome do produto" 
                               value="<?php if (isset($produto)) echo $produto['nomeproduto']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="cor">Cor do Produto</label>
                        <input type="text" class="form-control" id="cor" name="cor" placeholder="Ex: VERDE|AZUL|BRANCO" 
                               value="<?php if (isset($produto)) echo $produto['cor']; ?>" required onkeypress="return Onlychars(event)" minlength="1" maxlength="30">
                    </div>
                    <div class="col-md-2">
                        <label for="numeroproduto">Número do Produto</label>
                        <input type="text" class="form-control" id="numeroproduto" name="numeroproduto" placeholder="Ex: 39|40|41" 
                               value="<?php if (isset($produto)) echo $produto['numeroproduto']; ?>" onkeypress="return Onlynumbers(event)" maxlength="3">
                    </div>
                    <div class="col-md-2">
                        <label for="tamanho">Tamanho do Produto</label>
                        <input type="text" class="form-control" id="tamanho" name="tamanho" placeholder="Ex: P|M|G|GG" 
                               value="<?php  if (isset($produto)) echo $produto['tamanho']; ?>" onkeypress="return Onlychars(event)" minlength="1" maxlength="8">
                    </div>

                    <div class="col-md-3">

                        <label for="sexo">Gênero do Produto</label>
                        <br><input type="radio" name="sexo" value="F" checked="" <?php if(isset($produto)) if ($produto['sexo'] == 'F'){echo 'checked';}else{$produto == null;} ?>> Feminino
                        <input type="radio" name="sexo" value="M" <?php if(isset($produto)) if ($produto['sexo'] == 'M'){echo 'checked';}else{$produto == null;}?>> Masculino
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3">
                        <label for="idgrupo">Grupo</label>
                        <select class="form-control" name="idgrupo" id="idgrupo" required data-errormessage-value-missing="Selecione o Grupo">
                            <option value="">Selecione o Grupo</option>>
                            <?php
                            foreach ($listaGrupos as $grupos) {
                                $selected = (isset($produto) && $produto['idgrupo'] == $grupos['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $grupos['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $grupos['descricao']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="foto">Foto</label>
                        <input name="arquivo" type="file"/><br/>
                        <?php
                            $aplicacao = $_SERVER['SCRIPT_NAME']; 
                            $removercaracteres = str_replace("/","",$aplicacao);
                            $nomeaplicacao = str_replace("index.php","",$removercaracteres);
                            
                            //Cairá neste teste sempre que o usuário for alterar um registro
                            if(isset($produto['fotoproduto']) != null){
                                if($produto['fotoproduto'] == ''){
                                    if( isset($_SERVER['HTTPS'] ) ) {
                                        $prefixo = 'https://';
                                    }else{
                                        $prefixo = 'http://';
                                    }
                                    
                                    $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/' . $nomeaplicacao . '/protected/imagens/noimagens/noimagem.png';
                                    echo '<img src = "' . "$urlbase". '" style="width: 100px;"/>';   
                                }else{
                                    echo '<img src = "' . $produto['fotoproduto'] . '" style="width: 100px; heigth: 80px;"/>';
                                }
                            }//Cairá nesse teste sempre que o usuário for inserir um novo veículo
                            else{
                                if(isset($_SERVER['HTTPS'] ) ) {
                                    $prefixo = 'https://';
                                }else{
                                    $prefixo = 'http://';
                                }
                                if(isset($_GET['id']) != ''){
                                    $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/' . $nomeaplicacao . '/protected/imagens/noimagens/noimagem.png';
                                    echo '<img src = "' . "$urlbase". '" style="width: 100px;"/>';   
                                }
                            }
                        ?>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-md-3 pull-right">
                        <label for="quantidade">Quantidade do Produto</label>
                        <input type="text" class="form-control text-right" id="quantidade" name="quantidade" placeholder="Quantidade" 
                               value="<?php if (isset($produto)) echo $produto['quantidade']; ?>" required onkeypress="return Onlynumbers(event)" maxlength="6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 pull-right">
                        <label for="valor">Valor do Produto</label>
                        <input type="text" class="form-control text-right" id="valor" name="valor" placeholder="R$ 0,00" 
                               value="<?php if (isset($produto)) echo $produto['valor']; ?>" required maxlength="16">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formProduto").validate({
        rules: {
            referencia: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            nomeproduto: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            cor: {
                required: true,
                minlength: 1,
                maxlength: 30
            },
            sexo: {
                required: true,
                maxlength: 1
            },
            idgrupo: {
                required: true
            },
            quantidade: {
                required: true
            },
            valor: {
                required: true
            }
        },
        messages: {
            referencia: {
                required: "Por favor, informe a referência do produto",
                minlength: "A referência do produto deve ter pelo menos 3 caracteres",
                maxlength: "A referência do produto deve ter no máximo 50 caracteres"
            },
            nomeproduto: {
                required: "Por favor, informe o nome do produto",
                minlength: "O nome do produto deve ter pelo menos 3 caracteres",
                maxlength: "O nome do produto deve ter no máximo 100 caracteres"
            },
            cor: {
                required: "Por favor, informe a cor do produto",
                minlength: "A cor do produto deve ter pelo menos 1 caracteres",
                maxlength: "A cor do produto deve ter no máximo 30 caracteres"
            },
            sexo: {
                required: "Por favor, informe o gênero do produto",
                maxlength: "O gênero do produto deve ter no máximo 1 caracteres"
            },
            idgrupo: {
                required: "Por favor, Selecione um Grupo que o Produto pertence"
            },
            quantidade: {
                required: "Por favor, informe a quantidade do produto"
            },
            valor: {
                required: "Por favor, informe o valor do produto"
            }
        }
    });
</script>