<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Produto</div>
            <div class="panel-body">
                <?php
                //Controle removido a pedido do Lorivan
		    //Data Solicitação: 28/02/2017
		    //Data Alteração: 01/03/2017
					
                    $tipousuariocolaborador = $_SESSION['login'];
                    $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                    $rscolaborador = pg_fetch_array ($colaboradormaster);
                    $colaboradortipo = $rscolaborador['tipocolaborador'];
                    //if($colaboradortipo != 'C'){
                ?>
                <?php if($colaboradortipo == 'M' || $colaboradortipo == 'D'){ ?>
                    <a href="index.php?controle=produtoController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
                <?php }
                ?> 
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1200px;">
                    <thead>
                    <th>Referência</th>
                    <th>Produto</th>
                    <th>Grupo</th>
                    <th>Cor</th>
                    <th>Número</th>
                    <th>Tamanho</th>
                    <th>Sexo</th>
                    <th>Quantidade</th>
                    <th>Valor</th>
                    <th>Data Cadastro</th>
                    <th>Imagem</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['referencia'];
                            echo '<td>' . $item['nomeproduto'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['descricaogrupo'];
                            echo '<td>' . $item['cor'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['numeroproduto'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['tamanho'];
                            echo '<td>' . $item['sexo'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['quantidadeproduto'];
                            echo '<td> ' . $item['valor'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['datacadastro'];
                            if($item['nomeproduto'] != null){
                                
                                if($item['nomefoto'] == ''){
                                    if( isset($_SERVER['HTTPS'] ) ) {
                                        $prefixo = 'https://';
                                    }else{
                                        $prefixo = 'http://';
                                    }
                                    $aplicacao = $_SERVER['SCRIPT_NAME']; 
                                    $removercaracteres = str_replace("/","",$aplicacao);
                                    $nomeaplicacao = str_replace("index.php","",$removercaracteres);
                                    
                                }
                                echo '<td>' . '<img src = "' . $item['fotoproduto'] . '" style="width: 100px; heigth: 80px;"/>';
                            }
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            //consultar usuario, se for tipo Master pode alterar
                            $tipousuariocolaborador = $_SESSION['login'];
                            $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                            $rscolaborador = pg_fetch_array ($colaboradormaster);
                            $colaboradortipo = $rscolaborador['tipocolaborador'];
                            if($colaboradortipo == 'M' || $colaboradortipo == 'D'){
                                echo "<td> <a href='index.php?controle=produtoController&acao=buscar&id=$idencriptografa'>
                                <span class='glyphicon glyphicon-pencil'> </span>
                                </a> </td>
                                <td> <a onclick='excluir(\"excluir\",\"produtoController\",\"$idencriptografa\")' href='#'>
                                <span class='glyphicon glyphicon-trash customDialog'> </span>
                                </a> </td>";
                            }
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>