<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Bônus de Cliente - Troca de Produtos </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1600px;">
                    <thead>
                    <th>Nome Cliente</th>
                    <th>Produto Devolvido</th>
                    <th>Produto Trocado</th>
                    <th style="width: 102px;">Valor Diferença</th>
                    <th style="width: 72px;">Data Troca</th>
                    <th style="width: 80px;">Hora Troca</th>
                    <th style="width: 100px;">Status Troca</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td>' . $item['nomeprodutodevolvido'];
                            echo '<td>' . $item['nomeprodutotrocado'];
                            echo '<td style="padding-left: 36px;">' . $item['valordiferenca'];
                            echo '<td style="padding-right: 0px; padding-left: 22px;">' . $item['datatroca'];
                            echo '<td style="padding-right: 0px; padding-left: 30px;">' . $item['horatroca'];
                            echo '<td style="width: 90px; padding-left: 32px;">' . $item['statustroca'];
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>