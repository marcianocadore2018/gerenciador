<?php
    $id = $_GET['id'];
    $idproduto = $_GET['idproduto'];
    $iditemproduto = $_GET['iditemproduto'];
    $idvendatroca = $_GET['idvendatroca'];
    
    $produtodevolvido = pg_query("SELECT pro.referencia, 
       pro.numeroproduto, 
       pro.tamanho, 
       pro.nomeproduto, 
       (SELECT Concat(Sum(valortotal) - ( Sum(valortotal) * 
                                                COALESCE 
                                                        (( (SELECT 
                                                porcentagemdescontovenda 
                                                                               FROM 
                                                           descontovenda 
                                                           WHERE 
                                                           id = 
                                    vda.desconto) / 
                                               100 
                                             ), 0.00) ) 
                                    - valor_entrada - descontofinal) AS valor 
                             FROM   itensproduto 
                             WHERE  idvenda = vda.id)                            AS valor, 
                            Concat(( (SELECT Sum(valortotal) AS total 
                                                     FROM   itensproduto 
                                                     WHERE  idvenda = vda.id) / vda.qtdparcelas ) 
                                                  - valor_entrada) AS parcelado 
                            FROM   venda vda 
                                   INNER JOIN itensproduto itens 
                                           ON vda.id = itens.idvenda 
                                   INNER JOIN produto pro 
                                           ON itens.idproduto = pro.id 
                            WHERE  pro.id = $idproduto 
                                   AND vda.id = $idvendatroca 
                            ORDER  BY Date(vda.datavenda) DESC, 
                                      Cast(vda.horavenda AS TIME) DESC; ");
    $rsprodutodevolvido = pg_fetch_array ($produtodevolvido);
    $referencia = $rsprodutodevolvido['referencia'];
    $nomeproduto = $rsprodutodevolvido['nomeproduto'];
    $numeroproduto = $rsprodutodevolvido['numeroproduto'];
    $tamanho = $rsprodutodevolvido['tamanho'];
    $valorparaformatar = $rsprodutodevolvido['valor'];
    $valor = number_format($valorparaformatar, 2);
    
    if($numeroproduto == ''){
        $tamanhonumero = $tamanho;
        $descricaotamanho = 'Tamanho: ';
    }else if($tamanho == ''){
        $tamanhonumero = $numeroproduto;
        $descricaotamanho = 'Número: ';
    }
    
    $sqlbuscaprodutostroca = pg_query("SELECT pro.id as id, 
                                              pro.numeroproduto,
                                              upper(pro.nomeproduto) as nomeproduto,
                                              upper(pro.referencia) as referencia,
                                              pro.tamanho as tamanho,
                                              pro.quantidade as quantidadeproduto,
                                              (pro.referencia || ' - ' || pro.nomeproduto) as nomeproduto,
                                              pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel
                                         FROM produto pro
                                        INNER JOIN grupo gru
                                           ON pro.idgrupo = gru.id
                                         LEFT JOIN itensproduto itensprod
                                           ON itensprod.idproduto = pro.id
                                        GROUP BY pro.id
                                        ORDER BY pro.referencia, pro.nomeproduto ASC;");
    $num = pg_numrows($sqlbuscaprodutostroca);
    
    $acao = "index.php?controle=manutencaotrocaprodutoController&acao=trocarproduto";
    
?>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Manutenção Troca de Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formManutencaoTrocarproduto" id="formManutencaoTrocarproduto" method="POST" class="form" role="form">
                <input type="hidden" name="idprodutodevolvido" value="<?php echo $idproduto;?>"/>
                <input type="hidden" name="iditemproduto" value="<?php echo $iditemproduto;?>"/>
                <div class="row">
                    <div class="col-md-2">
                        <label for="numerotransacao">Transação (Venda)</label>
                        <input type="text" class="form-control" id="idtransacao" name="idvendatroca" value="<?php echo $idvendatroca;?>" readonly="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="produtodevolvido">Nome do Produto a ser trocado</label>
                        <select class="form-control selectpicker" name="idprodutotrocado" id="idprodutotrocado" required data-errormessage-value-missing="Selecione o Produto" data-live-search="true">
                            <option value="">Selecione o Produto</option>
                            <?php
                                for($count=0;$count < $num && $data=pg_fetch_object($sqlbuscaprodutostroca,$count);$count++){?>
                                <?php
                                    if($data->quantidadedisponivel != null){
                                        $disponibilidade = $data->quantidadedisponivel;
                                     }else if($data->quantidadeproduto != null){
                                         $disponibilidade = $data->quantidadeproduto;
                                    }
                                    if($disponibilidade == 0){
                                        $statusdisponibilidade = "style='color: red'";
                                    }else{
                                        $statusdisponibilidade = "";
                                    }
                                ?>
                            
                            <option <?php echo $statusdisponibilidade?> value='<?php echo $data->id; ?>' <?php if($disponibilidade == 0){?>
                                                                        disabled=""
                                        <?php } ?>>
                                           <?php 
                                                if($data->numeroproduto == ''){
                                                   $tamanhonumeroproduto = $data->tamanho;
                                                }else if($data->tamanho == ''){
                                                   $tamanhonumeroproduto = $data->numeroproduto;
                                                }
                                                ?>                                                  
                                            <?php
                                                if($disponibilidade == 0){
                                                    echo $data->nomeproduto . ' -- ' . $tamanhonumeroproduto . " -- Produto Indisponível";
                                                }else{
                                                    echo $data->nomeproduto . ' -- ' . $tamanhonumeroproduto;
                                                }
                                            ?>
                                                
                                </option>
                            <?php }
                            ?>
                        </select>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <!--<div class="col-md-2">-->
                        <!--
                        <label for="tamanhonumero"><?php //echo $descricaotamanho; ?></label>
                        <?php 
                        /*if($numeroproduto != ''){ ?>
                            <input type="text" class="form-control" onkeypress="return Onlynumbers(event)" autocomplete="off" id="tamanhonumero" name="tamanhonumero" value="">
                        <?php }else if($tamanho != ''){?>
                            <input type="text" class="form-control" onkeypress="return Onlychars(event)" autocomplete="off" id="tamanhonumero" name="tamanhonumero" value="">
                       <?php }*/?>
                    </div>-->
                    <div class="col-md-2">
                        <label for="quantidade">Quantidade Trocada</label>
                        <input type="text" class="form-control" onkeypress="return Onlynumbers(event)" id="quantidade" name="quantidade" value="" autocomplete="off">
                    </div>
                    <div class="col-md-2">
                        <label for="desconto">Desconto</label>
                        <input type="text" class="form-control" title="Somente se o valor da Troca for maior" onkeypress="return Onlynumbers(event)" id="desconto" name="desconto_linha" value="" autocomplete="off" placeholder="% Porcentagem">
                    </div>
                </div>
                <br/>
                <hr style="margin-top: 0;"/>
                <h4>Produto Devolvido:</h4>
                <hr style="width: 500px; margin-left: 0px; margin-top: 0px;"/>
                <div class="row">
                    <div class="col-md-6">
                        <label for="produtodevolvido">Produto a ser Devolvido</label>
                        <input type="text" class="form-control" id="nomeproduto" name="nomeprodutodevolvido" value="<?php echo $nomeproduto;?>" readonly="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="referencia">Referência</label>
                        <input type="text" class="form-control" id="referencia" name="referencia" value="<?php echo $referencia;?>" readonly="true">
                    </div>
                    <div class="col-md-2">
                        <label for="tamanhonumero"><?php echo $descricaotamanho; ?></label>
                        <input type="text" class="form-control" id="tamanhonumero" name="tamanhonumero" value="<?php echo $tamanhonumero;?>" readonly="true">
                    </div>
                    <div class="col-md-2">
                        <label for="valor">Valor</label>
                        <input type="text" class="form-control" id="valor" name="valor" value="<?php echo 'R$ ' . $valor;?>" readonly="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="quantidade">Quantidade devolvida</label>
                        <input type="text" onkeypress="return Onlynumbers(event)" autocomplete="off" class="form-control" id="quantidadedevolvida" id="quantidadedevolvida" name="quantidadedevolvida">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success" title="Clique aqui para realizar a troca do produto">Trocar</button>
                <button type="submit" class="btn btn-primary" title="Clique aqui para limpar o formulário">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formManutencaoTrocarproduto").validate({
        rules: {
            idprodutotrocado: {
                required: true
            },
            idproduto: {
                required: true
            },
            tamanhonumero: {
                required: true
            },
            quantidade: {
                required: true
            },
            quantidadedevolvida: {
                required: true
            }
        },
        messages: {
            idprodutotrocado: {
                required: "Por favor, Selecione um produto a ser Trocado"
            },
            idproduto: {
                required: "Por favor, Selecione um produto a ser Trocado"
            },
            tamanhonumero: {
                required: "Por favor, informe o Número ou o Tamanho do produto a ser Trocado"
            },
            quantidade: {
                required: "Por favor, informe a Quantidade a ser Trocada"
            },
            quantidadedevolvida: {
                required: "Por favor, informe a Quantidade a ser Devolvida"
            }
        }
    });
</script>