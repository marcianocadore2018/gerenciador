<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">E-mails Enviados - Aniversariantes</div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Nome Cliente</th>
                    <th>Data Nascimento</th>
                    <th>Celular</th>
                    <th>E-mail</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datanascimento'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['celular'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['email'];
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>