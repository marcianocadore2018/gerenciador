<?php
    require_once("../../../config/confloginrel.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gerenciador de Projetos</title>
        <link href="../../../includes/css/bootstrap.css" rel="stylesheet">
        <link rel="shortcut icon" href="http://atlantajeans.com.br/gerenciador/includes/imagens/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="../../../includes/css/datatables.min.css">
        <link rel="stylesheet" href="../../../includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="../../../includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        
        <!-- Excluir Registro - Mensagem-->
        <script src="../../../includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="../../../includes/css/jquery-ui.css" type="text/css" />
        <!-- JQuery formata Valores -->
        <script>
            jQuery(function ($) {
                $("#datapagamentoinicio").mask("99/99/9999");
                $("#datapagamentofim").mask("99/99/9999");
                $("#datapagamentoinicio").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
                $("#datapagamentofim").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
            });

        </script>
        <style>
            #menutitle{
                color: white;
                padding-left: 25px;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #A44160;
            }
            div.ui-datepicker{
             font-size:12px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #A44160">
            <div class="container" style="padding-left: 0px">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Filtro Relatório de Pagamento</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 120%;"></div>
            </div>
        </nav>
        <br/><br/><br/>
        <div class="container">
        <form name="filtrorelatoriopagamento" id="filtrorelatoriopagamento" action="rel_relatoriopagamento.php" method="post">
            <div class="form-group">
                <label>Colaborador</label>
                <select name="idcolaborador" id="idcolaborador" class="form-control">
                    <?php
                    $sqlColaborador = "select distinct col.id as idcolaborador,
                                              col.nome as nomecolaborador
                                         from pagamentos pg
                                        inner join colaboradores col
                                           on pg.idcolaborador = col.id
                                     order by col.nome asc;";
                    $sqlColaboradorResult = pg_query($sqlColaborador);
                    echo '<option value="">
                            Selecione...
                        </option>';
                    while ($sqlColaboradorResultFim = pg_fetch_assoc($sqlColaboradorResult)) {
                        ?>
                        <option value="<?php echo $sqlColaboradorResultFim["idcolaborador"] ?>">
                            <?php echo $sqlColaboradorResultFim["nomecolaborador"] ?>
                        </option>

                        <?php
                    }
                    ?>
                </select>
            </div>
            <br/><br/><br/><br/>
            <div class="row">
                <div class="col-xs-5">
                    <label for="datapagamentoinicio">Data Pagamento >= </label>
                    <input class="form-control" id="datapagamentoinicio" name="datapagamentoinicio"/>
                </div>
                <div class="col-xs-5">
                    <label for="datapagamentofim"><= Data Pagamento</label>
                    <input class="form-control" id="datapagamentofim" name="datapagamentofim"/>
                </div>
            </div>            
            <br/>
            <button type="submit" class="btn btn-success">Gerar Relatório</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
        </form>
    </div>
    </body>
</html>