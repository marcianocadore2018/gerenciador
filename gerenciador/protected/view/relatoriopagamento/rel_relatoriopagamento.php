<?php
include '../../libs/verificaambiente.php';
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");

$in_idcolaborador = $_POST['idcolaborador'];
$in_datapagamentoinicio = $_POST['datapagamentoinicio'];
$in_datapagamentofim = $_POST['datapagamentofim'];

//Filtro apenas com o Colaborador
if($in_idcolaborador != "" && $in_datapagamentoinicio == "" && $in_datapagamentofim == ""){
   $filtropesquisa = 'where co.id = ' . " $in_idcolaborador";
}else if($in_idcolaborador == "" && $in_datapagamentoinicio != "" && $in_datapagamentofim != ""){
  // Filtro com Data Pagamento Início e Data Pagamento Fim
  $filtropesquisa = 'where pa.datapagamento >= ' . "'$in_datapagamentoinicio'" . ' and pa.datapagamento <= ' . "'$in_datapagamentofim'";
}else if($in_idcolaborador != null && $in_datapagamentoinicio != "" && $in_datapagamentofim != ""){
    //filtro com colaborador data pagamento de inicio e fim
    $filtropesquisa = 'where co.id >= ' . "$in_idcolaborador" . ' and pa.datapagamento >= ' . "'$in_datapagamentoinicio'" . " and pa.datapagamento <= " . "'$in_datapagamentofim'";
}else if($in_idcolaborador == null && $in_datapagamentoinicio == "" && $in_datapagamentofim == ""){
  //Pesquisa por nenhum filtro
  $filtropesquisa = null;
}
if($in_datapagamentoinicio != null && $in_datapagamentofim == null){
    echo "<script>window.location='filtrorelatoriopagamento.php';"
        . "alert('Por favor informe a <= Data de Pagamento');</script>";
}else if($in_datapagamentoinicio == null && $in_datapagamentofim != null){
  echo "<script>window.location='filtrorelatoriopagamento.php';"
        . "alert('Por favor informe a Data Pagamento >=');</script>";
}else{
    $pdf = new FPDF("P", "pt", "A4");

    $pdf->AddPage('L');
    $pdf->SetFont('arial', 'B', 12);
    $pdf->Cell(0, 5, utf8_decode("Gerenciador - Relatório de Pagamentos"), 0, 1, 'C');
    $pdf->Ln(3);

    //Contorno Margens do relatório
    $pdf->Line(830, 10, 10, 10);
    $pdf->Line(10, 580, 10, 10);
    $pdf->Line(830, 580, 830, 10);
    $pdf->Line(830, 580, 10, 580);

    //CABEÇALHO DO RELATÓRIO
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(80);
    $pdf->Ln(20);
    
    //CABEÇALHO DA TABELA
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->setFillColor(180, 180, 180);
    $pdf->Cell(245, 16, utf8_decode('Nome Cliente'), 1, 0, 'L', 1);
    $pdf->Cell(245, 16, utf8_decode('Nome Colaborador'), 1, 0, 'L', 1);
    $pdf->Cell(100, 16,  utf8_decode('Data Pagamento'), 1, 0, 'C', 1);
    $pdf->Cell(100, 16,  utf8_decode('Hora Pagamento'), 1, 0, 'C', 1);
    $pdf->Cell(100, 16,  utf8_decode('Valor Pago'), 1, 0, 'C', 1);
    $pdf->Ln();

    //DADOS DA TABELA
    $pdf->SetFont('Arial', '', 10);
    $query = "select col.id as idcolaborador,
                      CASE WHEN cli.nome is null THEN rap.nome 
                           ELSE cli.nome
                      END as nomecliente,
                      col.nome as nomecolaborador,
                      CASE WHEN pa.datapagamento is null THEN 'Data não informada' 
                         ELSE to_char(pa.datapagamento, 'dd/MM/yyyy')
                      END as datapagamento,
                      CASE WHEN pa.horapagamento is null THEN 'Hora não informada' 
                         ELSE to_char(pa.horapagamento, 'HH24:mi')
                       END as horapagamento,
                     'R$ ' || LTRIM(to_char(pa.valorparcelas, '9G999G990D99')) as valorpago
                from parcelas pa
               inner join venda ve
                  on pa.idvenda = ve.id
               inner join colaboradores col
                  on ve.idcolaborador = col.id
                left join cliente cli
                  on pa.idcliente = cli.id
                left join clienterapido rap
                  on pa.idcliente = rap.id
                     $filtropesquisa
                 and pa.statusparcela = 'PG';";
    $result = pg_query($query);
    
    while ($consulta = pg_fetch_assoc($result)) {
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(245, 14, utf8_decode($consulta['nomecliente']), 1, 0, 'L', 1);
        $pdf->Cell(245, 14, utf8_decode($consulta['nomecolaborador']), 1, 0, 'L', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta['datapagamento']), 1, 0, 'C', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta['horapagamento']), 1, 0, 'C', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta['valorpago']), 1, 0, 'C', 1);
        $pdf->Ln();
    }
    
    //VALOR TOTAL PAGAMENTO
    $pdf->setFillColor(180, 180, 180);
    $pdf->Cell(590, 16, utf8_decode(''), 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 10);
    
    $queryvalortotal = "select sum(pa.valorparcelas) as valortotalpago,
                               sum(pa.valorparcelas) as totalpago
                          from parcelas pa
                         inner join venda ve
                            on pa.idvenda = ve.id
                         inner join colaboradores col
                            on ve.idcolaborador = col.id
                          left join cliente cli
                            on pa.idcliente = cli.id
                          left join clienterapido rap
                            on pa.idcliente = rap.id
                               $filtropesquisa
                           and pa.statusparcela = 'PG';";
    $resultvalortotal = pg_query($queryvalortotal);
    
    while ($consultatotal = pg_fetch_assoc($resultvalortotal)) {
          $valortotalpago = $consultatotal['valortotalpago'];
          $totalpago      = $consultatotal['totalpago'];
    }
    
    //CONSULTA VALOR BÔNUS TROCA
    $sqlbonustroca = "select ltrim(to_char(sum(valordiferenca), '9G999G990D99')) as valorbonustroca,
                             sum(valordiferenca) as valordiferencabonus
                        from trocaproduto 
                       where statustroca='B'";
    $resultbonustroca = pg_query($sqlbonustroca);
    while ($consultabonustroca = pg_fetch_assoc($resultbonustroca)) {
        $valorbonustroca = $consultabonustroca['valorbonustroca'];
        $bonustroca = $consultabonustroca['valordiferencabonus'];
    }
    
    //CONSULTA VALOR PAGO TROCA
    $sqlvalorpagotroca = "select trim(cast(sum(valordiferenca) as text), '-') as valorpagotroca,
                                 trim(cast(sum(valordiferenca) as text), '-') as valorpago
                            from trocaproduto 
                           where statustroca='D';";
    $resultvalorpagotroca = pg_query($sqlvalorpagotroca);
    while ($consultavalorpagotroca = pg_fetch_assoc($resultvalorpagotroca)) {
        $valorpagotroca = $consultavalorpagotroca['valorpagotroca'];
        $valorpago = $consultavalorpagotroca['valorpago'];
    }
   
    //Soma Descontos Finais
    $sqldescontosfinais = "select sum(descontofinal) as descontofinalformatado
                             from venda;";
    $resultdescontosfinais = pg_query($sqldescontosfinais);
    while ($consultadescontosfinais = pg_fetch_assoc($resultdescontosfinais)) {
        $valordescontofinal = $consultadescontosfinais['descontofinalformatado'];
    }
    
    $valortotaldesconto = ($valortotalpago - $valordescontofinal);
    $pdf->setFillColor(200, 200, 200);
    $pdf->SetFont('Arial', 'B', 8);
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, "VALOR TOTAL SEM DESCONTO: ", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, "R$ " . number_format($valortotalpago, 2, ',', '.'), 1, 1, 'R');
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, "VALOR DESCONTO: ", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, "R$ " . number_format($valordescontofinal, 2, ',', '.'), 1, 1, 'R');
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, "VALOR TOTAL: ", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, "R$ " . number_format($valortotaldesconto, 2, ',', '.'), 1, 1, 'R');
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, "", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, "" . "", 1, 1, 'R');
    $pdf->ln(0);
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode("VALOR BÔNUS TROCA: "), 1, 0, 'R', 1);
    $pdf->Cell(93, 12, 'R$ ' . number_format($valorbonustroca, 2, ',', '.'), 1, 1, 'R'); 

    $pdf->ln(0);
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, "VALOR PAGO TROCA: ", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, 'R$ ' . number_format($valorpagotroca, 2, ',', '.'), 1, 1, 'R');
    
    $valorfinaltotalpago = ($valortotaldesconto + $valorpagotroca);
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, "VALOR TOTAL TROCA: ", 1, 0, 'R', 1);
    $pdf->Cell(93, 12, "R$ " . number_format($valorfinaltotalpago, 2, ',', '.'), 1, 1, 'R');
    
    // Rodapé
    $pdf->Line(552, 810, 452, 810);
    $pdf->SetXY(01, 530);
    $data = date("d/m/Y H:i:s");
    $conteudo = $data . utf8_decode(" Pág. ") . $pdf->PageNo();
    $pdf->Cell(790, 5, $conteudo, 10, 0, "R");

    $pdf->Output("rel_relatoriopagamento.pdf", "D");

    pg_close($conexao);
}