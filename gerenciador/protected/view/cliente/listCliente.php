<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Cliente</div>
            <div class="panel-body">
                <a href="index.php?controle=clienteController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 2000px;">
                    <thead>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Gênero</th>
                    <th>CPF</th>
                    <th>Celular</th>
                    <th>Cidade</th>
                    <th>Bairro</th>
                    <th>Estado</th>
                    <th>Número</th>
                    <th>Complemento</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php


                        //consultar usuario, se for tipo Master pode alterar
                        $tipousuariocolaborador = $_SESSION['login'];
                        $colaboradormaster      = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                        $rscolaborador          = pg_fetch_array ($colaboradormaster);
                        $colaboradortipo        = $rscolaborador['tipocolaborador'];

                        $sql                    = "select r.id,
                                                   r.descricaorenda,
                                                   to_char(r.valorrenda, 'L9G999G990D99') as valorrenda,
                                                   to_char(r.datainiciorenda, 'dd/MM/yyyy') as datainiciorenda,
                                                   to_char(r.datacadastrorenda, 'dd/MM/yyyy') as datacadastrorenda,
                                                   r.idloja,
                                                   r.idcliente,
                                                   r.idcolaborador
                                              from rendacliente r";

                        $res                    = pg_query($sql);
                        $listaRendas            = pg_fetch_all($res);

                        foreach ($listaDados as $item) {

                            $id = $item['id'];

                            if($item['sexo'] == 'F'){
                                $item['sexo'] = 'Feminino';
                            }else
                                $item['sexo'] = 'Masculino';

                            echo '<tr>';

                            echo '<td>' . $item['id'];
                            echo '<td>' . $item['nome'];
                            echo '<td style="padding-left: 50px;">' . $item['datanascimento'];
                            echo '<td>' . $item['sexo'];
                            echo '<td>' . $item['cpf'];
                            echo '<td>' . $item['celular'];
                            echo '<td>' . $item['cidade'];
                            echo '<td>' . $item['bairro'];
                            echo '<td style="padding-left: 30px;">' . $item['uf'];
                            echo '<td style="padding-left: 30px;">' . $item['numero'];
                            echo '<td style="padding-left: 30px;">' . $item['complemento'];
                            
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='#' data-toggle='modal' data-target='#exibirRendas$id'>"
                            . " <span class='glyphicon glyphicon-book' title='Ver rendas'> </span>"
                            . "</a> </td>";
                            echo "<td> <a href='index.php?controle=clienteController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            if($colaboradortipo != 'C'){
                            echo "<td> <a onclick='excluir(\"excluir\",\"clienteController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            }
                            if($colaboradortipo == 'C'){
                                echo "<td></td>";
                            }
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--Exibir rendas -->
<?php 

//Esse método de montagem pode ser considerado rudimentar, mas evita que seja usado o ajax (:
foreach ($listaDados as $key) {
    
?>
<!-- Modal -->
<div class="modal fade" id="exibirRendas<?= $key['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 850px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Exibição de rendas do cliente</h4>
            </div>
            <div class="modal-body">
                 <div class="table-responsive">
                    <table class="table" id="example1" style="width: 800px;">
                        <thead>
                        <th style="width: 400px;">Descrição</th>
                        <th style="width: 100px;">Valor</th>
                        <th style="width: 100px;">Data de Início</th>
                        <th style="width: 100px;">Data cadastro</th>
                        </thead>
                        <tbody>
                            <?php

                                foreach ($listaRendas as $i) {

                                    if($key['id'] == $i['idcliente']){
                                        echo '<tr>';
                                        echo '<td>' . $i['descricaorenda'];
                                        echo '<td>' . $i['valorrenda'];
                                        echo '<td>' . $i['datainiciorenda'];
                                        echo '<td>' . $i['datacadastrorenda'];
                                        echo '</tr>';
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!--Fim da exibicao de rendas -->