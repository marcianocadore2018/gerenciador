<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Cliente</div>
        <div class="panel-body">
            <form action="<?=  $acao ?>" name="formCliente" id="formCliente" method="POST" class="form" role="form">
                <!-- 
                    Hidden fields
                -->
                <input type="hidden" class="form-control" id="idregistropessoa" name="idregistropessoa" readonly="true" value="<?php if (isset($cliente)) echo $cliente['idregistropessoa']; ?>">
                <input type="hidden" class="form-control" id="idcolaborador" name="idcolaborador" readonly="true" value="<?php if (isset($cliente)) echo $cliente['idcolaborador']; ?>">
                <input type="hidden" class="form-control" id="idloja" name="idloja" readonly="true" value="<?php if (isset($cliente)) echo $cliente['idloja']; ?>">

                        <div class="row">
                            <div class="col-md-1">
                                <label for="id">Id</label>
                                <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                       value="<?php if (isset($cliente)) echo $cliente['id']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="descricao">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome" 
                                       value="<?php if (isset($cliente)) echo $cliente['nome']; ?>" required minlength="3" maxlength="100">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="datanascimento">Data/Nascimento</label>
                                <input type="text" class="form-control" id="data" name="datanascimento" placeholder="Informe a Data/Nascimento" 
                                       value="<?php if (isset($cliente)) echo $cliente['datanascimento']; ?>" required>
                            </div>
                            <div class="col-md-3">
                                <label for="sexo">Gênero</label>
                                <br>
                                <input type="radio" name="sexo" value="M" checked="" 
                                    <?php if(isset($cliente)) if ($cliente['sexo'] == 'M'){echo 'checked';}else{$cliente == null;} ?>> Masculino
                                <input type="radio" name="sexo" value="F" 
                                    <?php if(isset($cliente)) if ($cliente['sexo'] == 'F'){echo 'checked';}else{$cliente == null;} ?>> Feminino
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="rg">RG</label>
                                <input type="text" class="form-control" id="rg" name="rg" placeholder="Informe o RG" 
                                       value="<?php if (isset($cliente)) echo $cliente['rg']; ?>" required maxlength="10">
                            </div>
                            <div class="col-md-2">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Informe o CPF" 
                                       onkeypress="return Onlynumbers(event)" value="<?php if (isset($cliente)) echo $cliente['cpf']; ?>" required maxlength="14">
                            </div>
                            <script>
                                $(document).ready(function(){

                                    $("#jus").hide();
                                    $("#contatocliente").hide();
                                    
                                    $("#situacaocliente").change(function() {
                                        loadSituation();
                                    });


                                    loadSituation();
                                });

                                function loadSituation(){
                                    var val = $("#situacaocliente").val();
                                    if(val==1){
                                        $("#jus").fadeIn();
                                        $("#justificativaclienteinativo").attr("required", true);
                                    }else{
                                        $("#jus").fadeOut();
                                        $("#justificativaclienteinativo").attr("required", false);
                                    }
                                }

                                function verificaIdade(data){

                                    var d = new Date,
                                    ano_atual = d.getFullYear(),
                                    mes_atual = d.getMonth() + 1,
                                    dia_atual = d.getDate(),

                                    ano_aniversario = data.substring(6,10);
                                    mes_aniversario = data.substring(3,5);
                                    dia_aniversario = data.substring(0,2);

                                    quantos_anos = ano_atual - ano_aniversario;

                                    if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
                                        quantos_anos--;
                                    }

                                    if(quantos_anos < 0 ? 0 : quantos_anos < 18){
                                        $("#contatocliente").fadeIn();
                                    }else{
                                        $("#contatocliente").hide();
                                    }
                                };
                            </script>
                            <?php if($_GET['acao'] == 'buscar'){?>
                            <div class="col-md-2">
                                <label for="situacaocliente">Situação</label>
                                        <select class="form-control" name="situacaocliente" id="situacaocliente" required data-errormessage-value-missing="Selecione a situação">
                                        <?php
                                            if($cliente['situacaocliente'] == '0'){
                                                $selAtivo   = 'selected';
                                                $selInativo = '';
                                            }else if($cliente['situacaocliente'] == '1'){
                                                $selAtivo   = '';
                                                $selInativo = 'selected';
                                            }else{
                                                $selAtivo   = 'selected';
                                            }
                                        ?>
                                        <option value="0" <?= $selAtivo?> >Ativo</option>
                                        <option value="1" <?= $selInativo?> >Inativo</option>
                                    </select>
                                    <br>
                            </div>
                            <div class="row" id="jus">
                                <div class="col-md-5">
                                    <label for="justificativaclienteinativo">Justificativa para Cliente Inativo</label>
                                    <textarea class="form-control" id="justificativaclienteinativo" name="justificativaclienteinativo" placeholder="Informe a Justificativa" minlength="10" maxlength="300"><?php if (isset($cliente)) echo $cliente['justificativaclienteinativo']; ?></textarea>
                                </div>
                            </div>
                            <?php }else{?>
                            <div class="col-md-2">
                                <label for="situacaocliente">Situação</label>
                                <br/>
                                <input type="radio" name="situacaocliente" value="0" checked=""> Ativo
                            </div>
                            <div class="row" id="jus">
                                <div class="col-md-6">
                                    <input type="radio" name="justificativaclienteinativo" value="" checked="" style="width: 0px">
                                </div>
                            </div>
                            <?php }
                            ?>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="observacao">Observação</label>
                                <textarea class="form-control" id="observacao" name="observacao" maxlength="2000"><?php if (isset($cliente)) echo $cliente['observacao']; ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço" 
                                       value="<?php if (isset($cliente)) echo $cliente['endereco']; ?>" required minlength="3" maxlength="100">
                            </div>
                            <div class="col-md-3">
                                <label for="bairro">Bairro</label>
                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o Bairro" 
                                       value="<?php if (isset($cliente)) echo $cliente['bairro']; ?>" required minlength="3" maxlength="80">
                            </div>
                            <div class="col-md-3">
                                <label for="numero">Número</label>
                                <input type="text" class="form-control" id="numero" name="numero" maxlength="5" placeholder="Digite a Número" 
                                       value="<?php if (isset($cliente)) echo $cliente['numero']; ?>" onkeypress="return Onlynumbers(event);" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="cidade">Cidade</label>
                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Digite a Cidade" 
                                       value="<?php if (isset($cliente)) echo $cliente['cidade']; ?>" required minlength="3" maxlength="100">
                            </div>
                            <div class="col-md-2">
                                <label for="idestado">Estado</label>
                                <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                                    <?php
                                        foreach ($listaEstados as $estados) {
                                            $selected = (isset($cliente) && $cliente['idestado'] == $estados['id']) ? 'selected' : '';
                                            ?>
                                            <option value='<?php echo $estados['id']; ?>'
                                                    <?php echo $selected; ?>> 
                                                        <?php echo $estados['uf']; ?>
                                            </option>
                                        <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cep">CEP</label>
                                <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o Cep" 
                                       value="<?php if (isset($cliente)) echo $cliente['cep']; ?>" required maxlength="9">
                            </div>
                            <div style="padding-top: 30px; font-size: 12px; color: blue">
                                <a href="http://m.correios.com.br/movel/buscaCep.do" target="_blank" title="Clique aqui para pesquisar o CEP"><b>Verificar CEP Correios</b></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="complemento">Complemento</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Digite a Complemento" 
                                       value="<?php if (isset($cliente)) echo $cliente['complemento']; ?>">
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Digite o Telefone" 
                                       value="<?php if (isset($cliente)) echo $cliente['telefone']; ?>" required maxlength="20">
                            </div>
                            <div class="col-md-2">
                                <label for="celular">Celular</label>
                                <input type="text" class="form-control" attrname="telephone1" name="celular" placeholder="Digite o Celular" 
                                        value="<?php if (isset($cliente)) echo $cliente['celular']; ?>">
                            </div>
                            <div class="col-md-4" id="contatocliente">
                                <label for="contato">Contato</label>
                                <input type="text" class="form-control" id="contato" name="contato" placeholder="Informe o Contato Próximo" 
                                        value="<?php if (isset($cliente)) echo $cliente['contato']; ?>">
                            </div>
                            <div class="col-md-4">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="exemplo@atlantajeans.com.br" 
                                        value="<?php if (isset($cliente)) echo $cliente['email']; ?>">
                            </div>
                        </div>
                        <!-- ESSA MANUTENÇÃO SE REFERE A MANUTENÇÃO DE RENDA DO CLIENTE
                             A RENDA DO CLIENTE EU FIZ E ESTÁ NO MENU DA RENDA.
                             AQUI EU "APENAS" ADICIONEI A IDEIA QUE DEVE SE SEGUIR PARA DESENVOLVER A RENDA PARA O CLIENTE
                             NA MANUTENÇÃO DO CLIENTE.
                             DATA INCLUSÃO: 31/12/2016
                             OBS: NÃO ALTEREI NENHUM OUTRO CÓDIGO DO CLIENTE-->
                             <!--INÍCIO DA MANUTENÇÃO DE EXEMPLO DA RENDA DO CLIENTE -->
                             <br/>
                                <div class="row">
                                    <script>
                                        $(document).ready(function(){
                                            $("#btnCadastrarRenda").click(function(){
                                                <?php

                                                    if(strpos($acao, 'atualizar') !== false || strpos($acao, 'buscar') !== false){
                                                        ?> 
                                                            $('#cadastrarRenda').modal('show');
                                                        <?php
                                                    } else {
                                                        ?> 
                                                            alert('Atenção! Deve-se salvar cliente antes de cadastrar rendas.');
                                                        <?php
                                                    }
                                                ?>
                                            });
                                        });
                                    </script>
                                    <div class="col-md-2">
                                        <label>Cadastro de rendas</label>
                                        <button type="button" class="btn btn-primary btn-default" id="btnCadastrarRenda">
                                          Cadastrar
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <label>Exibir rendas</label>
                                        <button type="button" class="btn btn-primary btn-default" data-toggle="modal" data-target="#exibirRendas">
                                          Exibir
                                        </button> 
                                    </div>
                                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
            <form action="<?=  $acaoRenda ?>" name="formRenda" id="formRenda" method="POST" class="form" role="form">

            	<input type="hidden" value="<?= $acao ?>" id="acaoBack" name="acaoBack"/>
            	<input type="hidden" value="<?= $_GET['id'] ?>" id="idcliente" name="idcliente"/>
            	<input type="hidden" value="<?= base64_encode(gzdeflate(serialize($cliente))) ?>" id="cliente" name="cliente"/>

				<!-- Modal -->
				<div class="modal fade" id="cadastrarRenda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
					    <div class="modal-content">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        	<h4 class="modal-title" id="myModalLabel">Insira a Renda do Cliente</h4>
					      	</div>
					      	<div class="modal-body">
					            <div class="row">
					                <div class="col-md-10">
					                    <label for="descricaorenda">Descrição Renda</label>
					                    <input type="text" class="form-control" id="descricaorenda" name="descricaorenda" placeholder="Digite a Descrição da Renda" 
					                           value="<?php if (isset($renda)) echo $renda['descricaorenda']; ?>" required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-3">
					                    <label for="valorrenda">Valor Renda</label>
					                    <input type="text" class="form-control" id="valor" name="valorrenda" placeholder="Digite o Valor" 
					                           value="<?php if (isset($renda)) echo $renda['valorrenda']; ?>" required>
					                </div>
					            </div>
					            <div class="row">
					                <div class="col-md-4">
					                    <label for="datainiciorenda">Data Início Renda</label>
					                    <input type="text" class="form-control" id="datainiciorenda" name="datainiciorenda" placeholder="Digite a Data" 
					                           value="<?php if (isset($renda)) echo $renda['datainiciorenda']; ?>" required>
					                </div>
					            </div>
					      	</div>
					      	<script>
					      	function getAbsolutePath() {
							    var loc = window.location;
							    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
							    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
							}
						      	$(document).ready(function(){
						      		$("#gravarRenda").click(function(){
						      			var $form = $("#formRenda");
						      			if(!$form.valid()) return false;

						      			$("#formRenda").submit();
							        });
						      	});
					      	</script>
						    <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						        <button type="button" class="btn btn-primary" id="gravarRenda">Gravar</button>
						    </div>
					    </div>
					</div>
				</div>
			</form>
        	<!--FIM DA MANUTENÇÃO DE EXEMPLO DA RENDA DO CLIENTE -->
        </div>

        <!--Exibir rendas -->
        <!-- Modal -->
        <div class="modal fade" id="exibirRendas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="width: 1000px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Exibição de rendas do cliente</h4>
                    </div>
                    <div class="modal-body">
                         <div class="table-responsive">
                            <table class="table" id="example1" style="width: 1000px;">
                                <thead>
                                <th style="width: 400px;">Descrição</th>
                                <th style="width: 100px;">Valor</th>
                                <th style="width: 100px;">Data de Início</th>
                                <th style="width: 100px;">Data cadastro</th>
                                </thead>
                                <tbody>
                                    <?php

                                    foreach ($listaRendas as $i) {

                                        echo '<tr>';
                                        echo '<td>' . $i['descricaorenda'];
                                        echo '<td>' . $i['valorrenda'];
                                        echo '<td>' . $i['datainiciorenda'];
                                        echo '<td>' . $i['datacadastrorenda'];
                                        echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Fim da exibicao de rendas -->
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    jQuery.extend(jQuery.validator.messages, {
        email:"Por favor informe um email válido."
    });
    $("#formRenda").validate({
    	rules: {
            descricaorenda: {
                required: true,
                minlength: 10,
                maxlength: 100
            },
            valorrenda: {
                required: true
            },
            datainiciorenda: {
                required: true
            }
    	},
    	messages: {
    		
            descricaorenda: {
                required: "Por favor, informe a Descrição da Renda",
                minlength: "A Descrição da Renda deve ter pelo menos 10 caracteres",
                maxlength: "A Descrição da Renda deve ter no máximo 100 caracteres"
            },
            valorrenda: {
                required: "Por favor, informe o Valor da Renda do Cliente"
            },
            datainiciorenda: {
                required: "Por favor, informe a Data de Início da Renda"
            }
    	}
    });

    $("#formCliente").validate({
        rules: {
            nome: {
                required: true
            },
            datanascimento: {
                required: true
            },
            rg: {
                required: true
            },
            cpf: {
                required: true
            },
            justificativaclienteinativo: {
                required: true
            },
            endereco: {
                required: true
            },
            bairro: {
                required: true
            },
            numero: {
                required: true
            },
            cidade: {
                required: true
            },
            idestado: {
                required: true
            },
            cep: {
                required: true
            },
            telefone: {
                required: true
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome do Cliente",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 100 caracteres"
            },
            datanascimento: {
                required: "Por favor, informe a Data de Nascimento"
            },
            rg: {
                required: "Por favor, informe o RG"
            },
            cpf: {
                required: "Por favor, informe o CPF"
            },
            justificativaclienteinativo: {
                required: "Por favor, informe a Justificativa do Cliente Inativo",
                minlength: "A Justificativa deve ter pelo menos 10 caracteres",
                maxlength: "A Justificativa deve ter no máximo 300 caracteres"
            },
            endereco: {
                required: "Por favor, informe o Endereço",
                minlength: "O Endereço deve ter pelo menos 3 caracteres",
                maxlength: "O Endereço deve ter no máximo 100 caracteres"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                minlength: "O Bairro deve ter pelo menos 3 caracteres",
                maxlength: "O Bairro deve ter no máximo 100 caracteres"
            },
            numero: {
                required: "Por favor, Informe o Número"
            },
            cidade: {
                required: "Por favor, informe o Nome da Cidade",
                minlength: "A Cidade deve ter pelo menos 3 caracteres",
                maxlength: "A Cidade deve ter no máximo 100 caracteres"
            },
            idestado: {
                required: "Por favor, Informe o Estado"
            },
            cep: {
                required: "Por favor, informe o CEP",
                minlength: "O CEP deve ter pelo menos 9 caracteres",
                maxlength: "O CEP deve ter no máximo 9 caracteres"
            },
            telefone: {
                required: "Por favor, informe o Telefone",
                maxlength: "O Telefone deve ter no máximo 20 caracteres"
            },
            descricaorenda: {
                required: "Por favor, informe a Descrição da Renda",
                minlength: "A Descrição da Renda deve ter pelo menos 10 caracteres",
                maxlength: "A Descrição da Renda deve ter no máximo 100 caracteres"
            },
            valorrenda: {
                required: "Por favor, informe o Valor da Renda do Cliente"
            },
            datainiciorenda: {
                required: "Por favor, informe a Data de Início da Renda"
            }
        }
    });
</script>