<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Gerenciamento Financeiro Entrada</div>
            <div class="panel-body">
                <a href="index.php?controle=gerenciamentofinanceiroentradaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
                    <label style="margin-left: 100px;">Valor Disponível: </label>
                    <!-- Cálculo com o que tem em caixa (Gerenciamento Financeiro Entrada) - valor de saída(Gerenciamento Financeiro Saída) -->
                    <?php
                        foreach ($listaDadosValoresEntrada as $item) {
                            if($item['somavalorentrada'] == null){
                               $valorentrada = "R$ 0.00";
                            }else{
                               $valorentrada  = $item['somavalorentrada'];
                            }
                        }
                        foreach ($listaDadosValoresSaida as $item) {
                            if($item['somavalorsaida'] == null){
                                $valorpagamento = "R$ 0.00";
                            }else{
                                $valorpagamento  = $item['somavalorsaida'];
                            }
                        }
                        $valordisponivel = $valorentrada - $valorpagamento;
                        if($valordisponivel < 0){
                            $corfundolabel = "background-color: #CD5C5C;";
                        }else if($valordisponivel > 0){
                            $valordisponivel = number_format($valordisponivel, 2, ',', '.');
                            $corfundolabel = "background-color: #006400;";
                        }
                        if($valordisponivel == 0){
                            $valordisponivel = "0,00";
                            $corfundolabel = "background-color: #006400;";
                        }
                    ?>
                    <label style=" <?php echo $corfundolabel; ?>" id="box">
                        <div id="valordisponivel">
                            <?php echo "R$ "; 
                                echo $valordisponivel;
                            ?>  
                        </div>
                    </label>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>#</th>
                    <th style="width: 400px;">Descrição</th>
                    <th>Valor</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            
                            echo '<td>' . $item['id'];
                            echo '<td>' . $item['descricao'];
                            echo '<td>' . $item['valor'];
                            echo '<td>' . $item['datacadastro'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <a href='index.php?controle=gerenciamentofinanceiroentradaController&acao=buscar&id=$idencriptografa '>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"gerenciamentofinanceiroentradaController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>