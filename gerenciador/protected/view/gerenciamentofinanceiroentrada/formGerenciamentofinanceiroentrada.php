<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Gerenciamento Financeiro de Entrada</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formGerenciamentofinanceiroentrada" id="formGerenciamentofinanceiroentrada" method="POST" class="form" role="form">
                <?php
                    date_default_timezone_set('America/Sao_Paulo');
                    $dataCadastro = date('d/m/Y');
                ?>
                <input type=hidden name=datacadastrohidden value="<?php echo $dataCadastro; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($gerenciamentofinanceiroentrada)) echo $gerenciamentofinanceiroentrada['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição" 
                               value="<?php if (isset($gerenciamentofinanceiroentrada)) echo $gerenciamentofinanceiroentrada['descricao']; ?>" required minlength="3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="valor">Valor</label>
                        <input type="text" class="form-control" id="valor" name="valor" placeholder="Digite o Valor" 
                               value="<?php if (isset($gerenciamentofinanceiroentrada)) echo $gerenciamentofinanceiroentrada['valor']; ?>" required>
                    </div>
                    <!-- Buscar data automaticamente -->
                    <div class="col-md-2">
                        <label for="datacadastro">Data Cadastro</label>
                        <input type="text" class="form-control" id="datacadastro" name="datacadastro" value="<?php echo $dataCadastro; ?>" disabled="" style="background-color: #D3D3D3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="maioresinformacoes">Maiores Informações</label>
                        <textarea class="form-control" id="maioresinformacoes" name="maioresinformacoes" placeholder="Informe Maiores Informações" minlength="10" maxlength="300"><?php if (isset($gerenciamentofinanceiroentrada)) echo $gerenciamentofinanceiroentrada['maioresinformacoes']; ?></textarea>
                    </div>
                </div>
                <br/>

                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formGerenciamentofinanceiroentrada").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            valor: {
                required: true
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição",
                minlength: "A Descrição deve ter pelo menos 3 caracteres",
                maxlength: "A Descrição deve ter no máximo 80 caracteres"
            },
            valor: {
                required: "Por favor, informe o Valor"
            }
        }
    });
</script>