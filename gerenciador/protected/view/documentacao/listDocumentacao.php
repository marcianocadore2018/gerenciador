<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Documentação Colaborador</div>
            <div class="panel-body">
                <a href="index.php?controle=documentacaoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Colaborador</th>
                    <th>PIS/PASEP</th>
                    <th>Número</th>
                    <th>Série</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>UF</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecolaborador'];
                            echo '<td>' . $item['pispasep'];
                            echo '<td>' . $item['numero'];
                            echo '<td style="padding-right: 0px; padding-left: 18px;">' . $item['serie'];
                            echo '<td>' . $item['cpf'];
                            echo '<td>' . $item['rg'];
                            echo '<td style="text-align: center; text-align: center; padding-left: -5;padding-left: -2;padding-right: 20px;padding-left: 0px;">' . $item['uf'];
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);   
                         
                            echo "<td> <a href='index.php?controle=documentacaoController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"documentacaoController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>