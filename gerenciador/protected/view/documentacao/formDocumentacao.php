<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Dccumentação do Colaborador</div>
            <div class="panel-body">
               <form action="<?php echo $acao; ?>" name="formDocumentacao" id="formDocumentacao" method="POST" class="form" role="form">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($documentacao)) echo $documentacao['id']; ?>">
                        </div>
                    </div>
                   <div class="row">
                        <div class="col-md-9">
                            <label for="idcolaborador">Colaborador</label>
                                <select class="form-control" name="idcolaborador" id="idcolaborador" required data-errormessage-value-missing="Selecione o Colaborador">
                                <option value="">Selecione o Colaborador</option>>
                                <?php
                                foreach ($listaColaboradores as $colaboradores) {
                                    $selected = (isset($documentacao) && $documentacao['idcolaborador'] == $colaboradores['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $colaboradores['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $colaboradores['nome']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="pispasep">Pis/Pasep</label>
                            <input type="text" class="form-control" id="pispasep" name="pispasep" placeholder="Digite o PIS/PASEP" 
                                   value="<?php if (isset($documentacao)) echo $documentacao['pispasep']; ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label for="numero">Número</label>
                            <input type="text" class="form-control" id="numero" name="numero" placeholder="Digite o Número" maxlength="10"
                                   value="<?php if (isset($documentacao)) echo $documentacao['numero']; ?>" required onkeypress="return Onlynumbers(event)">
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="serie">Série</label>
                                <input type="text" class="form-control" id="serie" name="serie" placeholder="Digite a Série" 
                                       value="<?php if (isset($documentacao)) echo $documentacao['serie']; ?>" required style="width: 247px">
                            </div>
                        </div>
                    </div>
                   <div class="row">
                        <div class="col-md-3">
                            <label for="idestado">UF</label>
                                <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                                <option value="">Selecione o Estado</option>>
                                <?php
                                foreach ($listaEstados as $estados) {
                                    $selected = (isset($documentacao) && $documentacao['idestado'] == $estados['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $estados['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $estados['uf']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-md-3">
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Digite o CPF" 
                                   value="<?php if (isset($documentacao)) echo $documentacao['cpf']; ?>" required>
                        </div>
                       <div class="col-md-3">
                            <label for="rg">RG</label>
                            <input type="text" class="form-control" id="rg" name="rg" placeholder="Digite o RG" 
                                   value="<?php if (isset($documentacao)) echo $documentacao['rg']; ?>" required>
                        </div>
                    </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>       
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formDocumentacao").validate({
        rules: {
            idcolaborador: {
                required: true
            },
            pispasep: {
                required: true
            },
            numero: {
                required: true
            },
            idestado: {
                required: true
            },
            serie: {
                required: true
            },
            cpf: {
                required: true
            },
            rg: {
                required: true
            }
        },
        messages: {
            idcolaborador: {
                required: "Por favor, Selecione o Colaborador"
            },
            pispasep: {
                required: "Por favor, informe o PIS/PASEP"
            },
            numero: {
                required: "Por favor, informe o Número"
            },
            idestado: {
                required: "Por favor, Selecione o Estado"
            },
            serie: {
                required: "Por favor, informe a Série"
            },
            cpf: {
                required: "Por favor, informe o CPF"
            },
            rg: {
                required: "Por favor, informe o RG"
            }
        }
    });
</script>
