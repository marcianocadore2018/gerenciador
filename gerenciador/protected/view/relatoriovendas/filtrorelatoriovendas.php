<?php
    include '../../libs/verificaambiente.php';
    include '../../../config/confloginrel.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gerenciador de Projetos</title>
        <link href="../../../includes/css/bootstrap.css" rel="stylesheet">
        <link rel="shortcut icon" href="http://atlantajeans.com.br/gerenciador/includes/imagens/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="../../../includes/css/datatables.min.css">
        <link rel="stylesheet" href="../../../includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="../../../includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        
        <!-- Excluir Registro - Mensagem-->
        <script src="../../../includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="../../../includes/css/jquery-ui.css" type="text/css" />
        <!-- JQuery formata Valores -->
        <script>
            jQuery(function ($) {
                $("#periodovendainicio").mask("99/99/9999");
                $("#periodovendafim").mask("99/99/9999");
                $("#periodovendainicio").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
                $("#periodovendafim").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
            });

        </script>
        <style>
            #menutitle{
                color: white;
                padding-left: 25px;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #A44160;
            }
            div.ui-datepicker{
             font-size:12px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #A44160">
            <div class="container" style="padding-left: 0px">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Filtro Relatório de Vendas</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 120%;"></div>
            </div>
        </nav>
        <br/><br/><br/>
        <div class="container">
        <form name="filtrorelatoriovendas" id="filtrorelatoriovendas" action="rel_relatoriovendas.php" method="post">
            <div class="form-group">
                <label>Colaborador Vendedor</label>
                <select name="idcolaboradorvendedor" id="idcolaboradorvendedor" class="form-control">
                    <?php
                    $sqlColaboradorVendedor = "select distinct c.id as idcolaboradorvendedor, 
                                                      c.nome as nomevendedor 
                                                 from venda v 
                                                inner join colaboradores c 
                                                   on c.id = v.idcolaboradorvendedor 
                                                order by c.nome asc;";
                    $sqlColaboradorVendedorResult = pg_query($sqlColaboradorVendedor);
                    echo '<option value="">
                            Selecione...
                        </option>';
                    while ($sqlColaboradorVendedorResultFim = pg_fetch_assoc($sqlColaboradorVendedorResult)) {
                        ?>
                        <option value="<?php echo $sqlColaboradorVendedorResultFim["idcolaboradorvendedor"] ?>">
                            <?php echo $sqlColaboradorVendedorResultFim["nomevendedor"] ?>
                        </option>

                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <label for="referencia">Referência Produto</label>
                    <input class="form-control" id="referencia" name="referencia"/>
                </div>
            </div>            
            <br/>
            <div class="row">
                <div class="col-xs-5">
                    <label for="periodovendainicio">Período Venda Inicio >= </label>
                    <input class="form-control" id="periodovendainicio" name="periodovendainicio"/>
                </div>
                <div class="col-xs-5">
                    <label for="periodovendafim"><= Período Venda Fim</label>
                    <input class="form-control" id="periodovendafim" name="periodovendafim"/>
                </div>
            </div>
            <br/>
            <button type="submit" class="btn btn-success">Gerar Relatório</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
        </form>
    </div>
    </body>
</html>