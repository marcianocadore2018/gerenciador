<?php
include '../../libs/verificaambiente.php';
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");

 
$in_idcolaboradorvendedor = $_POST['idcolaboradorvendedor'];
$in_referencia = $_POST['referencia'];
$in_periodovendainicio = $_POST['periodovendainicio'];
$in_periodovendafim = $_POST['periodovendafim'];

//Filtro apenas com o Colaborador
if($in_idcolaboradorvendedor != null && $in_referencia == "" && $in_periodovendainicio == "" && $in_periodovendafim == ""){
  $filtropesquisa = 'where cove.id = ' . " $in_idcolaboradorvendedor";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else if($in_idcolaboradorvendedor != null && $in_referencia != null && $in_periodovendainicio == "" && $in_periodovendafim == ""){
  // Filtro com Colaborador e Referência
  $filtropesquisa = 'where cove.id = ' . " $in_idcolaboradorvendedor" . 'and prod.referencia = ' . "'$in_referencia'";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else if($in_idcolaboradorvendedor != null && $in_referencia == null && $in_periodovendainicio != null && $in_periodovendafim != null){
  // Filtro com Colaborador e Data Período de Venda Início e Data Período de Venda Fim
  $filtropesquisa = 'where cove.id = ' . " $in_idcolaboradorvendedor" . ' and v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else if($in_idcolaboradorvendedor == null && $in_referencia != null && $in_periodovendainicio == null && $in_periodovendafim == null){
  //Pesquisa apenas por Referência
  $filtropesquisa = 'where prod.referencia = ' . "'$in_referencia'";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else if($in_idcolaboradorvendedor == null && $in_referencia != null && $in_periodovendainicio == null && $in_periodovendafim == null){
  //Pesquisa por referencia e data de venda inicio e data de venda fim
  $filtropesquisa = 'where prod.referencia = ' . '$in_referencia' . ' and v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";  
}else if($in_idcolaboradorvendedor == null && $in_referencia == null && $in_periodovendainicio != null && $in_periodovendafim != null){
  //Pesquisa apenas por data de venda inicio e data de venda fim  
  $filtropesquisa = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else if($in_idcolaboradorvendedor != null && $in_referencia != null && $in_periodovendainicio != null && $in_periodovendafim != null){
  //Pesquisa por todos os filtros
  $filtropesquisa = 'where cove.id = ' . " $in_idcolaboradorvendedor" 
                  . ' and prod.referencia = ' . " '$in_referencia' " 
                  . ' and v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
  
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = 'where v.datavenda BETWEEN ' . "'$in_periodovendainicio'" . ' and ' . "'$in_periodovendafim'";
}else{
  //Busca o primeiro registro cadastrado.
  
  //Pesquisa por nenhum filtro
  $filtropesquisa = null;
  //Filtro para valores finais do relatório de vendas
  $filtropesquisavaloresfinais = null;
}

if($in_periodovendainicio != null && $in_periodovendafim == null){
    echo "<script>window.location='filtrorelatoriovendas.php';"
        . "alert('Por favor informe o Período Venda Fim');</script>";
}else if($in_periodovendainicio == null && $in_periodovendafim != null){
  echo "<script>window.location='filtrorelatoriovendas.php';"
        . "alert('Por favor informe o Período Venda Início');</script>";
}

    $pdf = new FPDF("P", "pt", "A4");

    $pdf->AddPage('L');
    $pdf->SetFont('arial', 'B', 12);
    $pdf->Cell(0, 5, utf8_decode("Gerenciador - Relatório de Vendas"), 0, 1, 'C');
    $pdf->Ln(3);

    //Contorno Margens do relatório
    $pdf->Line(830, 10, 10, 10);
    $pdf->Line(10, 580, 10, 10);
    $pdf->Line(830, 580, 830, 10);
    $pdf->Line(830, 580, 10, 580);

    //CABEÇALHO DO RELATÓRIO
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(80);
    $pdf->Ln(20);

    //CABEÇALHO DA TABELA
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setFillColor(180, 180, 180);
    $pdf->Cell(60, 14, utf8_decode('Nº Transação'), 1, 0, 'C', 1);
    $pdf->Cell(120, 14, utf8_decode('Nome Vendedor'), 1, 0, 'L', 1);
    $pdf->Cell(147, 14, utf8_decode('Nome Cliente'), 1, 0, 'L', 1);
    $pdf->Cell(85, 14, 'Data/Hora', 1, 0, 'C', 1);
    $pdf->Cell(120, 14, utf8_decode('Descrição Peça'), 1, 0, 'L', 1);
    $pdf->Cell(25, 14, utf8_decode('TAM.'), 1, 0, 'C', 1);
    $pdf->Cell(35, 14, utf8_decode('REF.'), 1, 0, 'C', 1);
    $pdf->Cell(35, 14, utf8_decode('QTD/UN'), 1, 0, 'C', 1);
    $pdf->Cell(45, 14, utf8_decode('PREÇO/UN'), 1, 0, 'C', 1);
    $pdf->SetFont('Arial', 'B', 6);
    $pdf->Cell(35, 14, utf8_decode('% ITEM'), 1, 0, 'C', 1);
    $pdf->Cell(40, 14, utf8_decode('% VENDA'), 1, 0, 'C', 1);
    $pdf->Cell(45, 14, utf8_decode('TOTAL'), 1, 0, 'C', 1);
    $pdf->Ln();

    //DADOS DA TABELA
    $query = "select 
                     v.id,
                     cove.id as idcolaboradorvendedor,
                     cove.nome as nomecolaboradorvendedor,
                     pro.desconto_linha,
                     CASE WHEN clir.nome is null THEN cli.nome 
                       WHEN sobrenome is null THEN clir.nome 
                        ELSE (clir.nome || ' ' || clir.sobrenome)
                     END as nomecliente,
                     to_char(v.datavenda, 'dd/MM/yyyy') as datavenda,
                     v.horavenda as horavenda,
                     prod.nomeproduto as descricaopeca,
                     prod.cor as cor,
                     prod.numeroproduto as numeropeca,
                     'R$ ' || ltrim(to_char(prod.valor, '9G999G990D99')) as valor,
                     prod.referencia as referencia,
                     pro.quantidade as quantidadeunitaria,
                     (SELECT sum(valor_entrada) FROM venda) as valor_entrada,
                     (select valortotal from itensproduto where idvenda = v.id and idproduto = prod.id) as valortotal,
                     (select valoritemroupa*quantidade from itensproduto where idvenda = v.id and idproduto = prod.id) as valortotalpedido,
                     'R$ ' || ltrim(to_char(valortotal, '9G999G990D99')) as valortotalproduto,
                     'R$ ' || ltrim(to_char(((select valortotal from itensproduto where idvenda = v.id and idproduto = prod.id) - (select valortotal from itensproduto where idvenda = v.id and idproduto = prod.id) * 
                                COALESCE(( (SELECT porcentagemdescontovenda FROM descontovenda WHERE id = v.desconto) / 100 ),0.00)), '9G999G990D99')) as total_com_desconto_formatado,
                     ((select valortotal from itensproduto where idvenda = v.id and idproduto = prod.id) - (select valortotal from itensproduto where idvenda = v.id and idproduto = prod.id) * 
                                COALESCE(( (SELECT porcentagemdescontovenda FROM descontovenda WHERE id = v.desconto) / 100 ),0.00)) as total_com_desconto,
                     COALESCE((SELECT porcentagemdescontovenda FROM descontovenda WHERE id = v.desconto),0.00) as desconto
                from venda v
               inner join itensproduto pro
                  on pro.idvenda = v.id
                left join cliente cli
                  on v.idcliente = cli.id  
                left join clienterapido clir
                  on v.idcliente = clir.id
               inner join colaboradores co
                  on v.idcolaborador = co.id
               inner join colaboradores cove
                  on v.idcolaboradorvendedor = cove.id
               inner join produto prod
                  on prod.id = pro.idproduto
                 and v.tipopagamento is not null
               $filtropesquisa
               order by datavenda asc;";
    
    $result = pg_query($query);

    $valor_total_pedido = 0.00;
    $valor_total        = 0.00;
    $valor_entrada      = 0.00;
    $qtd_total          = 0.000;

    while ($consulta = pg_fetch_assoc($result)) {

        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(60, 10, utf8_decode($consulta['id']), 1, 0, 'C', 1);
        $pdf->Cell(120, 10, utf8_decode($consulta['nomecolaboradorvendedor']), 1, 0, 'L', 1);
        $pdf->Cell(147, 10, utf8_decode($consulta['nomecliente']), 1, 0, 'L', 1);
        $pdf->Cell(85, 10, utf8_decode($consulta['datavenda'].'  '.$consulta['horavenda']), 1, 0, 'C', 1);
        $pdf->Cell(120, 10, utf8_decode($consulta['descricaopeca']), 1, 0, 'L', 1);
        $pdf->Cell(25, 10, utf8_decode($consulta['numeropeca']), 1, 0, 'C', 1);
        $pdf->Cell(35, 10, utf8_decode($consulta['referencia']), 1, 0, 'C', 1);
        $pdf->Cell(35, 10, number_format(utf8_decode($consulta['quantidadeunitaria']),3), 1, 0, 'R', 1);
        $pdf->SetFont('Arial', '', 6);
        $pdf->Cell(45, 10, utf8_decode($consulta['valor']), 1, 0, 'R', 1);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(35, 10, utf8_decode($consulta['desconto_linha'])."%", 1, 0, 'R', 1);
        $pdf->Cell(40, 10, utf8_decode($consulta['desconto'])."%", 1, 0, 'R', 1);
        $pdf->SetFont('Arial', '', 6);
        $pdf->Cell(45, 10, $consulta['total_com_desconto_formatado'], 1, 0, 'R', 1);
        $pdf->Ln();

        $valor_total_pedido += $consulta['total_com_desconto'];
        $valor_total        += $consulta['valortotalpedido'];
        $qtd_total          += $consulta['quantidadeunitaria'];
        $valor_entrada       = $consulta['valor_entrada'];
    }
    
    //Soma Descontos Finais
    $sqldescontosfinais = "select sum(v.descontofinal) as descontofinalformatado
                             from venda v
                             $filtropesquisavaloresfinais;";
    $resultdescontosfinais = pg_query($sqldescontosfinais);
    while ($consultadescontosfinais = pg_fetch_assoc($resultdescontosfinais)) {
        $valordescontofinal = $consultadescontosfinais['descontofinalformatado'];
    }
    
    //CONSULTA VALOR BÔNUS TROCA
    $sqlbonustroca = "select ltrim(to_char(sum(valordiferenca), '9G999G990D99')) as valorbonustroca,
                             sum(valordiferenca) as valordiferencabonus
                        from trocaproduto 
                       where statustroca='B'";
    $resultbonustroca = pg_query($sqlbonustroca);
    while ($consultabonustroca = pg_fetch_assoc($resultbonustroca)) {
        $valorbonustroca = $consultabonustroca['valorbonustroca'];
        $bonustroca = $consultabonustroca['valordiferencabonus'];
    }
    //CONSULTA VALOR PAGO TROCA
    $sqlvalorpagotroca = "select trim(cast(sum(valordiferenca) as text), '-') as valorpagotroca,
                                 sum(valordiferenca) as valorpago
                            from trocaproduto 
                           where statustroca='D'";
    $resultvalorpagotroca = pg_query($sqlvalorpagotroca);
    while ($consultavalorpagotroca = pg_fetch_assoc($resultvalorpagotroca)) {
        $valorpagotroca = $consultavalorpagotroca['valorpagotroca'];
        $valorpago = $consultavalorpagotroca['valorpago'];
    }
    
    $pdf->Ln();
    
    //VALOR TOTAL SEM DESCONTO
    $valorfinalsemdesconto = ($valor_total_pedido);
    
    //VALOR TOTAL COM DESCONTO
    $valorfinalrelatorio = ($valor_total_pedido - $valordescontofinal);
   
    $pdf->setFillColor(200, 200, 200);
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Cell(792, 12, "TOTAIS/DESCONTOS: ", 1, 0, 'R', 1);
    $pdf->ln();
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode('QTDE-TOTAL:'), 1, 0, 'R');
    $pdf->Cell(95, 12, number_format($qtd_total), 1, 1, 'R'); 

    $pdf->ln(0);
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode('VALOR TOTAL SEM DESCONTO:'), 1, 0, 'R');
    $pdf->Cell(95, 12, 'R$ ' . number_format($valorfinalsemdesconto, 2, ',', '.'), 1, 1, 'R'); 
    
    $pdf->ln(0);
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode('VALOR-DESCONTO:'), 1, 0, 'R');
    $pdf->Cell(95, 12, 'R$ ' . $valordescontofinal, 1, 1, 'R');
    
    $pdf->ln(0);
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, utf8_decode('TOTAL-GERAL:'), 1, 0, 'R');
    $pdf->Cell(95, 12, 'R$ ' . number_format($valorfinalrelatorio, 2, ',', '.'), 1, 1, 'R');
    
    $pdf->ln(0);
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, utf8_decode(''), 1, 0, 'R');
    $pdf->Cell(95, 12, '' . '', 1, 1, 'R');
    
    $pdf->ln(0);
    if($bonustroca == ""){
        $bonustroca = "0.00";
    }else{
        $bonustroca = $bonustroca;
    }
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode('VALOR BÔNUS TROCA:'), 1, 0, 'R');
    $pdf->Cell(95, 12, 'R$ ' . $bonustroca, 1, 1, 'R');
    
    $pdf->ln(0);
    if($valorpagotroca == ""){
        $valorpagotroca = "0.00";
    }else{
        $valorpagotroca = $valorpagotroca;
    }
    
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'R');
    $pdf->Cell(137, 12, utf8_decode('VALOR PAGO TROCA:'), 1, 0, 'R');
    $pdf->Cell(95, 12, 'R$ ' . $valorpagotroca, 1, 1, 'R');

    $pdf->ln(0);
    
    $pdf->ln(0);
    $pdf->Cell(560, 12, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(137, 12, utf8_decode('TOTAL-GERAL TROCA:'), 1, 0, 'R');
    
    $valorfinaltroca = ($valorfinalrelatorio + $valorpagotroca);
    $pdf->Cell(95, 12, 'R$ ' . number_format($valorfinaltroca, 2, ',', '.'), 1, 1, 'R');

    // Rodapé
    $pdf->Line(552, 810, 452, 810);
    $pdf->SetXY(01, 530);
    $data = date("d/m/Y H:i:s");
    $conteudo = $data . utf8_decode(" Pág. ") . $pdf->PageNo();
    $pdf->Cell(790, 5, $conteudo, 10, 0, "R");

    $pdf->Output("rel_relatoriovendas.pdf", "D");

    pg_close($conexao);