<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Cliente Autorizado</div>
            <div class="panel-body">
                <a href="index.php?controle=clienteautorizadoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Cliente</th>
                    <th>Autorizado</th>
                    <th>CPF/RG</th>
                    <th>Telefone</th>
                    <th>Data Autorização</th>
                    <th>Vendedor</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td>' . $item['nomepessoaautorizada'];
                            echo '<td>' . $item['cpf'];
                            echo '<td>' . $item['telefone'];
                            echo '<td>' . $item['dataautorizada'];
                            echo '<td>' . $item['nomevendedor'];
                            $id = $item['id'];

                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=clienteautorizadoController&acao=buscar&id=$idencriptografa '>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"clienteautorizadoController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>