<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Autorização Cliente</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formAutorizacaocliente" id="formAutorizacaocliente" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($clienteautorizado)) echo $clienteautorizado['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <label for="idcliente">Cliente</label>
                            <select data-placeholder="Selecione um cliente" class="form-control selectpicker" tabindex="1" name="idcliente" id="idcliente" data-live-search="true">
                                <option value="">Selecione</option>
                                <?php
                                    foreach ($listaClientes as $clientes) {
                                        $selected = (isset($clienteautorizado) && $clienteautorizado['idcliente'] == $clientes['id']) ? 'selected' : '';
                                        ?>
                                        <option data-tokens="<?php echo $clientes['nome']; ?>" value='<?php echo $clientes['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $clientes['nome']; ?>
                                        </option>
                                    <?php }
                                ?>
                            </select>
                        </select>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <label for="nome">Nome Autorizado</label>
                        <input type="text" class="form-control" id="nome" name="nome" value="<?php if (isset($clienteautorizado)) echo $clienteautorizado['nome']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="cpf">CPF/RG</label>
                        <input type="text" class="form-control" id="cpf" name="cpf" maxlength="14" onkeypress="return Onlynumbers(event)" value="<?php if (isset($clienteautorizado)) echo $clienteautorizado['cpf']; ?>">
                    </div>
                    <div class="col-md-3">
                        <label for="telefone">Telefone</label>
                        <input type="text" class="form-control" id="telefone" name="telefone" value="<?php if (isset($clienteautorizado)) echo $clienteautorizado['telefone']; ?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formAutorizacaocliente").validate({
        rules: {
            idcliente: {
                required: true
            },
            nome: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            cpf: {
                required: true
            },
            telefone: {
                required: true
            }
        },
        messages: {
            idcliente: {
                required: "Por favor, selecione o Cliente"
            },
            nome: {
                required: "Por favor, informe o Nome do Autorizado",
                minlength: "O Nome do Cliente Autorizado deve ter pelo menos 3 caracteres",
                maxlength: "O Nome do Cliente Autorizado deve ter no máximo 50 caracteres"
            },
            cpf: {
                required: "Por favor, informe o CPF/RG do Cliente Autorizado"
            },
            telefone: {
                required: "Por favor, informe o Telefone do Cliente Autorizado"
            }
        }
    });
</script>