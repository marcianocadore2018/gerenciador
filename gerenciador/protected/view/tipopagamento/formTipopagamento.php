<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Forma de pagamento</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formFormaPgto" id="formFormaPgto" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($tipopagamento)) echo $tipopagamento['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição" 
                               value="<?php if (isset($tipopagamento)) echo $tipopagamento['descricao']; ?>" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <label for="tipo">Comprovar renda</label>
                        <br>
                        <input type="radio" name="tipo" value="R" 
                            <?php if(isset($tipopagamento)) if ($tipopagamento['tipo'] == 'R'){echo 'checked';}else{$tipopagamento == null;} ?>> Sim
                        <input type="radio" name="tipo" value="D" 
                            <?php if(isset($tipopagamento)) if ($tipopagamento['tipo'] == 'D'){echo 'checked';}else{$tipopagamento == null;} ?>> Não
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <label for="tipo">Parcelar total da venda</label>
                        <br>
                        <input type="radio" name="parcelar" value="S" 
                            <?php if(isset($tipopagamento)) if ($tipopagamento['parcelar'] == 'S'){echo 'checked';}else{$tipopagamento == null;} ?>> Sim
                        <input type="radio" name="parcelar" value="N"
                            <?php if(isset($tipopagamento)) if ($tipopagamento['parcelar'] == 'N'){echo 'checked';}else{$tipopagamento == null;} ?>> Não
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="tipo">Data de vencimento</label>
                        <br>
                        <input type="radio" name="data_vencimento" value="S" 
                            <?php if(isset($tipopagamento)) if ($tipopagamento['data_vencimento'] == 'S'){echo 'checked';}else{$tipopagamento == null;} ?>> Sim
                        <input type="radio" name="data_vencimento" value="N"
                            <?php if(isset($tipopagamento)) if ($tipopagamento['data_vencimento'] == 'N'){echo 'checked';}else{$tipopagamento == null;} ?>> Não
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formTipopagamento").validate({
        rules: {
            descricao: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a descrição da forma de pagamento",
                maxlength: "A descrição da forma de pagamento deve ter no máximo 50 caracteres"
            }
        }
    });
</script>