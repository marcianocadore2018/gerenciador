<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de forma de pagamentos</div>
            <div class="panel-body">
                <a href="index.php?controle=tipopagamentoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Descrição</th>
                    <th>Comprova/Renda</th>
                    <th>Parcelar/Total</th>
                    <th>Data/Vencimento</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['descricao'];
                            if($item['tipo'] == 'D'){
                                echo '<td>' . 'Não';
                            }else{
                                echo '<td>' . 'Sim';
                            }

                            if($item['parcelar'] == 'S'){
                                echo '<td>' . 'Sim';
                            }else{
                                echo '<td>' . 'Não';
                            }

                            if($item['data_vencimento'] == 'S'){
                                echo '<td>' . 'Sim';
                            }else{
                                echo '<td>' . 'Não';
                            }
                            
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=tipopagamentoController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"tipopagamentoController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>