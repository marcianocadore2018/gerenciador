<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Restaurar acesso colaborador</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formManutencaocolaborador" id="formManutencaocolaborador" method="POST" class="form" role="form">
                <?php
                    $idget = $_GET['id'];
                    $decodeget = base64_decode($idget);
                    $remover = str_replace("passar", "", $decodeget);
                    $id = str_replace("metodoget", "", $remover);
                ?>
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php echo $id; ?>">
                <div class="row">
                    <div class="col-md-6">
                        <label for="login">Login</label>
                        <input type="text" class="form-control" id="login" name="login" placeholder="Digite o Login" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="senha">Senha</label>
                        <input type="password" class="form-control" id="senha" name="senha" placeholder="Digite a Senha" required maxlength="20">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formManutencaocolaborador").validate({
        rules: {
            login: {
                required: true,
                maxlength: 20
            },
            senha: {
                required: true,
                minlength: 8,
                maxlength: 20
            }
        },
        messages: {
            login: {
                required: "Por favor, informe o Login",
                maxlength: "O Login deve ter no máximo 20 caracteres"
            },
            senha: {
                required: "Por favor, informe a Senha",
                minlength: "A Senha deve ter no mínimo 8 caracteres",
                maxlength: "A Senha deve ter no máximo 20 caracteres"
            }
        }
    });
</script>
