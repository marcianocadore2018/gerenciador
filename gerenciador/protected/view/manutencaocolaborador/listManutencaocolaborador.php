<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Cargo</div>
            <div class="panel-body">
                <a href="index.php?controle=cargoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Nome</th>
                    <th>Tipo Colaborador</th>
                    <th>Qtd. Acesso</th>
                    <th style="text-align: center">Restaurar Acesso</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="padding-left: 15px;">' . $item['nome'];
                            echo '<td style="padding-left: 15px;">' . $item['tipocolaborador'];
                            echo '<td style="padding-right: 0px; padding-left: 50px;">' . $item['quantidadeacesso'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <center><a href='index.php?controle=manutencaocolaboradorController&acao=restaurar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-refresh' title='Clique aqui para restaurar o acesso'> </span>"
                            . "</a></center> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>