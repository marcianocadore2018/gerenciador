<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Gerenciamento Financeiro Saída</div>
            <div class="panel-body">
                <a href="index.php?controle=gerenciamentofinanceirosaidaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>#</th>
                    <th>Vendedor</th>
                    <th>Descrição</th>
                    <th>Valor Pagamento</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            
                            echo '<td>' . $item['id'];
                            echo '<td>' . $item['nomevendedor'];
                            echo '<td>' . $item['descricao'];
                            echo '<td style="padding-left: 30px">' . $item['valorpagamento'];
                            echo '<td style="padding-left: 30px">' . $item['datacadastro'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <a href='index.php?controle=gerenciamentofinanceirosaidaController&acao=buscar&id=$idencriptografa '>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"gerenciamentofinanceirosaidaController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>