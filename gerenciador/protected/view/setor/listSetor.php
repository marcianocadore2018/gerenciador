<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Setores</div>
            <div class="panel-body">
                <?php
                    //consultar usuario, se for tipo Master pode alterar
                    $tipousuariocolaborador = $_SESSION['login'];
                    $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                    $rscolaborador = pg_fetch_array ($colaboradormaster);
                    $colaboradortipo = $rscolaborador['tipocolaborador'];
                    if($colaboradortipo == 'M' || $colaboradortipo == 'D'){?>
                        <a href="index.php?controle=setorController&acao=novo">
                            <span class='glyphicon glyphicon-plus'> Adicionar</span>
                        </a>
                   <?php }
                ?>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Descrição</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['descricao'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datacadastro'];
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            //consultar usuario, se for tipo Master pode alterar
                            $tipousuariocolaborador = $_SESSION['login'];
                            $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                            $rscolaborador = pg_fetch_array ($colaboradormaster);
                            $colaboradortipo = $rscolaborador['tipocolaborador'];
                            
                            if($colaboradortipo == 'M' || $colaboradortipo == 'D'){
                            echo "<td> <a href='index.php?controle=setorController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"setorController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            }
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>