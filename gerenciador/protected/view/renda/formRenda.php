<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Renda</div>
            <div class="panel-body">
               <form action="<?php echo $acao; ?>" name="formRenda" id="formRenda" method="POST" class="form" role="form">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($renda)) echo $renda['id']; ?>">
                        </div>
                    </div>
                   <div class="row">
                        <div class="col-md-8">
                            <label for="idcliente">Cliente</label>
                                <select class="form-control" name="idcliente" id="idcliente" required data-errormessage-value-missing="Selecione o Cliente">
                                <option value="">Selecione o Cliente</option>>
                                <?php
                                foreach ($listaClientes as $clientes) {
                                    $selected = (isset($renda) && $renda['idcliente'] == $clientes['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $clientes['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $clientes['nome']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                    <div class="row">
                        <div class="col-md-8">
                            <label for="descricaorenda">Descrição Renda</label>
                            <input type="text" class="form-control" id="descricaorenda" name="descricaorenda" placeholder="Digite a Descrição da Renda" 
                                   value="<?php if (isset($renda)) echo $renda['descricaorenda']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="valorrenda">Valor Renda</label>
                            <input type="text" class="form-control" id="valor" name="valorrenda" placeholder="Digite o Valor" 
                                   value="<?php if (isset($renda)) echo $renda['valorrenda']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="datainiciorenda">Data Início Renda</label>
                            <input type="text" class="form-control" id="data" name="datainiciorenda" placeholder="Digite a Data" 
                                   value="<?php if (isset($renda)) echo $renda['datainiciorenda']; ?>" required>
                        </div>
                    </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>       
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formRenda").validate({
        rules: {
            idcliente: {
                required: true
            },
            descricaorenda: {
                required: true,
                minlength: 10,
                maxlength: 100
            },
            valorrenda: {
                required: true
            },
            datainiciorenda: {
                required: true
            }
        },
        messages: {
            idcliente: {
                required: "Por favor, Selecione o Cliente"
            },
            descricaorenda: {
                required: "Por favor, informe a Descrição da Renda",
                minlength: "A Descrição da Renda deve ter pelo menos 10 caracteres",
                maxlength: "A Descrição da Renda deve ter no máximo 100 caracteres"
            },
            valorrenda: {
                required: "Por favor, informe o Valor da Renda do Cliente"
            },
            datainiciorenda: {
                required: "Por favor, informe a Data de Início da Renda"
            }
        }
    });
</script>
