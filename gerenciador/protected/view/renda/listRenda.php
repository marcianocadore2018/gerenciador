<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Rendas</div>
            <div class="panel-body">
                <a href="index.php?controle=rendaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" style="width: 1200px" id="example1">
                    <thead>
                    <th style="width: 200px">Cliente</th>
                    <th style="width: 200px">Descrição Renda</th>
                    <th>Valor Renda</th>
                    <th>Data Início Renda</th>
                    <th>Data Cadastro</th>
                    <th style="width: 200px">Loja</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td>' . $item['descricaorenda'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['valorrenda'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['datainiciorenda'];
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['datacadastrorenda'];
                            echo '<td>' . $item['nomefantasia'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <a href='index.php?controle=rendaController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"rendaController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>