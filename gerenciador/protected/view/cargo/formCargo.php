<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Cargo</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formCargo" id="formCargo" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($cargo)) echo $cargo['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição" 
                               value="<?php if (isset($cargo)) echo $cargo['descricao']; ?>" required minlength="3">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formCargo").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 3,
                maxlength: 100
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição do Cargo",
                minlength: "A Descrição do Cargo deve ter pelo menos 3 caracteres",
                maxlength: "A Descrição do Cargo deve ter no máximo 80 caracteres"
            }
        }
    });
</script>