<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Cargo</div>
            <div class="panel-body">
                <a href="index.php?controle=cargoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Descrição</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['descricao'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datacadastro'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <a href='index.php?controle=cargoController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"cargoController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>