<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Loja</div>
        <div class="panel-body">
           <form action="<?php echo $acao; ?>" name="formLoja" id="formLoja" method="POST" class="form" role="form">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($loja)) echo $loja['id']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="nomefantasia">Nome Fantasia</label>
                            <input type="text" class="form-control" id="nomefantasia" name="nomefantasia" placeholder="Digite o Nome Fantasia" 
                                   value="<?php if (isset($loja)) echo $loja['nomefantasia']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="razaosocial">Razão Social</label>
                            <input type="text" class="form-control" id="razaosocial" name="razaosocial" placeholder="Digite a Razão Social" 
                                   value="<?php if (isset($loja)) echo $loja['razaosocial']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cnpj">CNPJ</label>
                            <input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="Digite o CNPJ" 
                                   value="<?php if (isset($loja)) echo $loja['cnpj']; ?>" required minlength="18" maxlength="18" onkeypress="return ValidarCNPJ(event)" 
                                   onBlur="ValidarCNPJ(formLoja.cnpj);">
                        </div>
                        <div class="col-md-3">
                            <label for="dataabertura">Data Abertura</label>
                            <input type="text" class="form-control" id="data" name="dataabertura" placeholder="Digite a Data" 
                                   value="<?php if (isset($loja)) echo $loja['dataabertura']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="situacaoloja">Situação Loja</label>
                            <input type="radio" name="situacaoloja" value="A" checked="" <?php if(isset($loja)) if ($loja['situacaoloja'] == 'A'){echo 'checked';}else{$loja == null;} ?>> Ativa
                            <input type="radio" name="situacaoloja" value="I" <?php if(isset($loja)) if ($loja['situacaoloja'] == 'I'){echo 'checked';}else{$loja == null;}?>> Inativa
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="endereco">Endereço</label>
                            <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço" 
                                   value="<?php if (isset($loja)) echo $loja['endereco']; ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label for="bairro">Bairro</label>
                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o Bairro" 
                                   value="<?php if (isset($loja)) echo $loja['bairro']; ?>" required>
                        </div>
                        <div class="col-md-2">
                            <label for="numero">Número</label>
                            <input type="text" class="form-control" id="numero" name="numero" placeholder="Digite o Número" 
                                   value="<?php if (isset($loja)) echo $loja['numero']; ?>" maxlength="4" required onkeypress="return Onlynumbers(event)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cidade">Cidade</label>
                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Digite a Cidade" 
                                   value="<?php if (isset($loja)) echo $loja['cidade']; ?>" required>
                        </div>
                        <div class="col-md-2">
                            <label for="idestado">Estado</label>
                            <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                                <option value="">Selecione o Estado</option>>
                                <?php
                                foreach ($listaEstados as $estados) {
                                    $selected = (isset($loja) && $loja['idestado'] == $estados['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $estados['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $estados['uf']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="cep">CEP</label>
                            <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o CEP" 
                                   value="<?php if (isset($loja)) echo $loja['cep']; ?>" required>
                        </div>
                        <div style="padding-top: 30px; font-size: 12px; color: blue">
                            <a href="http://m.correios.com.br/movel/buscaCep.do" target="_blank" title="Clique aqui para pesquisar o CEP"><b>Verificar CEP Correios</b></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="complemento">Complemento</label>
                            <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Digite o Complemento" 
                                   value="<?php if (isset($loja)) echo $loja['complemento']; ?>" required>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-8">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite o E-mail" 
                                   value="<?php if (isset($loja)) echo $loja['email']; ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="telefone">Telefone</label>
                            <input type="text" class="form-control" attrname="telephone1" name="telefone" placeholder="Digite o Telefone" 
                                   value="<?php if (isset($loja)) echo $loja['telefone']; ?>" required>
                        </div>
                    </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    jQuery.extend(jQuery.validator.messages, {
        email:"Por favor informe um email válido."
    });
    $("#formLoja").validate({
        rules: {
            nomefantasia: {
                required: true,
                maxlength: 100
            },
            razaosocial: {
                required: true,
                maxlength: 100
            },
            cnpj: {
                required: true,
                maxlength: 18
            },
            dataabertura: {
                required: true
            },
            situacaoloja: {
                required: true
            },
            endereco: {
                required: true,
                maxlength: 100
            },
            bairro: {
                required: true,
                maxlength: 80
            },
            idestado: {
                required: true
            },
            cidade: {
                required: true,
                maxlength: 100
            },
            cep: {
                required: true,
                maxlength: 9
            },
            telefone: {
                required: true,
                maxlength: 90
            },
            email: {
                required: true,
                maxlength: 100
            },
            numero: {
                required: true
            },
            complemento: {
                maxlength: 80
            }
        },
        messages: {
            nomefantasia: {
                required: "Por favor, informe o Nome Fantasia",
                minlength: "O Nome Fantasia deve ter pelo menos 3 caracteres",
                maxlength: "O Nome Fantasia deve ter no máximo 100 caracteres"
            },
            razaosocial: {
                required: "Por favor, informe a Razão Social",
                minlength: "A Razão Social deve ter pelo menos 3 caracteres",
                maxlength: "A Razão Social deve ter no máximo 100 caracteres"
            },
            cnpj: {
                required: "Por favor, informe o CNPJ",
                minlength: "O CNPJ deve ter pelo menos 18 caracteres",
                maxlength: "O CNPJ deve ter no máximo 18 caracteres"
            },
            dataabertura: {
                required: "Por favor, informe a Data de Abertura"
            },
            situacaoloja: {
                required: "Por favor, informe a Situação da Loja Ativa ou Inativa"
            },
            endereco: {
                required: "Por favor, informe o Endereço",
                maxlength: "O Endereço deve ter no máximo 100 caracteres"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                maxlength: "O Bairro deve ter no máximo 80 caracteres"
            },
            idestado: {
                required: "Por favor, Selecione o Estado"
            },
            cidade: {
                required: "Por favor, informe o Nome da Cidade",
                maxlength: "A Cidade deve ter no máximo 100 caracteres"
            },
            cep: {
                required: "Por favor, informe o CEP",
                minlength: "O CEP deve ter pelo menos 9 caracteres",
                maxlength: "O CEP deve ter no máximo 9 caracteres"
            },
            telefone: {
                required: "Por favor, informe o Telefone",
                maxlength: "O Telefone deve ter no máximo 20 caracteres"
            },
            email: {
                required: "Por favor, informe o E-mail",
                maxlength: "O E-mail deve ter no máximo 100 caracteres"
            },
            numero: {
                required: "Por favor, informe o Número"
            },
            complemento: {
                maxlength: "O Complemento deve ter no máximo 80 caracteres"
            }
        }
    });
</script>
