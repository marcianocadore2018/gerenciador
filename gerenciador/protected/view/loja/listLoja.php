<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Lojas</div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 2500px;">
                    <thead>
                    <th>Nome Fantasia</th>
                    <th>Razão Social</th>
                    <th>CNPJ</th>
                    <th>Data Abertura</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th>Data Cadastro</th>
                    <th>Situação Loja</th>
                    <th>Endereço</th>
                    <th>Cidade</th>
                    <th>Bairro</th>
                    <th>UF</th>
                    <th>CEP</th>
                    <th>Número</th>
                    <th>Complemento</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomefantasia'];
                            echo '<td>' . $item['razaosocial'];
                            echo '<td>' . $item['cnpj'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['dataabertura'];
                            echo '<td>' . $item['telefone'];
                            echo '<td>' . $item['email'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datacadastro'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['situacaoloja'];
                            echo '<td>' . $item['endereco'];
                            echo '<td>' . $item['cidade'];
                            echo '<td>' . $item['bairro'];
                            echo '<td>' . $item['uf'];
                            echo '<td>' . $item['cep'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['numero'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['complemento'];
                            $id = $item['id'];
                            
                            //consultar usuario, se for tipo Master pode alterar
                            $tipousuariocolaborador = $_SESSION['login'];
                            $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                            $rscolaborador = pg_fetch_array ($colaboradormaster);
                            $colaboradortipo = $rscolaborador['tipocolaborador'];
                            if($colaboradortipo == 'M'){
                                echo "<td> <a href='index.php?controle=lojaController&acao=buscar&id=$id'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                            }
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>