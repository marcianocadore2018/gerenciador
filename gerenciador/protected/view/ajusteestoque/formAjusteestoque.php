<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Ajuste de Produto - Estoque</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formAjusteproduto" id="formAjusteproduto" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="idproduto">Produto</label>
                            <select data-placeholder="Selecione um produto" class="form-control chosen-select" tabindex="1" name="idproduto" id="idproduto">
                                <option value="">Selecione</option>
                                <?php
                                    foreach ($listaAjusteProdutos as $produtos) {
                                        $selected = (isset($ajusteestoque) && $ajusteestoque['idproduto'] == $produtos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $produtos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $produtos['referencia'] . '&nbsp&nbsp|&nbsp&nbsp' . 
                                                               $produtos['nomeproduto'] . '&nbsp&nbsp|&nbsp&nbsp' . 
                                                               'Nº: ' . $produtos['numeroproduto'] . '&nbsp&nbsp|&nbsp&nbsp' .
                                                               'Qtd: ' . $produtos['quantidade']
                                                    ?>
                                        </option>
                                    <?php }
                                ?>
                            </select>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="quantidade">Quantidade</label>
                        <input type="text" class="form-control" id="quantidade" name="quantidade">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="condicao">Condição</label>
                        <input type="radio" name="condicao" value="aumentar" checked="true"> Aumentar &nbsp
                        <input type="radio" name="condicao" value="diminuir"> Diminuir &nbsp
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formAjusteproduto").validate({
        rules: {
            idproduto: {
                required: true
            },
            quantidade: {
                required: true
            }
        },
        messages: {
            idproduto: {
                required: "Por favor, selecione um produto"
            },
            quantidade: {
                required: "Por favor, informe a quantidade"
            }
        }
    });
</script>