<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Ajuste de Produto - Estoque</div>
            <div class="panel-body">
                <a href="index.php?controle=ajusteestoqueController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Descrição</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomeproduto'];
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>