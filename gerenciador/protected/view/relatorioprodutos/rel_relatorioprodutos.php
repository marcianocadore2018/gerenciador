<?php
include '../../libs/verificaambiente.php';
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");


$in_idgrupo = $_POST['idgrupo'];
$in_referenciainicio = $_POST['referenciainicio'];
$in_referenciafim = $_POST['referenciafim'];
$in_periodocadastroinicio = $_POST['periodocadastroinicio'];
$in_periodocadastrofim = $_POST['periodocadastrofim'];

//Filtro apenas com o Grupo
if($in_idgrupo != "" && $in_referenciainicio == "" && $in_referenciafim == "" && $in_periodocadastroinicio == "" && $in_periodocadastrofim == ""){
  $filtropesquisa = 'where gru.id = ' . " $in_idgrupo";
}else if($in_idgrupo != "" && $in_referenciainicio != "" && $in_referenciafim != "" && $in_periodocadastroinicio == "" && $in_periodocadastrofim == ""){
  // Filtro com Grupo e Referência Início e Fim
  $filtropesquisa = 'where gru.id = ' . " $in_idgrupo" . ' and pro.referencia >= ' . "'$in_referenciainicio'" . ' and pro.referencia <= ' . "'$in_referenciafim'";
}else if($in_idgrupo == "" && $in_referenciainicio != "" && $in_referenciafim != "" && $in_periodocadastroinicio == "" && $in_periodocadastrofim == ""){
  // Filtro com Referência Produto Início e Referência Produto Fim
  $filtropesquisa = 'where pro.referencia >= ' . "'$in_referenciainicio'" . ' and pro.referencia <= ' . "'$in_referenciafim'";
}else if($in_idgrupo == "" && $in_referenciainicio == "" && $in_referenciafim == "" && $in_periodocadastroinicio != "" && $in_periodocadastrofim != ""){
  //Pesquisa Data Período Cadastro Início e Período Data Cadastro Fim
  $filtropesquisa = 'where pro.datacadastro >= ' . "'$in_periodocadastroinicio'" . ' and pro.datacadastro <= ' . "'$in_periodocadastrofim'";
}else if($in_idgrupo != null && $in_referenciainicio == "" && $in_referenciafim == "" && $in_periodocadastroinicio != "" && $in_periodocadastrofim != ""){
  //Pesquisa por Grupo Período Cadastro Início e Período Cadastro Fim
  $filtropesquisa = 'where pro.datacadastro >= ' . "'$in_periodocadastroinicio'" . ' and pro.datacadastro <= ' . "'$in_periodocadastrofim' and gru.id = $in_idgrupo";
}else if($in_idgrupo == null && $in_referenciainicio != "" && $in_referenciafim != "" && $in_periodocadastroinicio != "" && $in_periodocadastrofim != ""){
    //filtro com referência produto inicio e fim e periodo cadastra inicio e fim
    $filtropesquisa = 'where pro.referencia >= ' . "'$in_referenciainicio'" . ' and pro.referencia <= ' . "'$in_referenciafim'" . " and pro.datacadastro >= " . "'$in_periodocadastroinicio'" . " and pro.datacadastro <= " . "'$in_periodocadastrofim'";
}else if($in_idgrupo != null && $in_referenciainicio != "" && $in_referenciafim != "" && $in_periodocadastroinicio != "" && $in_periodocadastrofim != ""){
    //Filtrar por todos os parâmetros
    $filtropesquisa = 'where pro.idgrupo = ' . $in_idgrupo . ' and pro.referencia >= ' . "'$in_referenciainicio'" . ' and pro.referencia <= ' . "'$in_referenciafim'" . " and pro.datacadastro >= " . "'$in_periodocadastroinicio'" . " and pro.datacadastro <= " . "'$in_periodocadastrofim'";
}
else{
  //Pesquisa por nenhum filtro
  $filtropesquisa = null;
}
if($in_periodocadastroinicio != null && $in_periodocadastrofim == null){
    echo "<script>window.location='filtrorelatorioprodutos.php';"
        . "alert('Por favor informe a <= Data Cadastro Final');</script>";
}else if($in_periodocadastroinicio == null && $in_periodocadastrofim != null){
  echo "<script>window.location='filtrorelatorioprodutos.php';"
        . "alert('Por favor informe a Data Cadastro Inicial >=');</script>";
}else if($in_referenciainicio != null && $in_referenciafim == null){
    echo "<script>window.location='filtrorelatorioprodutos.php';"
        . "alert('Por favor informe a <= Referência Produto Final');</script>";
}else if($in_referenciainicio == null && $in_referenciainicio != null){
    echo "<script>window.location='filtrorelatorioprodutos.php';"
        . "alert('Por favor informe a Referência Produto Inicial >=');</script>";
}

if($in_referenciainicio > $in_referenciafim){
    echo "<script>window.location='filtrorelatorioprodutos.php';"
        . "alert('Referência Produto Inícial >= nao pode ser maior que <= Referência Produto Final');</script>";
}else{
    $pdf = new FPDF("P", "pt", "A4");

    $pdf->AddPage('L');
    $pdf->SetFont('arial', 'B', 12);
    $pdf->Cell(0, 5, utf8_decode("Gerenciador - Relatório de Produtos"), 0, 1, 'C');
    $pdf->Ln(3);

    //Contorno Margens do relatório
    $pdf->Line(830, 10, 10, 10);
    $pdf->Line(10, 580, 10, 10);
    $pdf->Line(830, 580, 830, 10);
    $pdf->Line(830, 580, 10, 580);

    //CABEÇALHO DO RELATÓRIO
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(80);
    $pdf->Ln(20);
    
    //CABEÇALHO DA TABELA
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->setFillColor(180, 180, 180);
    $pdf->Cell(215, 14, utf8_decode('Nome Produto'), 1, 0, 'L', 1);
    $pdf->Cell(60, 14,  utf8_decode('Referência'), 1, 0, 'C', 1);
    $pdf->Cell(90, 14, 'Grupo', 1, 0, 'L', 1);
    $pdf->Cell(40, 14,  utf8_decode('Número'), 1, 0, 'C', 1);
    $pdf->Cell(75, 14,  utf8_decode('Cor'), 1, 0, 'L', 1);
    $pdf->Cell(60, 14, utf8_decode('Gênero'), 1, 0, 'L', 1);
    $pdf->Cell(60, 14, utf8_decode('Valor'), 1, 0, 'C', 1);
    $pdf->Cell(60, 14, utf8_decode('Quantidade'), 1, 0, 'C', 1);
    $pdf->Cell(65, 14, utf8_decode('Qtd. Disponível'), 1, 0, 'C', 1);
    $pdf->Cell(65, 14, utf8_decode('Qtd. Vendida'), 1, 0, 'C', 1);
    $pdf->Ln();

    //DADOS DA TABELA
    $pdf->SetFont('Arial', '', 8);
    $query = "select upper(pro.nomeproduto) as nomeproduto,
                     upper(pro.referencia) as referenciaproduto,
                     to_char(pro.datacadastro, 'dd/MM/yyyy') as datacadastro,
	             upper(gru.descricao) as nomegrupo,
	             pro.numeroproduto as numeroproduto,
	             upper(pro.cor) as corproduto,
	             CASE WHEN pro.sexo='M' THEN 'Masculino'
	                  WHEN pro.sexo='F' THEN 'Feminino'
		     END as generoproduto,
	             pro.quantidade as quantidadeproduto,
	             pro.valor as valorproduto,
                     pro.quantidade as quantidadeproduto,
	             pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel,
                     pro.quantidade - (pro.quantidade - sum(itensprod.quantidade)) as quantidadevendida
                from produto pro
               inner join grupo gru
                  on pro.idgrupo = gru.id
                left join itensproduto itensprod
                  on itensprod.idproduto = pro.id
                     $filtropesquisa
               group by itensprod.idproduto, 
                        pro.id,
                        gru.descricao
               order by pro.nomeproduto";
    $result = pg_query($query);
    
    while ($consulta = pg_fetch_assoc($result)) {
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(215, 12, utf8_decode($consulta['nomeproduto']), 1, 0, 'L', 1);
        $pdf->Cell(60, 12, utf8_decode($consulta['referenciaproduto']), 1, 0, 'C', 1);
        $pdf->Cell(90, 12, utf8_decode($consulta['nomegrupo']), 1, 0, 'L', 1);
        $pdf->Cell(40, 12, $consulta['numeroproduto'], 1, 0, 'C', 1);
        $pdf->Cell(75, 12, utf8_decode($consulta['corproduto']), 1, 0, 'L', 1);
        $pdf->Cell(60, 12, utf8_decode($consulta['generoproduto']), 1, 0, 'L', 1);
        $pdf->Cell(60, 12, utf8_decode("R$ " . $consulta['valorproduto']), 1, 0, 'L', 1);
        $pdf->Cell(60, 12, utf8_decode($consulta['quantidadeproduto']), 1, 0, 'C', 1);
        if($consulta['quantidadedisponivel'] != null){
            $pdf->Cell(65, 12, utf8_decode($consulta['quantidadedisponivel']), 1, 0, 'C', 1);
        }else{
            $pdf->Cell(65, 12, utf8_decode($consulta['quantidadeproduto']), 1, 0, 'C', 1);
        }
        $pdf->Cell(65, 12, utf8_decode($consulta['quantidadevendida']), 1, 0, 'C', 1);
        $pdf->Ln();
    }
    $pdf->Ln();
    
    //Cálculos Totais Quantidade Total, Valor Total
    /*$queryquantval = "select  'Quantidade Total: ' || sum(pro.quantidade) as quantidadetotal,
                              'Valor Total: R$ ' || sum(prod.valor * pro.quantidade) as valorsomatotal
                        from venda v
                       inner join itensproduto pro
                          on pro.idvenda = v.id
                       inner join cliente cli
                          on v.idcliente = cli.id  
                       inner join colaboradores co
                          on v.idcolaborador = co.id
                       inner join colaboradores cove
                          on v.idcolaboradorvendedor = cove.id
                       inner join produto prod
                          on prod.id = pro.idproduto
                         and v.tipopagamento is not null
                             $filtropesquisa";
    
    $resultquantval = pg_query($queryquantval);
    while ($consultaquantval = pg_fetch_assoc($resultquantval)) {
        $pdf->setFillColor(200, 200, 200);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(582, 12, "Valores Totais: ", 1, 0, 'R', 1);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(115, 12, $consultaquantval['quantidadetotal'], 1, 0, 'L', 1);
        $pdf->Cell(95, 12, $consultaquantval['valorsomatotal'], 1, 0, 'L', 1);
    }*/

    // Rodapé
    $pdf->Line(552, 810, 452, 810);
    $pdf->SetXY(01, 530);
    $data = date("d/m/Y H:i:s");
    $conteudo = $data . utf8_decode(" Pág. ") . $pdf->PageNo();
    $pdf->Cell(790, 5, $conteudo, 10, 0, "R");

    $pdf->Output("rel_relatorioprodutos.pdf", "D");

    pg_close($conexao);
}