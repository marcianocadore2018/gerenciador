<?php
    require_once("../../../config/confloginrel.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gerenciador de Projetos</title>
        <link href="../../../includes/css/bootstrap.css" rel="stylesheet">
        <link rel="shortcut icon" href="http://atlantajeans.com.br/gerenciador/includes/imagens/favicon.png" type="image/x-icon"/>
        <link rel="stylesheet" href="../../../includes/css/datatables.min.css">
        <link rel="stylesheet" href="../../../includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="../../../includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../../../includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        
        <!-- Excluir Registro - Mensagem-->
        <script src="../../../includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="../../../includes/css/jquery-ui.css" type="text/css" />
        <!-- JQuery formata Valores -->
        <script>
            jQuery(function ($) {
                $("#periodocadastroinicio").mask("99/99/9999");
                $("#periodocadastrofim").mask("99/99/9999");
                $("#periodocadastroinicio").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
                $("#periodocadastrofim").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
            });

        </script>
        <style>
            #menutitle{
                color: white;
                padding-left: 25px;
            }
            .ui-datepicker .ui-datepicker-header {
                position: relative;
                padding: .2em 0;
                background-color: #A44160;
            }
            div.ui-datepicker{
             font-size:12px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #A44160">
            <div class="container" style="padding-left: 0px">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Filtro Relatório de Produtos</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 120%;"></div>
            </div>
        </nav>
        <br/><br/><br/>
        <div class="container">
        <form name="filtrorelatorioprodutos" id="filtrorelatorioprodutos" action="rel_relatorioprodutos.php" method="post">
            <div class="form-group">
                <label>Grupo</label>
                <select name="idgrupo" id="idgrupo" class="form-control">
                    <?php
                    $sqlGrupo = "select g.id as idgrupo, 
                                        g.descricao 
                                   from grupo g 
                                  where id in (select p.idgrupo as idgrupo
                                                 from produto p)
                                 order by g.descricao asc;";
                    $sqlGrupoResult = pg_query($sqlGrupo);
                    echo '<option value="">
                            Selecione...
                        </option>';
                    while ($sqlGrupoResultFim = pg_fetch_assoc($sqlGrupoResult)) {
                        ?>
                        <option value="<?php echo $sqlGrupoResultFim["idgrupo"] ?>">
                            <?php echo $sqlGrupoResultFim["descricao"] ?>
                        </option>

                        <?php
                    }
                    ?>
                </select>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-5">
                    <label for="referenciainicio">Referência Produto Inícial >= </label>
                    <input class="form-control" id="referenciainicio" name="referenciainicio"/>
                </div>
                <div class="col-xs-5">
                    <label for="referenciafim"><= Referência Produto Final</label>
                    <input class="form-control" id="referenciafim" name="referenciafim"/>
                </div>
            </div>            
            <br/>
            <div class="row">
                <div class="col-xs-5">
                    <label for="periodocadastroinicio">Data Cadastro Inicial >= </label>
                    <input class="form-control" id="periodocadastroinicio" name="periodocadastroinicio"/>
                </div>
                <div class="col-xs-5">
                    <label for="periodocadastrofim"><= Data Cadastro Final</label>
                    <input class="form-control" id="periodocadastrofim" name="periodocadastrofim"/>
                </div>
            </div>
            <br/>
            <button type="submit" class="btn btn-success">Gerar Relatório</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
        </form>
    </div>
    </body>
</html>