<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Foto do Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formFotoProduto" id="formFotoProduto" method="POST" class="form" role="form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($fotoproduto)) echo $fotoproduto['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="idproduto">Produto</label>
                        <select class="form-control selectpicker" tabindex="1" name="idproduto" id="idproduto" required data-errormessage-value-missing="Selecione o Produto" data-live-search="true">
                            <option value="">Selecione o Produto</option>>
                            <?php
                            foreach ($listaProdutos as $produtos) {
                                $selected = (isset($fotoproduto) && $fotoproduto['idproduto'] == $produtos['id']) ? 'selected' : '';
                                ?>
                                <option data-tokens="<?php echo $produtos['nomeproduto']; ?>" value='<?php echo $produtos['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php 
                                                if($produtos['tamanho'] != ''){
                                                    echo $produtos['nomeproduto'] . ' - ' . $produtos['tamanho'];
                                                }else if($produtos['numeroproduto'] != ''){
                                                    echo $produtos['nomeproduto'] . ' - ' . $produtos['numeroproduto'];
                                                }
                                             ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3">
                        <label for="principal">Foto Principal</label>
                        <br><input type="radio" name="principal" value="S" checked="" <?php if(isset($fotoproduto)) if ($fotoproduto['principal'] == 'S'){echo 'checked';}else{$fotoproduto == null;} ?>> Sim
                        <input type="radio" name="principal" value="N" <?php if(isset($fotoproduto)) if ($fotoproduto['principal'] == 'N'){echo 'checked';}else{$fotoproduto == null;}?>> Não
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="ordem">Ordem Foto</label>
                        <input type="text" class="form-control" id="ordem" name="ordem" placeholder="Digite a Ordem" 
                               value="<?php if (isset($fotoproduto)) echo $fotoproduto['ordem']; ?>" maxlength="2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="foto">Foto</label>
                        <input name="arquivo" type="file" required=""/>
                        <?php
                            //Cairá neste teste sempre que o usuário for alterar um registro
                            if(isset($fotoproduto['fotoproduto']) != null){
                                if($fotoproduto['fotoproduto'] == ''){
                                    if( isset($_SERVER['HTTPS'] ) ) {
                                        $prefixo = 'https://';
                                    }else{
                                        $prefixo = 'http://';
                                    }
                                    $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/gerenciador/protected/imagens/noimagens/noimagem.png';
                                    echo '<img src = "' . "$urlbase". '" style="width: 100px;"/>';   
                                }else{
                                    echo '<img src = "' . $fotoproduto['fotoproduto'] . '" style="width: 100px; heigth: 80px;"/>';
                                }
                            }//Cairá nesse teste sempre que o usuário for inserir um novo veículo
                            else{
                                if(isset($_SERVER['HTTPS'] ) ) {
                                    $prefixo = 'https://';
                                }else{
                                    $prefixo = 'http://';
                                }
                                $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/gerenciador/protected/imagens/noimagens/noimagem.png';
                                echo '<img src = "' . "$urlbase". '" style="width: 100px;"/>';   
                            }
                        ?>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formFotoProduto").validate({
        rules: {
            idproduto: {
                required: true
            },
            arquivo: {
                required: true
            }
        },
        messages: {
            idproduto: {
                required: "Por favor, selecione um Produto"
            },
            arquivo: {
                required: "Por favor, informe a Imagem do Produto."
            }
        }
    });
</script>