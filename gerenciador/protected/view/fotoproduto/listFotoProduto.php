<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Foto Produto</div>
            <div class="panel-body">
                <a href="index.php?controle=fotoprodutoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Referência</th>
                    <th>Produto</th>
                    <th>Ordem</th>
                    <th>Foto Produto</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="padding: 8px 10px; text-align: center;">' . $item['referencia'];
                            if($item['tamanho'] != ''){
                                echo '<td>' . $item['nomeproduto'] . ' - ' .  $item['tamanho'];
                            }else if($item['numeroproduto'] != ''){
                                echo '<td>' . $item['nomeproduto'] . ' - ' .  $item['numeroproduto'];
                            }
                            
                            if($item['ordem'] != null){
                                echo '<td>' . $item['ordem'];
                            }else{
                                echo '<td>' . 'Ordem Automática';
                            }
                            if($item['nomeproduto'] != null){
                                echo '<td>' . '<img src = "' . $item['fotoproduto'] . '" style="width: 100px; heigth: 80px;"/>';
                            }else{
                                $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/gerenciador/protected/imagens/noimagens/noimagem.png"';
                                echo '<td><img src = "' . "$urlbase". '" style="width: 100px;"/>';  
                            }
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=fotoprodutoController&acao=buscar&id=$idencriptografa'>
                            <span class='glyphicon glyphicon-pencil'> </span>
                            </a> </td>
                            <td> <a onclick='excluir(\"excluir\",\"fotoprodutoController\",\"$idencriptografa\")' href='#'>
                            <span class='glyphicon glyphicon-trash customDialog'> </span>
                            </a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>