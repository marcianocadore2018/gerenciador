<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Clientes Inadimplentes</div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 2000px">
                    <thead>
                    <th>ID Transação</th>
                    <th>Cliente</th>
                    <th>CPF</th>
                    <th>Contato </th>
                    <th>Qtd/Parcelas</th>
                    <th>Data Venda</th>
                    <th>Dt. Venc Parcela</th>
                    <th>Valor Parcelado</th>
                    <th>Cidade</th>
                    <th>Endereço</th>
                    <th>E-mail</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['idvenda'];
                            echo '<td>' . $item['nomecliente'];
                            echo '<td style="padding-right: 0px; padding-left: 18px;">' . $item['cpf'];
                            echo '<td style="padding-right: 0px; padding-left: 18px;">' . $item['contatocliente'];
                            echo '<td style="padding-right: 0px; padding-left: 60px;">' . $item['qtdparcelas'];
                            echo '<td style="padding-right: 0px; padding-left: 18px;">' . $item['datavenda'];
                            echo '<td style="padding-right: 0px; padding-left: 18px;">' . $item['datavencimentoparcela'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['parcelado'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['cidadecliente'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['enderecliente'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['emailcliente'];
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>