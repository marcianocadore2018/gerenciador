<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Descontos Tipos de Pagamentos - (Vendas)</div>
            <div class="panel-body">
                <a href="index.php?controle=descontotipopagamentoController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Tipo Pagamento</th>
                    <th>Permitir Desconto</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="padding-left: 20px;">' . $item['tipopagamento'];
                            if($item['permitirdesconto'] == 'S'){
                                echo '<td style="padding-left: 60px;">' . 'Sim';
                            }else{
                                echo '<td style="padding-left: 60px;">' . 'Não';
                            }
                            
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datadescontopermitido'];
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=descontotipopagamentoController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>