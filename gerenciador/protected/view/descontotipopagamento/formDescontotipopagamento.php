<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Desconto Tipo de Pagamento - Vendas</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formDescontotipopagamento" id="formDescontotipopagamento" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($descontotipopagamento)) echo $descontotipopagamento['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8"> 
                        <label for="idtipopagamento">Tipo Pagamento</label>
                        <select class="form-control selectpicker" tabindex="1" name="idtipopagamento" id="idtipopagamento" required data-errormessage-value-missing="Selecione o Produto" data-live-search="true">
                            <option value="">Selecione o Tipo de Pagamento</option>>
                            <?php
                            foreach ($listaTipopagamentos as $tipopagamentos) {
                                $selected = (isset($descontotipopagamento) && $descontotipopagamento['idtipopagamento'] == $tipopagamentos['id']) ? 'selected' : '';
                                ?>
                                <option data-tokens="<?php echo $tipopagamentos['descricao']; ?>" value='<?php echo $tipopagamentos['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php 
                                                echo $tipopagamentos['descricao'];
                                            ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3">
                        <label for="permitirdesconto">Permitir Desconto</label>
                        <br>
                        <input type="radio" name="permitirdesconto" value="S" checked="" <?php if(isset($descontotipopagamento)) if ($descontotipopagamento['permitirdesconto'] == 'S'){echo 'checked';}else{$descontotipopagamento == null;} ?>> Sim
                        <input type="radio" name="permitirdesconto" value="N" <?php if(isset($descontotipopagamento)) if ($descontotipopagamento['permitirdesconto'] == 'N'){echo 'checked';}else{$descontotipopagamento == null;}?>> Não
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formDescontotipopagamento").validate({
        rules: {
            idtipopagamento: {
                required: true
            }
        },
        messages: {
            idtipopagamento: {
                required: "Por favor, selecione o Tipo de Pagamento"
            }
        }
    });
</script>