<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro Desconto Valor Máximo</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="FormDescontovalormaximo" id="formDescontovalormaximo" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($descontomaximo)) echo $descontomaximo['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="porcentagemvalormaximo">Porcentagem Valor Máximo</label>
                        <input type="text" class="form-control" id="porcentagemvalormaximo" name="porcentagemvalormaximo" placeholder="Digite Porcentagem Valor Máximo" 
                               value="<?php if (isset($descontomaximo)) echo $descontomaximo['porcentagemvalormaximo']; ?>" required>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formDescontovalormaximo").validate({
        rules: {
            porcentagemvalormaximo: {
                required: true
            }
        },
        messages: {
            porcentagemvalormaximo: {
                required: "Por favor, informe a Porcentagem desconto máximo"
            }
        }
    });
</script>
