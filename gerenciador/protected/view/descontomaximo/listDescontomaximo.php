<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem Desconto Valor Máximo</div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Data Cadastro</th>
                    <th>Porcentagem Valor Máximo</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['datacadastro'];
                            echo '<td style="padding-right: 0px; padding-left: 40px;">' . $item['porcentagemvalormaximo']; echo "%";
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td> <a href='index.php?controle=descontomaximoController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>