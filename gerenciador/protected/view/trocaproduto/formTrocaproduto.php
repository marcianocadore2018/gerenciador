<div class="col-md-12 col-offset-2">
    <?php
        if(isset($_GET['erro'])){?>
            <div class="alert alert-warning" >
                <b>Por favor, informe pelo menos uma informação abaixo. Para ser possível buscar a venda do cliente!</b>
            </div>
        <?php 
        }
    ?>
    <?php
        if(isset($_POST['cpf']) || isset($_POST['idvenda']) || isset($_POST['idcliente'])){
            $cpffiltro = $_POST['cpf'];
            $transacaofiltro = $_POST['idvenda'];
            $clientefiltro = $_POST['idcliente'];
        }else if(isset($_GET['cpf'])){
            $cpffiltro = $_GET['cpf'];
            $transacaofiltro = $_GET['transacaofiltro'];
        }else{
            $cpffiltro = '';
            $transacaofiltro = "";
            $clientefiltro = "";
        }
    ?>
    
    <div class="panel panel-primary">
        <div class="panel-heading">Trocar Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formTrocarproduto" id="formTrocarproduto" method="POST" class="form" role="form">
                <?php 
                    if(isset($_GET['error'])==1){?>
                        <div class="alert alert-danger">
                            Não foi possível realizar a troca, pois a troca já foi realizada.
                        </div>
                    <?php }else if(isset($_GET['error'])==2){?>
                        <div class="alert alert-danger">
                            Não foi possível realizar a troca do produto, entre em contato com o administrador.
                        </div>
                    <?php }else if(isset($_GET['errortroca'])=="ZXJyb3I9ZXJyb3J0cm9jYWlndWFs"){ ?>
                        <div class="alert alert-warning" >
                            <b>Não é possível realizar a troca pelo mesmo produto, pois os produtos Devolvidos e Trocados são iguais!</b>
                        </div>
                    <?php }else if(isset($_GET['errorqtd']) == "ZXJyb3JxdGQ="){?>
                            <div class="alert alert-danger" >
                                <b>Não é possível realizar a troca pois não existe este produto encontra-se com a quantidade zerada!</b>
                            </div>
                    <?php }else if(isset($_GET['errorqtddisp']) == "ZXJyb3JxdGRkaXNw"){?>
                        <div class="alert alert-danger" >
                            <b>Não é possível realizar a troca pois não existe quantidade disponível!</b>
                        </div>
                    <?php }else if(isset ($_GET['errordevcomp']) == "ZXJyb3JkZXZvbHVjYW9tYWlvcnF1ZWNvbXByYQ=="){?>
                        <div class="alert alert-danger" >
                            <b>Não é possível realizar a troca pois a quantidade da Devolução é maior que a quantidade comprada!</b>
                        </div>
                    <?php }else if(isset ($_GET['errorupdt']) != ""){?>
                        <div class="alert alert-danger" >
                            <b>Não é possível atualizar o registro dos Itens dos Produtos!</b>
                        </div>
                    <?php }else if(isset ($_GET['sucessmsmupdt']) == "YXR1YWxpemFkb2NvbXN1Y2Vzc28="){ ?>
                        <div class="alert alert-success" >
                            <b>Produto Alterado com Sucesso!</b>
                        </div>
                    <?php }else if(isset ($_GET['errorqtddif']) == "ZXJyb3I9cXVhbnRpZGFkZWRpZmVyZW50ZQ=="){ ?>
                        <div class="alert alert-danger">
                            <b>Não foi possível realizar a troca, pois a quantidade está devolvida e a quantidade trocada são diferentes!</b>
                        </div>
                    <?php }else if(isset ($_GET['vldisperr']) == "ZXJyb3I9dmFsb3JkaXNwb25pdmVsZGlmZXJlbnRl"){?>
                        <div class="alert alert-danger">
                            <b>Não foi possível realizar a troca, pois o valor disponível do cliente é menor que o valor trocado!</b>
                        </div>
                    <?php }
                ?>
                
                
                <div class="row">
                    <div class="col-md-3">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Digite o CPF" value="<?php echo $cpffiltro;?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Cliente</label>
                            <select data-placeholder="Selecione um cliente" class="form-control selectpicker" tabindex="1" name="idcliente" id="idcliente" data-live-search="true">
                                <option value="">Selecione</option>
                                <?php
                                    foreach ($listaClientes as $clientes) {
                                        ?>
                                        <option data-tokens="<?php echo $clientes['nome']; ?>" value='<?php echo $clientes['id']; ?>' style="text-transform: uppercase"> 
                                                    <?php echo $clientes['nome']; ?>
                                        </option>
                                    <?php 
                                    }
                                    ?>
                            </select>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-3">
                        <label for="numerotransacao">Número Transação (Venda)</label>
                        <input type="text" onkeypress="return Onlynumbers(event)" class="form-control" autocomplete="off" onkeypress="return Onlynumbers(event)" id="idvenda" name="idvenda" placeholder="Digite o Número da Transação" value="<?php echo $transacaofiltro;?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Filtrar Venda</button>
            </form>
        </div>
        
        <?php
            
            if($_POST != NULL || $_GET != NULL){
             
                if(isset($listaDados) != NULL){?>
                <div class="table-responsive">
                        <table class="table" id="example1" style="width: 98%;">
                            <thead>
                                <th>Id Transação</th>
                                <th>Referência</th>
                                <th>Produto</th>
                                <th style="padding-left: 44px;">Valor</th>
                                <th>Quantidade</th>
                                <th>Número/Tamanho</th>
                                <th>Data Transação</th>
                                <th>Trocar Produto</th>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($listaDados as $item) {
                                    $id = ""; $cpf = $item['cpf']; $idvenda = $item['idtransacao']; $idproduto = $item['idproduto']; $iditemproduto = $item['iditemproduto'];
                                    echo '<tr>';
                                    echo '<td style="padding-left: 3%; padding-top: 14px;">' . $item['idvenda'];
                                    echo '<td style="padding-left: 3%; padding-top: 14px;">' . $item['referencia'];
                                    echo '<td style="padding-left: 22.391;padding-top: 14px;">' . $item['nomeproduto'];
                                    echo '<td style="padding-left: 3%; padding-top: 14px; width: 66px;">' . $item['total'];
                                    echo '<td style="padding-left: 5%; padding-top: 14px;">' . $item['quantidade'];
                                    if($item['numeroproduto'] != ''){
                                        $tamanhonumero = $item['numeroproduto'];
                                    }else if($item['tamanhoproduto'] != ''){
                                        $tamanhonumero = $item['tamanhoproduto'];
                                    }
                                    echo '<td style="padding-left: 6%; padding-top: 14px;">' . $tamanhonumero;
                                    echo '<td style="padding-left: 2%; padding-top: 14px;">' . $item['datavenda'];
                                    echo "<td style='padding-left: 50px;width: 80px;'> <a  href='index.php?controle=trocaprodutoController&acao=trocarproduto&id=$id&idproduto=$idproduto&iditemproduto=$iditemproduto&idvendatroca=$idvenda' title='Clique aqui para Trocar o Produto' class='glyphicon glyphicon-edit'> Trocar"
                                    . "</a> </td>";
                                    echo '<td>';
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
            <?php }else{?>
                    <!--<div class="alert alert-danger">
                         Não existe parcelas para o filtro informado!
                    </div>-->
                <?php }
            }
            ?>
        </div>
</div>