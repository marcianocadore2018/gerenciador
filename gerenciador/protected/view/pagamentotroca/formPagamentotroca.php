<?php
    $ultimatroca = $_GET['ultimatroca'];
    $sqlultimatroca = pg_query("select pro.nomeproduto as nomeproduto,
                                       troca.valordiferenca as valor,
                                       troca.idprodutotrocado,
                                       troca.idvenda,
                                       troca.statustroca,
                                       troca.porcentagemtroca
                                  from trocaproduto troca
                                 inner join produto pro
                                    on troca.idprodutotrocado = pro.id
                                 where troca.id = $ultimatroca");
    $rsultimatroca = pg_fetch_array ($sqlultimatroca);
    $nomeproduto = $rsultimatroca['nomeproduto'];
    $valor = $rsultimatroca['valor'];
    $idprodutotrocado = $rsultimatroca['idprodutotrocado'];
    $idvenda = $rsultimatroca['idvenda'];
    $statustroca = $rsultimatroca['statustroca'];
    $porcentagemtroca = $rsultimatroca['porcentagemtroca'];
?>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Pagamento Troca Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formPagamentotroca" id="formPagamentotroca" method="POST" class="form" role="form">
                <input type="hidden" name="idtrocaproduto" value="<?php echo $ultimatroca?>">
                <!-- Parâmetros para verificação -->
                <input type="hidden" name="idprodutotrocado" value="<?php echo $idprodutotrocado; ?>">
                <input type="hidden" name="idvenda" value="<?php echo $idvenda; ?>">
                <input type="hidden" name="statustroca" value="<?php echo $statustroca; ?>">
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="nomeproduto">Produto</label>
                        <input type="text" class="form-control" id="nomeproduto" name="nomeproduto" value="<?php echo $nomeproduto; ?>" readonly="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="valor">Valor Diferença a ser cobrada</label>
                        <?php 
                        $removesinal = str_replace("-", "", $valor); 
                        ?>
                        <input type="text" class="form-control" id="valor" name="valor" value="<?php echo 'R$ ' . $removesinal; ?>" readonly="true">
                    </div>
                </div>
                <?php
                    //VALOR COM PORCENTAGEM
                    $valorporcentagemconta = ($valor * ($porcentagemtroca / 100));
                    $removesinalpocentagem = str_replace("-", "", $valorporcentagemconta);
                    $removevalor = str_replace("-", "", $valor);
                    
                    $valorporcentagem = ($removevalor - $removesinalpocentagem);
                    
                    if($porcentagemtroca != "0"){?>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="porcentagemtroca">Porcentagem Troca</label>
                                <input type="text" class="form-control" id="valor" name="porcentagem" value="<?php echo $porcentagemtroca . ' %'; ?>" readonly="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="valorporcentagem">Valor com Porcentagem</label>
                                <input type="text" class="form-control" id="valorporcentagem" name="valorporcentagem" value="<?php echo 'R$ ' . number_format($valorporcentagem, 2, ',', '.'); ?>" readonly="true">
                            </div>
                        </div>
                   <?php }
                ?>
                
                <div class="row">
                    <div class="col-md-3">
                        <label for="valor">Forma de Pagamento</label>
                        <select class="form-control" name="idformapagamento" id="idformapagamento" required data-errormessage-value-missing="Selecione a Forma de Pagamento" style="width: 261px;">
                            <option value="">Selecione a Forma de Pagamento</option>>
                            <option value="1">À Vista</option>
                            <option value="3">Cartão de Débito</option>
                            <option value="4">Cartão de Crédito</option>
                        </select>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formPagamentotroca").validate({
        rules: {
            idformapagamento: {
                required: true
            }
        },
        messages: {
            idformapagamento: {
                required: "Por favor, selecione a Forma de Pagamento"
            }
        }
    });
</script>
