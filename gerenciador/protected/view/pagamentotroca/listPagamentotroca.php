<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação Pagamentos de Troca dos Produtos</div>
            <div class="panel-body">
                <a href="index.php?controle=trocaprodutoController&acao=novo" title="Clique aqui para fazer uma nova Alteração de Produto" class="btn btn-primary" role="button">Alterar Novo Produto</a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1400px">
                    <thead>
                    <th style="width: 200px;">Nome Cliente</th>
                    <th style="width: 200px;">Produto Devolvido</th>
                    <th style="width: 200px;">Produto Trocado</th>
                    <th style="width: 120px;">Data Troca</th>
                    <th style="width: 120px;">Valor Diferença</th>
                    <th style="width: 200px;">Colaborador</th>
                    </thead>
                    <tbody>
                        <?php
                        //SOMA VALORES 
                        $sqlvalortotal = pg_query("select trim(cast((valordiferenca - (sum(valordiferenca) * porcentagemtroca / 100)) as text), '-') as valortotal
                                                     from trocaproduto where statustroca = 'D'
                                                    group by porcentagemtroca, valordiferenca;");
                        $resvalortotal   = pg_fetch_array ($sqlvalortotal);
                        $valortotal = $resvalortotal['valortotal'];
                        $formatarvalor = number_format($valortotal, 2, ',', '.');
                        foreach ($listaDados as $item) {
                            
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td>' . $item['nomeprodutodevolvido'];
                            echo '<td>' . $item['nomeproduto'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datatroca'] . ' - ' . $item['horatroca'];
                            $valorcomporcentagem = (number_format($item['valor'] , 2, '.', '.'));
                            $porcentagemtroca = $item['porcentagemtroca'];
                            $valorporcentagem = ($valorcomporcentagem - ($valorcomporcentagem / 100 * $porcentagemtroca));
                            $valorfinal = ($valorporcentagem);
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . "R$ " . number_format($valorfinal , 2, ',', '.');
                            echo '<td>' . $item['colaboradorresponsavel'];
                            echo '</tr>';
                        }
                        ?>
                        <!--<tr>
                        <td colspan="4" style="padding-left: 61%; background-color: #FFFACD"> <b>Valor Total:</b>
                        <td colspan="2" style="padding-left: 2%; background-color: #FFFACD"> <b>R$ <?php //echo $formatarvalor; ?></b>
                        </tr>-->
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>