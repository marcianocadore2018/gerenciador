<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Controle Estoque</div>
            <div style="margin-left: 20px; height: 190px">
                <form action="<?= $acao; ?>" method="post">
                    <div class="row">
                        <label>Filtrar por:</label>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="padding-left: 2px;">
                            <label>Grupo:</label>
                            <select class="form-control" name="idgrupo" id="idgrupo" data-errormessage-value-missing="Selecione o Grupo">
                                <option value="">Selecione o Grupo</option>>
                                <?php
                                foreach ($listaGrupos as $grupos) {
                                    $selected = (isset($produto) && $produto['idgrupo'] == $grupos['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $grupos['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $grupos['descricao']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label>Situação:</label>
                        <input type="radio" name="situacao" value="E" <?= $checked1 ?>> Entrada
                        <input type="radio" name="situacao" value="S" <?= $checked2 ?>> Saída
                    </div>
                    <button type="submit" class="btn btn-success">Pesquisar</button>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th style="width: 200px;">Nome Produto</th>
                    <th>Referência</th>
                    <th>Grupo</th>
                    <th>Número</th>
                    <th>Cor</th>
                    <th>Gênero</th>
                    <th>Valor</th>
                    <th>Qtd. Disponível Estoque/Loja</th>
                    <?php 
                        if($_POST != null && $_POST['situacao'] == 'S'){ ?>
                            <th>Qtd. Vendidas</th>
                       <?php }
                    ?>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomeproduto'];
                            echo '<td style="text-align: center;padding-left: 0px;padding-right: 30px;">' . $item['referenciaproduto'];
                            echo '<td>' . $item['nomegrupo'];
                            echo '<td style="text-align: center;padding-left: 0px;padding-right: 30px;">' . $item['numeroproduto'];
                            echo '<td>' . $item['corproduto'];
                            echo '<td>' . $item['generoproduto'];
                            echo '<td>' . $item['valorproduto'];
                            if($item['quantidadedisponivel'] != null){
                                $quantidadedisponivel = $item['quantidadedisponivel'];
                            }else{
                                $quantidadedisponivel = $item['quantidadeproduto'];
                            }
                            if($quantidadedisponivel == ''){
                               $qtddisponivel = 0;
                            }else{
                               $qtddisponivel = $quantidadedisponivel;
                            }
                            echo '<td style="text-align: center;padding-left: 0px;padding-right: 30px;">' . $qtddisponivel;
                            
                            if($_POST != null && $_POST['situacao'] == 'S'){ ?>
                                <td style="text-align: center;padding-left: 0px;padding-right: 30px;"> <?php echo $item['quantidadevendidas'] ?>
                            <?php }
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>