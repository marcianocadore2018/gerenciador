<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Alteração de Parcelamento</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formParcelamento" id="formParcelamento" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($parcelamento)) echo $parcelamento['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="qtdparcelas">Quantidade Parcelas</label>
                        <input type="text" class="form-control" id="qtdparcelas" name="qtdparcelas"
                               onkeypress="return Onlynumbers(event)"
                               value="<?php if (isset($parcelamento)) echo $parcelamento['quantidadeparcelas']; ?>" required maxlength="3">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formParcelamento").validate({
        rules: {
            qtdparcelas: {
                required: true,
                maxlength: 3
            }
        },
        messages: {
            qtdparcelas: {
                required: "Por favor, informe a Quantidade de Parcelas",
                maxlength: "A Quantidade de Parcelas deve ter no máximo 3 caracteres"
            }
        }
    });
</script>
