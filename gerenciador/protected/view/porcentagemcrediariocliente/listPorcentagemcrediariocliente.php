<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem Porcentagem de Crediário</div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Data Cadastro</th>
                    <th>Porcentagem</th>
                    <th>Ações</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['datacadastro'];
                            echo '<td style="padding-right: 0px; padding-left: 40px;">' . $item['porcentagem']; echo "%";
                            $id = $item['id'];
                            
                            //consultar usuario, se for tipo Master pode alterar
                            $tipousuariocolaborador = $_SESSION['login'];
                            $colaboradormaster = pg_query("select tipocolaborador from colaboradores where login = '$tipousuariocolaborador';");
                            $rscolaborador = pg_fetch_array ($colaboradormaster);
                            $colaboradortipo = $rscolaborador['tipocolaborador'];
                            if($colaboradortipo == 'M'){
                                echo "<td> <a href='index.php?controle=porcentagemcrediarioclienteController&acao=buscar&id=$id'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                            }
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>