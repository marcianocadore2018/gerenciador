<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro Porcentagem de Crediário</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="FormPorcentagemcrediariocliente" id="formPorcentagemcrediariocliente" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($porcentagemcrediariocliente)) echo $porcentagemcrediariocliente['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="porcentagem">Porcentagem</label>
                        <input type="text" class="form-control" id="porcentagem" name="porcentagem" placeholder="Digite Porcentagem" 
                               value="<?php if (isset($porcentagemcrediariocliente)) echo $porcentagemcrediariocliente['porcentagem']; ?>" required>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formPorcentagemcrediariocliente").validate({
        rules: {
            porcentagem: {
                required: true
            }
        },
        messages: {
            porcentagem: {
                required: "Por favor, informe a Porcentagem"
            }
        }
    });
</script>
