<div id="fundo">
    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação Exclusão de Venda</div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Nome Vendedor</th>
                    <th style="width: 98px;">Data Exclusão</th>
                    <th style="width: 98px;">Hora Exclusão</th>
                    <th>Nome Cliente</th>
                    <th style="width: 120px;">Transação Venda</th>
                    <th>Motivo Exclusão</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td style="padding-left: 18px;">' . $item['nomevendedor'];
                            echo '<td style="padding-left: 28px;">' . $item['dataexclusao'];
                            echo '<td style="padding-left: 4%;">' . $item['horaexclusao'];
                            echo '<td style="padding-left: 18px;">' . $item['nomecliente'];
                            echo '<td style="padding-left: 5%;">' . $item['transacaovenda'];
                            echo '<td style="padding-left: 18px;">' . $item['motivoexclusao'];
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>