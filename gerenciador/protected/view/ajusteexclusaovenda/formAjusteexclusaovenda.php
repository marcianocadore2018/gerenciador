<?php
    $sqlvendedorresponsavel   = pg_query("SELECT id, upper(nome) as nome FROM COLABORADORES WHERE login = '" . $_SESSION['login'] . "'");
    $resvendedorresponsavel   = pg_fetch_array ($sqlvendedorresponsavel);
    $nomevendedorresponsavel = $resvendedorresponsavel['nome'];
    $idvendedorresponsavel = $resvendedorresponsavel['id'];
    $idvenda = $_GET['id'];
?>
<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Ajuste de Exclusão de Venda</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formAjusteexclusaovenda" id="formAjusteexclusaovenda" method="POST" class="form" role="form">
                <input type="hidden" name="idvendedor" value="<?php echo $idvendedorresponsavel; ?>">
                <input type="hidden" name="idvenda" value="<?php echo $idvenda; ?>">
                <div class="row">
                    <div class="col-md-8">
                        <label for="vendedor">Responsável pela Exclusão</label>
                        <input type="text" class="form-control" id="idvendedor" name="idvendedor" disabled="" value="<?php echo $nomevendedorresponsavel;?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="motivoexclusao">Motivo Exclusão</label>
                        <textarea class="form-control" id="motivoexclusao" name="motivoexclusao" placeholder="Informe o Motivo da Exclusão" minlength="10" maxlength="300"/></textarea>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formAjusteexclusaovenda").validate({
        rules: {
            motivoexclusao: {
                required: true,
                minlength: 10,
                maxlength: 100
            }
        },
        messages: {
            motivoexclusao: {
                required: "Por favor, informe o Motivo da Exclusão desta venda",
                minlength: "O Motivo da Exclusão deve ter pelo menos 10 caracteres",
                maxlength: "O Motivo da Exclusão deve ter no máximo 100 caracteres"
            }
        }
    });
</script>