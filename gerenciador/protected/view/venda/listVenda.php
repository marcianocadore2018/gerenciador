<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Relação das Vendas</h2>
        <ol class="breadcrumb">
            <div class="m-t text-righ">
                <a href="index.php?controle=vendaController&acao=novo" class="btn btn-xs btn-outline btn-default">NOVA VENDA 
                    <i class="glyphicon glyphicon-plus"></i> </a>
            </div>
        </ol>
    </div>
    <div class="col-lg-2">
        
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class=" table-responsive">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" id="examplevenda" data-page-size="8">
                        <thead>
                        <tr>

                            <th data-toggle="true">Cliente</th>
                            <th data-hide="phone">Vendedor</th>
                            <th data-hide="phone">Loja</th>
                            <th data-hide="phone">Data/Venda</th>
                            <th data-hide="phone">Tipo/Pagamento</th>
                            <th data-hide="phone">% Venda</th>

                            <th data-hide="phone" data-sort-ignore="true">TOTAL</th>

                            <th class="text-center" data-sort-ignore="true">Ações</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($listaDados as $dados) {
                                echo '<tr>';
                            
                                if($dados['idclientevenda'] >= 100000){
                                        echo '<td style="color: blue">' . $dados['nomecliente']              . '</td>';	// NOME DO CLIENTE
                                    }else{
                                        echo '<td>' . $dados['nomecliente']              . '</td>';	// NOME DO CLIENTE
                                    }
                                echo '<td>' . $dados['nomecolaboradorvendedor']  . '</td>'; // COLABORADOR
                                echo '<td>' . $dados['nomefantasia']             . '</td>'; // LOJA
                                echo '<td>' . $dados['datavenda']                . '</td>'; // DATA DE VENDA

                                if($dados['total'] == 'R$ '){
                                    $dados['total'] = 'R$ 0,00';
                                }


                                if ($dados['tipopagamento'] == '1' && $dados['data_vencimento'] == 'S'){
                                    echo '<td><span class="label label-default">'.$dados['forma_desc'].'</span></td>'; // TIPO DO PAGAMENTO
                                }else if ($dados['tipopagamento'] == '1' && $dados['parcelar'] == 'S'){
                                    echo '<td><span class="label label-warning">'.$dados['forma_desc'].'</span></td>'; // TIPO DO PAGAMENTO
                                }else if ($dados['tipopagamento'] == '1' && $dados['parcelar'] == 'N' && $dados['data_vencimento'] == 'N'){
                                    echo '<td><span class="label label-success">'.$dados['forma_desc'].'</span></td>'; // TIPO DO PAGAMENTO
                                }else if ($dados['tipopagamento'] == ' ' || $dados['tipopagamento'] == ''){
                                    echo '<td><span class="label label-danger">EM ANDAMENTO</span></td>'; 
                                }

                                if($dados['desconto_cabecalho']=='')
                                    echo '<td>0,00 %</td>';
                                else
                                    echo '<td>' .$dados['desconto_cabecalho']. ' %</td>';

                                echo '<td>' .$dados['total']. '</td>';

                                $id = $dados['id'];

                                // Se a venda ainda nao estiver concluída permite editar/excluir
                                if ($dados['tipopagamento'] == ' ' || $dados['tipopagamento'] == ''){
                                    echo "<td class='text-center'>";
                                    echo    "<a href='index.php?controle=vendaController&acao=buscar&id=$id' class='btn btn-xs btn-outline btn-default' title='Editar esta venda'>";
                                    echo        "<span class='glyphicon glyphicon-pencil'> </span>";
                                    echo    '</a>';
                                    echo  '&nbsp;';

                                    echo "<a onclick='excluir(\"excluir\",\"vendaController\",$id)' href='#' class='btn btn-xs btn-outline btn-danger' title='Excluir esta venda'>";
                                    echo    "<span class='glyphicon glyphicon-trash customDialog'> </span>";
                                    echo '</a>';
                                    echo '</td>';
                                // Se a venda estiver concluida permite somente visualizar.
                                }else{
                                    echo "<td class='text-center'>";
                                    echo    "<a href='index.php?controle=vendaController&acao=buscar&id=$id' class='btn btn-xs btn-outline btn-default' title='Visualizar esta venda'>";
                                    echo        "<span class='glyphicon glyphicon-eye-open'> </span>";
                                    echo    '</a>';
                                    echo  '&nbsp;';

                                    echo    "<a href='protected/view/comprovantevenda/comprovante.php?id=$id' class='btn btn-xs btn-outline btn-default' title='Gerar comprovante'>";
                                    echo        "<span class='glyphicon glyphicon-list-alt'> </span>";
                                    echo    '</a>';
                                    echo '</td>';
                                }
                                
                                echo '</tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>