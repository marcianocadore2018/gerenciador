<?php date_default_timezone_set('America/Sao_Paulo'); ?>

<form id="form-main">
    <div class="col-md-12 container">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2 id="main-title">Pré-Venda </h2>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-8 m-b-xs">
                        <div data-toggle="buttons" class="btn-group">
                            <label id="opt1" class="btn btn-sm btn-default active"> 
                                <input type="radio" id="option1" name="options"> 1.CAPA </label>
                            <label id="opt2" class="btn btn-sm btn-default disabled"> 
                                <input type="radio" id="option2" name="options"> 2.ITENS </label>
                            <label id="opt3" class="btn btn-sm btn-default disabled"> 
                                <input type="radio" id="option3" name="options"> 3.CONCLUIR VENDA </label>
                            <input type="hidden" id="order_id" name="order_id" value="<?php echo $venda['id']?>" placeholder="Ordem ID" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class='pull-right control-group'>
                             <?php 
                                if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                                ?>
                                <button id="btn-pesquisa-item" type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-pesquisar-item">PESQUISAR</button>
                                <button id="btn-salva-capa" type="button" class="btn btn-success">SALVAR</button> 
                                <button id="btn-finalizar" type="button" class="btn btn-danger">FINALIZAR VENDA</button> 
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
        </div>
    </div>
    <div id="container"></div>
</form>

<!-- Modal PESQUISAR ITENS -->
<div class="modal fade" id="modal-pesquisar-item" tabindex="-1" role="dialog" aria-labelledby="modal-pesquisar-item">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-pesquisar-item">Pesquisar produtos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                          <input id="txt-pesquisar" type="text" class="form-control" placeholder="Pesquisar ...">
                          <span class="input-group-btn">
                            <button id="btn-pesquisar"  class="btn btn-default" type="button">Pesquisar!</button>
                          </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table" id="table-item">
                                <thead>
                                <tr>
                                    <th style="width: 45px;">REF:</span></th>
                                    <th style="width: 180px;">Produto</span></th>
                                    <th style="width: 35px;">Número</span></th>
                                    <th style="width: 55px;">Preço</td>
                                    <th style="width: 65px;">Data/Cadastro</th>
                                    <th style="width: 25px;">Estoque</th>
                                    <th style="width: 25px;">Vendido</th>
                                    <th style="width: 45px;">Ação</th>
                                </tr>
                                </thead> 
                                <tbody id="gridTable">
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- 
    Etapa 1 do pedido
    Nessa parte é criado os dados do cabeçalho
-->
<form id="form-step1">
    <div id="order_step1" class="col-md-12 container">
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for="datavenda">Data Venda</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <input type="text" class="form-control" id="datavendareadonly" name="datavenda" readonly
                                           value="<?php if (isset($venda)) echo $venda['datavenda']; else echo date('d/m/Y'); ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="font-normal">Cliente</label>
                            <?php 
                                if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                                ?>
                                <select data-placeholder="Selecione um cliente" class="form-control chosen-select" tabindex="1" name="idcliente" id="idcliente">
                                    <option value="0">Selecione</option>
                                    <?php
                                        foreach ($listaClientes as $clientes) {
                                            $selected = (isset($venda) && $venda['idcliente'] == $clientes['id']) ? 'selected' : '';
                                            ?>
                                            <option value='<?php echo $clientes['id']; ?>'
                                                    <?php echo $selected; ?>> 
                                                        <?php echo $clientes['nome']; ?>
                                            </option>
                                        <?php 
                                        }
                                        ?>
                                </select>
                                <?php 
                                }else{
                                    foreach ($listaClientes as $clientes) {
                                        if($venda['idcliente'] == $clientes['id']){
                                            echo "<input type='text' id='clienteSel' name='clienteSel' value='".$clientes['nome']."' class='form-control' readonly>";
                                        }
                                    }
                                }
                                ?>
                        </div>
                    </div>
                    <?php 
                        if($venda['tipopagamento'] == '1'){
                        ?>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="font-normal">Resumo da venda:</label>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                        <div class="col-sm-2">
                        <?php 
                            if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                            ?>
                            <div class="limiteCompra form-group">
                                <label class="control-label" for="order_id">Limite de compra</label>
                                
                                <input type="text" class="form-control" id="valorlimitevendaproduto" name="valordisponivelcompra" placeholder="R$ 0,00" 
                                               value="<?php if (isset($valordisponivelcompra)) echo $venda['valordisponivelcompra']; ?>" readonly>
                            </div>
                        <?php } ?>
                        </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="font-normal">Vendedor</label>
                            <?php 
                                if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                                ?>
                                <select data-placeholder="Selecione um vendedor" class="form-control chosen-select" tabindex="1" name="idcolaboradorvendedor" id="idcolaboradorvendedor" required data-errormessage-value-missing="Selecione o Vendedor">
                                    <option value="0">Selecione</option>
                                    <?php
                                        foreach ($listaColaboradores as $colaboradores) {
                                            $selected = (isset($venda) && $venda['idcolaborador'] == $colaboradores['id']) ? 'selected' : '';
                                            ?>
                                            <option value='<?php echo $colaboradores['id']; ?>'
                                                    <?php echo $selected; ?>> 
                                                        <?php echo $colaboradores['nome']; ?>
                                            </option>
                                    <?php } ?>
                                </select>
                                <?php 
                                }else{
                                    foreach ($listaColaboradores as $colaboradores) {
                                        if($venda['idcolaborador'] == $colaboradores['id']){
                                            echo "<input type='text' id='colalboradorSel' name='clienteSel' value='".$colaboradores['nome']."' class='form-control' readonly>";
                                        }
                                    }
                                }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="font-normal">Forma de pagamento</label>
                            <?php 
                                if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                                ?>
                                <select data-placeholder="Selecione uma forma de pagamento" class="form-control chosen-select" tabindex="1" name="idformapgto" id="idformapgto" required data-errormessage-value-missing="Selecione uma forma de pagamento">
                                    <option value="0">Selecione</option>
                                    <?php
                                        foreach ($listaFormas as $forma) {

                                            $selected = (isset($venda) && $venda['formapgto'] == $forma['id']) ? 'selected' : '';
                                            ?>
                                            <option value='<?php echo $forma['id']; ?>;<?php echo $forma['tipo']; ?>;<?php echo $forma['parcelar']; ?>;<?php echo $forma['data_vencimento']; ?>' <?php echo $selected; ?> > 
                                                        <?php echo $forma['descricao']; ?>
                                            </option>
                                    <?php } ?>
                                </select>
                                <?php 
                                }else{
                                    foreach ($listaFormas as $forma) {
                                        if($venda['formapgto'] == $forma['id']){
                                            echo "<input type='text' id='formaSele' name='formaSel' value='".$forma['descricao']."' class='form-control' readonly>";
                                        }
                                    }
                                }
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row"></div>
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-3">
                            <div class="limiteCompra">
                                    <label class="control-label" for="order_id">Melhor Dia de Pagamento</label>
                                    <input type="text" class="form-control" id="melhordiapagamentocarne" name="melhordiapagamentocarne">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="font-normal">Observações da venda</label>
                            <?php 
                            
                              if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                                $status_field = '';  
                              }else{
                                $status_field = 'readonly'; 
                              }
                            ?>
                              <textarea type="text" class="form-control" id="obs-venda" <?php echo $status_field ?> ><?php echo $venda['observacao'] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- 
    Etapa 2 do pedido
    Nessa parte é exibido os itens do pedido
-->
<form id="form-step2">
    <div id="order_step2" class="col-md-12 container">
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table" id="table1">
                                <thead>
                                <tr>
                                    <th>REF:</span></th>
                                    <th>Produto</span></th>
                                    <th>Número</span></th>
                                    <th>Valor/UN</td>
                                    <th>% Item</td>
                                    <th>Total</td>
                                    <th>QTD</th>
                                    <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody id="gridTableItem">
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php 
                    if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                        echo '<h4 class="modal-title" id="myModalLabel">Adicionar item na venda</h4>';
                    }else{
                        echo '<h4 class="modal-title" id="myModalLabel">Visualizar item da venda</h4>';
                    }
                ?>
            </div>
            <div class="modal-body">
                <div class="limiteCompra row">
                    <?php
                    if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                        ?>
                        <div class="col-md-4">
                            <label for="valordisponivel">Valor Disponível</label>
                            <input type="text" class="form-control" id="valordisponivelproduto" name="valordisponivel" placeholder="R$0,00" 
                                   value="<?php if (isset($venda)) echo $venda['valordisponivel']; ?>" required readonly>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="produtoNome">Produto</label>
                        <input type="text" class="form-control" id="produtoNome" name="produtoNome" placeholder="" 
                               value="" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="quantidade">Quantidade (UN)</label>
                        <?php 
                            if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' ')
                                $type = '';
                            else
                                $type = ' readonly';
                        ?>
                        <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Digite a Quantidade" 
                               value="<?php if (isset($venda)) echo $venda['quantidade']; ?>" autocomplete="off" onkeypress="return Onlynumbers(event);" 
                               required <?php echo $type ?> >
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="desconto_linha">Desconto (%)</label>
                        <?php 
                            if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' ')
                                $type = '';
                            else
                                $type = ' readonly';
                        ?>
                        <input type="text" class="form-control" id="desconto_linha" name="desconto_linha" placeholder="Digite o Desconto" 
                               value="<?php if (isset($venda)) echo $venda['desconto_linha']; ?>" autocomplete="off" onkeypress="return Onlynumbers(event);" 
                               required <?php echo $type ?> >
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <label for="valoritemroupa">Valor Item Roupa</label>
                        <input type="text" class="form-control" id="valoritemroupa" name="valoritemroupa" placeholder="R$ 0,00" 
                             value="<?php if (isset($venda)) echo $venda['valor']; ?>" required readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <label for="valortotal">Valor Total</label>
                        <input type="text" class="form-control" id="valor" name="valortotal" placeholder="R$ 0,00" 
                               value="<?php if (isset($venda)) echo $venda['valortotal']; ?>" required readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="fecharModalAddItem()">Fechar</button>
                <?php
                    if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
                    ?>
                        <button type="button" class="btn btn-primary" onclick="adicionarProduto()">Adicionar</button>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
</div>

<!-- 
    Etapa 3 do pedido
    Concluir a venda
-->
<?php
    date_default_timezone_set('America/Sao_Paulo');
    $dataatual = date('d/m/Y');
?>
<form id="form-step3">
    <div id="order_step3" class="col-md-12 container">
        <div id="section-data" class="col-sm-2">
            <div class="form-group">
                <label class="control-label" for="datavencimento">Data/Vencimento</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="text" class="form-control" id="data" name="datavencimento" placeholder="" 
                          value="<?php echo $dataatual; ?>" required>
                </div>
            </div>
        </div>
        
        <div id="section-valor-entrada" class="col-sm-4">
            <div class="form-group">
                
                <label class="control-label" for="valorentrada">Valor entrada</label>
                <input type="text" class="form-control monetary" id="valorentrada" name="valorentrada" placeholder="R$0,00" style="width: 206px;"
                    value="<?php if (isset($venda)) echo $venda['valorentrada']; ?>" required>
                    <br>
                <label class="control-label" for="idparcelamento">À Prazo/Cartão de crédito</label>
                <select class="form-control" name="idparcelamento" id="idparcelamento" 
                        required data-errormessage-value-missing="Selecione ..." style="width: 206px;" 
                        title="Escolha a quantidade de parcelas">
                </select>

            </div>
        </div>

        <!-- VENDA A VISTA APLICAR DESCONTO  -->
        <div id="section-desconto" class="col-sm-5">
            <div class="form-group">
                <label class="control-label" for="iddescontos">Descontos</label>
                <select class="form-control" name="iddescontos" id="iddescontos" 
                        required data-errormessage-value-missing="Selecione ..." style="width: 260px;" 
                        title="Escolha um desconto">
                        <option value='0'>
                            SEM DESCONTO N/A
                        </option>
                        <?php
                            foreach ($listaDescontos as $desconto) {
                                $selected = (isset($venda) && $venda['desconto'] == $desconto['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $desconto['id'] ?>' <?php echo $selected; ?> > 
                                            <?php echo $desconto['descricao'] . ' ' . $desconto['porcentagemdescontovenda'] . '%'; ?>
                                </option>
                        <?php } ?>
                </select>

            </div>
        </div>
        
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label" for="valordescontofinal">Valor Desconto Final</label>
                <input type="text" class="form-control monetary" id="descontofinal" name="descontofinal" placeholder="R$ 0,00" style="width: 206px;">
            </div>
        </div>
        <div class="col-lg-12">
            <h3><strong>RESUMO TOTAL</strong>: <span id="money-total" class="text-navy">R$ 0,00</span></h3>
        </div>
    </div>
</form>


<link rel="stylesheet" type="text/css" href="./protected/view/venda/vendaStyle.css">
<script>

    function init(){

        colaboradorSelecionado    = $("#idcolaboradorvendedor").val();
        clienteSelecionado        = $("#idcliente").val();
        formaPagamentoSelecionado = $("#idformapgto").val();

        if(!colaboradorSelecionado)    colaboradorSelecionado    = $("#colalboradorSel").val();
        if(!clienteSelecionado)        clienteSelecionado        = $("#clienteSel").val();
        if(!formaPagamentoSelecionado) formaPagamentoSelecionado = $("#formaSele").val();

        if(formaPagamentoSelecionado){
            formaObject               = formaPagamentoSelecionado.split(';');
            if(formaObject[1]=='R'){
                calculaLimiteDisponivel();
                toggleLimiteCompra(true);
            }
            else{
                calculaLimiteDisponivel();
                toggleLimiteCompra(false);
            }
        } 

        if($("#order_id").val()!==''){
            $('#opt2').removeClass('disabled');
            var response = requestHTTP('verificaConsistencia', clienteSelecionado, $("#order_id").val(), null);
            $("#container").html( response );
        }

        //Carrega os itens da venda
        if($('#order_id').val()!='') $("#gridTableItem").html(requestHTTP('gridTable', $('#order_id').val(), 'gridTableItem', null));

        $("#money-total").html(' Parcelado em ' + $('#idparcelamento option:selected').html() + '' );
    } 

    function initEvents(){

        $("#btn-salva-capa").click(function(){

            var cliente     = clienteSelecionado;
            var colaborador = colaboradorSelecionado;
            var formaPgto   = formaPagamentoSelecionado;
            var dataVenda   = $("#datavendareadonly").val();
            var dataVenci   = $("#data").val();
            var idvenda     = $("#order_id").val();
            var melhordiapagamentocarne = $("#melhordiapagamentocarne").val();
            var obs_venda   = $("#obs-venda").val();

            <?php 
                if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
            ?>
                    if(!cliente || cliente==0){
                        alert('Selecione um cliente!');
                        return;
                    }

                    if(!colaborador || colaborador==0){
                        alert('Selecione um vendedor!');
                        return;
                    }

                    if(!formaPgto || formaPgto==0){
                        alert('Selecione uma forma de pagamento!');
                        return;
                    }
            <?php
                }
            ?>

            $("#container").html(
                requestHTTP('salvaVendaCapa', null, null, "idvenda="+ idvenda + "&cliente="+cliente+"&colaborador="+colaborador+"&data="+dataVenda+"&formaPgto="+formaObject[0]+'&obs='+obs_venda+'&melhordiapagamentocarne='+melhordiapagamentocarne));

        });

        // Pesquisa produtos %%
        $("#btn-pesquisar").click(function(){ 

            pesquisa = $("#txt-pesquisar").val();
            if(pesquisa=='')
                pesquisa = '0';

            rebuildGrid(true);
            $("#gridTable").html(requestHTTP('gridTable', $('#order_id').val(), pesquisa, null));
            rebuildGrid(false);
        });

        // Encerra a venda (finaliza)
        $("#btn-finalizar").click(function(){ 

            var id_venda = $("#order_id").val();
            var dataVenc = $("#data").val();
            var qtd      = qtdParcelas;
            var forma    = formaObject[0];
            var cliente  = clienteSelecionado;
            var desconto = descontoSelecionado;
            var descontofinal = $("#descontofinal").val(); 
            var valor_parcela = $('#idparcelamento').val().split(";");

            var response = requestHTTP('finalizarVenda', null, null, 
                            "idvenda="+id_venda+"&"+
                            "datavencimento="+dataVenc+"&"+
                            "qtd="+qtd+"&"+
                            "forma="+forma+"&"+
                            "cliente="+cliente+"&"+
                            "desconto="+descontoSelecionado+"&"+
                            "descontofinal="+descontofinal+"&"+
                            "valor_parcela="+valor_parcela[1]+"&"+
                            "valor_entrada="+valorEntrada);
            $("#container").html(response); 
        });

        //Seleciona cliente
        $("#idcliente").on('change', function(){
            if(this.value == 0){
                if(clienteSelecionado!=0){
                    clienteSelecionado = 0;
                }
            }else{
                clienteSelecionado = this.value;

                if(formaObject){
                    if(formaObject[1]=='R'){
                        calculaLimiteDisponivel();
                        toggleLimiteCompra(true);
                    }else{
                        toggleLimiteCompra(false);
                    }
                }
            }
        });

        //Seleciona um vendedor
        $("#idcolaboradorvendedor").on('change', function(){
            if(this.value == 0){
                if(colaboradorSelecionado!=0){
                    colaboradorSelecionado = 0;
                }
            }else{
                colaboradorSelecionado = this.value;
            }
        });

        //Seleciona uma forma de pagamento
        $("#idformapgto").on('change', function(){
            if(this.value == 0){
                if(formaPagamentoSelecionado!=0){
                    formaPagamentoSelecionado = 0;
                }
            }else{
                formaPagamentoSelecionado = this.value;

                formaObject = formaPagamentoSelecionado.split(';');

                if(clienteSelecionado && formaObject[1]=='R'){
                    calculaLimiteDisponivel();
                    toggleLimiteCompra(true);
                }else{
                    //var old = clienteSelecionado;
                    //clienteSelecionado = 0;
                    //calculaLimiteDisponivel();
                    toggleLimiteCompra(false);
                    //clienteSelecionado = old
                }
            }
        });

        //
        $("#idparcelamento").on( "change", function( event ) {
            qtdParcelas = this.value.split(";",1);
            $("#money-total").html(' Parcelado em ' + $('#idparcelamento option:selected').html() + '' );
        });

        $("#iddescontos").on( "change", function( event ) {

            descontoSelecionado = $('#iddescontos option:selected').val();
            var response        = requestHTTP('aplicarDesconto', $('#order_id').val(), descontoSelecionado, 'desconto_linha=' + $('#desconto_linha').val());

            $("#money-total").html( response );
        });

        //Alterna para venda capa
        $("#option1").change(function(){
            $("#main-title").html("Pré-Venda");
            $("#form-step1").show();
            $("#form-step2").hide();
            $("#form-step3").hide();
            $('#btn-salva-capa').show();
            $("#btn-pesquisa-item").hide();
            $("#btn-finalizar").hide();
        });

        //Alterna para itens da venda
        $("#option2").change(function(){
            if(checkCapa()){
                $("#main-title").html("Itens da venda");
                $("#form-step1").hide();
                $("#form-step2").show();
                $("#form-step3").hide();
                $('#btn-salva-capa').hide();
                $("#btn-pesquisa-item").show();
                $("#btn-finalizar").hide();
                if(formaObject[1]=='D' && formaObject[2]=='N' && formaObject[3]=='N')
                    $('#section-desconto-linha').show();
                else{
                    $('#section-desconto-linha').hide();
                }
            }
        });

        //Alterna para concluir venda
        $("#option3").change(function(){
            if(checkCapa()){
                $("#main-title").html("Concluir venda");
                $("#form-step1").hide();
                $("#form-step2").hide();
                $("#form-step3").show();
                $('#btn-salva-capa').hide();
                $("#btn-pesquisa-item").hide();
                $("#btn-finalizar").show();
            }

            $('#section-valor-entrada').hide();
            $('#section-data').hide(); 
            $('#section-parcelamento').hide(); 
            $('#section-desconto').hide();
            $('#section-valor-entrada').hide(); 

            if(formaObject[3]=='S'){
                $('#section-data').show();
            }else{
                $('#section-data').hide(); 
            }

            if(formaObject[1]=='D' && formaObject[2]=='S'){
                $('#section-parcelamento').show();
            }else{
                $('#section-parcelamento').hide(); 
            }

            if(formaObject[1]=='D' && formaObject[2]=='N' && formaObject[3]=='N')
                $('#section-desconto').show();
            else{
                $('#section-desconto').hide();
            }

            if(formaObject[1]=='R' && formaObject[2]=='S'){
                $('#section-valor-entrada').show();
            }else{
                $('#section-valor-entrada').hide(); 
            }

            var response = requestHTTP('verificaConsistencia', clienteSelecionado, $("#order_id").val(), null);
            $("#container").html( response );
        });

        //Ao digitar no campo quantidade deve-se recalcular os preços/totais
        $("#quantidade").on( "keyup", function( event ) {

            if(produtoSelecionado){
                recalcularPrecos();
            }else{
                alert('Favor selecionar um produto!');
                $("#quantidade").val(null);
            }
        });

        //Ao digitar no campo desconto_linha deve-se recalcular os preços/totais
        $("#desconto_linha").on( "keyup", function( event ) {

            if(produtoSelecionado){

                if(this.value!=''){
                    descontoLinha = this.value;
                }else{
                    descontoLinha = 0.00;
                }

                recalcularPrecos();

            }else{
                alert('Favor selecionar um produto!');
                $("#desconto_linha").val("");
            }
        });

        //Ao digitar no campo valor de entrada deve-se recalcular o total
        $("#valorentrada").on( "keyup", function( event ) {
            valorEntrada = $("#valorentrada").val();
            recalculaTotal();
        });
    }

    //Função que reliza requisicoes para servidor (WS) via ajax
    function requestHTTP(service, param1, param2, p){  

        var hostname = 'http://'+window.location.hostname + window.location.pathname;
        var response = null;
        var params   = "";
        var uri      = null; 

        if(param1)
            params = '/' + param1;
        if(param2)
            params +='/' + param2;

        uri = hostname + '/' + service + params; 

        $.ajax({ 
            url : uri,
            type : 'POST',
            async: false,  
            data : p,
            dataType: 'text',
            timeout: 2900,    
            success: function(retorno){
                response = retorno;
            },
            error: function(erro){
                console.log("ERRO: "+erro);
            }       
        });
        return response;
    }

    function rebuildGrid(st){
        if(st){

            $('#table-item').DataTable().destroy();
        }else{

            $('#table-item').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'destroy': true,
                    'autoWidth': false,
                    'oLanguage': {
                    'sProcessing': 'Aguarde enquanto os dados são carregados ...',
                    'sLengthMenu': 'Mostrar _MENU_ registros por pagina',
                    'sZeroRecords': 'Nenhum registro correspondente ao criterio encontrado',
                    'sInfoEmtpy': 'Exibindo 0 a 0 de 0 registros',
                    'sInfo': 'Exibindo de _START_ a _END_ de _TOTAL_ registros',
                    'sInfoFiltered': '',
                    'oPaginate': {
                       'sFirst':    'Primeiro',
                       'sPrevious': 'Anterior',
                       'sNext':     'Próximo',
                       'sLast':     'Último'
                    }
                  } 
            });
        }
    }

    // Recalcula total da venda
    function recalculaTotal(){
        var response = requestHTTP('recalcularValores', $('#order_id').val(), valorEntrada, 'idcliente='+clienteSelecionado);
        $("#container").html( response );
    }

    // Recalcula os preços
    function recalcularPrecos(){

        var id_produto = produtoSelecionado;
        var quantidade = $('#quantidade').val();
        var desc_linha = descontoLinha;

        if(!id_produto)
            id_produto = 0;
        if(!quantidade)
            quantidade = 0;
        if(!desc_linha)
            desc_linha = 0;

        var response = requestHTTP('calculaPrecoProduto', id_produto, quantidade, 'desconto_linha=' + desc_linha);
        $("#valor").val( response );
    }

    // Exclui item da venda
    function removeProduto(id){
        var r = confirm("Deseja excluir este item da venda?");
        if (r == true) {
            produtoSelecionado = id;
            $("#valoritemroupa").val( requestHTTP('removeProduto', id, $('#order_id').val(), null) );
            recalcularPrecos();
            calculaLimiteDisponivel();
            $('#gridTableItem').html(requestHTTP('gridTable', $('#order_id').val(), 'gridTableItem', null));
            if(pesquisa=='')
                pesquisa = '0';
            rebuildGrid(true);
            $('#gridTable').html(requestHTTP('gridTable', $('#order_id').val(), pesquisa, null));
            rebuildGrid(false);

            var response = requestHTTP('verificaConsistencia', clienteSelecionado, $("#order_id").val(), null);
            $("#container").html( response );

        } else {
            //
        }
        
    }

    // Adiciona um produto na venda
    function adicionarProduto(){
        var dados = requestHTTP('adicionarItemVenda', 
                        $('#order_id').val(), 
                        null, 
                        "idvenda= " + $("#order_id").val() + "&" +
                        "idproduto=" + produtoSelecionado + "&" +
                        "idcliente=" + clienteSelecionado + "&" +
                        "qtd="+ $("#quantidade").val()  + "&" +
                        "desconto_linha=" + descontoLinha
                    );

        $("#container").html(dados);

        //verifica se já possui itens na venda
        var response = requestHTTP('verificaConsistencia', clienteSelecionado, $("#order_id").val(), null);
        $("#container").html( response );
    }

    // Seta o produto selecionado pelo botão CART na listagem de produtos pesquisados pelo usuário
    function changeProduto(id, nome){
        if(formaObject[1]=='R'){
            calculaLimiteDisponivel();
            toggleLimiteCompra(true);
        }
        else{
            calculaLimiteDisponivel();
            toggleLimiteCompra(false);
        }
        $("#modal-pesquisar-item").modal('hide');
        produtoSelecionado = id;
        $("#produtoNome").val(nome);
        $("#quantidade").val(null);
        $("#desconto_linha").val("");
        $("#valoritemroupa").val( requestHTTP('buscaPrecoProduto', produtoSelecionado, null) );
        recalcularPrecos();
        calculaLimiteDisponivel();
    }

    // Exibe o produto em modo de visualizao
    function viewProduto(id, nome, qtd){
        produtoSelecionado = id;
        $("#produtoNome").val(nome);
        $("#quantidade").val(qtd);
        $("#valoritemroupa").val( requestHTTP('buscaPrecoProduto', produtoSelecionado, null) );
        recalcularPrecos();
        calculaLimiteDisponivel();
    }

    function fecharModalAddItem(){
        if(pesquisa=='0') pesquisa = '';
        $("#txt-pesquisar").val(pesquisa);
        $("#myModal").modal('hide');
        
        <?php if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){ ?>
            $("#modal-pesquisar-item").modal('show');
        <?php }?>
    }

    //Função para calcular limite disponivel (somente quando selecionado forma de pagamento com tipo "declarar renda")
    function calculaLimiteDisponivel(){

        var order_id = 'null';

        if( $('#order_id').val() != ''){
            order_id = $('#order_id').val();
        }

        var valor = requestHTTP('calculaLimiteDisponivel', order_id, clienteSelecionado, null); 
      
        var tmp   = valor;

        if(tmp)
            tmp     = tmp.replace("R$ ", "").replace(",","");

        if(tmp<=0 && clienteSelecionado!=0){
            $("#valorlimitevendaproduto, #valordisponivelproduto").css('color', 'red');
        }else{
            $("#valorlimitevendaproduto, #valordisponivelproduto").css('color', '#444');
        }

        $("#valorlimitevendaproduto, #valordisponivelproduto").val( valor );
    }

    function toggleLimiteCompra(status){
        if(status){
            $(".limiteCompra").show();
        }else{
            $(".limiteCompra").hide();
        }
    }

    function checkCapa(){

        if(clienteSelecionado && formaPagamentoSelecionado && colaboradorSelecionado && $("#order_id").val() != ''){
            return true;
        }
        return false;
    }

    var produtoSelecionado        = null;
    var clienteSelecionado        = null;
    var colaboradorSelecionado    = null;
    var formaPagamentoSelecionado = null;
    var formaObject               = null;
    var descontoSelecionado       = 0.00;
    var descontoLinha             = 0.00;

    var valorEntrada              = 0.00;

    // Guarda pesquisa
    var pesquisa                  = null;
    // Guarda quantidade de parcelas
    var qtdParcelas               = 1;

    $(document).ready(function(){

        $("#form-step2").hide();
        $("#form-step3").hide();
        $("#btn-pesquisa-item").hide();
        $("#btn-finalizar").hide();
        toggleLimiteCompra(false);

        //Incializa a grid table (dataTable) dos itens da venda
        $('#table1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'destroy': true,
                'autoWidth': false,
                'oLanguage': {
                'sProcessing': 'Aguarde enquanto os dados são carregados ...',
                'sLengthMenu': 'Mostrar _MENU_ registros por pagina',
                'sZeroRecords': 'Nenhum registro correspondente ao criterio encontrado',
                'sInfoEmtpy': 'Exibindo 0 a 0 de 0 registros',
                'sInfo': 'Exibindo de _START_ a _END_ de _TOTAL_ registros',
                'sInfoFiltered': '',
                'oPaginate': {
                   'sFirst':    'Primeiro',
                   'sPrevious': 'Anterior',
                   'sNext':     'Próximo',
                   'sLast':     'Último'
                }
            } 
        });

        // Init application components
        //
        //
        //

        init();

        $('.btn-group').on("click", ".disabled", function(event) {
            event.preventDefault();
            return false;
        });

        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        initEvents();
    });
</script>