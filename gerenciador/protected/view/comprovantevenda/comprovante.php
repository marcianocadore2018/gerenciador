<?php
include '../../libs/verificaambiente.php';
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");
 
//P = Paisagem
//L = Retrato
$pdf = new FPDF("L", "pt", "A4");

$pdf->AddPage('P');
$pdf->Ln(3);

//Esse valor vai vir através do GET
$idvenda = $_GET['id'];

//Contorno Margens do relatório
$pdf->Line(585, 10, 10, 10);
$pdf->Line(10, 825, 10, 10);
$pdf->Line(585, 825, 585, 10);
$pdf->Line(585, 825, 10, 825);

//Contorno do logo Atlanta
$pdf->Line(135, 20, 15, 20);
$pdf->Line(15, 97, 15, 20);
$pdf->Image('../../../includes/imagens/logomarca.jpg', +20,30,110);
$pdf->Line(135, 97, 15, 97);
$pdf->Line(135, 97, 135, 20);

//Contorno do Endereço
$pdf->Line(450, 20, 145, 20);
$pdf->Line(145, 97, 145, 20);
$pdf->Line(450, 97, 145, 97);
$pdf->Line(450, 97, 450, 20);

//Contorno da Data de Venda
$pdf->Line(460, 20, 578, 20);
$pdf->Line(460, 97, 460, 20);
$pdf->Line(460, 97, 578, 97);
$pdf->Line(578, 97, 578, 20);

//Contorno Referente aos Dados do Cliente
$pdf->SetFont('Arial', 'B', 10);
$pdf->Line(579, 225, 15, 225);
$pdf->Line(15, 225, 15, 115);
$pdf->Line(579, 225, 579, 115);
$pdf->setFillColor(230,230,230);

//Contorno Referente aos Pagamentos
$pdf->Line(30, 10, 10, 10);
$pdf->Line(10, 30, 10, 10);
//$pdf->Line(579, 380, 15, 380);
//$pdf->Line(15, 380, 15, 230);
//$pdf->Line(579, 380, 579, 230);

//SQL REFERENTE AOS ENDEREÇOS DA LOJA ATLANTA
$query = "select lo.cnpj,
                 reg.endereco,
                 reg.cidade || ' - ' || est.uf as cidadeestado,
                 reg.cep,
                 col.nome as nomecolaborador
            from venda v
           inner join loja lo
              on v.idloja = lo.id
           inner join registropessoa reg
              on lo.idregistropessoa = reg.id
           inner join estado est
              on reg.idestado = est.id
            left join colaboradores col
              on v.idcolaboradorvendedor = col.id
           where v.id = $idvenda";
    $result = pg_query($query);
    
 while ($consulta = pg_fetch_assoc($result)) {
        $cnpj = $consulta["cnpj"];
        $endereco = $consulta["endereco"];
        $cidadeestado = $consulta["cidadeestado"];
        $cep = $consulta["cep"]; 
        $nomecolaborador = $consulta["nomecolaborador"];
 }
 
 //SQL REFERENTE A DATA E HORA DA VENDA
  $queryvenda = "select ven.id as numerotransacao,
                        (to_char(ven.datavenda, 'dd/MM/yyyy') || ' - ' || to_char(ven.horavenda::time, 'HH24:MI')) as datahoravencimento,
                        co.nome as nomevendedor
                   from venda ven
                  inner join colaboradores co
                     on ven.idcolaboradorvendedor = co.id
                  where ven.id = $idvenda";
  $resultvenda = pg_query($queryvenda);
    
 while ($consultavenda = pg_fetch_assoc($resultvenda)) {
        $nomevendedor = $consultavenda["nomevendedor"];
        $numerotransacao = $consultavenda["numerotransacao"];
        $datahoravencimento = $consultavenda["datahoravencimento"];
 }

 //CONSULTA ID DO CLIENTE - CLIENTE RÁPIDO
 $queryconsultaclientevenda = "select case when cli.id is not null then
                                            cli.id
                                          else
                                            rap.id
                                        end as idclientevenda
                                   from venda ven
                                   left join cliente cli
                                     on ven.idcliente = cli.id
                                   left join clienterapido rap
                                     on ven.idcliente = rap.id
                                  where ven.id = $idvenda";
 $resultconsultaclientevenda = pg_query($queryconsultaclientevenda);
 while ($consultaclientevenda = pg_fetch_assoc($resultconsultaclientevenda)) {
        $idclientevenda = $consultaclientevenda["idclientevenda"];
 }
 
$pdf->SetFont('arial', 'B', 14);
$pdf->Cell(550, 7, utf8_decode('Loja Atlanta Jeans'), 0, 0, 'C');
$pdf->SetFont('arial', 'B', 11);
$pdf->Cell(-70, 7, utf8_decode('Vendas'), 0, 0, 'R');
$pdf->SetFont('arial', '', 10);
$pdf->Ln();$pdf->Ln();
$pdf->Cell(160, 7, utf8_decode('CNPJ: '), 0, 0, 'R');
$pdf->Cell(90, 7, utf8_decode($cnpj), 0, 0, 'R');
//Referente a Venda
$pdf->Cell(300, 7, utf8_decode('Data / Hora Transação: '), 0, 0, 'R');
$pdf->Ln();$pdf->Ln();
$pdf->Cell(177, 7, utf8_decode('Endereço: '), 0, 0, 'R');
$pdf->Cell(100, 7, utf8_decode($endereco), 0, 0, 'L');
$pdf->Cell(260, 7, utf8_decode($datahoravencimento), 0, 0, 'R');
$pdf->Ln();$pdf->Ln();

$pdf->Cell(166, 7, utf8_decode('Cidade: '), 0, 0, 'R');
$pdf->Cell(83, 7, utf8_decode($cidadeestado), 0, 0, 'R');

$pdf->Cell(80, 7, utf8_decode('CEP: '), 0, 0, 'R');
$pdf->Cell(50, 7, utf8_decode($cep), 0, 0, 'R');
$pdf->Cell(130, 7, utf8_decode('Nº Transação: '), 0, 0, 'R');
$pdf->Cell(25, 7, utf8_decode($numerotransacao), 0, 0, 'R');
$pdf->Ln();$pdf->Ln();
$pdf->Cell(493, 7, utf8_decode('Nº Cliente: '), 0, 0, 'R');
$pdf->Cell(35, 7, utf8_decode($idclientevenda), 0, 0, 'R');


//SQL CONSULTA CLIENTE
$queryconsultacliente = "  SELECT vda.id,
         CASE WHEN clir.nome is null THEN cli.nome 
        ELSE clir.nome
     END as nomecliente,
         CASE WHEN reg.endereco is null THEN 'Endereço não informado'
           ELSE reg.endereco
    END as endereco,
    CASE WHEN reg.cep is null THEN 'CEP não informado'
           ELSE reg.cep
    END as cep,
    CASE WHEN reg.email is null THEN 'E-mail não informado'
           ELSE reg.email
    END as email,
    CASE WHEN cli.cpf is null THEN 'CPF não informado'
           ELSE cli.cpf
    END as cpf,
    CASE WHEN reg.telefone is null THEN 'Telefone não informado'
           ELSE reg.telefone
    END as contato,
    CASE WHEN cli.rg is null THEN 'RG não informado'
           ELSE cli.rg
    END as rg,
    CASE WHEN reg.bairro is null THEN 'Bairro não informado'
           ELSE reg.bairro
    END as bairro,
    CASE WHEN cli.celular is null THEN 'Celular não informado'
           ELSE cli.celular
    END as celular,
    CASE WHEN reg.cidade is null THEN 'Cidade não informada'
           ELSE reg.cidade
    END as cidade,
    CASE WHEN reg.cidade is not null THEN (
          select est.uf
            from estado est
           inner join registropessoa re
              on est.id = re.idestado
           inner join cliente clien
              on clien.idregistropessoa = re.id
           inner join venda ved
              on ved.idcliente = clien.id
           where ved.id = $idvenda)
         ELSE 'UF não informado'
    END as uf
  FROM venda vda
  LEFT JOIN      cliente cli
    ON cli.id = vda.idcliente
 INNER JOIN loja loj
    ON loj.id = vda.idloja
 INNER JOIN colaboradores col
    ON col.idloja = loj.id
   AND col.id = vda.idcolaborador
  LEFT JOIN clienterapido clir
    ON vda.idcliente = clir.id
  LEFT JOIN registropessoa reg
    ON cli.idregistropessoa = reg.id
  WHERE vda.id = $idvenda";
$resultconsultacliente = pg_query($queryconsultacliente);
while ($consultacliente = pg_fetch_assoc($resultconsultacliente)) {
       $nomecliente = $consultacliente["nomecliente"];
       $endereco = $consultacliente["endereco"];
       $cidade = $consultacliente["cidade"];
       $cep = $consultacliente["cep"];
       $email = $consultacliente["email"];
       $cpf = $consultacliente["cpf"];
       $contato = $consultacliente["contato"];
       $rg = $consultacliente["rg"];
       $bairro = $consultacliente["bairro"];
       $estado = $consultacliente["uf"];
       $celular = $consultacliente["celular"];
}

//CABEÇALHO DO RELATÓRIO
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(20);
$pdf->Ln(20);

//TÍTULO DADOS DO CLIENTE
$pdf->Cell(-14, 15, utf8_decode(''), 0, 0, 'C', 10);
$pdf->Cell(565, 15, utf8_decode('Dados do Cliente'), 1, 1, 'C', 10);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(110, 15, utf8_decode('Nome / Razão Social: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(250, 15, utf8_decode($nomecliente), 0, 1, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(110, 15, utf8_decode('Endereço: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(200, 15, utf8_decode($endereco), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('Bairro: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(220, 18, utf8_decode($bairro), 0, 1, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(50, 15, utf8_decode('CEP: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(180, 15, utf8_decode($cep), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('Cidade: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(150, 15, utf8_decode($cidade), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('Estado: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(220, 15, utf8_decode($estado), 0, 1, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(50, 15, utf8_decode('E-mail: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(180, 15, utf8_decode($email), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('Telefone: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(130, 15, utf8_decode($contato), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('Celular: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(220, 15, utf8_decode($celular), 0, 1, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(50, 15, utf8_decode('CPF: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(180, 15, utf8_decode($cpf), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(50, 15, utf8_decode('RG: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(180, 15, utf8_decode($rg), 0, 0, 'L');

$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(80);
$pdf->Ln(45);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('Informações referente ao pagamento'), 1, 1, 'C', 10);
$pdf->SetFont('Arial', 'B', 10);

/*Informações referente ao Tipo de Pagamento*/
//Contorno Informações Referente ao Tipo de Pagamento
//Verifica qual é o tipo da venda 

$querytipovenda = "select tipo.descricao as tipovenda,
                          tipo.id as idtipovenda,
                          to_char(vda.datavencimento, 'dd/MM/yyyy') as datavencimento
                     from venda vda
                    inner join tipopagamento tipo
                       on cast(vda.formapgto as int) = tipo.id
                    where vda.id = $idvenda";
$resulttipovenda = pg_query($querytipovenda);
    
 while ($consultatipovenda = pg_fetch_assoc($resulttipovenda)) {
        $tipopavenda = $consultatipovenda["tipovenda"];
        $idtipovenda = $consultatipovenda["idtipovenda"];
        $datavencimento = $consultatipovenda["datavencimento"];
 }
 
/*ESTA PARTE SERÁ RESPONSÁVEL PELAS CONSULTAS DOS ITENS, DESCONTOS, PORCENTAGENS DE VENDAS
  MOTIVO DESTA MUDANÇA FOI DE CONCENTRAR AS CONSULTAS EM UM SÓ LUGAR, POSSIBILITANDO UMA FORMA MAIS FLEXÍVEL 
  PARA INFORMAR OS VALORES EM QUALQUER LUGAR DO RELATÓRIO REFERENTE AOS ITENS E VALORES DOS PRODUTOS
 * Desenvolvedor: Marciano Luis Cadore
 * Data Modificação: 10/08/2017 */
 
 //INFORMAÇÃO REFERENTE AS PORCENTAGENS DE DESCONTOS
$aplicar_desconto = 
          " ,(
            SELECT 
              COALESCE(porcentagemdescontovenda / 100) as desconto,
              porcentagemdescontovenda desconto_porcento
            FROM 
              descontovenda, 
              venda v
            WHERE descontovenda.id = v.desconto
            AND v.id = $idvenda
            ) b";
$mostra_desconto = "'R$ ' || LTRIM(to_char( valor_total_item - (valor_total_item * desconto), '9G999G990D99')) as desconto, 
        desconto_porcento";

$verifica_desconto  = "SELECT count(*) FROM venda WHERE desconto <> 0 AND id = $idvenda";

$resultado          = pg_query($verifica_desconto);

while ($resultado = pg_fetch_assoc($resultado)) {
    if($resultado['count']=='0'){
        $aplicar_desconto = "";
        $mostra_desconto  = "'R$ 0.00' as desconto";
    }
}

//SQL REFERENTE AOS ITENS DA VENDA
$queryitensprodutos = "SELECT
      vda_id,
      quantidade,
      LEFT(descricao || ' - ' || numerotamanho, 50) as descricao,
      'R$ ' || LTRIM(to_char(valor_item, '9G999G990D99')) as valoritem,
      LTRIM(to_char(valor_total_item, '9G999G990D99')) as valor,
      valor_entrada,
      desconto_linha,
      $mostra_desconto
                FROM
                (
                    SELECT
                      COALESCE(item.valortotal,0.00) valor_total_item,
                      case when cast(p.numeroproduto as text) is not null then
				cast(p.numeroproduto as text)
				else 
				p.tamanho
		      end as numerotamanho,
		      (p.referencia || ' - ' || p.nomeproduto) as descricao,
                      item.quantidade as quantidade,
                      v.id as vda_id,
                      item.valoritemroupa as valor_item,
                      v.valor_entrada as valor_entrada,
                      item.desconto_linha
                  FROM 
                    itensproduto item, 
                    venda v,
                    produto p
                  WHERE 
                    v.id = $idvenda
                  AND 
                    item.idvenda = v.id
                  AND 
                    item.idproduto = p.id
                ) a
                    $aplicar_desconto
                ;";
$resultitensprodutos = pg_query($queryitensprodutos);

$resultvaloresitensprodutos = pg_query($queryitensprodutos);
while ($resultvaloresitensprodutos = pg_fetch_assoc($resultvaloresitensprodutos)) {
        $valordesconto_porcento   = ltrim($resultvaloresitensprodutos["desconto_porcento"]);
}

//Consulta Valor Total a Vista
$queryvalortotalvendaavista = "SELECT
                      'R$ ' || LTRIM(to_char( valor_total_item - (valor_total_item * desconto) - descontofinal, '9G999G990D99')) as valorfinalvendaavista
                      
                                FROM
                                (
                                    SELECT
                                      COALESCE(SUM(item.valortotal),0.00) valor_total_item,
                                      v.valor_entrada as valor_entrada,
                                      v.descontofinal as descontofinal
                                  FROM 
                                    itensproduto item, 
                                    venda v,
                                    produto p
                                  WHERE 
                                    v.id = $idvenda
                                  AND 
                                    item.idvenda = v.id
                                  AND 
                                    item.idproduto = p.id
                                  GROUP BY v.valor_entrada, descontofinal
                                ) a
                                     ,(
            SELECT 
              COALESCE(porcentagemdescontovenda / 100) as desconto,
              porcentagemdescontovenda desconto_porcento
            FROM 
              descontovenda, 
              venda v
            WHERE descontovenda.id = v.desconto
            AND v.id = $idvenda
            ) b";

$resultvendaavista = pg_query($queryvalortotalvendaavista);
    
 while ($consultavendaavista = pg_fetch_assoc($resultvendaavista)) {
        $valorvendaavista = $consultavendaavista["valorfinalvendaavista"];
 }

$cabecalho = true;

//INFORMAÇÕES REFERENTES AS PARCELAS
$querybuscaparcelas = "select to_char(pa.datavencimentoparcela, 'dd/MM/yyyy') as datavencimentoparcela,
                              LTRIM(to_char(pa.valorparcelas, '9G999G990D99')) as valorparcelas,
                              numeroparcela,
                              case when statusparcela = 'PE' then
                                   'Pendente'
                                else
                                   'Pago'
                              end as statusparcela
                        from parcelas pa
                       where pa.idvenda = $idvenda
                       order by pa.id asc;";
$resultbuscaparcelas = pg_query($querybuscaparcelas);

//INFORMAÇÕES REFERENTE AOS DESCONTOS E VALORES FINAIS DA VENDA
$queryvalortotal = "SELECT
                      LTRIM(to_char(valor_total_item, '9G999G990D99')) as valortotal,
                      LTRIM(to_char(valor_total_item - valor_entrada - descontofinal, '9G999G990D99')) as valortotal_final,
                      LTRIM(to_char(valor_total_item - descontofinal, '9G999G990D99')) as valordescontototal,
                      LTRIM(to_char(valor_total_item - descontofinal, '9G999G990D99')) as valortotal_finalavista,
                      valor_entrada,
                      'R$ ' || LTRIM(to_char(descontofinal, '9G999G990D99')) as descontofinal,
                      $mostra_desconto
                                FROM
                                (
                                    SELECT
                                      COALESCE(SUM(item.valortotal),0.00) valor_total_item,
                                      v.valor_entrada as valor_entrada,
                                      v.descontofinal as descontofinal
                                  FROM 
                                    itensproduto item, 
                                    venda v,
                                    produto p
                                  WHERE 
                                    v.id = $idvenda
                                  AND 
                                    item.idvenda = v.id
                                  AND 
                                    item.idproduto = p.id
                                  GROUP BY v.valor_entrada, descontofinal
                                ) a
                                    $aplicar_desconto
                                ;";

$resultvalortotal = pg_query($queryvalortotal);
while ($consultavalortotal = pg_fetch_assoc($resultvalortotal)) {
        $valorentrada     = ltrim($consultavalortotal["valor_entrada"]);
        $valortotal       = ltrim($consultavalortotal["valortotal"]);
        $valortotal_final = ltrim($consultavalortotal["valortotal_final"]);
        $descontofinal    = ltrim($consultavalortotal["descontofinal"]);
        $total_desconto   = ltrim($consultavalortotal["desconto"]);
        $valordescontototal   = ltrim($consultavalortotal["valordescontototal"]);
        
        $valordescontototalfinal = str_replace("," , "" , $valordescontototal); // Depois tira a vírgula
        $valordescontoremovevirgula = str_replace("," , "" , $valordescontototalfinal);
        $valordescontoformatado = str_replace("." , "" , $valordescontoremovevirgula);
        
        if($valordescontoformatado >= 000 && $valordescontoformatado < 10000){
            $descontoproximacompra = '0% (Sem bônus)';
        }else if($valordescontoformatado >= 10000 && $valordescontoformatado < 20000){
            $descontoproximacompra = '10%';
        }else if($valordescontoformatado >= 20000 && $valordescontoformatado < 30000){
            $descontoproximacompra = '20%';
        }else if($valordescontoformatado >= 30000 && $valordescontoformatado < 40000){
            $descontoproximacompra = '30%';
        }else if($valordescontoformatado >= 40000 && $valordescontoformatado < 50000){
            $descontoproximacompra = '40%';
        }else if($valordescontoformatado >= 50000){
            $descontoproximacompra = '50%';
        }
}


//Formatar desconto final
$descontofinalsemcifrao = substr($descontofinal, 3);
$removevirgula =  str_replace(",", "", $descontofinalsemcifrao);
$descontofinalformatado = $removevirgula;

/*FINAL DOS SQLS, IMPORTANTE QUE DE AGORA EM DIANTE TODOS OS SQLS DO RELATÓRIO SEJAM ADICIONADOS NESTE ESPAÇO*/
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(100, 18, utf8_decode('Tipo de Pagamento: '), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(150, 18, utf8_decode($tipopavenda), 0, 0, 'L');
$pdf->SetFont('Arial', 'B', 10);

$pdf->Cell(190, 18, utf8_decode('Percentual Desconto Próxima Compra:'), 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(150, 18, utf8_decode($descontoproximacompra), 0, 1, 'L');

if($idtipovenda == 5){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(87, 18, utf8_decode('Data Vencimento:'), 0, 0, 'R');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(150, 18, utf8_decode($datavencimento), 0, 1, 'L');
}

//Verifica o tipo de pagamento, se for a carnê mostra as parcelas
if($idtipovenda == 2){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
    $pdf->Cell(150, 18, utf8_decode('Data Vencimento'), 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(100, 18, utf8_decode('Valor'), 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(100, 18, utf8_decode('Nº Parcela'), 0, 0, 'L');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(220, 18, utf8_decode('Status(Pago/Pendente)'), 0, 1, 'L');
}

    
 while ($consultabuscaparcelas = pg_fetch_assoc($resultbuscaparcelas)) {
        $datavencimentoparcela = $consultabuscaparcelas["datavencimentoparcela"];
        $valorparcelas = $consultabuscaparcelas["valorparcelas"];
        $numeroparcela = $consultabuscaparcelas["numeroparcela"];
        $statusparcela = $consultabuscaparcelas["statusparcela"]; 
        
        //Valores Referente as Parcelas
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(130, 10, $datavencimentoparcela, 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(100, 10, 'R$ ' . $valorparcelas, 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(50, 10, $numeroparcela, 0, 0, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(80, 10);
        $pdf->Cell(220, 10, $statusparcela, 0, 0, 'L');
        $pdf->ln();
 }
 
//Informações Referente aos Produtos e Valores
while($consulta = pg_fetch_assoc($resultitensprodutos)) {

    if($cabecalho){
      $pdf->SetFont('Arial', 'B', 10);
      $pdf->Ln(20);
      $pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
      $pdf->setFillColor(230,230,230);
      $pdf->Cell(565, 14, utf8_decode('Descrição Detalhada Itens'), 1, 1, 'C', 10);
        //Cabeçalho referente aos produtos
      $pdf->SetFont('Arial', 'B', 10);
      $pdf->Cell(80);
      $pdf->setFillColor(230,230,230);
      $pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
      $pdf->Cell(295, 15, utf8_decode('Descrição'), 1, 0, 'L', 10);
      $pdf->Cell(70, 15, utf8_decode('Quantidade'), 1, 0, 'C', 10);
      $pdf->Cell(80, 15, utf8_decode('Valor Unitário'), 1, 0, 'C', 10);

      $pdf->Cell(60, 15, ltrim(utf8_decode('Total/Item')), 1, 0, 'C', 10);
      $pdf->Cell(60, 15, utf8_decode('Desc. Item'), 1, 0, 'C', 10);

      $cabecalho = false;
      $pdf->ln();
    }

    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
    $pdf->Cell(295, 15, utf8_decode($consulta['descricao']), 1, 0, 'L');
    $pdf->Cell(70, 15, utf8_decode($consulta['quantidade']), 1, 0, 'C');
    $pdf->Cell(80, 15, utf8_decode($consulta['valoritem']), 1, 0, 'C');
    $pdf->Cell(60, 15, ltrim('R$ '.$consulta['valor']), 1, 0, 'C');

    $pdf->Cell(60, 15, ltrim('% '.$consulta['desconto_linha']), 1, 1, 'R');
}

//Valor total
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(361, 15, utf8_decode(''), 0, 0, 'r');
$pdf->Cell(100, 15, utf8_decode('VALOR-TOTAL:'), 1, 0, 'R');
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(90, 15, 'R$ ' . utf8_decode($valortotal), 1, 1, 'R');
$pdf->Cell(490, 15, utf8_decode(''), 0, 0, 'C');

$pdf->ln(0);

//Valor final total
if($total_desconto!='R$ 0.00'){
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(361, 15, utf8_decode(''), 0, 0, 'C');
  $pdf->Cell(100, 15, utf8_decode('DESC-PEDIDO:'), 1, 0, 'R');
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(90, 15, '% ' . $valordesconto_porcento, 1, 1, 'R');
  $pdf->Cell(490, 15, utf8_decode(''), 0, 0, 'C');
  
  $pdf->ln(0);
  
  if($descontofinalformatado != '000'){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(361, 15, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(100, 15, utf8_decode('DESC. FINAL:'), 1, 0, 'R');
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(90, 15, utf8_decode($descontofinal), 1, 1, 'R');
    $pdf->Cell(490, 15, utf8_decode(''), 0, 0, 'C');
  }
  $pdf->ln(0);
  
  //Total Venda a Vista 25/08/2017
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(361, 15, utf8_decode(''), 0, 0, 'C');
  $pdf->Cell(100, 15, utf8_decode('TOTAL-GERAL:'), 1, 0, 'R');
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(90, 15, utf8_decode($valorvendaavista), 1, 1, 'R');
  $pdf->Cell(490, 15, utf8_decode(''), 0, 0, 'C');
}else{

  if($valorentrada>0){
    //Valor entrada
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(361, 18, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(100, 18, utf8_decode('VALOR-ENTRD:'), 1, 0, 'R');
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(90, 18, 'R$ ' . utf8_decode($valorentrada), 1, 1, 'R');
    $pdf->Cell(490, 18, utf8_decode(''), 0, 0, 'C');


    $pdf->ln(0);
  }

  if($descontofinalformatado != '000'){
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(361, 15, utf8_decode(''), 0, 0, 'C');
    $pdf->Cell(100, 15, utf8_decode('DESC. FINAL:'), 1, 0, 'R');
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(90, 15, utf8_decode($descontofinal), 1, 1, 'R');
    $pdf->Cell(490, 15, utf8_decode(''), 0, 0, 'C');
   
  }
  
  $pdf->ln(0);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(361, 18, utf8_decode(''), 0, 0, 'C');
  $pdf->Cell(100, 18, utf8_decode('TOTAL-GERAL:'), 1, 0, 'R');
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(90, 18, 'R$ ' . utf8_decode($valortotal_final), 1, 1, 'R');
  $pdf->Cell(490, 18, utf8_decode(''), 0, 0, 'C');
}

// Rodapé
//Nome do Vendedor
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(80);
$pdf->Ln(20);
$pdf->Cell(400, 8, utf8_decode(''), 0, 0, 'C');
$pdf->Cell(40, 8, utf8_decode('Vendedor:'), 0, 0, 'C');
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(122, 8, utf8_decode($nomecolaborador), 0, 1, 'L');

date_default_timezone_set('America/Sao_Paulo');
$data = date("d/m/Y-H:i");
$pdf->Output("comprovante-". $data .".pdf", "D");

pg_close($conexao);