<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Informação Adicional Colaborador</div>
            <div class="panel-body">
               <form action="<?php echo $acao; ?>" name="formInformacaoadicionalcolaborador" id="formInformacaoadicionalcolaborador" method="POST" class="form" role="form">
                    <?php
                        date_default_timezone_set('America/Sao_Paulo');
                        $dataCadastro = date('d/m/Y');
                    ?>
                    <div class="row">
                        <div class="col-md-1">
                            <label for="id">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly="true" 
                                   value="<?php if (isset($informacaoadicionalcolaborador)) echo $informacaoadicionalcolaborador['id']; ?>">
                        </div>
                    </div>
                   <div class="row">
                        <div class="col-md-9">
                            <label for="idcolaborador">Colaborador</label>
                                <select class="form-control" name="idcolaborador" id="idcolaborador" required data-errormessage-value-missing="Selecione o Colaborador">
                                <option value="">Selecione o Colaborador</option>>
                                <?php
                                foreach ($listaColaboradores as $colaboradores) {
                                    $selected = (isset($informacaoadicionalcolaborador) && $informacaoadicionalcolaborador['idcolaborador'] == $colaboradores['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $colaboradores['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $colaboradores['nome']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-md-9">
                            <label for="idcargo">Cargo</label>
                                <select class="form-control" name="idcargo" id="idcargo" required data-errormessage-value-missing="Selecione o Cargo">
                                <option value="">Selecione o Cargo</option>>
                                <?php
                                foreach ($listaCargos as $cargos) {
                                    $selected = (isset($informacaoadicionalcolaborador) && $informacaoadicionalcolaborador['idcargo'] == $cargos['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $cargos['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $cargos['descricao']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-md-9">
                            <label for="idsetor">Setor</label>
                                <select class="form-control" name="idsetor" id="idsetor" required data-errormessage-value-missing="Selecione o Setor">
                                <option value="">Selecione o Setor</option>>
                                <?php
                                foreach ($listaSetores as $setores) {
                                    $selected = (isset($informacaoadicionalcolaborador) && $informacaoadicionalcolaborador['idsetor'] == $setores['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $setores['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $setores['descricao']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                   </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="admissao">Data Admissão</label>
                            <input type="text" class="form-control" id="data" name="admissao" placeholder="Digite a Data de Admissão" 
                                   value="<?php if (isset($informacaoadicionalcolaborador)) echo $dataCadastro; ?>" required>
                        </div>
                        <!--Se o Colaborador tiver o Status de demitido é necessário mostrar o campo abaixo -->
                        <script>
                            $(document).ready(function(){
                                $("#jus").hide();
                            });

                            function loadSituation(val){
                                if((val==="admitido")){
                                    $("#datademissao").attr("required", false);
                                    $("#mostrardata").fadeOut(500);
                                    $("#esconderexibicaodemissao").fadeOut(500);
                                    $("#mostradataautomatica").fadeIn(500);
                                }else if(val==="demitido"){
                                    $("#jus").fadeIn(500);
                                    $("#mostradataautomatica").fadeOut(500);
                                    $("#datademissao").attr("required", true);
                                    $("#esconderexibicaodemissao").fadeOut(500);
                                    $("#mostrardata").fadeIn(500);
                                }
                            }

                            function mostrardatademissao(){
                                $("#mostrardata").fadeIn(500);
                                $("#esconderexibicaodemissao").fadeOut(500);
                            }
                        </script>
                        <?php
                            if($_GET['acao'] == "novo"){ ?>
                                <div class="col-md-3" style="top: 20px;">
                                    <label for="situacaodemissao">Situação:</label>
                                    <input type="radio" name="situacaodemissao" id="situacaodemissao" checked="">Admitido
                                </div>        
                           <?php } else if($_GET['acao'] == "buscar"){ ?>
                                <div class="col-md-3" style="top: 20px;">
                                <?php  
                                    if($informacaoadicionalcolaborador['demissao'] != ""){
                                        $checkeddemitido = "checked";
                                        $checkedademitido = "";
                                    }else{
                                        $checkeddemitido = "";
                                        $checkedademitido = "checked";
                                    }
                                ?>
                                    <label for="situacaodemissao">Situação:</label>
                                    <input type="radio" name="situacaodemissao" id="situacaodemissao" value="0" <?php echo $checkedademitido;?> onclick="loadSituation('admitido')">Admitido
                                    <input type="radio" name="situacaodemissao" id="situacaodemissao" value="1" <?php echo $checkeddemitido;?> onclick="loadSituation('demitido')">Demitido
                                </div>
                                <?php
                                    if($informacaoadicionalcolaborador['demissao'] != ""){ ?>
                                        <div style="margin-top: 20px;" id="esconderexibicaodemissao">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="mostrardatademissao()">Exibir Data Demissão</button>
                                        </div>
                                    <?php }
                                ?>
                                <div class="col-md-3" id="mostrardata" style="display: none;">
                                    <label for="demissao">Data Demissão</label>
                                    <input type="text" class="form-control" id="datademissao" name="demissao" placeholder="Digite a Demissão" 
                                           value="<?php if (isset($informacaoadicionalcolaborador)) echo $informacaoadicionalcolaborador['demissao']; ?>">
                                </div>
                           <?php }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="salario">Salário</label>
                            <input type="text" class="form-control" id="valor" name="salario" placeholder="Digite o Salário" 
                                   value="<?php if (isset($informacaoadicionalcolaborador)) echo $informacaoadicionalcolaborador['salario']; ?>" required>
                        </div>
                        <div class="col-md-3">
                            <label for="comissao">Comissão</label>
                            <input type="text" class="form-control" id="valorcomissao" name="comissao" placeholder="Digite a Comissão Exp: Números" 
                                   value="<?php if (isset($informacaoadicionalcolaborador)) echo $informacaoadicionalcolaborador['comissao']; ?>">
                        </div>
                    </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>       
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formInformacaoadicionalcolaborador").validate({
        rules: {
            idcolaborador: {
                required: true
            },
            idcargo: {
                required: true
            },
            idsetor: {
                required: true
            },
            admissao: {
                required: true
            },
            demissao: {
                required: true
            },
            salario: {
                required: true
            }
        },
        messages: {
            idcolaborador: {
                required: "Por favor, Selecione o Colaborador"
            },
            idcargo: {
                required: "Por favor, Selecione o Cargo"
            },
            idsetor: {
                required: "Por favor, Selecione o Setor"
            },
            admissao: {
                required: "Por favor, informe a Data de Admissão"
            },
            demissao: {
                required: "Por favor, informe a Data de Demissão"
            },
            salario: {
                required: "Por favor, informe o Salário"
            }
        }
    });
</script>
