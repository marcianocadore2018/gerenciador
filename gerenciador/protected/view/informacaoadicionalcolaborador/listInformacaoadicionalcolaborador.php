<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Informação Adicional Colaborador</div>
            <div class="panel-body">
                <a href="index.php?controle=informacaoadicionalcolaboradorController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1200px">
                    <thead>
                    <th style="width: 250px">Colaborador</th>
                    <th style="width: 50px">Admissão</th>
                    <th style="width: 50px">Demissão</th>
                    <th style="width: 75px">Salário</th>
                    <th style="width: 75px">Comissão</th>
                    <th style="width: 200px">Cargo</th>
                    <th style="width: 200px">Setor</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecolaborador'];
                            echo '<td style="text-align: center">' . $item['admissao'];
                            echo '<td style="text-align: center">' . $item['demissao'];
                            echo '<td style="text-align: center">' . $item['salario'];
                            echo '<td style="text-align: center">' . $item['comissao'];
                            echo '<td>' . $item['descricaocargo'];
                            echo '<td>' . $item['descricaosetor'];
                            $id = $item['id'];
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=informacaoadicionalcolaboradorController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"informacaoadicionalcolaboradorController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>