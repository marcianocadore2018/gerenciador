<div class="col-md-12 col-offset-2" style="padding-left: 0px; padding-right: 0px;">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Cliente Rápido</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formClienterapido" id="formClienterapido" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <label for="nome">Nome do Cliente</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['nome']; ?>" required minlength="3">
                    </div>
                    <div class="col-md-5">
                        <label for="sobrenome">Sobrenome</label>
                        <input type="text" class="form-control" id="sobrenome" name="sobrenome" placeholder="Digite o Sobrenome" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['sobrenome']; ?>" required="" minlength="3">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <label for="telefone">Telefone</label>
                        <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Digite o Telefone" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['telefone']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="dataaniversario">Data Aniversário</label>
                        <input type="text" class="form-control" id="data" name="dataaniversario" placeholder="Digite a Data de Aniversário" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['dataaniversario']; ?>">
                    </div>
                </div>
                
                <?php
                    date_default_timezone_set('America/Sao_Paulo');
                    $dataCadastro = date('d/m/Y');
                ?>
                <input type=hidden name="datacadastro" value="<?php echo $dataCadastro; ?>">
                
                <div class="row">
                    <div class="col-md-4">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Digite o E-mail do Cliente" 
                               value="<?php if (isset($clienterapido)) echo $clienterapido['email']; ?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    jQuery.extend(jQuery.validator.messages, {
        email:"Por favor informe um email válido."
    });
    $("#formClienterapido").validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            sobrenome: {
                required: true,
                minlength: 3,
                maxlength: 100
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome do Cliente",
                minlength: "O Nome do Cliente deve ter pelo menos 3 caracteres",
                maxlength: "O Nome do Cliente deve ter no máximo 100 caracteres"
            },
            sobrenome: {
                required: "Por favor, informe o Sobrenome do Cliente",
                minlength: "O Sobrenome do Cliente deve ter pelo menos 3 caracteres",
                maxlength: "O Sobrenome do Cliente deve ter no máximo 100 caracteres"
            }
        }
    });
</script>