<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem de Permissão Colaborador Produtos</div>
            
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Tipo Colaborador</th>
                    <th>Cadastrar</th>
                    <th>Excluir</th>
                    <th>Alterar</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['tipocolaborador'];
                            echo '<td>' . $item['cadastrar'];
                            echo '<td>' . $item['deletar'];
                            echo '<td>' . $item['alterar'];
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);

                            echo "<td> <a href='index.php?controle=permissaocolaboradorProdutosController&acao=buscar&id=$idencriptografa '>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>