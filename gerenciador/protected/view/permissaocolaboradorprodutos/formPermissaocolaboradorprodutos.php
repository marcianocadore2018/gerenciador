<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Permissão Colaborador Produtos</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formPermissaocolaboradorprodutos" id="formPermissaocolaboradorprodutos" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($permissaocolaboradorproduto)) echo $permissaocolaboradorproduto['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="tipocolaborador">Tipo Colaborador</label> <br/>
                        <input type="radio" name="tipocolaborador" value="M" checked="" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['tipocolaborador'] == 'M'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Master
                                <input type="radio" name="tipocolaborador" value="C" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['tipocolaborador'] == 'C'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Colaborador
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="cadastrar">Cadastrar</label> <br/>
                        <input type="radio" name="cadastrar" value="S" checked="" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['cadastrar'] == 'S'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Sim
                                <input type="radio" name="cadastrar" value="N" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['cadastrar'] == 'N'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Não
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="excluir">Excluir</label> <br/>
                        <input type="radio" name="deletar" value="S" checked="" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['deletar'] == 'S'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Sim
                                <input type="radio" name="deletar" value="N" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['deletar'] == 'N'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Não
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="alterar">Alterar</label> <br/>
                        <input type="radio" name="alterar" value="S" checked="" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['alterar'] == 'S'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Sim
                                <input type="radio" name="alterar" value="N" 
                                    <?php if(isset($permissaocolaboradorproduto)) if ($permissaocolaboradorproduto['alterar'] == 'N'){echo 'checked';}else{$permissaocolaboradorproduto == null;} ?>> Não
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div