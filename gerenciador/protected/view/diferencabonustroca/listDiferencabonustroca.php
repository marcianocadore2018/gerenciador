<?php
    if(isset ($_GET['sucessmsmupdt']) == "YXR1YWxpemFkb2NvbXN1Y2Vzc28="){ ?>
        <div class="alert alert-success" >
            <b>Produto Alterado com Sucesso!</b>
        </div>
    <?php }
?> 

<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Diferença Bônus de Trocas</div>
            <div class="panel-body">
                <a href="index.php?controle=trocaprodutoController&acao=novo" title="Clique aqui para fazer uma nova Alteração de Produto" class="btn btn-primary" role="button">Alterar Novo Produto</a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 1500px">
                    <thead>
                    <th style="width: 200px;">Nome Cliente</th>
                    <th style="width: 200px;">Produto Trocado (Atual)</th>
                    <th style="width: 200px;">Produto Devolvido</th>
                    <th style="width: 120px;">Data Troca</th>
                    <th style="width: 120px;">Valor Diferença</th>
                    <th style="width: 200px;">Colaborador</th>
                    </thead>
                    <tbody>
                        <?php
                        $sqlvalortotal = pg_query("select trim(cast(sum(valordiferenca) as text), '-') as valortotal
                                                     from trocaproduto where statustroca = 'B';");
                        $resvalortotal   = pg_fetch_array ($sqlvalortotal);
                        $valortotal = $resvalortotal['valortotal'];
                        $formatarvalor = number_format($valortotal, 2, ',', '.');
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['nomecliente'];
                            echo '<td>' . $item['nomeprodutotrocado'];
                            echo '<td>' . $item['nomeprodutodevolvido'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datatroca'] . ' - ' . $item['horatroca'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' .'R$ ' . $item['valordiferenca'];
                            echo '<td>' . $item['nomecolaborador'];
                            echo '</tr>';
                        } ?>
                        <tr>
                        <td colspan="4" style="padding-left: 58%; background-color: #FFFACD"><b>Valor Total Bônus:</b>
                        <td colspan="2" style="padding-left: 2%; background-color: #FFFACD"><b>R$ <?php echo $formatarvalor; ?></b>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>