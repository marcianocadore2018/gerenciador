<div class="col-md-12 col-offset-2">
    <?php
        if(isset($_GET['erro'])){?>
        <div class="alert alert-warning" >
            <b>Por favor, informe pelo menos uma informação abaixo. Para ser possível buscar as parcelas do cliente!</b>
        </div>
        <?php }
    ?>
    <?php
        if(isset($_POST['cpf']) || isset($_POST['idvenda']) || isset($_POST['idcliente'])){
            $cpffiltro = $_POST['cpf'];
            $transacaofiltro = $_POST['idvenda'];
            $clientefiltro = $_POST['idcliente'];
        }else if(isset($_GET['cpf'])){
            $cpffiltro = $_GET['cpf'];
            $transacaofiltro = $_GET['transacaofiltro'];
        }else{
            $cpffiltro = '';
            $transacaofiltro = "";
            $clientefiltro = "";
        }
    ?>
    
    <div class="panel panel-primary">
        <div class="panel-heading">Filtrar Pagamento</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formPagamento" id="formPagamento" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-3">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Digite o CPF" value="<?php echo $cpffiltro;?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Cliente</label>
                            <select data-placeholder="Selecione um cliente" class="form-control selectpicker" tabindex="1" name="idcliente" id="idcliente" data-live-search="true">
                                <option value="">Selecione</option>
                                <?php
                                    foreach ($listaClientes as $clientes) {
                                        ?>
                                        <option data-tokens="<?php echo $clientes['nome']; ?>" value='<?php echo $clientes['id']; ?>' style="text-transform: uppercase"> 
                                                    <?php echo $clientes['nome']; ?>
                                        </option>
                                    <?php 
                                    }
                                    ?>
                            </select>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-3">
                        <label for="numerotransacao">Número Transação (Venda)</label>
                        <input type="text" class="form-control" id="idvenda" name="idvenda" placeholder="Digite o Número da Transação" value="<?php echo $transacaofiltro;?>">
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Filtrar Parcelas</button>
            </form>
        </div>
        
        <?php
            
            if($_POST != NULL || $_GET != NULL){
             
                if(isset($listaDados) != NULL){?>
                <div class="table-responsive">
                        <table class="table" id="example1" style="width: 100%;">
                            <thead>
                                <th>Cliente</th>
                                <th>Data Venda</th>
                                <th>Forma Pgto</th>
                                <th>Situação</th>
                                <th>Nº Parcela</th>
                                <th>Data Vencimento</th>
                                <th>Valor</th>
                                <th colspan="2" style='padding-left: 60px; padding-right: 0px; width: 78px;'>Comprovante</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($listaDados as $item) {
                                    echo '<tr>';
                                    echo '<td style="padding-left: 2%; padding-top: 14px;">' . $item['nomecliente'];
                                    echo '<td style="padding-left: 2%; padding-top: 14px;">' . $item['datavenda'];
                                    if($item['formapgto'] == 2){
                                       echo '<td style="padding-left: 21px; padding-top: 14px;"><span class="label label-warning">Carnê</span></td>';; 
                                    }
                                    if($item['statusparcela'] == 'PE'){
                                       echo '<td style="padding-left: 21px; padding-top: 14px;"><span class="label label-warning">Pendente</span></td>';; 
                                    }else if($item['statusparcela'] == 'PG'){
                                        echo '<td style="padding-left: 21px; padding-top: 14px;"><span class="label label-success">Pago</span></td>';;
                                    }
                                    echo '<td style="padding-left: 4%; padding-top: 14px;">' . $item['numeroparcela'];
                                    echo '<td style="padding-left: 2%; padding-top: 14px;">' . $item['datavencimentoparcela'];
                                    echo '<td style="padding-top: 20px;padding-left: 20px;width: 75px;">' . $item['valorparcelas'];
                                    $id = $item['id'];
                                    $cpf = $item['cpf'];
                                    $idvenda = $item['idvenda'];
                                    echo '<td>';
                                    if($item['statusparcela'] == 'PG'){
                                        echo "<a style='' href='index.php?controle=pagamentoController&acao=filtrarpagamento&idparcela=$id' disabled='' class='btn btn-success btn-sm'>Pago</a>";
                                    }else if($item['statusparcela'] == 'PE'){
                                        echo "<a style='' href='index.php?controle=pagamentoController&acao=filtrarpagamento&idparcela=$id&cpf=$cpf&transacaofiltro=$idvenda' class='btn btn-success btn-sm'>Pagar</a>";
                                    }
                                    echo '<td>';
                                    if($item['statusparcela'] == 'PG'){
                                        echo    "<a href='protected/view/comprovantevenda/comprovante.php?id=$idvenda' class='btn btn-xs btn-outline btn-default' title='Gerar comprovante'>";
                                        echo        "<span class='glyphicon glyphicon-list-alt'> </span>";
                                        echo    '</a>';
                                    }else if($item['statusparcela'] == 'PE'){
                                        echo    "<a href='protected/view/comprovantevenda/comprovante.php?id=$idvenda' disabled='' class='btn btn-xs btn-outline btn-default' title='Não é possível gerar o comprovante'>";
                                        echo    "<span class='glyphicon glyphicon-list-alt'> </span>";
                                        echo    '</a>';
                                    }
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
            <?php }else{?>
                    <div class="alert alert-danger">
                         Não existe parcelas para o filtro informado!
                    </div>
                <?php }
            }
            ?>
        </div>
</div>