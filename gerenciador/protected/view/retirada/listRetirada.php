<div id="fundo">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Listagem Retirada de Produtos</div>
            <div class="panel-body">
                <a href="index.php?controle=retiradaController&acao=novo">
                    <span class='glyphicon glyphicon-plus'> Adicionar</span>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Descrição</th>
                    <th>Data Cadastro</th>
                    <th>Quantidade</th>
                    <th>Código Peça</th>
                    <th>Data Devolução</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            echo '<tr>';
                            echo '<td>' . $item['descricao'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datacadastro'];
                            echo '<td style="padding-right: 0px; padding-left: 50px;">' . $item['quantidade'];
                            echo '<td style="padding-right: 0px; padding-left: 40px;">' . $item['codigopeca'];
                            echo '<td style="padding-right: 0px; padding-left: 28px;">' . $item['datadevolucao'];
                            $id = $item['id'];
                                                        
                            echo "<td> <a href='index.php?controle=retiradaController&acao=buscar&id=$id'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td> <a onclick='excluir(\"excluir\",\"retiradaController\",$id)' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>