<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro Retirada de Produto</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formRetirada" id="formRetirada" method="POST" class="form" role="form">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($retirada)) echo $retirada['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição" 
                               value="<?php if (isset($retirada)) echo $retirada['descricao']; ?>" required minlength="3">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="quantidade">Quantidade</label>
                        <input type="text" class="form-control" id="quantidade" name="quantidade" placeholder="Digite a Quantidade" 
                               value="<?php if (isset($retirada)) echo $retirada['quantidade']; ?>" required onkeypress="return Onlynumbers(event)">
                    </div>
                    <div class="col-md-4">
                        <label for="codigopeca">Código Peça</label>
                        <input type="text" class="form-control" id="codigopeca" name="codigopeca" placeholder="Digite o Código" 
                               value="<?php if (isset($retirada)) echo $retirada['codigopeca']; ?>" required onkeypress="return Onlynumbers(event)">
                    </div>
                </div>
                <?php
                    if(isset($_GET['id']) != null){?>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="datadevolucao">Data Devolução</label>
                                <input type="text" class="form-control" id="data" name="datadevolucao" placeholder="Digite a Data de Devolução" 
                                       value="<?php if (isset($retirada)) echo $retirada['datadevolucao']; ?>">
                            </div>
                        </div>
                    <?php }
                ?>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>
<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    $("#formRetirada").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            quantidade: {
                required: true
            },
            codigopeca: {
                required: true
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição da Retirada",
                minlength: "A Descrição da Retirada deve ter pelo menos 3 caracteres",
                maxlength: "A Descrição da Retirada deve ter no máximo 100 caracteres"
            },
            quantidade: {
                required: "Por favor, informe a Quantidade de Peça"
            },
            codigopeca: {
                required: "Por favor, informe o Código da Peça"
            }
        }
    });
</script>