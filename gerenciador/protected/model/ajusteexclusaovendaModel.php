<?php

class AjusteexclusaovendaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "SELECT 
                  vda.id,
                  vda.idcliente as idclientevenda,
                   (
                    SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto
                   ) as desconto_cabecalho,
       CASE WHEN clir.nome is null THEN cli.nome 
         ELSE clir.nome
  END as nomecliente,
       col.nome as nomecolaboradorvendedor,
       to_char(vda.datavenda, 'dd/MM/yyyy') as datavenda,
       to_char(vda.datavencimento, 'dd/MM/yyyy') as datavencimento,
       to_char(cast(vda.horavenda as time), 'HH24:MI:SS') as horavenda,
       loj.nomefantasia,
       vda.tipopagamento,
       coalesce((SELECT descricao FROM tipopagamento WHERE id = vda.formapgto),' ') as forma_desc,
       coalesce((SELECT tipo FROM tipopagamento WHERE id = vda.formapgto),'N') as tipo,
       coalesce((SELECT parcelar FROM tipopagamento WHERE id = vda.formapgto),'N') as parcelar,
       coalesce((SELECT data_vencimento FROM tipopagamento WHERE id = vda.formapgto),'N') as data_vencimento,
       vda.qtdparcelas,
       vda.formapgto,
       vda.observacao,
       (
    SELECT 
      concat('R$ ',to_char(
        sum(valortotal) - (sum(valortotal) * 
        COALESCE(( (SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto) / 100 ),0.00))- valor_entrada - descontofinal,'99999999999999999D99')
      ) as total 
    FROM 
      itensproduto 
    WHERE  
      idvenda = vda.id
       ) as total,
       concat('R$ ',to_char(((SELECT sum(valortotal) as total FROM itensproduto WHERE idvenda = vda.id) / vda.qtdparcelas)- valor_entrada,'99999999999999999D99')) as parcelado
       
  FROM venda vda
  LEFT JOIN      cliente cli
    ON cli.id = vda.idcliente
 INNER JOIN loja loj
    ON loj.id = vda.idloja
 INNER JOIN colaboradores col
    ON col.id = vda.idcolaboradorvendedor
 LEFT JOIN clienterapido clir
    ON vda.idcliente = clir.id
  ORDER BY DATE(vda.datavenda) DESC, cast(vda.horavenda as time) DESC;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        
        
        $dataexclusao = date('d/m/Y');
        $horaexclusao = date('H:m');
        $idvendedor = $_POST['idvendedor'];
        $idvenda = $_POST['idvenda'];
        $motivoexclusao = $_POST['motivoexclusao'];
        
        //CONSULTA IDCLIENTE - VENDA
        //DECRIPTAR VARIAVEL
        
        $idget = $_POST['idvenda'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $idvenda = str_replace("metodoget", "", $remover);
        
        $consultaidclientevenda = "select idcliente from venda where id = $idvenda";
        
        $sqlconsultaclientevenda = $this->bd->prepare($consultaidclientevenda);
        $sqlconsultaclientevenda->execute();
        if ($sqlconsultaclientevenda->rowCount() > 0) {
            foreach ($sqlconsultaclientevenda as $rs){
                $idconsultacliente = $rs["idcliente"];
            }
        }
        
        $sql = "insert into ajusteexclusaovenda(idvendedor, dataexclusao, horaexclusao, motivoexclusao, idvenda, idcliente)
                                        values ($idvendedor, '$dataexclusao', '$horaexclusao', '$motivoexclusao', $idvenda, $idconsultacliente)";
        
        unset($dados['idvendedor']);
        unset($dados['dataexclusao']);
        unset($dados['horaexclusao']);
        unset($dados['motivoexclusao']);
        unset($dados['idvenda']);
        unset($dados['idcliente']);
        $query = $this->bd->prepare($sql);
        
        
        if($query->execute($dados) == 1){
            //DELETE A VENDA
            $sql   = "DELETE FROM itensproduto WHERE idvenda = $idvenda";
            unset($dados['idvenda']);
            $queryexcluitensproduto = $this->bd->prepare($sql);
            $excluiritensprodutos = $queryexcluitensproduto->execute($dados);
            
            $sqlparcelas   = "DELETE FROM parcelas WHERE idvenda = $idvenda";
            unset($dados['idvenda']);
            $queryexcluiparcelas = $this->bd->prepare($sqlparcelas);
            $queryexcluiparcelas->execute($dados);
           
            //Consultar se o idvenda esta presente na tabela pagamentodiferencatroca
            //Excluir os dados da troca de produto
            $consultavendapagamentodiferenca = "select pag.id as idpagamentodiferenca 
                                                  from trocaproduto pro
                                                 inner join pagamentodiferencatroca pag
                                                    on pro.id = pag.idtrocaproduto
                                                 where pro.idvenda = $idvenda";
            $sqlconsultavendapagamentodiferenca = $this->bd->prepare($consultavendapagamentodiferenca);
            $sqlconsultavendapagamentodiferenca->execute();
            if ($sqlconsultavendapagamentodiferenca->rowCount() > 0) {
                foreach ($sqlconsultavendapagamentodiferenca as $rs){
                    $existevendanopagamentodiferencatroca = $rs["idpagamentodiferenca"];
                }
            }
           
            if($existevendanopagamentodiferencatroca > 0){
                $sqlpagamentodiferencatroca   = "DELETE FROM pagamentodiferencatroca WHERE id = $existevendanopagamentodiferencatroca";
                unset($dados['idvenda']);
                $queryexcluipagamentodiferenca = $this->bd->prepare($sqlpagamentodiferencatroca);
                $queryexcluipagamentodiferenca->execute($dados);
                if($queryexcluipagamentodiferenca->execute($dados) == 1){
                    $sqlexcluitrocaproduto   = "DELETE FROM trocaproduto WHERE idvenda = $idvenda";
                    unset($dados['idvenda']);
                    $queryexcluitrocaproduto = $this->bd->prepare($sqlexcluitrocaproduto);
                    $queryexcluitrocaproduto->execute($dados);
                }
            }
            
            //Executar somente a exclusão da troca do bônus
            //Verificar se o id da venda esta presenta na tabela trocaproduto
            $consultatrocaprodutobonus = "select count(*) as existe
                                            from trocaproduto pro
                                           where pro.idvenda = $idvenda";
            $sqlconsultatrocaprodutobonus = $this->bd->prepare($consultatrocaprodutobonus);
            $sqlconsultatrocaprodutobonus->execute();
            if ($sqlconsultatrocaprodutobonus->rowCount() > 0) {
                foreach ($sqlconsultatrocaprodutobonus as $rs){
                    $existetrocaprodutobonus = $rs["existe"];
                }
            }
            
            if($existetrocaprodutobonus > 0){
                $sqlexcluitrocaproduto   = "DELETE FROM trocaproduto WHERE idvenda = $idvenda";
                unset($dados['idvenda']);
                $queryexcluitrocaproduto = $this->bd->prepare($sqlexcluitrocaproduto);
                $queryexcluitrocaproduto->execute($dados);
            }
            
            if($excluiritensprodutos == 1){
              $sql   = "DELETE FROM venda WHERE id = $idvenda";
              
              unset($dados['idvenda']);
              $queryexcluirvenda = $this->bd->prepare($sql);
              $excluirvenda = $queryexcluirvenda->execute($dados);
              if($excluirvenda == 1){
                  return $queryexcluirvenda->execute($dados);
              }
            }
        }
    }
    
    public function buscar($id) {
        $sql = "select v.id as idvenda
                  from venda v
                 WHERE v.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));
    }

}