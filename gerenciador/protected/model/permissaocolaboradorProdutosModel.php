<?php

class PermissaocolaboradorprodutosModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    
    public function inserir(array $dados) {
        $sql = "INSERT INTO permissaocolaborador(cadastrar, deletar, alterar, tipocolaborador) "
                . " VALUES(:cadastrar, :deletar, :alterar, :tipocolaborador)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select id, 
                        CASE WHEN cadastrar='S' THEN 'Sim'
                              ELSE 'Não'
                        END as cadastrar,
                        CASE WHEN alterar='S' THEN 'Sim'
                              ELSE 'Não'
                        END as alterar, 
                        CASE WHEN deletar='S' THEN 'Sim'
                              ELSE 'Não'
                        END as deletar,
                        CASE WHEN tipocolaborador='M' THEN 'Master'
                              ELSE 'Colaborador'
                        END as tipocolaborador
                   from permissaocolaborador";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, cadastrar, alterar, deletar, tipocolaborador FROM permissaocolaborador WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $sql = "UPDATE permissaocolaborador SET cadastrar = :cadastrar, alterar = :alterar, deletar = :deletar, tipocolaborador = :tipocolaborador WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM permissaocolaborador WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
