<?php

class PorcentagemcrediarioclienteModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $porcentagem = $_POST['porcentagem'];
        $sql = "INSERT INTO porcentagemcrediariocliente(datacadastro, porcentagem) "
                . " VALUES('$dataCadastro', $porcentagem)";
       
        unset($dados['id']);
        unset($dados['datacadastro']);
        unset($dados['porcentagem']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT id, to_char(datacadastro,'dd/MM/yyyy') as datacadastro, porcentagem FROM porcentagemcrediariocliente order by datacadastro asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, to_char(datacadastro, 'dd/MM/yyyy') as datacadastro, porcentagem FROM porcentagemcrediariocliente WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $porcentagem = $_POST['porcentagem'];
        $sql = "UPDATE porcentagemcrediariocliente SET datacadastro = '$dataCadastro', porcentagem = '$porcentagem' WHERE id = :id";
        unset($dados['datacadastro']);
        unset($dados['porcentagem']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM porcentagemcrediariocliente WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
