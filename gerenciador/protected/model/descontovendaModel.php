<?php

class DescontovendaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricao = $_POST['descricao'];
        $porcentagemdescontovenda = $_POST['porcentagemdescontovenda'];
        
        //Verificar se a descrição ja esta cadastrada
        $sql   = "select count(*) as existe from descontovenda where descricao = '$descricao'";
        $res   = $this->bd->prepare($sql);
        $res->execute();
        if ($res->rowCount() > 0) {
            foreach ($res as $rs) {
                $existe = $rs["existe"];
            }
        }
        if($existe > 0){
            echo "<script>alert('Esta descrição já encontra-se cadastrado na base de dados! Favor informe outra descrição.');</script>";
        }else{
            $sql = "INSERT INTO descontovenda(descricao, porcentagemdescontovenda) "
                    . " VALUES('$descricao', $porcentagemdescontovenda)";
            unset($dados['id']);
            unset($dados['descricao']);
            unset($dados['porcentagemdescontovenda']);
            $query = $this->bd->prepare($sql);
            return $query->execute($dados);
        }
    }

    public function buscarTodos() {
        $sql = "SELECT id, descricao, porcentagemdescontovenda FROM descontovenda order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, descricao, porcentagemdescontovenda FROM descontovenda WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $porcentagemdescontovenda = $_POST['porcentagemdescontovenda'];
        
        $sql = "UPDATE descontovenda SET descricao = '$descricao', 
                                         porcentagemdescontovenda = $porcentagemdescontovenda 
                                   WHERE id = $id";
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['porcentagemdescontovenda']);
        $query = $this->bd->prepare($sql);
        if($query->execute($dados) == 1){
            return $query->execute($dados);
        }else{
            echo "<script>alert('Esta descrição já encontra-se cadastrado na base de dados! Favor informe outra descrição.');</script>";
        }
    }

    public function excluir($id) {
        $sql = "DELETE FROM descontovenda WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}