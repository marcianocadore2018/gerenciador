<?php

class TipopagamentoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $sql = "INSERT INTO tipopagamento(descricao, tipo, parcelar, data_vencimento) "
                . " VALUES(:descricao, :tipo, :parcelar, :data_vencimento)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT id, descricao, tipo, parcelar, data_vencimento FROM tipopagamento order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, descricao, tipo, parcelar, data_vencimento FROM tipopagamento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $sql = "UPDATE tipopagamento SET descricao = :descricao, tipo = :tipo, parcelar = :parcelar, data_vencimento = :data_vencimento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM tipopagamento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}