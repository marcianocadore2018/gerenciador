<?php

class ClienterapidoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $nome = $_POST['nome'];
        $sobrenome = $_POST['sobrenome'];
        $telefone = $_POST['telefone'];
        $datacadastro = $_POST['datacadastro'];
        $dataaniversario = $_POST['dataaniversario'];
        $email = $_POST['email'];
        if($dataaniversario == ''){
            $valoraniversario = 'null';
        }else{
            $valoraniversario = "'" . $dataaniversario . "'";
        }
        //Função para capturar a data
        $sql = "INSERT INTO clienterapido(nome, datacadastro, telefone, dataaniversario, email, sobrenome) "
                . " VALUES('$nome', '$datacadastro', '$telefone', $valoraniversario, '$email', '$sobrenome')";
        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['datacadastro']);
        unset($dados['telefone']);
        unset($dados['dataaniversario']);
        unset($dados['email']);
        unset($dados['sobrenome']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT id,
                    CASE WHEN sobrenome is null THEN nome 
                    ELSE (nome || ' ' || sobrenome) END as nome, 
                    telefone, 
                    to_char(datacadastro,'dd/MM/yyyy') as datacadastro, 
                    to_char(dataaniversario,'dd/MM/yyyy') as dataaniversario, 
                    email  
               FROM clienterapido 
              order by nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, nome, telefone, sobrenome, to_char(datacadastro,'dd/MM/yyyy') as datacadastro, to_char(dataaniversario,'dd/MM/yyyy') as dataaniversario, email FROM clienterapido WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $sobrenome = $_POST['sobrenome'];
        $telefone = $_POST['telefone'];
        $valortelefone = str_replace("(__)____-____", "", $telefone);
        $datacadastro = $_POST['datacadastro'];
        $dataaniversario = $_POST['dataaniversario'];
        $email = $_POST['email'];
        if($dataaniversario == ''){
            $valoraniversario = 'null';
        }else{
            $valoraniversario = "'" . $dataaniversario . "'";
        }
        
        $sql = "UPDATE clienterapido SET nome = '$nome',
                                         telefone = '$valortelefone',
                                         datacadastro = '$datacadastro',
                                         dataaniversario = $valoraniversario,
                                         email = '$email',
                                         sobrenome = '$sobrenome'
                                   WHERE id = $id";
        unset($dados['nome']);
        unset($dados['datacadastro']);
        unset($dados['telefone']);
        unset($dados['dataaniversario']);
        unset($dados['email']);
        unset($dados['sobrenome']);
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM clienterapido WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}