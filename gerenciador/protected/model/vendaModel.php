<?php

class VendaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    //TO DO ADICIONAR ITEM VENDA

    public function salvarCapa($dados) {
        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');

        $data = $dados['data'];
        //$data = date('d/m/Y');
        $hora = date('H:i:s');

        //Busca a seção do usuário logado
        $logincolaborador = $_SESSION['login'];
        $login = "SELECT id AS idcolaborador, idloja FROM colaboradores WHERE login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja = $rs["idloja"];
            }
        }

        if ($_POST['melhordiapagamentocarne'] != '') {
            $melhordiapagamentocarne = "'" . $_POST['melhordiapagamentocarne'] . "'";
        } else {
            $melhordiapagamentocarne = 'null';
        }
        if ($dados['idvenda'] == '') {
            $sql = "INSERT INTO venda (
                  idcliente, 
                  idcolaborador, 
                  idcolaboradorvendedor, 
                  datavenda, 
                  datavencimento,
                  idloja, 
                  tipopagamento,
                  qtdparcelas,
                  horavenda,
                  formapgto,
                  observacao,
                  melhordiapagamentocarne,
                  desconto,
                  valor_entrada)
                VALUES(
                  :cliente,
                  $idcolaborador,
                  :colaborador,
                  '$data',
                  :data,
                  $idloja,
                  '',
                  1,
                  '$hora',
                  :formaPgto,
                  :obs,
                  $melhordiapagamentocarne,
                  0,
                  0
                )";
            unset($dados['idvenda']);
            unset($dados['melhordiapagamentocarne']);
        } else {
            //vai cair aqui para atualizar o melhor dia pagamento carne
            if ($_POST['melhordiapagamentocarne'] != '') {
                $melhordiapagamentocarne = "'" . $_POST['melhordiapagamentocarne'] . "'";
            } else {
                $melhordiapagamentocarne = 'null';
            }
            $sql = "UPDATE venda SET idcliente = :cliente, 
                                     idcolaborador = $idcolaborador, 
                                     idcolaboradorvendedor = :colaborador, 
                                     datavenda = '$data', 
                                     datavencimento = :data, 
                                     idloja = $idloja, 
                                     tipopagamento = '', 
                                     qtdparcelas = 1, 
                                     horavenda = '$hora', 
                                     formapgto = :formaPgto, 
                                     desconto = 0, 
                                     observacao = :obs, 
                                     melhordiapagamentocarne = $melhordiapagamentocarne,
                                     valor_entrada = 0
                   WHERE id = :idvenda";
        }
        unset($dados['melhordiapagamentocarne']);
        $query = $this->bd->prepare($sql);
        if ($query->execute($dados)) {

            if (!isset($dados['idvenda'])) {
                $sql = "SELECT MAX(id) as id FROM venda";
                $sql = $this->bd->prepare($sql);
                $sql->execute();
                if ($sql->rowCount() > 0) {
                    foreach ($sql as $rs) {
                        $ultimo = $rs["id"];
                    }
                }
            } else {
                $ultimo = $dados['idvenda'];
            }
        } else {
            $ultimo = '';
        }

        return $ultimo;
    }

    # ----------------------------------------------------------------------------
    # CALCULA O LIMITE DISPONIVEL DO CLIENTE
    # @param total_renda                  total da renda do cliente
    # @param total_itens                  total da compra
    # @param limite_disponivel            limite disponivel para esse cliente
    # @param limite_disponivel_formatado  limite disponivel para impressao na view
    # ----------------------------------------------------------------------------

    public function calculaLimiteDisponivel($idvenda, $idcliente) {


        if ($idvenda == 'null' || $idvenda == '') {
            $and = " (v.tipopagamento = '1') ";
        } else {
            $and = " (v.tipopagamento = ' ' or v.tipopagamento = '1') ";
        }


        $sql = "SELECT
                  total_pago,
                  total_itens,
                  total_compra_itens,
                  total_renda,
                  (total_renda - total_itens + total_pago) as limite_disponivel,
                  to_char( (total_renda - total_itens + total_pago), 'L9G99G990D99') as limite_disponivel_formatado 
                FROM
                (
                  SELECT
                    COALESCE(sum(p.valorpago),0.00) as total_pago
                  FROM 
                    pagamentos p
                  WHERE 
                    p.idcliente = $idcliente
                ) a,
                (
                  SELECT 
                       COALESCE((SUM(rc.valorrenda) * (SELECT porcentagem FROM PORCENTAGEMCREDIARIOCLIENTE) /100),0.00) AS total_renda
                    FROM 
                       RENDACLIENTE rc 
                    WHERE 
                       rc.idcliente = $idcliente
                ) b,
                (
                    SELECT
                    COALESCE(SUM(item.valortotal-v.valor_entrada),0.00) as total_compra_itens
                  FROM 
                    itensproduto item, 
                    venda v
                  WHERE 
                    v.idcliente = $idcliente
                  AND 
                    v.id = $idvenda
                  AND 
                    item.idvenda = v.id
                ) c,
                (
                SELECT
                    COALESCE(SUM(item.valortotal),0.00) as total_itens
                  FROM 
                    itensproduto item, 
                    venda v,
                    tipopagamento tp
                  WHERE 
                    v.idcliente = $idcliente
                  AND 
                    item.idvenda = v.id
                  AND
                    v.formapgto = tp.id
                  AND
                    tp.tipo = 'R'
                  AND
                    $and
                ) d;";

        $query = $this->bd->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

    /*
      public function buscaLimiteGasto($idcliente, $idvenda){

      if(!$idvenda){
      $idvenda = "item.idvenda"; // use for not idvenda search
      }else{
      $idvenda = " $idvenda AND item.idvenda = v.id";
      }

      $sql = "SELECT SUM(item.valortotal) as total FROM itensproduto item, venda v WHERE v.idcliente = $idcliente  AND v.id = $idvenda";
      $sql = $this->bd->prepare($sql);
      $sql->execute();
      if ($sql->rowCount() > 0) {
      foreach ($sql as $rs) {
      $limiteGasto = $rs["total"];
      }
      }else{
      $limiteGasto = 0;
      }

      return $limiteGasto;
      }
     */

    public function verificaStep2($idcliente, $idvenda) {
        $sql = "SELECT v.tipopagamento FROM itensproduto item, venda v WHERE v.idcliente = $idcliente AND v.id = $idvenda AND item.idvenda = v.id";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            foreach ($sql as $rs) {
                $dados = array('possui_itens' => $sql->rowCount(), 'tipopagamento' => $rs['tipopagamento']);
            }
        } else {
            $dados = array('possui_itens' => 0);
        }

        return $dados;
    }

    public function verificaEstoqueItem($id, $qtd) {

        $sql = "SELECT 
            p.quantidade - COALESCE((SELECT SUM(i.quantidade) as quantidade FROM itensproduto i WHERE i.idproduto = p.id),0) as quantidade 
          FROM 
            produto p 
          WHERE p.id = $id";

        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            foreach ($sql as $rs) {
                $estoque_item = $rs["quantidade"];
            }
        }

        if ($qtd <= $estoque_item) {
            return true;
        } else {
            return false;
        }
    }

    public function adicionarItemVenda(array $dados) {

        $sql = "SELECT valor FROM produto WHERE id = " . $dados['idproduto'];
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            foreach ($sql as $rs) {
                $valor = $rs["valor"];
            }
        } else {
            $valor = 0;
        }
        // Calcula o total
        $total = ( $dados['qtd'] * $valor);

        $desconto_linha = $dados['desconto_linha'];

        # Calculo desconto da linha
        if ($desconto_linha > 0 && $desconto_linha != '') {
            $desconto = $total * ($desconto_linha / 100);
        } else {
            $desconto = 0.00;
        }

        if ($desconto < 0)
            $desconto = 0;

        $total = $total - $desconto;

        $sql = "INSERT INTO itensproduto (valoritemroupa, quantidade, valortotal, idvenda, idproduto, desconto_linha) VALUES ($valor, :qtd, $total, :idvenda, :idproduto, :desconto_linha)";

        $query = $this->bd->prepare($sql);
        $inseriu = $query->execute($dados);

        return $inseriu;
    }

    public function buscarTodos() {
        $sql = "SELECT 
                  vda.id,
                  vda.idcliente as idclientevenda,
                   (
                    SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto
                   ) as desconto_cabecalho,
       CASE WHEN clir.nome is null THEN cli.nome 
         ELSE clir.nome
  END as nomecliente,
       col.nome as nomecolaboradorvendedor,
       to_char(vda.datavenda, 'dd/MM/yyyy') as datavenda,
       to_char(vda.datavencimento, 'dd/MM/yyyy') as datavencimento,
       to_char(cast(vda.horavenda as time), 'HH24:MI:SS') as horavenda,
       loj.nomefantasia,
       vda.tipopagamento,
       coalesce((SELECT descricao FROM tipopagamento WHERE id = vda.formapgto),' ') as forma_desc,
       coalesce((SELECT tipo FROM tipopagamento WHERE id = vda.formapgto),'N') as tipo,
       coalesce((SELECT parcelar FROM tipopagamento WHERE id = vda.formapgto),'N') as parcelar,
       coalesce((SELECT data_vencimento FROM tipopagamento WHERE id = vda.formapgto),'N') as data_vencimento,
       vda.qtdparcelas,
       vda.formapgto,
       vda.observacao,
       (
    SELECT 
      concat('R$ ',to_char(
        sum(valortotal) - (sum(valortotal) * 
        COALESCE(( (SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto) / 100 ),0.00))- valor_entrada - descontofinal,'99999999999999999D99')
      ) as total 
    FROM 
      itensproduto 
    WHERE  
      idvenda = vda.id
       ) as total,
       concat('R$ ',to_char(((SELECT sum(valortotal) as total FROM itensproduto WHERE idvenda = vda.id) / vda.qtdparcelas)- valor_entrada,'99999999999999999D99')) as parcelado
       
  FROM venda vda
  LEFT JOIN      cliente cli
    ON cli.id = vda.idcliente
 INNER JOIN loja loj
    ON loj.id = vda.idloja
 INNER JOIN colaboradores col
    ON col.id = vda.idcolaboradorvendedor
 LEFT JOIN clienterapido clir
    ON vda.idcliente = clir.id
  ORDER BY DATE(vda.datavenda) DESC, cast(vda.horavenda as time) DESC;";

        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select v.id,
                       v.idcliente,
                       v.idcolaborador,
                       v.idcolaboradorvendedor,
                       to_char(v.datavenda, 'dd/MM/yyyy') as datavenda,
                       to_char(v.datavencimento, 'dd/MM/yyyy') as datavencimento,
                       v.idloja,
                       v.tipopagamento,
                       v.qtdparcelas,
                       v.formapgto,
                       v.observacao,
                       (
                          SELECT sum(valortotal)-COALESCE(valor_entrada,0.00) as total FROM itensproduto WHERE idvenda = v.id
                       ) as total_sem_formatacao,
                       (
                          SELECT concat('R$ ',to_char(sum(valortotal)-COALESCE(valor_entrada,0.00),'99999999999999999D99')) as total FROM itensproduto WHERE idvenda = v.id
                       ) as total,
                       concat('R$ ',to_char(((SELECT sum(valoritemroupa*quantidade) as total FROM itensproduto WHERE idvenda = v.id) / v.qtdparcelas),'99999999999999999D99')) as parcelado
                  from venda v
                 WHERE v.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function buscarItemProduto($idProd, $idVenda) {
        $sql = "SELECT 
            *
            FROM 
              itensproduto i
            WHERE 
              idvenda = $idvenda 
            AND 
              idproduto = $idProd";

        $query = $this->bd->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

    public function buscarItemVenda($idvenda) {
        $sql = "SELECT 
                    p.id, 
                    p.referencia, 
                    p.nomeproduto, 
                    p.numeroproduto,
                    COALESCE(
                      (
                        SELECT desconto_linha FROM itensproduto WHERE idproduto = p.id AND idvenda = $idvenda
                      ),0
                    ) as desconto_linha,
                    to_char(i.quantidade,  '9999999G990D999') as quantidade, 
                    COALESCE((SELECT 1 FROM itensproduto WHERE idproduto = p.id AND idvenda = $idvenda),0) AS status,
                    to_char(i.valoritemroupa*i.quantidade, 'L9G999G990D99') as valor,
                    to_char(i.valortotal, 'L9G999G990D99') as valor_total
                FROM 
                    produto p,
                    venda v,
                    itensproduto i
                WHERE 
                    v.id = $idvenda
                AND
                    p.id = i.idproduto
                AND
                    i.idvenda = v.id
                ORDER BY 
                    p.nomeproduto ASC;";

        $query = $this->bd->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function buscarProdutoVenda($idvenda, $str) {

        if ($str != '') {
            $str = " WHERE CONCAT_WS('', p.referencia, upper(p.nomeproduto), p.numeroproduto, upper(p.cor), p.tamanho, upper(p.sexo), p.valor, p.datacadastro) LIKE upper('%$str%') ";
        }

        $sql = "SELECT 
                    p.id, 
                    p.referencia, 
                    p.nomeproduto, 
                    p.numeroproduto, 
                    p.cor, 
                    p.tamanho, 
                    COALESCE((SELECT 1 FROM itensproduto WHERE idproduto = p.id AND idvenda = $idvenda),0) AS status,
                    CASE WHEN p.sexo='M' THEN 'Masculino'
                              ELSE 'Feminino'
                       END as sexo,
                    to_char(p.quantidade - COALESCE((SELECT SUM(i.quantidade) as quantidade FROM itensproduto i WHERE i.idproduto = p.id),0),'9999999G990D999') as quantidade,
                    to_char(p.valor, 'L9G999G990D99') as valor,
                    to_char(p.datacadastro,'dd/MM/yyyy') as datacadastro 
                FROM 
                    produto p
                    $str
                ORDER BY 
                p.nomeproduto ASC;";

        $query = $this->bd->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function finalizarVenda($dados) {
        $idvenda = $dados['id'];
        $data_vencimento = $dados['data_vencimento'];
        $quantidade_parcela = $dados['quantidade_parcela'];
        $forma_pgto = $dados['forma_pgto'];
        $id_desconto = $dados['id_desconto'];
        $id_cliente = $dados['id_cliente'];
        $valor_entrada = $dados['valor_entrada'];
        $valor_parcela = $dados['valor_parcela'];
        $valordescontofinal = $dados['descontofinal'];

        $valor_entrada = str_replace("R$ ", "", $valor_entrada);
        $valor_entrada = str_replace(".", "", $valor_entrada);
        $valor_entrada = str_replace(",", ".", $valor_entrada);
        $valor_entrada = preg_replace("/\s+/", "", $valor_entrada);

        //Verifica se esta possui a informação descontofinal
        if ($valordescontofinal != '') {
            $descontofinal = $valordescontofinal;
            //echo $descontofinal . 'Possui valor desconto'; exit;
        } else {
            $descontofinal = 0.00;
            //echo 'Não possui valor final'; exit;
        }
        //echo 'nao entrou em nenhum teste'; exit;

        if ($forma_pgto['parcelar'] == 'N' && $forma_pgto['data_vencimento'] == 'N') {

            $sql = "UPDATE venda SET tipopagamento = '1', datavencimento = '01/01/1900', qtdparcelas = 1, desconto = $id_desconto, valor_entrada = 0, descontofinal=$descontofinal WHERE id = $idvenda";
        } else if ($forma_pgto['parcelar'] == 'S') {

            $sql = "UPDATE venda SET tipopagamento = '1', datavencimento = '01/01/1900', qtdparcelas = $quantidade_parcela, desconto = 0, valor_entrada = $valor_entrada, descontofinal=$descontofinal WHERE id = $idvenda";

            //Busca a Data Atual
            date_default_timezone_set('America/Sao_Paulo');
            $data = date('d/m/Y');
            $hora = date('H:i:s');

            //Busca a seção do usuário logado
            $logincolaborador = $_SESSION['login'];
            $login = "SELECT id AS idcolaborador, idloja FROM colaboradores WHERE login = '$logincolaborador'";
            $sqllogin = $this->bd->prepare($login);
            $sqllogin->execute();
            if ($sqllogin->rowCount() > 0) {
                foreach ($sqllogin as $rs) {
                    $id_colaborador = $rs["idcolaborador"];
                }
            }

            //Consulta melhordiapagamentocarne, se existir usa a data
            //Se não existir o fluxo continua o mesmo
            $consultamelhordiapagamento = "SELECT formapgto,
                                                  to_char(melhordiapagamentocarne, 'dd/MM/yyyy') as melhordiapagamentocarne 
                                             FROM venda 
                                            WHERE id = $idvenda";
            $sqlmelhordiapagamento = $this->bd->prepare($consultamelhordiapagamento);
            $sqlmelhordiapagamento->execute();
            if ($sqlmelhordiapagamento->rowCount() > 0) {
                foreach ($sqlmelhordiapagamento as $rs) {
                    $melhordiapagamentocarneconsulta = $rs["melhordiapagamentocarne"];
                    $formapagamento = $rs["formapgto"];
                }
            }


            if ($melhordiapagamentocarneconsulta != '') {
                $data = $melhordiapagamentocarneconsulta;
            } else {
                $data = date('d/m/Y');
            }

            $formapagamentovenda = $_POST['forma'];
            if ($formapagamentovenda == 2) {
                
                //Verifica os posts que estao vindo
                if($_POST['descontofinal'] == ''){
                    $descontofinalpost = 0;
                }else{
                    $descontofinalpost = $_POST['descontofinal'];
                }
                //Atualização da Function Finalizar Venda para Carnê
                //Data Atualização: 12/11/2017
                //Esta atualização tem como objetivo contemplar a melhoria referente ao arredondamento das parcelas
                //Desenvolvedor: Marciano Luis Cadore
                //Formata Desconto Final
                $formata = str_replace("R$", "", str_replace("", "", $descontofinalpost));
                $formatavalor = str_replace(".", "", str_replace("", "", $formata));
                $formatavalordescontofinal = str_replace(",", ".", str_replace("", "", $formatavalor));
                $pdescontofinal = $formatavalordescontofinal;
                
                //Formata Valor Entrada
                $pformatavalorentrada = str_replace("R$", "", str_replace("", "", $_POST['valor_entrada']));
                $formatavalorentrad = str_replace(".", "", str_replace("", "", $pformatavalorentrada));
                $formatavalorentr = str_replace(",", ".", str_replace("", "", $formatavalorentrad));
                $pvalorentrada = $formatavalorentr;

                //Formata Valor Parcela
                $pvalorparcela = number_format($valor_parcela, 2, '.', '.');

                //Verificar se os parâmetros estão vindo com valores
                if ($pdescontofinal != '') {
                    $pdescontofinal = $pdescontofinal;
                } else {
                    $pdescontofinal = 0.00;
                } if ($pvalorentrada != '') {
                    $pvalorentrada = $pvalorentrada;
                } else {
                    $pvalorentrada = 0.00;
                } if ($pvalorparcela != '') {
                    $pvalorparcela = $pvalorparcela;
                } else {
                    $pvalorparcela = 0.00;
                }
                
                //Valor da Parcela * o número da parcela = valortotalvenda sem entrada
                $valortotalvenda = ($pvalorparcela * $quantidade_parcela - $pdescontofinal);
                
                //Agora iremos buscar os valores e iremos realizar o arredondamento das parcelas
                $value = $valortotalvenda;
                $prest = $quantidade_parcela;
                $div = (int) ($value / $prest);
                $resto = $value % $prest;
                $init = $prest - 1;
                $fin = $div + $resto;
                $jogarvalores = $value - ($div * $init + $fin);
                $umaprestacao = ($fin + $jogarvalores);
                
                //Buscar valor da Venda com todas as parcelas
                $valorvendatotal = ((($_POST['qtd'] * $_POST['valor_parcela']) + $pvalorentrada) - $pdescontofinal);
                //Somar as parcelas menos uma parcela onde ficará os valores quebrados que será na primeira parcela
                $valorvendamenosumaparcela = (((($_POST['qtd'] - 1) * $div) + $pvalorentrada) - $pdescontofinal);
                $fvalorprimeiraparcelafinal = ($valorvendatotal - $valorvendamenosumaparcela - $pdescontofinal);
                //Agora teremos o valor da primeira parcela com os valores quebrados
                $valorprimeiraparcela = round($fvalorprimeiraparcelafinal * 100) / 100; 
                
                //Buscar valor de multiplicação das parcelas
                if($melhordiapagamentocarneconsulta != ''){
                $data = explode("/", $data);
                $data_parcela = date("d/m/Y", mktime(0,0,0,$data[1],$data[0],$data[2]));
                //Grava a parcela mais o resto de outras parcelas
                $sql_parcelas = "INSERT INTO parcelas (idcliente, 
                                                       idvenda, 
                                                       statusparcela, 
                                                       numeroparcela, 
                                                       datavencimentoparcela , 
                                                       valorparcelas)
                                                VALUES($id_cliente,
                                                       $idvenda,
                                                       'PE',
                                                       1,
                                                       '$data_parcela',
                                                       $valorprimeiraparcela);";
                $data = $data_parcela;
                $query = $this->bd->prepare($sql_parcelas);
                if($query->execute() == 1){
                    //Se existir Melhor dia de Pagamento primeiramente grava a primeira parcela com a data escolhida
                    //Após gravar o primeiro registro entra no for abaixo para gravar outras parcelas
                    //Caso Não informado o melhor dia de pagamento o processamento deverá cair direto dentro do for
                    for($i=2;$i<=$quantidade_parcela;$i++){
                        $data = explode("/", $data);
                        $data_parcela = date("d/m/Y", mktime(0,0,0,$data[1]+1,$data[0],$data[2]));
                        
                        $sql_parcelasmaisum = 
                            "INSERT INTO parcelas (
                              idcliente, 
                              idvenda, 
                              statusparcela , 
                              numeroparcela , 
                              datavencimentoparcela , 
                              valorparcelas)
                            VALUES(
                              $id_cliente,
                              $idvenda,
                              'PE',
                              $i,
                              '$data_parcela',
                              $div
                            );";
                          
                          $data = $data_parcela;
                          
                          $querymaisum = $this->bd->prepare($sql_parcelasmaisum);
                          $querymaisum->execute();
                    }
                }
                }else if($melhordiapagamentocarneconsulta == ''){
                    //Aqui precisaremos gravar a primeira parcela
                    $data = explode("/", $data);
                    $data_parcela = date("d/m/Y", mktime(0,0,0,$data[1],$data[0],$data[2]));
                    //Grava a parcela mais o resto de outras parcelas
                    $sql_parcelas = "INSERT INTO parcelas (idcliente, 
                                                           idvenda, 
                                                           statusparcela, 
                                                           numeroparcela, 
                                                           datavencimentoparcela , 
                                                           valorparcelas)
                                                    VALUES($id_cliente,
                                                           $idvenda,
                                                           'PE',
                                                           1,
                                                           '$data_parcela',
                                                           $valorprimeiraparcela);";
                    $data = $data_parcela;
                    $query = $this->bd->prepare($sql_parcelas);
                    if($query->execute() == 1){
                        //Se cair nesse teste quer dizer que o Melhor Dia de Pagamento veio vazio então segue o fluxo normal.
                        for($i=2;$i<=$quantidade_parcela;$i++){
                            $data = explode("/", $data);
                            $data_parcela = date("d/m/Y", mktime(0,0,0,$data[1]+1,$data[0],$data[2]));

                            $sql_parcelas = 
                                "INSERT INTO parcelas (
                                  idcliente, 
                                  idvenda, 
                                  statusparcela , 
                                  numeroparcela , 
                                  datavencimentoparcela , 
                                  valorparcelas)
                                VALUES(
                                  $id_cliente,
                                  $idvenda,
                                  'PE',
                                  $i,
                                  '$data_parcela',
                                  $div
                                );";

                              $data = $data_parcela;
                              $query = $this->bd->prepare($sql_parcelas);
                              $query->execute();
                        }
                    }
                }
            }
        } else if ($forma_pgto['data_vencimento'] == 'S') {

            $sql = "UPDATE venda SET tipopagamento = '1', datavencimento = '$data_vencimento', qtdparcelas = 1, desconto = 0, valor_entrada = 0, descontofinal=$descontofinal WHERE id = $idvenda";
        }

        $query = $this->bd->prepare($sql);
        if ($query->execute() == 1) {
            $consultasqldescontofinal = "SELECT descontofinal FROM venda WHERE id = " . $idvenda;
            $sqlconsultadescontofinal = $this->bd->prepare($consultasqldescontofinal);
            $sqlconsultadescontofinal->execute();
            if ($sqlconsultadescontofinal->rowCount() > 0) {
                foreach ($sqlconsultadescontofinal as $rsdescontofinal) {
                    $descontofinal = $rsdescontofinal["descontofinal"];
                }
            }
        }
        if ($descontofinal != 0.00) {
            //Se cair aqui neste teste as parcelas sofrerão ajuste no valor, pois existe o valor desconto final
            //Necessário buscar o valor total da venda e diminuir pelo desconto final
            //Após saber o novo valor final, é necessário recuperar a quantidade de parcelas e dividir o valor pela quantidade
            $sql = "SELECT Ltrim(To_char(valor_total_item - valor_entrada - descontofinal, '9G999G990D99')) AS valortotal_final 
                     FROM   (SELECT COALESCE(Sum(item.valortotal), 0.00) valor_total_item, 
                                    v.valor_entrada AS valor_entrada, 
                                    v.descontofinal AS descontofinal 
                             FROM   itensproduto item, 
                                    venda v, 
                                    produto p 
                             WHERE  v.id = $idvenda 
                                    AND item.idvenda = v.id 
                                    AND item.idproduto = p.id 
                             GROUP  BY v.valor_entrada, 
                                       descontofinal) a; ";
            $sql = $this->bd->prepare($sql);
            $sql->execute();
            if ($sql->rowCount() > 0) {
                foreach ($sql as $rs) {
                    $valortotalfinal = $rs["valortotal_final"];
                }
            }

            //Formata o Valor Total Final
            $valortotalfinalformatado = str_replace(",", ".", $valortotalfinal);
            $valorfinalparcelado = ($valortotalfinalformatado / $quantidade_parcela);
            
            //Agora é hora de realizar o update do valor de cada parcela quando a forma de pagamento não será 2
            //Data Alteração: 25/11/2017
             if ($formapagamentovenda != 2) {
                $sqlatualizavalorparcela = "UPDATE parcelas SET valorparcelas = $valorfinalparcelado WHERE idvenda = $idvenda";
                $sqlatualizaparcelas = $this->bd->prepare($sqlatualizavalorparcela);
                $sqlatualizaparcelas->execute();
             }
        }
        return $query->execute();
    }

    public function excluirItemVenda($id, $idvenda) {
        $sql = "DELETE FROM itensproduto WHERE idproduto = :id AND idvenda = :idvenda";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id, 'idvenda' => $idvenda));
    }

    public function excluir($id) {

        $sql = "DELETE FROM itensproduto WHERE idvenda = :id";
        $query = $this->bd->prepare($sql);
        $ret = $query->execute(array('id' => $id));

        if ($ret) {
            $sql = "DELETE FROM venda WHERE id = :id";
            $query = $this->bd->prepare($sql);
            $ret_1 = $query->execute(array('id' => $id));
        } else {
            $ret_1 = -1;
        }

        return $ret_1;
    }

}
