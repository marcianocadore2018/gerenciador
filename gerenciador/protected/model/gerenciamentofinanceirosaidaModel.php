<?php

class GerenciamentofinanceirosaidaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        if($_POST['idcolaboradorvendedor'] == null){
            $idcolaboradorvendedor = 'null';
        }else{
            $idcolaboradorvendedor = $_POST['idcolaboradorvendedor'];
        }
        $datacadastrohidden = $_POST['datacadastrohidden'];
        $valorpagamento = $_POST['valorpagamento'];
        
        $formata      = str_replace("R$", "", str_replace("", "", $valorpagamento));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $valorpagamentoformatado = str_replace(",", ".", str_replace("", "", $formatavalor));

        //Buscar o Id do colaborador e o Id da Loja pela seção
        $logincolaborador = $_SESSION['login'];
        $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($logincolaborador);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja        = $rs["idloja"];
            }
        }

        $sql = "INSERT INTO gerenciamentofinanceirosaida(descricao, 
                                                         datacadastro, 
                                                         valorpagamento, 
                                                         idcolaborador, 
                                                         idloja, 
                                                         maioresinformacoes, 
                                                         idcolaboradorvendedor) 
                                                  VALUES(:descricao, 
                                                         '$datacadastrohidden', 
                                                         $valorpagamentoformatado, 
                                                         $idcolaborador, 
                                                         $idloja, 
                                                         :maioresinformacoes, 
                                                         $idcolaboradorvendedor)";
        
        unset($dados['id']);
        unset($dados['datacadastro']);
        unset($dados['datacadastrohidden']);
        unset($dados['idcolaborador']);
        unset($dados['idloja']);
        unset($dados['valorpagamento']);
        unset($dados['idcolaboradorvendedor']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        //Buscar o idloja pela seção do colaborador
        $sql = "SELECT gs.id,
                       gs.descricao,
                       to_char(gs.valorpagamento, 'L9G999G990D99') as valorpagamento,
                       to_char(gs.datacadastro, 'dd/MM/yyyy') as datacadastro,
                       c.nome as nomevendedor
                  FROM gerenciamentofinanceirosaida gs
             left join colaboradores c
                    on gs.idcolaboradorvendedor = c.id
              order by gs.datacadastro asc;";
        

        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            
          return $sql;
        }else{
          return null;
        }
    }

    public function buscar($id) {
        $sql = "SELECT gs.id, 
                       gs.descricao,
                       to_char(gs.valorpagamento, 'L9G999G990D99') as valorpagamento,
                       to_char(gs.datacadastro, 'dd/MM/yyyy') as datacadastro,
                       gs.maioresinformacoes,
                       c.nome as nomevendedor,
                       gs.idcolaboradorvendedor
                  FROM gerenciamentofinanceirosaida gs
                 inner join colaboradores c
                    on gs.idcolaboradorvendedor = c.id
                 WHERE gs.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $datacadastrohidden = $_POST['datacadastrohidden'];
        $valorpagamentopost = $_POST['valorpagamento'];
        
        //Buscar o Id do colaborador e o Id da Loja pela seção
        $logincolaborador = $_SESSION['login'];
        $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($logincolaborador);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja        = $rs["idloja"];
            }
        }

        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $_POST['valorpagamento']));
        //Valor formatação Salário
        $valorpagamento = $formata;
        $verificavalor = substr($valorpagamento, -3, 1);
        if($verificavalor == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorpagamento));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorpagamentoformatado   = $formatavalor;
        }else{
            $valorpagamentopost = $valorpagamento;
            $removevirgula = str_replace(",", "", $valorpagamentopost);
            $valorpagamentoformatado = $removevirgula;
        }

        $sql = "UPDATE gerenciamentofinanceirosaida
                   SET descricao = :descricao, 
                       valorpagamento = $valorpagamentoformatado,
                       datacadastro = '$datacadastrohidden',
                       maioresinformacoes = :maioresinformacoes,
                       idcolaborador = $idcolaborador,
                       idloja = $idloja,
                       idcolaboradorvendedor = :idcolaboradorvendedor
                 WHERE id = :id";
        
        unset($dados['datacadastrohidden']);
        unset($dados['datacadastro']);
        unset($dados['idcolaborador']);
        unset($dados['idloja']);
        unset($dados['valorpagamento']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM gerenciamentofinanceirosaida WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
