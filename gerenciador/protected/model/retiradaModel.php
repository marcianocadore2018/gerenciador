<?php

class RetiradaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricao = $_POST['descricao'];
        $quantidade = $_POST['quantidade'];
        $codigopeca = $_POST['codigopeca'];
        $datadevolucao = $_POST['datadevolucao'];
        if($datadevolucao == ''){
            $novovalordatadevolucao = 'null';
        }else{
            $novovalordatadevolucao = "'" . $datadevolucao . "'";
        }
        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $sql = "INSERT INTO retirada(descricao, datacadastro, quantidade, codigopeca, datadevolucao) "
                . " VALUES('$descricao', '$dataCadastro', $quantidade, $codigopeca, $novovalordatadevolucao)";
        
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['quantidade']);
        unset($dados['codigopeca']);
        unset($dados['datadevolucao']);
        unset($dados['datacadastro']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT id, descricao, to_char(datacadastro,'dd/MM/yyyy') as datacadastro, to_char(datadevolucao,'dd/MM/yyyy') as datadevolucao, quantidade, codigopeca FROM retirada order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, descricao, to_char(datacadastro,'dd/MM/yyyy') as datacadastro, quantidade, codigopeca, to_char(datadevolucao,'dd/MM/yyyy') as datadevolucao FROM retirada WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $datadevolucao = $_POST['datadevolucao'];
        if($datadevolucao == ''){
            $novovalordatadevolucao = 'null';
        }else{
            $novovalordatadevolucao = "'" . $datadevolucao . "'";
        }
        $sql = "UPDATE retirada SET descricao = :descricao, datacadastro = '$dataCadastro', quantidade = :quantidade, codigopeca = :codigopeca, datadevolucao = $novovalordatadevolucao WHERE id = :id";
        unset($dados['datacadastro']);
        unset($dados['datadevolucao']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM retirada WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}