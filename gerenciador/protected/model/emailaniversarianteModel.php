<?php

class EmailaniversarianteModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    public function buscarTodos() {
        $sql = "select cli.nome as nomecliente,
                       to_char(cli.datanascimento, 'dd/MM/yyyy') as datanascimento, 
                       to_char(cli.datanascimento, 'MM') as mesdatanascimento,
                       cli.celular,
                       pes.email
                  from cliente cli
            inner join registropessoa pes
                    on cli.idregistropessoa = pes.id
                 where pes.email <> '' and cli.email_enviado = 't'
             union all
                select rap.nome as nomecliente,
                       to_char(rap.dataaniversario, 'dd/MM/yyyy') as datanascimento,
                       to_char(rap.dataaniversario, 'MM') as mesdatanascimento,
                       rap.telefone as celular,
                       rap.email
                  from clienterapido rap
                 where rap.email <> '' and rap.email_enviado = 't'
              order by mesdatanascimento desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}