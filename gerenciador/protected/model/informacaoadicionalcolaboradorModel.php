<?php

class InformacaoadicionalcolaboradorModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {

        $idcolaborador  = isset($dados['id']) ? $dados['id'] : false;
        $idcargo        = $dados['idcargo'];
        $idsetor        = $dados['idsetor'];
        $dataadmissao   = $dados['admissao'];
        $demissao       = isset($dados['demissao']) ? $dados['demissao'] : null;
        $salariopost    = $dados['salario'];
        $comissao       = $dados['comissao'];


        if($idcolaborador===''){
            $idcolaborador = $dados['idcolaborador'];
        }

        if(!$idcolaborador){
            $sql   = "SELECT MAX(id) as id FROM colaboradores;";
            $res   = $this->bd->prepare($sql);
            $res->execute();
            if ($res->rowCount() > 0) {
                foreach ($res as $rs) {
                    $idcolaborador = $rs["id"];
                }
            }
        }
        
        if(!$demissao){
            $datademissao = 'null';
        }else{
            $dtadmissao   = $demissao;
            $datademissao = "'$dtadmissao'";
        }
        if($comissao == null){
            $comissaocolaborador = 0.00;
        }else{
            $comissaocolaborador = $comissao;
        }

        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastrorenda = date('d/m/Y');
        
        $formata      = str_replace("R$", "", str_replace("", "", $salariopost));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        $valorsalarioformatado   = $formatavalor;

        $formata       = str_replace("R$", "", str_replace("", "", $comissaocolaborador));
        $formatavalor  = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor  = str_replace(",", ".", str_replace("", "", $formatavalor));
        $valorComissao = $formatavalor;
        
        $sql = "insert into informacaoadicionalcolaborador(admissao, 
                                                           demissao, 
                                                           salario, 
                                                           comissao, 
                                                           idcargo, 
                                                           idsetor, 
                                                           idcolaborador) "
                                                . " VALUES('$dataadmissao', 
                                                           $datademissao, 
                                                            $valorsalarioformatado, 
                                                            $valorComissao, 
                                                            $idcargo, 
                                                            $idsetor, 
                                                            $idcolaborador)";


        unset($dados['id']);
        unset($dados['salario']);
        unset($dados['demissao']);
        unset($dados['demitido']);
        unset($dados['situacaodemissao']);
        unset($dados['idcolaborador']);
        unset($dados['idsetor']);
        unset($dados['idcargo']);
        unset($dados['admissao']);
        unset($dados['comissao']);
        unset($dados['dataadmissao']);

        if(isset($dados['acaoBack'])){
            unset($dados['acaoBack']);
        }

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select info.id,
                       to_char(info.admissao, 'dd/MM/yyyy') as admissao,
                       to_char(info.demissao, 'dd/MM/yyyy') as demissao,
                       to_char(info.salario, 'L9G999G990D99') as salario,
                       info.comissao,
                       ca.descricao as descricaocargo,
                       se.descricao as descricaosetor,
                       co.nome as nomecolaborador
                  from informacaoadicionalcolaborador info
                 inner join cargo ca
                    on info.idcargo = ca.id
                 inner join setor se
                    on info.idsetor = se.id
                 inner join colaboradores co
                    on info.idcolaborador = co.id
                 order by info.admissao, co.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select info.id,
                       to_char(info.admissao, 'dd/MM/yyyy') as admissao,
                       to_char(info.demissao, 'dd/MM/yyyy') as demissao,
                       info.salario,
                       info.comissao,
                       info.idcargo,
                       info.idsetor,
                       info.idcolaborador
                  from informacaoadicionalcolaborador info
                 WHERE info.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function buscarToIAColaborador($id) {
        $sql = "select info.id,
                       to_char(info.admissao, 'dd/MM/yyyy') as admissao,
                       to_char(info.demissao, 'dd/MM/yyyy') as demissao,
                       info.salario,
                       info.comissao,
                       info.idcargo,
                       info.idsetor,
                       info.idcolaborador
                  from informacaoadicionalcolaborador info
                 WHERE info.idcolaborador = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
   
        if(isset($dados['idcolaborador'])){
            $id = $dados['idcolaborador'];
        }else{
            $id = $dados['id'];
        }
        
        $demissao    = $dados['demissao'];
        $salariopost = $dados['salario'];
        $comissao    = $dados['comissao'];
        
        $situacaodemissao = $dados['situacaodemissao'];
        
        if($dados['demissao'] == null || $situacaodemissao == "0"){
            $datademissao = "null";
        }else{
            $datademissao = "'".$dados['demissao']."'";
        }
        if($dados['comissao'] == null){
            $comissaocolaborador = 0.00;
        }else{
            $comissaocolaborador = $dados['comissao'];
        }
        
        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastrorenda = date('d/m/Y');
        
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['salario']));
        //Valor formatação Salário
        $valorsalario = $formata;
        $verificavalorsalario = substr($valorsalario, -3, 1);
        if($verificavalorsalario == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorsalario));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorsalarioformatado   = $formatavalor;
        }else{
            $salariopost = $valorsalario;
            $removevirgula = str_replace(",", "", $salariopost);
            $valorsalarioformatado = $removevirgula;
        }
        $sqlInformacaoAdicionalColaborador = "UPDATE informacaoadicionalcolaborador
                                                 SET admissao = :admissao,
                                                     demissao = $datademissao,
                                                     salario  = $valorsalarioformatado,
                                                     comissao = $comissaocolaborador,
                                                     idcargo  = :idcargo,
                                                     idsetor  = :idsetor,
                                                     idcolaborador = :idcolaborador\n";
        if(isset($dados['idcolaborador'])){
            $sqlInformacaoAdicionalColaborador .= "WHERE idcolaborador = $id";
        }else{
            $sqlInformacaoAdicionalColaborador .= "WHERE id = $id";
        }

        unset($dados['id']);
        unset($dados['salario']);
        unset($dados['demissao']);
        unset($dados['demitido']);
        unset($dados['comissao']);
        unset($dados['situacaodemissao']);

        if(isset($dados['acaoBack'])){
            unset($dados['acaoBack']);
        }

        $query = $this->bd->prepare($sqlInformacaoAdicionalColaborador);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM informacaoadicionalcolaborador WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
