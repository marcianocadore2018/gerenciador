<?php

class TrocaprodutoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    
    public function filtrartrocaproduto($idcliente, $cpf, $idvenda) {
        if($idcliente != null && $cpf == "" && $idvenda == null){
           $filtropesquisa = 'where vda.idcliente = ' . " $idcliente";
        }else if($idcliente == null && $cpf != "" && $idvenda == null){
            $filtropesquisa = 'where cli.cpf = ' . " '$cpf'";
        }else if($idcliente == null && $cpf == "" && $idvenda != null){
            $filtropesquisa = 'where vda.id = ' . "$idvenda";
        }else if($idcliente != null && $cpf != "" && $idvenda == null){
            $filtropesquisa = 'where vda.idcliente = ' . "$idcliente" . " and cli.cpf = '$cpf'";
        }else if($idcliente == null && $cpf != "" && $idvenda != null){
            $filtropesquisa = 'where cli.cpf = ' . "'$cpf'" . " and ve.id = $idvenda";
        }else if($idcliente != null && $cpf == "" && $idvenda != null){
            $filtropesquisa = 'where vda.idcliente = ' . "$idcliente" . " and vda.id = $idvenda";
        }else if($idcliente != null && $cpf != "" && $idvenda != null){
            $filtropesquisa = 'where vda.idcliente = ' . "$idcliente" . " and vda.id = $idvenda" . " and cli.cpf = '$cpf'";
        }else{
            $filtropesquisa = 'null';
        }
        
        $sql = "SELECT  itensp.id as iditemproduto,
                        vda.id as idvenda,
                        vda.id as idtransacao,
                        CASE WHEN clir.nome is null THEN cli.nome 
                              ELSE clir.nome
                        END as nomecliente,
                        cli.cpf,
                        vda.idcliente as idclientevenda,
                         (
                          SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto
                         ) as desconto_cabecalho,
                     CASE WHEN clir.nome is null THEN cli.nome 
                       ELSE clir.nome
                        END as nomecliente,
                     pro.referencia as referencia,
                     pro.nomeproduto as nomeproduto,
                     itensp.quantidade as quantidade,
                     pro.numeroproduto as numeroproduto,
                     pro.tamanho as tamanhoproduto,
                     pro.id as idproduto,
                     pro.valor,
                     to_char(vda.datavenda, 'dd/MM/yyyy') as datavenda,
                     to_char(vda.datavencimento, 'dd/MM/yyyy') as datavencimento,
                     to_char(cast(vda.horavenda as time), 'HH24:MI:SS') as horavenda,
                     loj.nomefantasia,
                     vda.tipopagamento,
                     coalesce((SELECT descricao FROM tipopagamento WHERE id = vda.formapgto),' ') as forma_desc,
                     coalesce((SELECT tipo FROM tipopagamento WHERE id = vda.formapgto),'N') as tipo,
                     coalesce((SELECT parcelar FROM tipopagamento WHERE id = vda.formapgto),'N') as parcelar,
                     coalesce((SELECT data_vencimento FROM tipopagamento WHERE id = vda.formapgto),'N') as data_vencimento,
                     vda.qtdparcelas,
                     vda.formapgto,
                     vda.observacao,
                     (
                  SELECT 
                    concat('R$ ',to_char(
                      sum(valortotal) - (sum(valortotal) * 
                      COALESCE(( (SELECT porcentagemdescontovenda FROM descontovenda WHERE id = vda.desconto) / 100 ),0.00))- valor_entrada - descontofinal,'99999999999999999D99')
                    ) as total 
                  FROM 
                    itensproduto 
                  WHERE  
                    idvenda = vda.id
                     ) as total,
                     concat('R$ ',to_char(((SELECT sum(valortotal) as total FROM itensproduto WHERE idvenda = vda.id) / vda.qtdparcelas)- valor_entrada,'99999999999999999D99')) as parcelado

                FROM venda vda
                LEFT JOIN      cliente cli
                  ON cli.id = vda.idcliente
               INNER JOIN loja loj
                  ON loj.id = vda.idloja
               INNER JOIN colaboradores col
                  ON col.id = vda.idcolaboradorvendedor
               INNER JOIN itensproduto itensp
                  ON itensp.idvenda = vda.id
               INNER JOIN produto pro
                  ON itensp.idproduto = pro.id
               LEFT JOIN clienterapido clir
                  ON vda.idcliente = clir.id
                     $filtropesquisa
               ORDER BY DATE(vda.datavenda) DESC, cast(vda.horavenda as time) DESC;";
        
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            
          return $sql;
        }else{
          return null;
        } 
        
    }
    
    public function trocarproduto($id, $idproduto, $iditemproduto, $idvendatroca) {
        require './protected/view/manutencaotrocaproduto/formManutencaoTrocaproduto.php';
    }
}