<?php

class ClienteinadimplenteModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "select cli.nome as nomecliente,
						 cli.cpf,
						 pa.idvenda,
						 pa.statusparcela,
						 CASE WHEN cli.celular <> '' THEN 
						      cli.celular
						 WHEN cli.celular = '' THEN 
						      cli.contato
						 ELSE 
						     reg.telefone
						 END as contatocliente,
						 reg.endereco as enderecliente,
						 upper(reg.cidade) || ' - ' || e.uf as cidadecliente,
						 CASE WHEN reg.email <> '' THEN 
						     reg.email
						WHEN reg.email = '' THEN 
						     'E-mail não informado'
						END as emailcliente,
						v.qtdparcelas,
						to_char(pa.datavencimentoparcela, 'dd/MM/yyyy') as datavencimentoparcela,
						to_char(v.datavenda, 'dd/MM/yyyy') as datavenda,
						CASE WHEN v.melhordiapagamentocarne is not null THEN 
						    (((CURRENT_DATE - pa.datavencimentoparcela) - 30) + 30)
						WHEN v.melhordiapagamentocarne is null THEN 
						    (((CURRENT_DATE - v.datavenda) - 30) + 30)
						END as dias,
						concat('R$ ',to_char(((SELECT sum(valoritemroupa * quantidade) as total FROM itensproduto WHERE idvenda = v.id) / v.qtdparcelas),'99999999999999999D99')) as parcelado
					    from parcelas pa
					    left join cliente cli
					      on pa.idcliente = cli.id
					    left join registropessoa reg
					      on cli.idregistropessoa = reg.id
					    left join estado e
					      on reg.idestado = e.id
					    left join venda v
					      on pa.idvenda = v.id
					    left join tipopagamento tipopag
					      on v.formapgto = tipopag.id
					   where (((CURRENT_DATE - v.datavenda) >= 30) or (CURRENT_DATE - pa.datavencimentoparcela) > 0)
					     and pa.statusparcela = 'PE'
					     and (CURRENT_DATE > pa.datavencimentoparcela)
					     and (tipopag.tipo = 'R' and tipopag.parcelar = 'S')
					     and v.qtdparcelas > 0
					   order by cli.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}