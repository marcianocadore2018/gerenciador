<?php

class AjusteestoqueModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $idproduto  = $_POST['idproduto'];
        $quantidade = $_POST['quantidade'];
        $condicao   = $_POST['condicao'];
        
        //Consulta a quantidade que tem em estoque
        $consultaquantidadeproduto = "select quantidade from produto where id = $idproduto";
        $sqlconsultaquantidadeproduto = $this->bd->prepare($consultaquantidadeproduto);
        $sqlconsultaquantidadeproduto->execute();
        
        if ($sqlconsultaquantidadeproduto->rowCount() > 0) {
            foreach ($sqlconsultaquantidadeproduto as $rs){
                $quantidadeproduto = $rs["quantidade"];
            }
        }
        if($condicao == 'aumentar'){
            $quantidadetotal = $quantidadeproduto + $quantidade;
            $sqlaumentaquantidadeproduto = "update produto set quantidade = $quantidadetotal where id = $idproduto";
            unset($dados['quantidade']);
            unset($dados['idproduto']);
            unset($dados['condicao']);
            $query = $this->bd->prepare($sqlaumentaquantidadeproduto);
            return $query->execute($dados);
        }else if($condicao == 'diminuir'){
            $quantidadetotal = $quantidadeproduto - $quantidade;
            $sqldiminuiquantidadeproduto = "update produto set quantidade = $quantidadetotal where id = $idproduto";
            unset($dados['quantidade']);
            unset($dados['idproduto']);
            unset($dados['condicao']);
            $query = $this->bd->prepare($sqldiminuiquantidadeproduto);
            return $query->execute($dados);
        }
    }
}
