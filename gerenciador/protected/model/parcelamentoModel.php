<?php

class ParcelamentoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "SELECT id, quantidadeparcelas, to_char(datacadastro,'dd/MM/yyyy') as datacadastro FROM formapagamento;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, quantidadeparcelas, to_char(datacadastro,'dd/MM/yyyy') as datacadastro FROM formapagamento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $sql = "UPDATE formapagamento SET quantidadeparcelas = :qtdparcelas, datacadastro = '$dataCadastro' WHERE id = :id";
        unset($dados['datacadastro']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
}
