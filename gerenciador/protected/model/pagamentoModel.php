<?php

class PagamentoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function filtrarpagamento($idcliente, $cpf, $idvenda) {
        if($idcliente != null && $cpf == "" && $idvenda == null){
           $filtropesquisa = 'where pa.idcliente = ' . " $idcliente";
        }else if($idcliente == null && $cpf != "" && $idvenda == null){
            $filtropesquisa = 'where cli.cpf = ' . " '$cpf'";
        }else if($idcliente == null && $cpf == "" && $idvenda != null){
            $filtropesquisa = 'where pa.idvenda = ' . "$idvenda";
        }else if($idcliente != null && $cpf != "" && $idvenda == null){
            $filtropesquisa = 'where pa.idcliente = ' . "$idcliente" . " and cli.cpf = '$cpf'";
        }else if($idcliente == null && $cpf != "" && $idvenda != null){
            $filtropesquisa = 'where cli.cpf = ' . "'$cpf'" . " and pa.idvenda = $idvenda";
        }else if($idcliente != null && $cpf == "" && $idvenda != null){
            $filtropesquisa = 'where pa.idcliente = ' . "$idcliente" . " and pa.idvenda = $idvenda";
        }else if($idcliente != null && $cpf != "" && $idvenda != null){
            $filtropesquisa = 'where pa.idcliente = ' . "$idcliente" . " and pa.idvenda = $idvenda" . " and cli.cpf = '$cpf'";
        }else{
            $filtropesquisa = 'null';
        }

        $sql = "select pa.id,
                       cli.nome as nomecliente,
                       cli.cpf,
                       ve.id as idvenda,
                       to_char(ve.datavenda, 'dd/MM/yyyy') as datavenda,
                       ve.formapgto,
                       pa.statusparcela as statusparcela,
                       pa.numeroparcela,
                       to_char(pa.datavencimentoparcela, 'dd/MM/yyyy') as datavencimentoparcela,
                       'R$ ' || LTRIM(to_char(pa.valorparcelas, '9G999G990D99')) as valorparcelas
                  from parcelas pa
                 inner join cliente cli
                    on pa.idcliente = cli.id
                 inner join venda ve
                    on ve.id = pa.idvenda
                       $filtropesquisa
                 order by pa.id asc;";
        
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            
          return $sql;
        }else{
          return null;
        }
    }
    
    public function filtrarpagamentoparcela($idparcela) {
        $cpfcliente = $_GET['cpf'];
        $transacaofiltro = $_GET['transacaofiltro'];
        $sqlparcela = "select pa.id,
                       cli.nome as nomecliente,
                       cli.cpf,
                       ve.id as idvenda,
                       ve.id as idvenda,
                       to_char(ve.datavenda, 'dd/MM/yyyy') as datavenda,
                       ve.formapgto,
                       pa.statusparcela as statusparcela,
                       pa.numeroparcela,
                       to_char(pa.datavencimentoparcela, 'dd/MM/yyyy') as datavencimentoparcela,
                       'R$ ' || LTRIM(to_char(pa.valorparcelas, '9G999G990D99')) as valorparcelas
                  from parcelas pa
                 inner join cliente cli
                    on pa.idcliente = cli.id
                 inner join venda ve
                    on ve.id = pa.idvenda
                 where cli.cpf = '$cpfcliente'
                 order by pa.id asc;";
        $sqlparcela = $this->bd->prepare($sqlparcela);
        $sqlparcela->execute();
        if ($sqlparcela->rowCount() > 0) {
            
          return $sqlparcela;
        }else{
          return null;
        }
    }
    
    public function pagar($idparcelapagamento) {
        $sql = "update parcelas set statusparcela = 'PG' where id = $idparcelapagamento;";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
          $idparcelapagto = $idparcelapagamento;
          return $sql;
        }else{
          return null;
        }
    }
}
