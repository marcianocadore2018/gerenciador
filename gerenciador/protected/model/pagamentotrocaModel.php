<?php

class PagamentotrocaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        if(isset($_POST['valorporcentagem']) != ""){
            $porcentagem = $_POST['porcentagem'];
        }else{
            $porcentagem = 0;
        }
       
        $idtrocaproduto = $_POST['idtrocaproduto'];
        $sqlultimatroca = pg_query("select patroca.formapgto
                                  from pagamentodiferencatroca patroca
                                 where patroca.idtrocaproduto = $idtrocaproduto");
        
        $rsultimatroca = pg_fetch_array ($sqlultimatroca);
        $idformapagamento = $rsultimatroca['formapgto'];
        
        $idtrocaproduto = $_POST['idtrocaproduto'];
        $valor = $_POST['valor'];
        $idvenda = $_POST['idvenda'];
        $idformapagamento = $_POST['idformapagamento'];
        $idprodutotrocado = $_POST['idprodutotrocado'];
        $statustroca = $_POST['statustroca'];
        $valorformatado = str_replace("R$ ", "", $valor);
        if(isset($valorporcentagem) != ''){
            $valorporcentagem = $_POST['valorporcentagem'];
        }else{
            $valorporcentagem = 0;
        }
        
    //FORMATA VALOR
        $formata = str_replace("R$", "", str_replace("", "", $valorporcentagem));
        $formatavalorprimero = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalorprimero));
        $formataporcentagemtroca = $formatavalor;
        
        $formataporcentagemcaracter = str_replace("%", "", str_replace("", "", $porcentagem));
        $formataporcentagem = str_replace("%", "", str_replace("", "", $formataporcentagemcaracter));
        
        if($formataporcentagem != ""){
            $formataporcentagem = $formataporcentagem;
        }else{
            $formataporcentagem = 0;
        }
        
        if($formataporcentagemtroca == ""){
            $formataporcentagemtroca = 0;
        }else if($formataporcentagemtroca != ""){
            $formataporcentagemtroca = $formataporcentagemtroca;
        }
        
        $sqlverificaduplicidade = pg_query("select count(*) as trocado
                                              from pagamentodiferencatroca patroca
                                             inner join trocaproduto troca
                                                on patroca.idtrocaproduto = troca.id
                                             where troca.idprodutotrocado = $idprodutotrocado
                                               and patroca.idtrocaproduto = $idtrocaproduto
                                               and troca.idvenda = $idvenda
                                               and troca.statustroca = '$statustroca'
                                               and patroca.formapgto = $idformapagamento");
        $rsverificaduplicidade = pg_fetch_array ($sqlverificaduplicidade);
        $verificaduplicidade = $rsverificaduplicidade['trocado'];
        
        if($verificaduplicidade != 1){
            $sql = "INSERT INTO pagamentodiferencatroca(formapgto, 
                                                        idtrocaproduto,
                                                        valor,
                                                        porcentagemtroca) VALUES($idformapagamento, 
                                                                      $idtrocaproduto,
                                                                      $valorformatado,
                                                                      $formataporcentagem)";
            unset($dados['id']);
            unset($dados['idtrocaproduto']);
            unset($dados['valor']);
            unset($dados['nomeproduto']);
            unset($dados['idformapagamento']);
            unset($dados['idprodutotrocado']);
            unset($dados['idvenda']);
            unset($dados['statustroca']);
            unset($dados['porcentagemtroca']);
            unset($dados['valorporcentagem']);
            unset($dados['porcentagem']);
            $query = $this->bd->prepare($sql);
            return $query->execute($dados);
        }
    }

    public function buscarTodos() {
        $sql = "select trim(cast(troca.valordiferenca as text), '-') as valor,
                        patroca.porcentagemtroca,
                        pro.nomeproduto as nomeproduto,
                        proddevolvido.nomeproduto as nomeprodutodevolvido,
                        to_char(troca.datatroca, 'dd/MM/yyyy') as datatroca,
                        to_char(troca.horatroca, 'HH24:mi') as horatroca,
                        CASE WHEN rap.nome is null THEN cli.nome 
                                 ELSE rap.nome
                          END as nomecliente,
                        col.nome as colaboradorresponsavel 
                   from pagamentodiferencatroca patroca
                  inner join trocaproduto troca
                     on patroca.idtrocaproduto = troca.id
                  inner join produto pro
                     on troca.idprodutotrocado = pro.id
                  inner join produto proddevolvido
                     on troca.idprodutodevolvido = proddevolvido.id
                   left join cliente cli
                     on troca.idcliente = cli.id
                   left join clienterapido rap
                     on troca.idcliente = rap.id  
                  inner join colaboradores col
                     on troca.idcolaboradorresponsavel = col.id
                  order by nomecliente, datatroca, horatroca asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, descricao, to_char(datacadastro,'dd/MM/yyyy') as datacadastro FROM cargo WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $sql = "UPDATE cargo SET descricao = :descricao, datacadastro = '$dataCadastro' WHERE id = :id";
        unset($dados['datacadastro']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
}
