<?php

class ManutencaotrocaprodutoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function realizartrocaproduto(array $dados) {
        //Função para data
        date_default_timezone_set('America/Sao_Paulo');
        $dataTroca = date('d/m/Y', time());
        $horaTroca = date('H:i', time());

        //Função para urlbase
        if (isset($_SERVER['HTTPS'])) { $prefixo = 'https://'; } else { $prefixo = 'http://'; }
        $app = $_SERVER['HTTP_REFERER'];
        if (strpos($app, 'gerenciadorlocal') == true) { $aplicacao = 'gerenciadorlocal';} else if (strpos($app, 'gerenciadortest') == true) { $aplicacao = 'gerenciadortest';
        } else if (strpos($app, 'gerenciador') == true) { $aplicacao = 'gerenciador'; }
        //URL BASE
        $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
        
        //POSTS RECEBIDOS
        $idprodutodevolvido   = $_POST['idprodutodevolvido'];
        $iditemproduto        = $_POST['iditemproduto'];
        $idtransacao          = $_POST['idvendatroca'];
        $idprodutotrocado     = $_POST['idprodutotrocado'];
        $quantidade           = $_POST['quantidade'];
        $desconto_linha       = $_POST['desconto_linha'];
        $nomeprodutodevolvido = $_POST['nomeprodutodevolvido'];
        $referencia           = $_POST['referencia'];
        $tamanhonumero        = $_POST['tamanhonumero'];
        $valor                = $_POST['valor'];
        $quantidadedevolvida  = $_POST['quantidadedevolvida'];
        
        ###################### CAPTURA DE VALORES ####################
        //Capturar a Seção do Usuário
        $loginsecao = $_SESSION['login'];
        $colaboradorlogado = pg_query("select id as idcolaborador from colaboradores where login = '$loginsecao';");
        $rscolaboradorlogado = pg_fetch_array ($colaboradorlogado);
        $idcolaboradorresponsavel = $rscolaboradorlogado['idcolaborador'];
        
        //Capturar o ID do Cliente
        $consultacliente = pg_query("select idcliente from venda where id = $idtransacao");
        $rscliente = pg_fetch_array ($consultacliente);
        $idcliente = $rscliente['idcliente'];
        
        //Capturar valor do Produto Devolvido
        $consultavalorprodutodevolvido = pg_query("SELECT (SELECT Concat(Sum(valortotal) - ( Sum(valortotal) *  COALESCE 
                                                             (( (SELECT  porcentagemdescontovenda 
                                                                   FROM  descontovenda 
                                                                  WHERE id = vda.desconto) / 100), 0.00) ) - valor_entrada - descontofinal) AS valorproduto 
                                                     FROM   itensproduto 
                                                    WHERE  idvenda = vda.id) AS valorproduto, 
                                                          Concat(( (SELECT Sum(valortotal) AS total 
                                                                      FROM   itensproduto 
                                                                     WHERE  idvenda = vda.id) / vda.qtdparcelas ) - valor_entrada) AS parcelado 
                                                                      FROM   venda vda 
                                                                     INNER JOIN itensproduto itens 
                                                                        ON vda.id = itens.idvenda 
                                                                     INNER JOIN produto pro 
                                                                        ON itens.idproduto = pro.id 
                                                                     WHERE  pro.id = $idprodutodevolvido 
                                                                       AND vda.id = $idtransacao 
                                                                     ORDER  BY Date(vda.datavenda) DESC, 
                                                                    Cast(vda.horavenda AS TIME) DESC;");
       
        $rsvalorprodutodevolvido = pg_fetch_array ($consultavalorprodutodevolvido);
        $valorprodutodevolvidoformatar = $rsvalorprodutodevolvido['valorproduto'];
        $valorprodutodevolvidoconsultado = number_format($valorprodutodevolvidoformatar, 2);
        $removervirgulavalorprodutodevolvido = number_format($valorprodutodevolvidoconsultado, 2, '', ',');
        
        //Capturar valor do produto Trocado
        $consultavalorprodutotrocado = pg_query("select pro.valor as valorproduto
                                                   from produto pro
                                                  where pro.id = $idprodutotrocado");
        
        $rsvalorprodutotrocado = pg_fetch_array ($consultavalorprodutotrocado);
        $valorprodutotrocadoconsultado = $rsvalorprodutotrocado['valorproduto'];
        $removervirgulavalorprodutotrocado = number_format($valorprodutotrocadoconsultado, 2, '', ',');
        
        ###################### FIM CAPTURA ####################
        
        
        #######################################################
        #################### VERIFICAÇÃO TROCA ################
        #######################################################
        //Verificar se tem Desconto
        if($_POST['desconto_linha'] == null){
            $desconto_linha = 0.00;
        }else{
            $desconto_linha = $_POST['desconto_linha'];
        }
       
        //Verificação para realização da troca
        //Verificar se a quantidade esta disponível para realizar a troca
        $verificardisponibilidadeproduto = pg_query("select pro.quantidade as quantidadeproduto,
                                                            pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel
                                                       from produto pro 
                                                      inner join grupo gru
                                                         on pro.idgrupo = gru.id
                                                       left join itensproduto itensprod
                                                         on itensprod.idproduto = pro.id
                                                      where pro.id = $idprodutotrocado
                                                      group by itensprod.idproduto, 
                                                              pro.id,
                                                              gru.descricao;");
        $rsverificardisponibilidadeproduto = pg_fetch_array ($verificardisponibilidadeproduto);
        $quantidadeproduto = $rsverificardisponibilidadeproduto['quantidadeproduto'];
        $quantidadedisponivel = $rsverificardisponibilidadeproduto['quantidadedisponivel'];
        
        if($quantidadeproduto == 0){
            $string = "errorqtd";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errorqtd=$encriptografa'</script>";
        }else if($quantidadeproduto == 0 && $quantidadedisponivel == ""){
            $string = "errorqtddisp";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errorqtddisp=$encriptografa'</script>";
        }
        
        //CONSULTA QUANTIDADE PRODUTO COMPRADO
        $consultaprodutoscomprado = pg_query("select itensprod.quantidade 
                                                from venda ve
                                               inner join itensproduto itensprod
                                                  on ve.id = itensprod.idvenda
                                               inner join produto pro
                                                  on itensprod.idproduto = pro.id
                                               where itensprod.idproduto = $idprodutodevolvido
                                                 and ve.idcliente = $idcliente");
        $rsconsultaprodutocomprado = pg_fetch_array ($consultaprodutoscomprado);
        $quantidadeprodutocomprado = $rsconsultaprodutocomprado['quantidade'];
        
        //Voltar a quantidade dos itens
        if($idprodutotrocado != $idprodutodevolvido){
            //Consultar quantidade do produto
            $sqlconsultarquantidadeitensproduto = pg_query("select quantidade 
                                                              from itensproduto 
                                                             where id = $idprodutodevolvido");
            $rsconsultarquantidadeitensproduto = pg_fetch_array ($sqlconsultarquantidadeitensproduto);
            $quantidadeprodutoitens = $rsconsultarquantidadeitensproduto['quantidade'];
            
            $atualizaquantidadeitemtroca = "update produto
                                               set quantidade = quantidade + $quantidadeprodutoitens
                                             where id = $idprodutodevolvido";
        }   
         
        //BÔNUS DO CLIENTE
        //echo 'Valor produto devolvido: '; echo $removervirgulavalorprodutodevolvido; echo '<br/>';
        //echo 'Valor produto trocado: '; echo $removervirgulavalorprodutotrocado;
        if($removervirgulavalorprodutodevolvido > $removervirgulavalorprodutotrocado){
            //CASO 1 - o status da tabela trocaproduto será B - Bônus
            //SE O VALOR DO PRODUTO FOR MENOR O CLIENTE DEVERÁ FICAR COM UM BÔNUS, PARA ISTO VAI PRECISAR DE UMA TABELA
            //$valordiferencasemdesconto = ($valorprodutodevolvidoconsultado - $valorprodutotrocadoconsultado);
            //$valortotal = ($valordiferencasemdesconto - ($valordiferencasemdesconto / 100 * $desconto_linha));
            //formatar valor total
            //$valortotalformatado = number_format($valortotal, 2, '.', '');
            //echo $valortotalformatado;
            
            //Executar SQL verificando o id da troca para usar no sql abaixo
            $sqlverificaidtroca = pg_query("select id from trocaproduto 
                                             where idvenda = $idtransacao 
                                               and idcliente = $idcliente
                                               and idprodutotrocado = $idprodutodevolvido");
            $rsverificaidtroca = pg_fetch_array ($sqlverificaidtroca);
            $idtrocaconsulta = $rsverificaidtroca['id'];
            if($idtrocaconsulta != ''){
                //Verificar se o produto trocado esta cadastrada na tabela pagamentodiferencatroca
                $sqlverificaprodutopagamento = pg_query("select count(*) existe
                                                           from pagamentodiferencatroca
                                                          where idtrocaproduto = $idtrocaconsulta");
                $rsverificaexistepagamentodiferenca = pg_fetch_array ($sqlverificaprodutopagamento);
                $existepagamentodiferenca = $rsverificaexistepagamentodiferenca['existe'];
                //Se existir no pagamento diferença vamos remover 
                if($existepagamentodiferenca > 0){
                    $sql   = "DELETE FROM pagamentodiferencatroca WHERE idtrocaproduto = $idtrocaconsulta";
                    $excluido = pg_query($sql);
                    
                    $sqlexcluiregistrotroca = "DELETE FROM trocaproduto where id = $idtrocaconsulta";
                    $excluidoregistrotroca = pg_query($sqlexcluiregistrotroca);
                }
            }
            
            //Calcular o valor do bônus
            $valorbonus = ($valorprodutodevolvidoconsultado - $valorprodutotrocadoconsultado);
            
            $sql = "insert into trocaproduto(idcliente, 
                                             datatroca, 
                                             horatroca, 
                                             idprodutotrocado, 
                                             idprodutodevolvido,
                                             valordiferenca,
                                             idcolaboradorresponsavel,
                                             statustroca,
                                             idvenda,
                                             porcentagemtroca) 
                                      VALUES($idcliente, 
                                             '$dataTroca', 
                                             '$horaTroca', 
                                             $idprodutotrocado, 
                                             $idprodutodevolvido,
                                             $valorbonus,
                                             $idcolaboradorresponsavel,
                                             'B',
                                             $idtransacao,
                                             null)";
            unset($dados['id']);
            unset($dados['idvendatroca']);
            unset($dados['tamanhonumero']);
            unset($dados['quantidade']);
            unset($dados['referencia']);
            unset($dados['quantidadedevolvida']);
            unset($dados['idcliente']);
            unset($dados['datatroca']);
            unset($dados['horatroca']);
            unset($dados['idprodutotrocado']);
            unset($dados['idprodutodevolvido']);
            unset($dados['valordiferenca']);
            unset($dados['idcolaboradorresponsavel']);
            unset($dados['statustroca']);
            unset($dados['nomeprodutodevolvido']);
            unset($dados['valor']);
            unset($dados['iditemproduto']);
            unset($dados['desconto_linha']);
            unset($dados['porcentagemtroca']);
            
            $query = $this->bd->prepare($sql);
            if($query->execute($dados) == 1){
                $sqlatualizaitensprodutos = "UPDATE itensproduto 
                                                SET idproduto = $idprodutotrocado 
                                              WHERE idproduto = $idprodutodevolvido
                                                AND idvenda = $idtransacao
                                                AND id = $iditemproduto";
                unset($dados['idproduto']);
                unset($dados['iditemproduto']);
                unset($dados['idvenda']);
                $queryatualizaitensprodutos = $this->bd->prepare($sqlatualizaitensprodutos);
                    //Atualizar
                    if($queryatualizaitensprodutos->execute($dados) == 1){
                        $string = "atualizadocomsucesso";
                        $encriptografa = base64_encode($string);
                        echo "<script>window.location.href='$urlbase/index.php?controle=diferencabonustrocaController&acao=listar&sucessmsmupdt=$encriptografa'</script>";
                    }
                }else{
                    $string = "naofoipossivelatualizar";
                    $encriptografa = base64_encode($string);
                    echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errorupdt=$encriptografa'</script>";
                }
        }else if($removervirgulavalorprodutodevolvido < $removervirgulavalorprodutotrocado){
            //CASO 2 -- o status da tabela relacaotrocaproduto sera D = Devedor
            //SE O VALOR FOR MAIOR DEVERÁ SER DIRECIONADO PARA  UMA MANUTENÇÃO ONDE O COLABORADOR IRÁ CONFIRMAR O PAGAMENTO
            //ESTE VALOR DEVERÁ SER ACRESCIDO NO RELATÓRIO DAS VENDAS
            //NESTA MANUTENÇÃO DEVERÁ SER TER UM CAMPO COM O VALOR DA TROCA EXIBIDO AUTOMÁTICAMENTE, E LOGO APÓS UM BOTÃO "PAGO"
            
            ############# BUSCAR O DESCONTO DO ITEM #############
            #DATA ALTERAÇÃO: 28/12/2017
            #AUTHOR: Marciano Luis Cadore
            
            //Executar SQL verificando o id da troca para usar no sql abaixo
            $sqlverificaidtroca = pg_query("select id from trocaproduto 
                                             where idvenda = $idtransacao 
                                               and idcliente = $idcliente 
                                               and idprodutotrocado = $idprodutodevolvido");
            $rsverificaidtroca = pg_fetch_array ($sqlverificaidtroca);
            $idtrocaconsulta = $rsverificaidtroca['id'];
            if($idtrocaconsulta != ''){
                $sqlverificaprodutotroca = pg_query("select count(*) existe
                                                       from trocaproduto
                                                      where id = $idtrocaconsulta");
                $rsverificaexistetrocaproduto = pg_fetch_array ($sqlverificaprodutotroca);
                $existetrocaproduto = $rsverificaexistetrocaproduto['existe'];

                //Se existir no pagamento diferença vamos remover 
                if($existetrocaproduto > 0){
                    $sql   = "DELETE FROM trocaproduto WHERE id = $idtrocaconsulta";
                    $excluido = pg_query($sql);
                }
            }
            
            //Aqui iremos buscar o valor da diferença do produto trocado - o produto devolvido
            $valordiferencatrocaproduto = ($valorprodutotrocadoconsultado - $valorprodutodevolvidoconsultado);
            
            $sql = "insert into trocaproduto(idcliente, 
                                             datatroca, 
                                             horatroca, 
                                             idprodutotrocado, 
                                             idprodutodevolvido,
                                             valordiferenca,
                                             idcolaboradorresponsavel,
                                             statustroca,
                                             idvenda,
                                             porcentagemtroca) 
                                      VALUES($idcliente, 
                                             '$dataTroca', 
                                             '$horaTroca', 
                                             $idprodutotrocado, 
                                             $idprodutodevolvido,
                                             $valordiferencatrocaproduto,
                                             $idcolaboradorresponsavel,
                                             'D',
                                             $idtransacao,
                                             $desconto_linha)";
                    
                    unset($dados['id']);
                    unset($dados['idvendatroca']);
                    unset($dados['tamanhonumero']);
                    unset($dados['quantidade']);
                    unset($dados['referencia']);
                    unset($dados['quantidadedevolvida']);
                    unset($dados['idcliente']);
                    unset($dados['datatroca']);
                    unset($dados['horatroca']);
                    unset($dados['idprodutotrocado']);
                    unset($dados['idprodutodevolvido']);
                    unset($dados['valordiferenca']);
                    unset($dados['idcolaboradorresponsavel']);
                    unset($dados['statustroca']);
                    unset($dados['nomeprodutodevolvido']);
                    unset($dados['valor']);
                    unset($dados['iditemproduto']);
                    unset($dados['desconto_linha']);
                    unset($dados['porcentagemtroca']);
                    $query = $this->bd->prepare($sql);
                    
                    if($query->execute($dados) == 1){
                        //ATUALIZAR
                        $sqlatualizaitensprodutos = "UPDATE itensproduto 
                                                        SET idproduto = $idprodutotrocado, 
                                                            desconto_linha = $desconto_linha 
                                                      WHERE idproduto = $idprodutodevolvido
                                                        AND idvenda = $idtransacao
                                                        AND id = $iditemproduto";
                        unset($dados['idproduto']);
                        unset($dados['desconto_linha']);
                        unset($dados['iditemproduto']);
                        unset($dados['idvenda']);
                        $queryatualizaitensprodutos = $this->bd->prepare($sqlatualizaitensprodutos);
                        
                        if($queryatualizaitensprodutos->execute($dados) == 1){
                            //ATUALIZAR O ITEM NA TABELA TROCA DE PRODUTO
                            //BUSCAR ULTIMO ID DA TROCA DO PRODUTO
                            $buscarultimoidtroca = pg_query("select max(id) as idtroca from trocaproduto troca");
                            $rsbuscarultimoidtroca = pg_fetch_array ($buscarultimoidtroca);
                            $ultimoidtroca = $rsbuscarultimoidtroca['idtroca'];
                            
                            //URL BASE
                            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/' . '' . $aplicacao . '/';
                            echo "<script>window.location.href='$urlbase/index.php?controle=pagamentotrocaController&acao=novo&ultimatroca=$ultimoidtroca'</script>";
                        }
                }else{
                    //URL BASE
                    $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/' . '' . $aplicacao . '/';
                    echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&error=2'</script>";
                }
        }else if($removervirgulavalorprodutodevolvido == $removervirgulavalorprodutotrocado){
            //OS VALORES DOS PRODUTOS SÃO IGUAIS, ENTÃO SÓ ALTERA O ITENS NA VENDA QUE JA EXISTE
            //Alteração no método 
            //Data Alteração: 28/12/2017
            //Author: Marciano Luis Cadore
            $sqlatualizaitensprodutos = "UPDATE itensproduto 
                                            SET idproduto = $idprodutotrocado, 
                                                desconto_linha = $desconto_linha 
                                          WHERE idproduto   = $idprodutodevolvido
                                            AND idvenda = $idtransacao";
            
            unset($dados['idproduto']);
            unset($dados['idvenda']);
            unset($dados['idprodutodevolvido']);
            unset($dados['idvendatroca']);
            unset($dados['idprodutotrocado']);
            unset($dados['quantidade']);
            unset($dados['nomeprodutodevolvido']);
            unset($dados['referencia']);
            unset($dados['tamanhonumero']);
            unset($dados['valor']);
            unset($dados['quantidadedevolvida']);
            unset($dados['iditemproduto']);
            unset($dados['desconto_linha']);
            unset($dados['porcentagemtroca']);
            $queryatualizaitensprodutos = $this->bd->prepare($sqlatualizaitensprodutos);
            
            if($queryatualizaitensprodutos->execute($dados) == 1){
                
                //consultar se o registro atraves do idcliente e produtotrocado ja existe, 
                //se ja existe é necessário realizar o update do registro
                $consultaregistrotroca = pg_query("select count(*) as existe 
                                                     from trocaproduto 
                                                    where idprodutotrocado = $idprodutodevolvido
                                                      and idvenda = $idtransacao");
                $rsconsultaregistrotroca = pg_fetch_array ($consultaregistrotroca);
                $existe = $rsconsultaregistrotroca['existe'];
                if($existe > 0){
                    //Iremos realizar o update do registro
                    $sqlatualizavalortroca = "update trocaproduto
                                                 set datatroca = '$dataTroca',
                                                     horatroca = '$horaTroca',
                                                     idprodutotrocado = $idprodutotrocado
                                               where idvenda = $idtransacao
                                                 and idcliente = $idcliente";
                    unset($dados['idproduto']);
                    unset($dados['idvenda']);
                    unset($dados['idprodutodevolvido']);
                    unset($dados['idvendatroca']);
                    unset($dados['idprodutotrocado']);
                    unset($dados['quantidade']);
                    unset($dados['nomeprodutodevolvido']);
                    unset($dados['referencia']);
                    unset($dados['tamanhonumero']);
                    unset($dados['valor']);
                    unset($dados['quantidadedevolvida']);
                    unset($dados['iditemproduto']);
                    unset($dados['desconto_linha']);
                    unset($dados['porcentagemtroca']);
                    $queryatualizatrocaprodutos = $this->bd->prepare($sqlatualizavalortroca);
                    if($queryatualizatrocaprodutos->execute($dados) == 1){
                        $stringx = "atualizadocomsucesso";
                        $encriptografa = base64_encode($stringx);
                        echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&sucessmsmupdt=$encriptografa'</script>";  
                    }
                }else{
                    //inserir valor na tabela trocaproduto
                    $sql = "insert into trocaproduto(idcliente, 
                                                     datatroca, 
                                                     horatroca, 
                                                     idprodutotrocado, 
                                                     idprodutodevolvido,
                                                     valordiferenca,
                                                     idcolaboradorresponsavel,
                                                     statustroca,
                                                     idvenda,
                                                     porcentagemtroca) 
                                              VALUES($idcliente, 
                                                     '$dataTroca', 
                                                     '$horaTroca', 
                                                     $idprodutotrocado, 
                                                     $idprodutodevolvido,
                                                     0,
                                                     $idcolaboradorresponsavel,
                                                     '',
                                                     $idtransacao,
                                                     $desconto_linha)";
                    unset($dados['id']);
                    unset($dados['idvendatroca']);
                    unset($dados['tamanhonumero']);
                    unset($dados['quantidade']);
                    unset($dados['referencia']);
                    unset($dados['quantidadedevolvida']);
                    unset($dados['idcliente']);
                    unset($dados['datatroca']);
                    unset($dados['horatroca']);
                    unset($dados['idprodutotrocado']);
                    unset($dados['idprodutodevolvido']);
                    unset($dados['valordiferenca']);
                    unset($dados['idcolaboradorresponsavel']);
                    unset($dados['statustroca']);
                    unset($dados['nomeprodutodevolvido']);
                    unset($dados['valor']);
                    unset($dados['iditemproduto']);
                    unset($dados['desconto_linha']);
                    unset($dados['porcentagemtroca']);
                    $query = $this->bd->prepare($sql);

                    if($query->execute($dados) == 1){
                       $stringx = "atualizadocomsucesso";
                       $encriptografa = base64_encode($stringx);
                       echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&sucessmsmupdt=$encriptografa'</script>";  
                    }
                }
            }            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function buscarBonus() {
        $sql = "select CASE WHEN rap.nome is null THEN cli.nome 
                                ELSE rap.nome
                         END as nomecliente,
                         produtotrocado.nomeproduto as nomeprodutotrocado,
                         produtodevolvido.nomeproduto as nomeprodutodevolvido,
                         col.nome as nomecolaboradorresponsavel,
                         to_char(troca.datatroca, 'dd/MM/yyyy') as datatroca,
                         to_char(troca.horatroca, 'HH24:mi') as horatroca,
                         'R$ ' || ltrim(to_char(troca.valordiferenca, '9G999G990D99')) as valordiferenca,
                         CASE WHEN troca.statustroca = 'B' THEN 'Bônus' 
                                ELSE 'Devedor'
                         END as statustroca,
                         troca.idvenda,
                         troca.idcliente
                   from trocaproduto troca
                  inner join produto as produtotrocado
                     on troca.idprodutotrocado = produtotrocado.id
                  inner join produto as produtodevolvido
                     on troca.idprodutodevolvido = produtodevolvido.id
                  inner join colaboradores col
                     on troca.idcolaboradorresponsavel = col.id
                   left join cliente cli
                     on troca.idcliente = cli.id
                   left join clienterapido rap
                     on troca.idcliente = rap.id 
                   where troca.statustroca = 'B'
                  order by datatroca, horatroca, nomecliente asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarTodos() {
        $sql = "select CASE WHEN rap.nome is null THEN cli.nome 
                                ELSE rap.nome
                         END as nomecliente,
                         produtotrocado.nomeproduto as nomeprodutotrocado,
                         produtodevolvido.nomeproduto as nomeprodutodevolvido,
                         col.nome as nomecolaboradorresponsavel,
                         to_char(troca.datatroca, 'dd/MM/yyyy') as datatroca,
                         to_char(troca.horatroca, 'HH24:mi') as horatroca,
                         'R$ ' || ltrim(to_char(troca.valordiferenca, '9G999G990D99')) as valordiferenca,
                         CASE WHEN troca.statustroca = 'B' THEN 'Bônus' 
                                ELSE 'Devedor'
                         END as statustroca,
                         troca.idvenda,
                         troca.idcliente
                   from trocaproduto troca
                  inner join produto as produtotrocado
                     on troca.idprodutotrocado = produtotrocado.id
                  inner join produto as produtodevolvido
                     on troca.idprodutodevolvido = produtodevolvido.id
                  inner join colaboradores col
                     on troca.idcolaboradorresponsavel = col.id
                   left join cliente cli
                     on troca.idcliente = cli.id
                   left join clienterapido rap
                     on troca.idcliente = rap.id 
                  order by datatroca, horatroca, nomecliente asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}
