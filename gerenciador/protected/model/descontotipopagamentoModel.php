<?php

class DescontotipopagamentoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    
    public function inserir(array $dados) {
        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $logincolaborador = $_SESSION['login'];
        //Buscar o Id do colaborador que está permitindo desconto no tipo da venda
        $consultaidcolaborador = "select id from colaboradores where login = '$logincolaborador'";
        $sqlconsultaidcolaborador = $this->bd->prepare($consultaidcolaborador);
        $sqlconsultaidcolaborador->execute();
        if ($sqlconsultaidcolaborador->rowCount() > 0) {
            foreach ($sqlconsultaidcolaborador as $rs){
                $idcolaboradorconsulta = $rs["id"];
            }
        }
        $dataCadastro = date('d/m/Y');
        $idtipopagamento = $_POST['idtipopagamento'];
        $permitirdesconto = $_POST['permitirdesconto'];
        
        $idcolaborador = $idcolaboradorconsulta;
        
        $sql = "INSERT INTO descontotipopagamento(idtipopagamento, permitirdesconto, idcolaborador, datadescontopermitido) "
                . " VALUES($idtipopagamento, '$permitirdesconto', $idcolaborador, '$dataCadastro')";
        
        unset($dados['id']);
        unset($dados['idtipopagamento']);
        unset($dados['permitirdesconto']);
        unset($dados['idcolaborador']);
        unset($dados['datacadastro']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function buscarTodos() {
        $sql = "SELECT desconto.id,
                       desconto.permitirdesconto, 
                       col.nome as nomecolaborador,
                       tipo.descricao as tipopagamento,
                       to_char(desconto.datadescontopermitido,'dd/MM/yyyy') as datadescontopermitido 
                  FROM descontotipopagamento desconto
                 INNER JOIN tipopagamento tipo
                    ON desconto.idtipopagamento = tipo.id
                 INNER JOIN colaboradores col
                    ON desconto.idcolaborador = col.id
                 ORDER BY tipo.descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscar($id) {
        $sql = "select id, idtipopagamento, permitirdesconto, to_char(datadescontopermitido, 'dd/MM/yyyy') as datadescontopermitido FROM descontotipopagamento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));
        return $query->fetch();
    }
    
    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        //Buscar o Id do colaborador que está permitindo desconto no tipo da venda
        $logincolaborador = $_SESSION['login']; 
        $consultaidcolaborador = "select id from colaboradores where login = '$logincolaborador'";
        $sqlconsultaidcolaborador = $this->bd->prepare($consultaidcolaborador);
        $sqlconsultaidcolaborador->execute();
        if ($sqlconsultaidcolaborador->rowCount() > 0) {
            foreach ($sqlconsultaidcolaborador as $rs){
                $idcolaboradorconsulta = $rs["id"];
            }
        }
        
        $dataCadastro = date('d/m/Y');
        $idcolaborador = $idcolaboradorconsulta;
        $sql = "UPDATE descontotipopagamento SET idtipopagamento = :idtipopagamento, permitirdesconto = :permitirdesconto, idcolaborador = $idcolaborador, datadescontopermitido = '$dataCadastro' WHERE id = :id";
        unset($dados['datacadastro']);
        unset($dados['idcolaborador']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function excluir($id) {
        $sql = "DELETE FROM descontotipopagamento WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }
}