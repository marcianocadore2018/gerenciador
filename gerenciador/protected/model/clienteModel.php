﻿<?php

/**
 * @author Felipe Müller - <felipe@camul.com.br>
 * -----------------------------------------------------------------------------
 * Este arquivo é parte do projeto Gerenciador Atlanta Jeans mantido pela Camul
 * www.camul.com.br
 * -----------------------------------------------------------------------------
 * Camada model, contém SQL de: INSERT, UPDATE, SELECT e EXCLUIR
 * -----------------------------------------------------------------------------
 * Funções internas
 * -----------------------------------------------------------------------------
 * @param inserir($dados)           = Insere os $dados (contém o post que veio do frontend)
 * @param buscarTodos()             = Busca todos os dados do BD
 * @param buscar($id)               = Busca os dados passando como parametro um ID
 * @param atualizar($dados)         = Faz um UPDADE no BD com os $dados
 * @param excluir($id)              = Exclui um registro referenciado pelo ID
 * ------------------------------------------------------------------------------
 */

class ClienteModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {

        $nome                        = $_POST['nome'];
        $datanascimento              = $_POST['datanascimento'];
        $sexo                        = $_POST['sexo'];
        $rg                          = $_POST['rg'];
        $cpf                         = $_POST['cpf'];
        $situacaocliente             = $_POST['situacaocliente'];
        $observacao                  = $_POST['observacao'];
        $endereco                    = $_POST['endereco'];
        $bairro                      = $_POST['bairro'];
        $numero                      = $_POST['numero'];
        $cidade                      = $_POST['cidade'];
        $idestado                    = $_POST['idestado'];
        $cep                         = $_POST['cep'];
        $complemento                 = $_POST['complemento'];
        $telefone                    = $_POST['telefone'];
        $celular                     = $_POST['celular'];
        $contato                     = $_POST['contato'];
        $email                       = $_POST['email'];
        $justificativaclienteinativo = $_POST['justificativaclienteinativo'];

        $dadosTmp                    = $dados;

        //Verifica CPF
        $sql = "SELECT cpf FROM cliente WHERE cpf = '" . $cpf . "'";

        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return "CPF do cliente já existente. Favor informar outro CPF!";
        }else{

            //Busca a seção do usuário 
            $logincolaborador = $_SESSION['login'];
            $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
            $sqllogin = $this->bd->prepare($logincolaborador);
            $sqllogin->execute();
            if ($sqllogin->rowCount() > 0) {
                foreach ($sqllogin as $rs) {
                    $idcolaborador = $rs["idcolaborador"];
                    $idloja        = $rs["idloja"];
                }
            }

            $sql = "INSERT INTO registropessoa(endereco, bairro, idestado, cidade, cep, telefone, email, numero, complemento) "
                    . " VALUES('$endereco', '$bairro', $idestado, '$cidade', '$cep', '$telefone', '$email', $numero, '$complemento')";
            
            unset($dados['id']);
            unset($dados['nome']);
            unset($dados['datanascimento']);
            unset($dados['sexo']);
            unset($dados['rg']);
            unset($dados['cpf']);
            unset($dados['endereco']);
            unset($dados['bairro']);
            unset($dados['cep']);
            unset($dados['complemento']);
            unset($dados['celular']);
            unset($dados['telefone']);
            unset($dados['numero']);
            unset($dados['email']);
            unset($dados['cidade']);
            unset($dados['contato']);
            unset($dados['situacaocliente']);
            unset($dados['justificativaclienteinativo']);
            unset($dados['observacao']);
            unset($dados['idregistropessoa']);
            unset($dados['idcolaborador']);
            unset($dados['idloja']);
            unset($dados['idestado']);
            
            $query = $this->bd->prepare($sql); 
            $res   = $query->execute($dados);

            $sql   = "SELECT MAX(id) as id FROM registropessoa;";
            $res   = $this->bd->prepare($sql);
            $res->execute();
            if ($res->rowCount() > 0) {
                foreach ($res as $rs) {
                    $idregistropessoa = $rs["id"];
                }
            }
            $sql = "INSERT INTO cliente(nome, datanascimento, sexo, rg, cpf, celular, contato, situacaocliente, justificativaclienteinativo, observacao, idcolaborador, idloja, idregistropessoa) "
                    . " VALUES('$nome', '$datanascimento', '$sexo', '$rg', '$cpf', '$celular', '$contato', $situacaocliente, '$justificativaclienteinativo', '$observacao', $idcolaborador, $idloja, $idregistropessoa)";
            
            $query = $this->bd->prepare($sql); 
            return $query->execute($dados);
        }
    }

    public function buscarTodos() {
        $sql = "SELECT 
                    c.id, 
                    upper(c.nome) as nome, 
                    to_char(c.datanascimento,'dd/MM/yyyy') as datanascimento , 
                    c.sexo, 
                    c.rg, 
                    c.cpf, 
                    c.celular, 
                    c.contato, 
                    c.situacaocliente, 
                    c.justificativaclienteinativo, 
                    c.observacao,
                    upper(p.cidade) as cidade,
                    upper(p.bairro) as bairro,
                    p.cep,
                    p.numero,
                    upper(p.complemento) as complemento,
                    e.uf
                FROM 
                    cliente c
                INNER JOIN 
                    registropessoa p
                ON 
                    c.idregistropessoa = p.id
                INNER JOIN 
                    estado e
                ON  
                    p.idestado = e.id
                ORDER BY 
                    c.nome;";
        $query = $this->bd->query($sql); 
        return $query->fetchAll();
    }
    
    public function buscarClientevenda() {
        $sql = "SELECT c.id, 
                       upper(c.nome) as nome, 
                       to_char(c.datanascimento,'dd/MM/yyyy') as datanascimento , 
                       c.sexo, 
                       c.rg, 
                       c.cpf, 
                       c.celular, 
                       c.contato, 
                       c.situacaocliente, 
                       c.justificativaclienteinativo, 
                       c.observacao,
                       upper(p.cidade) as cidade,
                       upper(p.bairro) as bairro,
                       p.cep,
                       p.numero,
                       upper(p.complemento) as complemento,
                       e.uf
                  FROM cliente c
                 INNER JOIN registropessoa p
                    ON c.idregistropessoa = p.id
                 INNER JOIN estado e
                    ON p.idestado = e.id
                 UNION ALL
                SELECT rap.id,
                       CASE WHEN rap.sobrenome is null THEN rap.nome 
                       ELSE (rap.nome || ' ' || rap.sobrenome) END as nome,
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       '',
                       null,
                       '',
                       ''
                  FROM clienterapido rap
                 ORDER BY nome asc;";
        $query = $this->bd->query($sql); 
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT 
                    c.id, 
                    upper(c.nome) as nome,
                    to_char(c.datanascimento,'dd/MM/yyyy') as datanascimento , 
                    c.sexo, 
                    c.rg, 
                    c.cpf, 
                    c.celular, 
                    c.contato, 
                    c.situacaocliente, 
                    c.justificativaclienteinativo, 
                    c.observacao,
                    c.idcolaborador,
                    c.idloja,
                    p.id as idregistropessoa,
                    p.endereco,
                    upper(p.bairro) as bairro,
                    p.idestado,
                    upper(p.cidade) as cidade,
                    p.cep,
                    p.telefone,
                    p.email,
                    p.numero,
                    upper(p.complemento) as complemento
                FROM 
                    cliente c,
                    registropessoa p
                WHERE
                    p.id = c.idregistropessoa
                AND     
                    c.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function calculaLimiteDisponivel($idcliente){ 
        //Busca a seção do usuário 
        $logincolaborador = $_SESSION['login'];
        $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($logincolaborador);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }

        $sql = "SELECT to_char(SUM(rc.valorrenda) * (SELECT porcentagem FROM PORCENTAGEMCREDIARIOCLIENTE)/100, 'L9G999G990D99') AS limite FROM RENDACLIENTE rc WHERE rc.idcliente = $idcliente"; 
        $res   = $this->bd->prepare($sql);
        $res->execute();
        if ($res->rowCount() > 0) {
            foreach ($res as $rs) {
                $limite = $rs["limite"];
            }
        }

        return $limite;
    }

    public function buscarUltimo() {
        $sql   = "SELECT MAX(id) as id FROM cliente;";
        $res   = $this->bd->prepare($sql);
        $res->execute();
        if ($res->rowCount() > 0) {
            foreach ($res as $rs) {
                $idcliente = $rs["id"];
            }
        }

        return $idcliente;
    }

    public function buscaAniversariantes($indicador){

        $sql = "SELECT
                    c.id,
                    lower(c.nome) as nome,
                    p.email 
                FROM 
                    cliente c, 
                    registropessoa p 
                WHERE 
                    extract(Month from c.datanascimento) = extract(month from now()) 
                AND 
                    c.idregistropessoa = p.id
                AND
                    c.email_enviado = $indicador
                UNION ALL
                SELECT r.id,
                       lower(r.nome) as nome,
                       r.email 
                  FROM clienterapido r
                 WHERE extract(Month from r.dataaniversario) = extract(month from now())
                   AND r.email_enviado = $indicador";

        $res = $this->bd->query($sql);
        return $res;
    }

    public function atualizaListaEmail($id_cliente){
        //Atualiza Cliente
        $sql   = "UPDATE cliente SET email_enviado = true WHERE id = $id_cliente";
        $query = $this->bd->prepare($sql);
        $res   = $query->execute();
        
        //Atualiza Cliente Rápido
        $sql   = "UPDATE clienterapido SET email_enviado = true WHERE id = $id_cliente";
        $query = $this->bd->prepare($sql);
        $res   = $query->execute();
    }

    public function atualizar(array $dados) {

        $dadosTmp = $dados;

        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['datanascimento']);
        unset($dados['sexo']);
        unset($dados['rg']);
        unset($dados['cpf']);
        unset($dados['celular']);
        unset($dados['contato']);
        unset($dados['situacaocliente']);
        unset($dados['justificativaclienteinativo']);
        unset($dados['observacao']);
        unset($dados['idcolaborador']);
        unset($dados['idloja']);

        $sql = "UPDATE registropessoa SET
                    endereco = :endereco,
                    bairro   = :bairro,
                    idestado = :idestado,
                    cidade   = :cidade,
                    cep      = :cep,
                    telefone = :telefone,
                    email    = :email,
                    numero   = :numero,
                    complemento =:complemento
                WHERE id = :idregistropessoa";

        $query = $this->bd->prepare($sql);
        $res   = $query->execute($dados);

        $dados = $dadosTmp;

        //unset($dados['id']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['idestado']);
        unset($dados['cidade']);
        unset($dados['cep']);
        unset($dados['telefone']);
        unset($dados['email']);
        unset($dados['numero']);
        unset($dados['complemento']);

        $sql = "UPDATE cliente SET
                    nome                            = :nome, 
                    datanascimento                  = :datanascimento, 
                    sexo                            = :sexo, 
                    rg                              = :rg, 
                    cpf                             = :cpf, 
                    celular                         = :celular, 
                    contato                         = :contato, 
                    situacaocliente                 = :situacaocliente, 
                    justificativaclienteinativo     = :justificativaclienteinativo, 
                    observacao                      = :observacao,
                    idcolaborador                   = :idcolaborador,
                    idloja                          = :idloja,
                    idregistropessoa                = :idregistropessoa
                WHERE id = :id";
                
        //Quando estiver selecionado a situação do cliente em Ativo
        //deve-se verificar se a justificativa do cliente inativo esta em branco
        //caso nao esteja, deve-se realizar a limpeza do campo
        if($dados['situacaocliente'] == '0' && $dados['justificativaclienteinativo'] != ''){
            $dados['justificativaclienteinativo'] = '';
        }

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        
        $idverificapessoa = $_GET['id'];
        $decodeget = base64_decode($idverificapessoa);
        $remover = str_replace("passar", "", $decodeget);
        $idverificapessoa  = str_replace("metodoget", "", $remover);

        //Verifica o registro pessoa para alterar
        $verificaregistropessoa = "select c.idregistropessoa, c.id
                                     from cliente c
                                    inner join registropessoa r
                                       on c.idregistropessoa = r.id
                                    where c.id = $idverificapessoa";
        
        $sqlverificaregistropessoa = $this->bd->prepare($verificaregistropessoa);
        $sqlverificaregistropessoa->execute();
        if ($sqlverificaregistropessoa->rowCount() > 0) {
            foreach ($sqlverificaregistropessoa as $rs) {
                $verificaidregistropessoa = $rs["idregistropessoa"];
                $idcliente                = $rs["id"];
            }
        }
        
        $rendacliente    = "delete from rendacliente where idcliente = $id";
        $sql = $this->bd->prepare($rendacliente);
        $sql->execute();

        $registrocliente = "delete from cliente where id = $idverificapessoa";
        $sql = $this->bd->prepare($registrocliente);
        $sql->execute();
        
        $sqlregistropessoa = "delete from registropessoa where id = $verificaidregistropessoa";
        $query = $this->bd->prepare($sqlregistropessoa);

        return $query->execute();
    }

}