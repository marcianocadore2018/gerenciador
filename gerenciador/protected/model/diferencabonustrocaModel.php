<?php

class DiferencabonustrocaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    public function buscarTodos() {
        $sql = "SELECT CASE WHEN rap.nome is null THEN cli.nome 
                                ELSE rap.nome
                       END as nomecliente,
                       proddevolvido.referencia || ' - ' || proddevolvido.nomeproduto as nomeprodutodevolvido,
                       prodtrocado.referencia || ' - ' || prodtrocado.nomeproduto as nomeprodutotrocado,
                       to_char(troca.datatroca, 'dd/MM/yyyy') as datatroca,
                       to_char(troca.horatroca, 'HH24:MI') as horatroca,
                       troca.valordiferenca as valordiferenca,
                       col.nome as nomecolaborador
                  FROM trocaproduto troca
                 INNER JOIN produto proddevolvido
                    ON troca.idprodutodevolvido = proddevolvido.id
                 INNER JOIN produto prodtrocado
                    ON troca.idprodutotrocado = prodtrocado.id
                  LEFT JOIN cliente cli
                   ON troca.idcliente = cli.id
                 LEFT JOIN clienterapido rap
                   ON troca.idcliente = rap.id
                INNER JOIN colaboradores col
                   ON troca.idcolaboradorresponsavel = col.id
                WHERE troca.statustroca = 'B'
                ORDER BY troca.datatroca desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}