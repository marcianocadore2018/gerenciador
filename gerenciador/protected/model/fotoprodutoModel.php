<?php

class FotoprodutoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }
    
    
    public function inserir(array $dados) {
        //Pegar o diretorio do projeto
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/gerenciador/protected/imagens/uploads/';
        $caminhodiretorio = 'gerenciador/protected/imagens/uploads/';
        
        $arquivofoto = $_FILES['arquivo']['name'];
        $nomefotoaleatorio = $arquivofoto;
        //str_shuffle gera um nome aleatório
        $nomealeatorio = str_shuffle($nomefotoaleatorio);
        //se houver algum ponto ou virgula remover por nada
        $nomefoto = str_replace("." , "" , $nomealeatorio); // Primeiro tira os pontos
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        //Pega extensão da imagem
        $extensaoimagem = substr($arquivofoto, -4);
        $destino = $diretorio . $nomefoto . $extensaoimagem;
        //alterar extensão de imagem para minúscula
        $res = strtolower($extensaoimagem);
        if($res != ''){
            if($res == '.jpg' || $res == '.png'){
                //$arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                move_uploaded_file($arquivo_tmp, $destino);
                //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
                $caminhofoto = $caminhodiretorio;
                $nomefotoaleatorio = $nomealeatorio;
                $nomefotoaleatoriosemponto = str_replace("." , "" , $nomefotoaleatorio);
                $nomefoto = $nomefotoaleatoriosemponto . $res;
            }else{
                echo '<div style="color: red;">A imagem não pode ser gravada, pois sua extensão não é válida. Extensões válidas (<b>.jpg, .png</b>)</div></br>';
                $caminhofoto = '';
                $nomefoto = '';
            }
        }
        $idproduto = $_POST['idproduto'];
        $principal = $_POST['principal'];
        $ordem = $_POST['ordem'];
        if($ordem != ''){
            $ordemproduto = $ordem;
        }else if($ordem == ''){
            $ordemproduto = 'null';
        }
        
        $sql = "INSERT INTO fotoproduto(principal, ordem, idproduto, nomefoto, caminhofoto) 
                        VALUES('$principal', $ordemproduto, $idproduto, '$nomefoto', '$caminhofoto')";
        unset($dados['id']);
        unset($dados['idproduto']);
        unset($dados['principal']);
        unset($dados['caminhofoto']);
        unset($dados['nomefoto']);
        unset($dados['ordem']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        //URL BASE
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
    	$sql = "select foto.id,
                       foto.principal,
                       foto.ordem,
                       foto.idproduto,
                       pro.nomeproduto,
                       pro.numeroproduto,
                       pro.tamanho,
                       pro.referencia,
                       ('$urlbase' || '' || foto.caminhofoto || '' || foto.nomefoto) as fotoproduto
                  from fotoproduto foto
                 inner join produto pro
                    on foto.idproduto = pro.id
                 order by foto.principal";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        //URL BASE
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "select foto.id,
                       foto.principal,
                       foto.ordem,
                       foto.idproduto,
                       pro.referencia,
                       pro.numeroproduto,
                       pro.tamanho,
                       pro.nomeproduto,
                       ('$urlbase' || '' || foto.caminhofoto || '' || foto.nomefoto) as fotoproduto
                  from fotoproduto foto
                 inner join produto pro
                    on foto.idproduto = pro.id
                 WHERE foto.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/gerenciador/protected/imagens/uploads/';
        $caminhodiretorio = 'gerenciador/protected/imagens/uploads/';
        
        //Consulta se nome da imagem ja existe e exclui a imagem do diretóio se o usuário alterar um registro 
        //que ja possui o mesmo nome da imagem
        $consultaimagem = "select nomefoto from fotoproduto where id = " . $_POST['id'];
        $sqlconsultaimagem = $this->bd->prepare($consultaimagem);
        $sqlconsultaimagem->execute();
        
        if ($sqlconsultaimagem->rowCount() > 0) {
            foreach ($sqlconsultaimagem as $rs){
                $nomeimagemveiculo = $rs["nomefoto"];
                if($nomeimagemveiculo != null){
                    //Pega o caminho da imagem
                    $apagararquivo = $diretorio . $nomeimagemveiculo;

                    //Remover arquivo de imagem
                    if(!unlink($apagararquivo)){
                      echo ("<div style='color:red'><b>Não foi possível alterar a imagem! </b></div>");
                    }else{
                      echo ("<div style='color:green'><b>Imagem alterada com sucesso!</b></div>");
                    }
                }
            }
        }
        
        $arquivofoto = $_FILES['arquivo']['name'];
        $nomefotoaleatorio = $arquivofoto;
        $nomealeatorio = str_shuffle($nomefotoaleatorio);
        
        //se houver algum ponto ou virgula remover por nada
        $nomefoto = str_replace("." , "" , $nomealeatorio);
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        $extensaoimagem = substr($arquivofoto, -4);
        
        //alterar extensão de imagem para minúscula
        $destino = $diretorio . $nomefoto . $extensaoimagem;
        
        //Converter a extensão para letra minúscula
        $res = strtolower($extensaoimagem);
        
        //Verificar se a imagem esta indo vazia
        if($res != ''){
            if($res == '.jpg' || $res == '.png'){
            $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
            move_uploaded_file($arquivo_tmp, $destino);

            //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
            $caminhofoto = $caminhodiretorio;
            $nomefotoaleatorio = $nomealeatorio;
            $nomefotoaleatoriosemponto = str_replace("." , "" , $nomefotoaleatorio);
            $nomefoto = $nomefotoaleatoriosemponto . $extensaoimagem;
            
            }else{
                echo '<div style="color: red;">A imagem não pode ser gravada, pois sua extensão não é válida. Extensões válidas (<b>.jpg, .png</b>)</div></br>';
                $caminhofoto = '';
                $nomefoto = '';
            }
        }
        
        $idproduto = $_POST['idproduto'];
        $principal = $_POST['principal'];
        $ordem = $_POST['ordem'];
        if($ordem != ''){
            $ordemproduto = $ordem;
        }else if($ordem == ''){
            $ordemproduto = 'null';
        }        
        
        $sql = "UPDATE fotoproduto 
                   SET idproduto = $idproduto,
                       principal  = '$principal',
                       ordem = $ordemproduto,
                       caminhofoto  = '$caminhofoto',
                       nomefoto  = '$nomefoto'
                 WHERE id = :id";
        unset($dados['nomefoto']);
        unset($dados['caminhofoto']);
        unset($dados['idproduto']);
        unset($dados['principal']);
        unset($dados['ordem']);

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/gerenciador/protected/imagens/uploads/';
        
        //Verifica qual é o nome da imagem se existir na base de dados para realizar a exclusão do arquivo
        $consultaimagem = "select nomefoto from fotoproduto where id = " . $_GET['id'];
        $sqlconsultaimagem = $this->bd->prepare($consultaimagem);
        $sqlconsultaimagem->execute();
        
        //Exclusão de foto
        if ($sqlconsultaimagem->rowCount() > 0) {
            foreach ($sqlconsultaimagem as $rs){
                $nomeimagemveiculo = $rs["nomefoto"];
                //Pega o caminho da imagem
                $apagararquivo = $diretorio . $nomeimagemveiculo;
                
                //Remover arquivo de imagem
                if(!unlink($apagararquivo)){
                  echo ("<div style='color:red'><b>Não foi possível deletar a imagem! </b></div>");
                }else{
                  echo ("<div style='color:green'><b>Imagem deletada com sucesso!</b></div>");
                }
            }
        }
        //Exclusão do registro
        $sql = "DELETE FROM fotoproduto WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }
}