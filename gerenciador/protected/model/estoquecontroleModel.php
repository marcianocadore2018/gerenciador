<?php

class EstoquecontroleModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "select  'Entrada' as entrada,
	                pro.id as idproduto,
	                pro.referencia as referenciaproduto,
	                gru.descricao as nomegrupo,
	                pro.nomeproduto as nomeproduto,
	                pro.numeroproduto as numeroproduto,
	                pro.cor as corproduto,
	                pro.tamanho as tamanhoproduto,
	                CASE WHEN pro.sexo='M' THEN 'Masculino'
	                     WHEN pro.sexo='F' THEN 'Feminino'
		        END as generoproduto,
	                pro.quantidade as quantidadeproduto,
	                to_char(pro.valor, 'L9G999G990D99') as valorproduto,
	                pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel
                   from produto pro
                  inner join grupo gru
                     on pro.idgrupo = gru.id
                   left join itensproduto itensprod
                     on itensprod.idproduto = pro.id
                  group by itensprod.idproduto, 
                           pro.id,
                           gru.descricao
                  order by pro.nomeproduto;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarFiltroEntrada($idgrupo) {
        if($_POST['idgrupo'] == null){
            $pesquisafiltro = '';
        }else{
            $pesquisafiltro = 'where pro.idgrupo = ' . $idgrupo;
        }
        $sql = "select  'Entrada' as entrada,
	                pro.id as idproduto,
	                pro.referencia as referenciaproduto,
	                gru.descricao as nomegrupo,
	                pro.nomeproduto as nomeproduto,
	                pro.numeroproduto as numeroproduto,
	                pro.cor as corproduto,
	                pro.tamanho as tamanhoproduto,
	                CASE WHEN pro.sexo='M' THEN 'Masculino'
	                     WHEN pro.sexo='F' THEN 'Feminino'
		        END as generoproduto,
	                pro.quantidade as quantidadeproduto,
	                to_char(pro.valor, 'L9G999G990D99') as valorproduto,
	                pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel
                   from produto pro
                  inner join grupo gru
                     on pro.idgrupo = gru.id
                   left join itensproduto itensprod
                     on itensprod.idproduto = pro.id
                  $pesquisafiltro
                  group by itensprod.idproduto, 
                           pro.id,
                           gru.descricao
                  order by pro.nomeproduto;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarFiltroSaida($idgrupo) {
        if($idgrupo!=''){
            $consultagruposaida = " where pro.idgrupo = $idgrupo";
        }else{
            $consultagruposaida = '';
        }

        $sql = "select  pro.nomeproduto as nomeproduto,
                        pro.referencia as referenciaproduto,
                        gru.descricao as nomegrupo,
                        pro.numeroproduto as numeroproduto,
                        pro.cor as corproduto,
                        CASE WHEN pro.sexo='M' THEN 'Masculino'
                             WHEN pro.sexo='F' THEN 'Feminino'
                        END as generoproduto,
                        to_char(pro.valor, 'L9G999G990D99') as valorproduto,
                        pro.quantidade - sum(itensprod.quantidade) as quantidadedisponivel,
                        pro.quantidade - (pro.quantidade - sum(itensprod.quantidade)) as quantidadevendidas
                   from produto pro
                  inner join grupo gru
                     on pro.idgrupo = gru.id
                  inner join itensproduto itensprod
                     on itensprod.idproduto = pro.id
                        $consultagruposaida
                  group by itensprod.idproduto, 
                           pro.id,
                           gru.descricao
                  order by pro.nomeproduto;";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            
          return $sql;
        }else{
          return null;
        }
    }
}