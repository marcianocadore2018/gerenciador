<?php

class PagamentodiferencatrocaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        
        $idtrocaproduto = $_POST['idtrocaproduto'];
        $idformapagamento = $_POST['idformapagamento'];
        
        $sql = "INSERT INTO pagamentodiferencatroca(idtrocaproduto,
                                                    formapgto)
                                             VALUES($idtrocaproduto,
                                                    $idformapagamento)";
        unset($dados['id']);
        unset($dados['idtrocaproduto']);
        unset($dados['idcliente']);
        unset($dados['datatroca']);
        unset($dados['horatroca']);
        unset($dados['valordiferenca']);
        unset($dados['idformapagamento']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT CASE WHEN rap.nome is null THEN cli.nome 
                                ELSE rap.nome
                       END as nomecliente,
                       CASE WHEN pgtotroca.formapgto = 1 THEN 'Dinheiro' 
                            WHEN pgtotroca.formapgto = 2 THEN 'Cartão de Débito'
                            ELSE 'Cartão de Crédito'
                       END as formapagamento,
                       proddevolvido.nomeproduto as nomeprodutodevolvido,
                       prodtrocado.nomeproduto as nomeprodutotrocado,
                       to_char(troca.datatroca, 'dd/MM/yyyy') as datatroca,
                       to_char(troca.horatroca, 'HH24:MI') as horatroca,
                       (troca.valordiferenca - ) as valordiferenca,
                       col.nome as nomecolaborador
                 FROM pagamentodiferencatroca pgtotroca
                INNER JOIN trocaproduto troca
                   ON pgtotroca.idtrocaproduto = troca.id
                INNER JOIN produto proddevolvido
                   ON troca.idprodutodevolvido = proddevolvido.id
                INNER JOIN produto prodtrocado
                   ON troca.idprodutotrocado = prodtrocado.id
                 LEFT JOIN cliente cli
                   ON troca.idcliente = cli.id
                 LEFT JOIN clienterapido rap
                   ON troca.idclienterapido = rap.id
                INNER JOIN colaboradores col
                   ON troca.idcolaboradorresponsavel = col.id
                ORDER BY troca.datatroca desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}
