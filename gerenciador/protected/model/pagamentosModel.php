<?php

class PagamentosModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    //SALVAR CAPA DO PAGAMENTO

    public function salvaPgtoCapa($dados){

        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');
        //$data = date('d/m/Y');
        $hora = date('H:i:s');

        //Busca a seção do usuário logado
        $logincolaborador = $_SESSION['login'];
        $login = "SELECT id AS idcolaborador, idloja FROM colaboradores WHERE login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }
     

        if($dados['id']==''){
            $sql = "INSERT INTO pagamentos (
                  idcliente, 
                  idcolaborador, 
                  datapagamento, 
                  horapagamento, 
                  valorpago, 
                  numeroparcela,
                  statuspagamento,
                  tipopagamento)
                VALUES(
                  :cliente,
                  $idcolaborador,
                  :datapagamento,
                  '$hora',
                  ".$dados['valorpago'].",
                  1,
                  '0',
                  '0'
                );";

                unset($dados['id']);
                unset($dados['valorpago']);
       
        }else{
            $sql = "UPDATE 
                        pagamentos 
                    SET (
                            idcliente = :cliente, 
                            idcolaborador = $idcolaborador, 
                            datapagamento = :datapagamento, 
                            horapagamento = '$hora', 
                            valorpago = ".$dados['valorpago'].", 
                            numeroparcela = 1, 
                            statuspagamento = '0', 
                            tipopagamento = '0'
                        )
                   WHERE 
                        id = :id";

                unset($dados['valorpago']);
        }

        $query = $this->bd->prepare($sql);
        $query->execute($dados);
            
        if(!isset($dados['id'])){
          $sql = "SELECT MAX(id) as id FROM pagamentos";
          $sql = $this->bd->prepare($sql);
          $sql->execute();
          if ($sql->rowCount() > 0) {
            foreach ($sql as $rs) {
                $ultimo = $rs["id"];
            }
          }
        }else{
          $ultimo = $dados['id'];
        }

        return $ultimo;

    }

    

    #------------------------------------------
    # Saber total do cliente em aberto
    #------------------------------------------
    # Calcula o total em aberto quando a forma
    # de pagamento for Renda
    #------------------------------------------
    public function totalEmAberto($idcliente){
        $sql = "SELECT
                  valor_total - valor_pago_total as total_aberto,
                  valor_total / qtd as quantidade_parcelas
                FROM (
                        SELECT
                          COALESCE(SUM(item.valortotal),0.00) as valor_total,
                          COALESCE(SUM(v.qtdparcelas),0) as qtd
                        FROM  
                          itensproduto item, 
                          venda v,
                          tipopagamento tp
                        WHERE 
                          v.idcliente = $idcliente
                        AND 
                          item.idvenda = v.id
                        AND
                          v.formapgto = tp.id
                        AND
                          tp.tipo = 'R'
                        AND
                          (v.tipopagamento <> ' ' or v.tipopagamento <> '')
                ) a, (
                       SELECT
                         COALESCE(SUM(valorpago),0.00) valor_pago_total
                       FROM
                         pagamentos
                       WHERE
                         idcliente = $idcliente
                       AND
                         statuspagamento = '1'
                ) c;";

        $query = $this->bd->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

    public function buscaValorPgto($idcliente, $idpgto){

        $sql = "SELECT valorpago as total FROM pagamentos WHERE id = $idpgto AND idcliente = $idcliente";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            foreach ($sql as $rs) {
                $total = $rs["total"];
            }
        }else{
            $total = 0;
        }

        return $total;
    }


    public function pagar($tipopagamento, $idpgto, $param){

        if($param==0) $param = 1; // prevent the use of 0

        if($tipopagamento=='D'){

            $sql = "UPDATE pagamentos SET tipopagamento = '$tipopagamento', statuspagamento = 1, datavencimento = '01/01/1900', numeroparcela = 1 WHERE id = $idpgto";
        }else if($tipopagamento=='P'){

            $sql = "UPDATE pagamentos SET tipopagamento = '$tipopagamento', statuspagamento = 1, datavencimento = '01/01/1900', numeroparcela = $param WHERE id = $idpgto";
        }else if($tipopagamento=='B'){

            $sql = "UPDATE pagamentos SET tipopagamento = '$tipopagamento', statuspagamento = 1, datavencimento = '$param', numeroparcela = 1 WHERE id = $idpgto";
        }

        $query = $this->bd->prepare($sql);
        return $query->execute();
    }

    public function buscarTodos() {
        $sql = "select pa.id,
                       to_char(pa.datapagamento, 'dd/MM/yyyy') as datapagamento,
                       to_char(pa.horapagamento, 'HH:mm') as horapagamento,
                       to_char(pa.valorpago, 'L9G999G990D99') as valorpago,
                       pa.numeroparcela,
                       pa.statuspagamento,
                       pa.tipopagamento,
                       cli.nome as nomecliente,
                       co.nome as nomecolaborador
                  from pagamentos pa
                 inner join cliente cli
                    on pa.idcliente = cli.id
                 inner join colaboradores co
                    on pa.idcolaborador = co.id
                 order by pa.datapagamento, cli.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select pa.id,
                       to_char(pa.datapagamento, 'dd/MM/yyyy') as datapagamento,
                       to_char(pa.horapagamento, 'HH:mm') as horapagamento,
                       to_char(pa.valorpago, 'L9G999G990D99') as valorpago,
                       pa.numeroparcela,
                       pa.statuspagamento,
                       pa.tipopagamento,
                       pa.idcolaborador,
                       pa.idcliente
                  from pagamentos pa
                 WHERE pa.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $numeroparcela = $_POST['numeroparcela'];

        if($numeroparcela == null){
            $numeroparcelaverifica = null;
        }else{
            $numeroparcelaverifica = $numeroparcela;
        }
        //Colaborador
        $logincolaborador = $_SESSION['login'];
        $login = "select id as idcolaborador from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }
        
        //Busca a Data e Hora Atual
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastropagamento = date('d/m/Y');
        $horacadastropagamento = date('HH:mm');

        
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valorpago']));
        //Valor formatação
        $valorpago = $formata;
        $verificavalorpago = substr($valorpago, -3, 1);
        if($verificavalorpago == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorpago));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorpagoformatado   = $formatavalor;
        }else{
            $valorpagopost = $valorpago;
            $removevirgula = str_replace(",", "", $valorpagopost);
            $valorpagoformatado = $removevirgula;
        }
        
        $sqlPagametos = "UPDATE pagamentos
                            SET idcliente       = :idcliente
                                idcolaborador   = :idcolaborador,
                                datapagamento   = '$datacadastropagamento',
                                horapagamento   = '$horacadastropagamento',
                                valorpagamento  = $valorpagoformatado,
                                numeroparcela:  = $numeroparcelaverifica,
                                statuspagamento = :statuspagamento,
                                tipopagamento   = :tipopagamento
                          WHERE id = $id";

        unset($dados['id']);
        unset($dados['datapagamento']);
        unset($dados['horapagamento']);
        unset($dados['valorpagamento']);
        unset($dados['numeroparcela']);
        $query = $this->bd->prepare($sqlPagamentos);
        return $query->execute($dados);
        
    }

    public function excluir($id) {
        $sql = "DELETE FROM pagamentos WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}