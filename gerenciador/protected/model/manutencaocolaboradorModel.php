<?php

class ManutencaocolaboradorModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "SELECT c.id,
                       c.nome, 
                       CASE WHEN c.tipocolaborador='M' THEN 'Master'
                            WHEN c.tipocolaborador='G' THEN 'Gerente'
                              ELSE 'Colaborador'
                       END as tipocolaborador,
                       c.tipocolaborador as tpcolaborador,
                       c.quantidadeacesso
                 FROM colaboradores c
                 order by c.nome desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function restaurar($id) {
    	$sql = "UPDATE colaboradores SET quantidadeacesso = 0, login = '12345', senha = '12345' WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $teste = $query->execute(array('id' => $id));
        print_r($teste); exit;
    }
    
    public function atualizarrestaurar($dados) {
    	$sql = "UPDATE colaboradores SET login = :login, 
                                         senha = :senha,
                                         quantidadeacesso = 0  
                                   WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
}
