<?php

class RendaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {

        $descricaorenda   = $_POST['descricaorenda'];
        $datainiciorenda  = $_POST['datainiciorenda'];
        $idcliente        = $_POST['idcliente'];
        $valorrendapost   = $_POST['valorrenda'];

        if(!$idcliente){
            $sql   = "SELECT MAX(id) as id FROM cliente;";
            $res   = $this->bd->prepare($sql);
            $res->execute();
            if ($res->rowCount() > 0) {
                foreach ($res as $rs) {
                    $idcliente = $rs["id"];
                }
            }
        }
        
        //Busca a seção do usuário logado
        $logincolaborador = $_SESSION['login'];
        $login = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja = $rs["idloja"];
            }
        }
        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastrorenda = date('d/m/Y');
        
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $valorrendapost));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        $valorrendaformatado   = $formatavalor;
        
        $sql = "insert into rendacliente(descricaorenda, valorrenda, datainiciorenda, datacadastrorenda, idloja, idcolaborador, idcliente) "
                    . " VALUES('$descricaorenda', $valorrendaformatado, '$datainiciorenda', '$datacadastrorenda', $idloja, $idcolaborador, $idcliente)";
        unset($dados['id']);
        unset($dados['descricaorenda']);
        unset($dados['valorrenda']);
        unset($dados['datainiciorenda']);
        unset($dados['datacadastrorenda']);
        unset($dados['idloja']);
        unset($dados['idcolaborador']);
        unset($dados['idcliente']);

        if(isset($dados['acaoBack'])){
            unset($dados['acaoBack']);
        }

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select r.id,
                       upper(r.descricaorenda) as descricaorenda,
                       to_char(r.valorrenda, 'L9G999G990D99') as valorrenda,
                       to_char(r.datainiciorenda, 'dd/MM/yyyy') as datainiciorenda,
                       to_char(r.datacadastrorenda, 'dd/MM/yyyy') as datacadastrorenda,
                       upper(cli.nome) as nomecliente,
                       l.nomefantasia,
                       c.nome as nomecolaborador
                  from rendacliente r
                 inner join loja l
                    on r.idloja = l.id
                 inner join colaboradores c
                    on r.idcolaborador = c.id
                 inner join cliente cli
                    on r.idcliente = cli.id
                 order by r.datainiciorenda desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select r.id,
                       upper(r.descricaorenda) as descricaorenda,
                       to_char(r.valorrenda, 'L9G999G990D99') as valorrenda,
                       to_char(r.datainiciorenda, 'dd/MM/yyyy') as datainiciorenda,
                       to_char(r.datacadastrorenda, 'dd/MM/yyyy') as datacadastrorenda,
                       r.idloja,
                       r.idcliente,
                       r.idcolaborador
                  from rendacliente r
                 WHERE r.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }


    public function buscarToCliente($id) {
        $sql = "select r.id,
                       r.descricaorenda,
                       to_char(r.valorrenda, 'L9G999G990D99') as valorrenda,
                       to_char(r.datainiciorenda, 'dd/MM/yyyy') as datainiciorenda,
                       to_char(r.datacadastrorenda, 'dd/MM/yyyy') as datacadastrorenda,
                       r.idloja,
                       r.idcliente,
                       r.idcolaborador
                  from rendacliente r
                 WHERE r.idcliente = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetchAll();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $descricaorenda = $_POST['descricaorenda'];
        $datainiciorenda = $_POST['datainiciorenda'];
        $idcliente = $_POST['idcliente'];
        
        //Colaborador
        $logincolaborador = $_SESSION['login'];
        $login = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja = $rs["idloja"];
            }
        }
        
        //Busca a Data Atual
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastrorenda = date('d/m/Y');
        
         //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valorrenda']));
        //Valor formatação
        $valorrenda = $formata;
        $verificavalorrenda = substr($valorrenda, -3, 1);
        if($verificavalorrenda == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorrenda));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorrendaformatado   = $formatavalor;
        }else{
            $valorrendapost = $valorrenda;
            $removevirgula = str_replace(",", "", $valorrendapost);
            $valorrendaformatado = $removevirgula;
        }
        
        $sqlRendaCliente = "UPDATE rendacliente
                               SET descricaorenda = '$descricaorenda',
                                   valorrenda = $valorrendaformatado,
                                   datainiciorenda = '$datainiciorenda',
                                   datacadastrorenda = '$datacadastrorenda',
                                   idloja = $idloja,
                                   idcolaborador = $idcolaborador,
                                   idcliente = $idcliente
                             WHERE id = $id";
        unset($dados['id']);
        unset($dados['descricaorenda']);
        unset($dados['valorrenda']);
        unset($dados['datainiciorenda']);
        unset($dados['datacadastrorenda']);
        unset($dados['idloja']);
        unset($dados['idcolaborador']);
        unset($dados['idcliente']);
        $query = $this->bd->prepare($sqlRendaCliente);
        return $query->execute($dados);
        
    }

    public function excluir($id) {
        $sql = "DELETE FROM rendacliente WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}