<?php

class RelacaoexclusaovendaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        $sql = "select col.nome as nomevendedor,
                       to_char(aj.dataexclusao, 'dd/MM/yyyy') as dataexclusao,
                       to_char(aj.horaexclusao, 'HH24:mi') as horaexclusao,
                       aj.motivoexclusao,
                       aj.idvenda as transacaovenda,
                       CASE WHEN clr.nome is null THEN cli.nome 
                            ELSE clr.nome
                       END as nomecliente
                  from ajusteexclusaovenda aj
                 inner join colaboradores col
                    on aj.idvendedor = col.id
                  left join clienterapido clr
                    on aj.idcliente = clr.id
                  left join cliente cli
                    on aj.idcliente = cli.id
                  order by dataexclusao desc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}