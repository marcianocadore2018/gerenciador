<?php

class DocumentacaoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Valor formatação
        $sql = "insert into documentacaocolaborador(pispasep, numero, serie, cpf, rg, idcolaborador, idestado) "
                    . " VALUES(:pispasep, :numero, :serie, :cpf, :rg, :idcolaborador, :idestado)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select doc.id,
                       doc.pispasep,
                       doc.numero,
                       doc.serie,
                       doc.cpf,
                       doc.rg,
                       doc.idcolaborador,
                       doc.idestado,
                       co.nome as nomecolaborador,
                       e.uf
                  from documentacaocolaborador doc
                 inner join colaboradores co
                    on doc.idcolaborador = co.id
                 inner join estado e
                    on doc.idestado = e.id
                 order by co.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select doc.id,
                       doc.pispasep,
                       doc.numero,
                       doc.serie,
                       doc.cpf,
                       doc.rg,
                       doc.idcolaborador,
                       doc.idestado
                  from documentacaocolaborador doc
                 WHERE doc.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $sqlDocumentacao = "UPDATE documentacaocolaborador
                               SET pispasep = :pispasep,
                                   numero = :numero,
                                   serie = :serie,
                                   cpf = :cpf,
                                   rg = :rg,
                                   idcolaborador = :idcolaborador,
                                   idestado = :idestado
                             WHERE id = $id";
        unset($dados['id']);
        $query = $this->bd->prepare($sqlDocumentacao);
        return $query->execute($dados);
        
    }

    public function excluir($id) {
        $sql = "DELETE FROM documentacaocolaborador WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
