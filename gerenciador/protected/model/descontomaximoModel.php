<?php

class DescontomaximoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $porcentagemvalormaximo = $_POST['porcentagemvalormaximo'];
        $sql = "INSERT INTO descontomaximo(datacadastro, porcentagemvalormaximo) "
                . " VALUES('$dataCadastro', $porcentagemvalormaximo)";
       
        unset($dados['id']);
        unset($dados['datacadastro']);
        unset($dados['porcentagemvalormaximo']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "SELECT id, to_char(datacadastro,'dd/MM/yyyy') as datacadastro, porcentagemvalormaximo FROM descontomaximo order by datacadastro asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, to_char(datacadastro, 'dd/MM/yyyy') as datacadastro, porcentagemvalormaximo FROM descontomaximo WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $id = $_POST['id'];
        $porcentagemvalormaximo = $_POST['porcentagemvalormaximo'];
        $sql = "UPDATE descontomaximo SET datacadastro = '$dataCadastro', porcentagemvalormaximo = '$porcentagemvalormaximo' WHERE id = $id";
        unset($dados['datacadastro']);
        unset($dados['porcentagemvalormaximo']);
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
}
