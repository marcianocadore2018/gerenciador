<?php

class GerenciamentofinanceiroentradaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricao = $_POST['descricao'];
        $maioresinformacoes = $_POST['maioresinformacoes'];
        $datacadastrohidden = $_POST['datacadastrohidden'];
        $valor = $_POST['valor'];
        
        $formata      = str_replace("R$", "", str_replace("", "", $valor));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        
        //Buscar o Id do colaborador e o Id da Loja pela seção
        $logincolaborador = $_SESSION['login'];
        $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($logincolaborador);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja        = $rs["idloja"];
            }
        }

        $sql = "INSERT INTO gerenciamentofinanceiroentrada(descricao, valor, datacadastro, maioresinformacoes, idcolaborador, idloja) "
                . " VALUES('$descricao', $formatavalor, '$datacadastrohidden', '$maioresinformacoes', $idcolaborador, $idloja)";
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['maioresinformacoes']);
        unset($dados['datacadastro']);
        unset($dados['datacadastrohidden']);
        unset($dados['idcolaborador']);
        unset($dados['idloja']);
        unset($dados['valor']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        //Buscar o idloja pela seção do colaborador
        $sql = "SELECT id,
                       descricao,
                       to_char(valor, 'L9G999G990D99') as valor,
                       to_char(datacadastro, 'dd/MM/yyyy') as datacadastro
                  FROM gerenciamentofinanceiroentrada
              order by datacadastro asc;";
        $query = $this->bd->query($sql);

        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, 
                       descricao,
                       to_char(valor, 'L9G999G990D99') as valor,
                       to_char(datacadastro, 'dd/MM/yyyy') as datacadastro,
                       maioresinformacoes
                  FROM gerenciamentofinanceiroentrada
                 WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $datacadastrohidden = $_POST['datacadastrohidden'];
        $valorpost = $_POST['valor'];
        
        //Buscar o Id do colaborador e o Id da Loja pela seção
        $logincolaborador = $_SESSION['login'];
        $logincolaborador = "select id as idcolaborador, idloja from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($logincolaborador);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
                $idloja        = $rs["idloja"];
            }
        }

        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $_POST['valor']));
        //Valor formatação Salário
        $valor = $formata;
        $verificavalor = substr($valor, -3, 1);
        if($verificavalor == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valor));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorformatado   = $formatavalor;
        }else{
            $valorpost = $valor;
            $removevirgula = str_replace(",", "", $valorpost);
            $valorformatado = $removevirgula;
        }

        $sql = "UPDATE gerenciamentofinanceiroentrada
                   SET descricao = :descricao, 
                       valor = $valorformatado,
                       datacadastro = '$datacadastrohidden',
                       maioresinformacoes = :maioresinformacoes,
                       idcolaborador = $idcolaborador,
                       idloja = $idloja 
                 WHERE id = :id";
        
        unset($dados['datacadastrohidden']);
        unset($dados['datacadastro']);
        unset($dados['idcolaborador']);
        unset($dados['idloja']);
        unset($dados['valor']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM gerenciamentofinanceiroentrada WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

    public function buscarTodosValoresEntrada() {
        //Consulta faz a soma dos valores de Entrada
        $sqlentrada = "select sum(fe.valor) somavalorentrada  from gerenciamentofinanceiroentrada fe;";
        $query = $this->bd->query($sqlentrada);
        return $query->fetchAll();
    }

    public function buscarTodosValoresSaida() {
        //Consulta faz a soma dos valores de Saída
        $sqlsaida = "select sum(fs.valorpagamento) somavalorsaida  from gerenciamentofinanceirosaida fs;";
        $query = $this->bd->query($sqlsaida);
        return $query->fetchAll();
    }

}
