<?php

class LojaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $nomefantasia = $_POST['nomefantasia'];
        $razaosocial = $_POST['razaosocial'];
        $cnpj = $_POST['cnpj'];
        $dataabertura = $_POST['dataabertura'];
        $situacaoloja = $_POST['situacaoloja'];
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $idestado = $_POST['idestado'];
        $cidade = $_POST['cidade'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        $numero = $_POST['numero'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        
        //Busca a seção do usuário 
        $logincolaborador = $_SESSION['login'];
        $login = "select id as idcolaborador from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }
        //Cadastra o registro da pessoa
        $sql = "INSERT INTO registropessoa(endereco, bairro, idestado, cidade, cep, telefone, email, numero, complemento) "
                    . " VALUES('$endereco', '$bairro', $idestado, '$cidade', '$cep', '$telefone', '$email', $numero, '$complemento')";
        unset($dados['id']);
        unset($dados['nomefantasia']);
        unset($dados['razaosocial']);
        unset($dados['cnpj']);
        unset($dados['dataabertura']);
        unset($dados['situacaoloja']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['idestado']);
        unset($dados['cidade']);
        unset($dados['email']);
        unset($dados['telefone']);
        unset($dados['numero']);
        unset($dados['cep']);
        unset($dados['complemento']);
        $query = $this->bd->prepare($sql);
        $query->execute($dados);
        
        //Consulta último registropessoa inserido
        $registropessoa = "select max(id) as idregistropessoa from registropessoa";
        $sqlregistropessoa = $this->bd->prepare($registropessoa);
        $sqlregistropessoa->execute();
        if ($sqlregistropessoa->rowCount() > 0) {
            foreach ($sqlregistropessoa as $rs) {
                $idregistropessoa = $rs["idregistropessoa"];
            }
        }
        
        if($idregistropessoa != null){
            //Função para capturar a data
            date_default_timezone_set('America/Sao_Paulo');
            $dataCadastro = date('d/m/Y');
            $sqlloja = "INSERT INTO loja(nomefantasia, razaosocial, cnpj, dataabertura, datacadastro, situacaoloja, idcolaborador, idregistropessoa) "
                    . " VALUES('$nomefantasia', '$razaosocial', '$cnpj', '$dataabertura', '$dataCadastro', '$situacaoloja', $idcolaborador, $idregistropessoa)";
            $query = $this->bd->prepare($sqlloja);
            return $query->execute($dados);
        } 
    }

    public function buscarTodos() {
        $sql = "select l.id,
                       l.nomefantasia as nomefantasia,
                       l.razaosocial,
                       l.cnpj,
                       to_char(l.dataabertura, 'dd/MM/yyyy') as dataabertura,
                       to_char(l.datacadastro, 'dd/MM/yyyy') as datacadastro,
                       l.situacaoloja, 
                       CASE WHEN l.situacaoloja='A' THEN 'Ativa'
                              ELSE 'Inativa'
                       END as situacaoloja,
                       r.endereco,
                       r.bairro,
                       r.cidade,
                       e.nomeestado,
                       e.uf,
                       r.cep,
                       r.telefone,
                       r.email,
                       r.numero,
                       r.complemento
                 from loja l
                inner join registropessoa r
                   on l.idregistropessoa = r.id
                inner join estado e
                   on r.idestado = e.id
                order by l.razaosocial, l.nomefantasia asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select l.id,
                       l.nomefantasia,
                       l.razaosocial,
                       l.cnpj,
                       to_char(l.dataabertura, 'dd/MM/yyyy') as dataabertura,
                       to_char(l.datacadastro, 'dd/MM/yyyy') as datacadastro,
                       l.situacaoloja,
                       r.endereco,
                       r.bairro,
                       r.cidade,
                       r.idestado as idestado,
                       e.nomeestado,
                       e.uf,
                       r.cep,
                       r.telefone,
                       r.email,
                       r.numero,
                       r.complemento
                  from loja l
                 inner join registropessoa r
                    on l.idregistropessoa = r.id
                 inner join estado e
                    on r.idestado = e.id
                 WHERE l.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
       
        $id = $_POST['id'];
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $idestado = $_POST['idestado'];
        $cidade = $_POST['cidade'];
        $cep = $_POST['cep'];
        $telefone = $_POST['telefone'];
        $email = $_POST['email'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $nomefantasia = $_POST['nomefantasia'];
        $razaosocial = $_POST['razaosocial'];
        $cnpj = $_POST['cnpj'];
        $dataabertura = $_POST['dataabertura'];
        $situacaoloja = $_POST['situacaoloja'];
        
        //Busca Seção do Colaborador Logado
        //Verifica o registro pessoa para alterar
        $verificaregistropessoa = "select idregistropessoa from loja j inner join registropessoa r on j.idregistropessoa = r.id where j.id = $id";
        $sqlverificaregistropessoa = $this->bd->prepare($verificaregistropessoa);
        $sqlverificaregistropessoa->execute();
        if ($sqlverificaregistropessoa->rowCount() > 0) {
            foreach ($sqlverificaregistropessoa as $rs) {   
                $verificaidregistropessoa = $rs["idregistropessoa"];
            }
        }
        
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        
        $sql = "UPDATE registropessoa 
                   SET endereco = '$endereco',
                       bairro = '$bairro',
                       idestado = $idestado,
                       cidade = '$cidade',
                       cep = '$cep',
                       telefone = '$telefone',
                       email = '$email',
                       numero = $numero,
                       complemento = '$complemento'
                 WHERE id = $id";
        unset($dados['id']);
        unset($dados['datacadastro']);
        unset($dados['nomefantasia']);
        unset($dados['razaosocial']);
        unset($dados['cnpj']);
        unset($dados['dataabertura']);
        unset($dados['situacaoloja']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['idestado']);
        unset($dados['cidade']);
        unset($dados['email']);
        unset($dados['telefone']);
        unset($dados['numero']);
        unset($dados['cep']);
        unset($dados['complemento']);
        $query = $this->bd->prepare($sql);
        $query->execute($dados);
        
        //Consultar o último registro de registro pessoa atualizado
        $registropessoa = "select id as idregistropessoa from registropessoa where id = $verificaidregistropessoa";
        $sqlregistropessoa = $this->bd->prepare($registropessoa);
        $sqlregistropessoa->execute();
        if ($sqlregistropessoa->rowCount() > 0) {
            foreach ($sqlregistropessoa as $rs) {
                $idregistropessoa = $rs["idregistropessoa"];
            }
        }
        
        //Atualização da Loja
        if($idregistropessoa != null){
            //Função para capturar a data
            date_default_timezone_set('America/Sao_Paulo');
            $dataCadastro = date('d/m/Y');
            
            
            $sqlLoja = "UPDATE loja
                           SET nomefantasia = '$nomefantasia',
                               razaosocial = '$razaosocial',
                               cnpj = '$cnpj',
                               dataabertura = '$dataabertura',
                               datacadastro = '$dataCadastro',
                               situacaoloja = '$situacaoloja',
                               idcolaborador = $verificaidregistropessoa,
                               idregistropessoa = $idregistropessoa
                         WHERE id = $id";
            $query = $this->bd->prepare($sqlLoja);
            return $query->execute($dados);
        }
    }

    public function excluir($id) {
        $idcliente = $_GET['id'];
        $clienteespecial = "delete from clienteespecial where idcliente = $idcliente";
        $sql = $this->bd->prepare($clienteespecial);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $cliente = "delete from cliente where id = $idcliente";
            $sqlcliente = $this->bd->prepare($cliente);
            $sqlcliente->execute();
        }

        $sql = "DELETE FROM clienteespecial WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}
