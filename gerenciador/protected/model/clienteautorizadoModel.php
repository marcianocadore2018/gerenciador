<?php

class ClienteautorizadoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Busca o id do colaborador logado
        $login      = "SELECT id as idcolaborador FROM colaboradores WHERE login = '" . $_SESSION['login'] . "'";
        $sqllogin   = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }
        unset($dados['id']);
        $nome = $_POST['nome'];
        $cpf  = $_POST['cpf'];
        $telefone = $_POST['telefone'];
        $idvendedor = $idcolaborador;
        $idcliente = $_POST['idcliente'];
        
        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        $sql = "INSERT INTO pessoaautorizada(nome, cpf, telefone, dataautorizada, idvendedor, idcliente) "
                . " VALUES('$nome', '$cpf', '$telefone', '$dataCadastro', $idvendedor, $idcliente)";
        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['cpf']);
        unset($dados['telefone']);
        unset($dados['idvendedor']);
        unset($dados['idcliente']);
        unset($dados['dataautorizada']);
        
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function buscarTodos() {
        $sql = "select pa.id,
                       pa.nome as nomepessoaautorizada,
                       pa.cpf,
                       pa.telefone,
                       to_char(pa.dataautorizada, 'dd/MM/yyyy') as dataautorizada,
                       col.nome as nomevendedor,
                       case when cli.nome is not null then
                            cli.nome
                          else 
                            clirap.nome
                          end as nomecliente
                  from pessoaautorizada pa
                 inner join colaboradores col
                    on pa.idvendedor = col.id
                  left join cliente cli
                    on pa.idcliente = cli.id
                  left join clienterapido clirap
                    on pa.idcliente = clirap.id
                 order by nomepessoaautorizada, dataautorizada asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select id, nome, cpf, telefone, to_char(dataautorizada, 'dd/MM/yyyy') as dataautorizada, idvendedor, idcliente from pessoaautorizada WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));
        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $login      = "SELECT id as idcolaborador FROM colaboradores WHERE login = '" . $_SESSION['login'] . "'";
        $sqllogin   = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');
        
        $idcliente = $_POST['idcliente'];
        $nome      = $_POST['nome'];
        $cpf       = $_POST['cpf'];
        $telefone  = $_POST['telefone'];
        $id        = $_POST['id'];
        
        $sql = "UPDATE pessoaautorizada 
                   SET nome = '$nome', 
                       cpf = '$cpf', 
                       telefone = '$telefone',  
                       dataautorizada = '$dataCadastro', 
                       idvendedor = $idcolaborador, 
                       idcliente = $idcliente 
                 WHERE id = $id";
        
        unset($dados['idcliente']);
        unset($dados['nome']);
        unset($dados['cpf']);
        unset($dados['telefone']);
        unset($dados['idvendedor']);
        unset($dados['dataautorizada']);
        unset($dados['id']);
        
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sql = "DELETE FROM pessoaautorizada WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }
}
