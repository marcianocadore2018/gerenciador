<?php

# -----------------------------------------------------------------------------------------------
# Declarar todos os SQL's antes da classe
# Para mantermos os códigos mais limpos e compreenssíveis
# Facilitando o entendimento e detecção de problemas(bugs)
# -----------------------------------------------------------------------------------------------
# Para querys de consulta (SELECT):
# que necessitem de passagem de parametros, deve-se utilizar:
# a constante 'STR_VALUE', caso precise de mais parametros pode-se criar novas constantes;
# Caso seja criadas novas constantes para esse fim, nao esquecer de adicionar a constante na lógica
# da função 'montarSql()', para que a montagem do sql ocorra sem problemas (:
# *** NAO PASSAR PARAMETROS DE FORMA DIRETO NOS SQLs ***
# -----------------------------------------------------------------------------------------------
# Para query's de inserção (INSERT):
# As colunas que receberão os VALUES devem ser iniciadas por: '_%' e terminadas por: '%_'
# Para que o interpretador (função) identifique quais colunas terão valores inseridos
# o VALUES() deve ter apenas a passagem da constante 'STR_VALUE'
# *** NAO PASSAR PARAMETROS DE FORMA DIRETO NOS SQLs ***
# -----------------------------------------------------------------------------------------------
# Para query's de inserção (UPDATE):
# As colunas que receberão o SET VALUES devem ser iniciadas por: '_%' e terminadas por: '%_'
# Para que o interpretador (função) identifique quais colunas terão valores setados
# na clausula where pode-se utilizar a constante STR_VALUE
# *** NAO PASSAR PARAMETROS DE FORMA DIRETO NOS SQLs ***
# -----------------------------------------------------------------------------------------------
# OBS:
# Quando um SQL é executado os marcadores '_%' e '%_' pegam  as colunas, após o método 'prepararInsert()' 
# busca dentro do objeto serializado '$colaboradores' os valores ($_POST)
# esse processo é feito dinamicamente
# -----------------------------------------------------------------------------------------------
# OBS:
# Quando for utilizado somente SQL (SELECT) sem passagem de parametros pode ser usado apenas
# a constante;
# Ex. $this->bd->query(SQL_BUSCAR_TODOS);
# -----------------------------------------------------------------------------------------------
# OBS:
# Passagem de parametros ':id' do framework Codeigniter não tem necessidade de uso
# -----------------------------------------------------------------------------------------------

#Defines sql

#Parametros
define('STR_VALUE', '%value');

#Sql verifica login da secao
define('SQL_LOGIN', "SELECT id as idcolaborador FROM colaboradores WHERE login = " . STR_VALUE);

#Sql verifica cpf
define('SQL_CPF', "SELECT _%cpf%_ FROM colaboradores WHERE cpf = " . STR_VALUE);

#Sql verifica pis-pasep
define('SQL_PIS_PASEP', "SELECT _%pispasep%_ FROM documentacaocolaborador WHERE pispasep = " . STR_VALUE);

#Sql verifica login existente no cadastro
define('SQL_LOGIN_CAD', "SELECT _%login%_ FROM colaboradores WHERE login = " . STR_VALUE);

#Sql verifica documentacao existente do colaborador
define('SQL_DOC_EXISTE', "SELECT id FROM documentacaocolaborador WHERE idcolaborador = " . STR_VALUE);

#Sql de insert
define('SQL_INSERT_PESSOA', 
          "INSERT INTO registropessoa(
              _%endereco, 
              bairro, 
              idestado, 
              cidade, 
              cep, 
              telefone, 
              email, 
              numero, 
              complemento%_) VALUES(".STR_VALUE.")");

#Sql max pessoa
define('SQL_MAX_PESSOA', 'SELECT max(id) as id FROM registropessoa');

#Sql max colaboradores
define('SQL_MAX_COLABORADORES', 'SELECT max(id) as id FROM colaboradores');

#Sql insert colaboradores
define('SQL_INSERT_COLABORADORES', 
            "INSERT INTO colaboradores(
                _%nome, 
                tipocolaborador, 
                idloja, 
                login, 
                senha, 
                rg, 
                cpf, 
                datacadastrocolaborador, 
                datanascimento, 
                quantidadeacesso, 
                idcargo, 
                idsetor, 
                idregistropessoa%_) VALUES(".STR_VALUE.")");

#Sql insert documentacao do colaborador
define('SQL_INSERT_DOC_COLABORADOR', 
            "INSERT INTO documentacaocolaborador(
                _%pispasep, 
                numero, 
                serie, 
                cpf, 
                rg, 
                idcolaborador, 
                idestado%_) VALUES (".STR_VALUE.")");

#Sql buscar ultimo
define('SQL_BUSCAR_ULTIMO', "SELECT MAX(id) as id FROM colaboradores");

#Sql buscar todos
define('SQL_BUSCAR_TODOS', "SELECT 
              c.id,
              c.nome, 
              CASE WHEN c.tipocolaborador='M' THEN 'Master'
                   WHEN c.tipocolaborador='G' THEN 'Gerente'
                     ELSE 'Colaborador'
              END as tipocolaborador,
              c.tipocolaborador as tpcolaborador,
              c.idloja as idloja,
              c.login,
              c.senha,
              c.rg,
              c.cpf,
              to_char(c.datacadastrocolaborador, 'dd/MM/yyyy') as datacadastrocolaborador,
              to_char(c.datanascimento, 'dd/MM/yyyy') as datanascimento,
              c.quantidadeacesso,
              lo.nomefantasia,
              car.descricao as descricaocargo,
              se.descricao as descricaosetor,
              p.cidade as descricaocidade,
              p.bairro,
              p.telefone,
              p.email,
              p.complemento,
              p.numero,
              p.complemento,
              p.cep,
              e.uf
            FROM colaboradores c
            INNER JOIN registropessoa p
            ON c.idregistropessoa = p.id
                 inner join estado e
                    on p.idestado = e.id
                 inner join cargo car
                    on c.idcargo = car.id
                 inner join setor se
                    on c.idsetor = se.id
                 inner join loja lo
                    on c.idloja = lo.id
                 where c.tipocolaborador <> 'D'
                 order by c.datacadastrocolaborador desc;");

define('SQL_BUSCAR', "SELECT 
                        c.id,
                        c.nome, 
                        c.tipocolaborador,
                        c.idloja,
                        c.login,
                        c.senha,
                        c.rg,
                        c.cpf,
                        to_char(c.datacadastrocolaborador, 'dd/MM/yyyy') as datacadastrocolaborador,
                        to_char(c.datanascimento, 'dd/MM/yyyy') as datanascimento,
                        c.quantidadeacesso,
                        c.idcargo,
                        c.idsetor,
                        c.idregistropessoa,
                        p.endereco,
                        p.bairro,
                        p.idestado,
                        p.cidade,
                        p.email,
                        p.telefone,
                        p.numero,
                        p.cep,
                        p.complemento,
                        doc.idestado as idestadocarteiratrabalho,
                        doc.pispasep,
                        doc.numero as numerocarteiratrabalho,
                        doc.serie,
                        doc.cpf as cpfcarteiratrabalho,
                        doc.rg as rgcarteiratrabalho
                      FROM colaboradores c
                       inner join registropessoa p
                          on c.idregistropessoa = p.id
                        left join documentacaocolaborador doc
                          on doc.idcolaborador = c.id
                       WHERE c.id = :id");

define('SQL_VERIFICA_PESSOA', "SELECT idregistropessoa FROM colaboradores c INNER JOIN registropessoa r ON c.idregistropessoa = r.id WHERE c.id = " . STR_VALUE);

define('SQL_ATUALIZA_PESSOA',   "UPDATE 
                                    registropessoa 
                                SET 
                                    _%endereco  = %endereco,
                                    bairro      = %bairro,
                                    idestado    = %idestado,
                                    cidade      = %cidade,
                                    cep         = %cep,
                                    telefone    = %telefone,
                                    email       = %email,
                                    numero      = %numero,
                                    complemento = %complemento%_
                                WHERE   
                                    id = " . STR_VALUE);

define('SQL_QUANTIDADE_ACESSO', "SELECT quantidadeacesso, tipocolaborador FROM colaboradores");

define('SQL_ATUALIZA_COLABORADORES', 
            "UPDATE 
                colaboradores
            SET 
                _%nome                  = %nome,
                tipocolaborador         = %tipocolaborador,
                idloja                  = %idloja,
                login                   = %login,
                senha                   = %senha,
                rg                      = %rg,
                cpf                     = %cpf,
                datacadastrocolaborador = %datacadastrocolaborador,
                datanascimento          = %datanascimento,
                quantidadeacesso        = %quantidadeacesso,
                idcargo                 = %idcargo,
                idsetor                 = %idsetor,
                idregistropessoa        = %idregistropessoa%_
            WHERE 
                id = " . STR_VALUE);

define('SQL_ATUALIZA_DOC_COLABORADOR', 
            "UPDATE 
                documentacaocolaborador
            SET 
                _%pispasep = %pispasep,
                numero     = %numerocarteiratrabalho,
                serie      = %serie,
                cpf        = %cpf,
                rg         = %rg,
                idestado   = %idestadocarteiratrabalho%_
            WHERE 
                idcolaborador = " . STR_VALUE);







class ColaboradoresModel extends Conexao {

    public $colaboradores;
    public $objParaSerializar;
    public $dadosTmp;
    public $operacao; # Para saber se esta INSERINDO ou EDITANDO

    function __construct() {
        parent::__construct();
        $this->dadosTmp = [];
    }

    # Pode ser usado para montar SQL's simples
    # Quando for necessário passar apenas 1 parametro que nao esteja serializado
    function montarSql($str, $target){  
        

        if($this->objParaSerializar !== NULL){
        
            if($this->operacao==='INSERT' || !$this->operacao){
                $step0 = str_replace(STR_VALUE, $str, $target);
                $step1 = str_replace(STR_VALUE, $step0, $this->objParaSerializar);
            }else{
                $step0 = str_replace(STR_VALUE, $str, $this->objParaSerializar);    # Seta o ID na clausula WHERE
                
                $start = strpos($step0, "_%");                                      # Seleciona o index START das colunas selecionadas
                $end   = strpos($step0, "%_");                                      # Seleciona o index END    //          //

                $step1 = substr($step0, $start+2, -(strlen($step0)-$end));          # Seleciona as colunas entre _% e %_
                $step1 = str_replace($step1, STR_VALUE, $step0);                    # Substitui tudo por %value
                $step1 = str_replace(STR_VALUE, $target, $step1);                   # Seta no %value as colunas com valores

            }

          $step2 = str_replace("_%", "", $step1);                                   # Remove caso tenha sobrado o seletor _%
          $step3 = str_replace("%_", "", $step2);                                   # Remove caso tenha sobrado o seletor %_

        }else{

            # Tratamento para quando parametro for == String
            if( !is_numeric($str) ){
                $str = "'".$str."'";
            }

            $step0 = str_replace(STR_VALUE, $str, $target);
            $step3 = $step0;
        }

        $this->objParaSerializar = NULL;

        return $step3;
    }

    # ------------------------------------------------------------------------------------
    # Deserializa um objeto e monta uma string dinamicamente para ser executado no SQL
    # Sempre vai retornar um String => "valor, valor2, valor"
    # Os valores são respectivos ao objeto serializado
    # ------------------------------------------------------------------------------------
    # As colunas a serem deserializadas devem ter os caracteres % (no inicio) e @ (no fim)
    # Ex. %coluna1, coluna2, coluna3@
    # O interpretador identifica que somente essas colunas serao processadas
    # ------------------------------------------------------------------------------------
    function prepararInsert(){
        
        $start    = strpos($this->objParaSerializar, "_%");
        $end      = strpos($this->objParaSerializar, "%_");

        $colunas  = substr($this->objParaSerializar, $start+2, -(strlen($this->objParaSerializar)-$end));

        $colunas  = explode(",", $colunas);

        $r="";

        foreach ($colunas as $key) {

            # Nesse caso existe uma excessao para pegar
            # A key das colunas corretamente no update
            if($this->operacao==='UPDATE'){

                $keyAndValue = $key;                             # Guarda a key com o value que vai ser setado (:
                $end         = strpos($key, "=");                # Seleciona a coluna
                $key         = substr($key, 0, $end);            # Pega a coluna selecionada
                $var         = $this->colaboradores[trim($key)]; # Nice :)

                if( is_numeric($var) ){
                    $r .= $key ."=". $var.","; 
                }else{
                    $r .= $key ."=". "'".$var."',";
                }
            }

            
            # Pega as colunas do INSERT 
            if($this->operacao==='INSERT'){
                $var = $this->colaboradores[trim($key)];
                if( is_numeric($var)){
                    $r .= $var.","; 
                }else{
                    $r .= "'".$var."',";
                }
            }
        }
        return ( rtrim($r, ',') );
    }

    # Serializa um objeto em tempo de execucao
    function serializar($dados){
        return $this->colaboradores = $dados;
    }

    public function inserir(array $dados) { 

        $this->serializar( $dados );
        unset($dados);
        $this->operacao = 'INSERT';
        
        # Busca a seção do usuário 
        $loginSecao = $_SESSION['login'];

        # Cria a variavel para controlar erros
        $erroCheck = 0;

        # Prepara SELECT para ser serializado com os valores
        $sql   = $this->montarSql( $loginSecao, SQL_LOGIN ); 
        $query = $this->bd->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0) {
            foreach ($query as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        } 
      
        # verifica se CPF ja existe, se ja existe não deixa cadastrar e exibe a mensagem de cpf existente
        # Prepara SELECT para ser serializado com os valores
        $this->objParaSerializar = SQL_CPF;

        # Executa o SELECT com o objeto serializado
        $query = $this->bd->prepare( $this->montarSql("", $this->prepararInsert() ) );
        $query->execute();
        if ($query->rowCount() > 0) {
            echo "<script>alert('Este CPF já encontra-se cadastrado na base de dados! Favor informe outro CPF');</script>";
            $erroCheck = 1;
        }

        # verifica se PIS-PASEP ja existe, se ja existe não deixa cadastrar e exibe a mensagem de pis-pasep existente
        # Prepara SELECT para ser serializado com os valores
        $this->objParaSerializar = SQL_PIS_PASEP;

        # Executa o SELECT com o objeto serializado
        $query = $this->bd->prepare( $this->montarSql("", $this->prepararInsert() ) );
        $query->execute();
        if ($query->rowCount() > 0) {
            echo "<script>alert('Este PIS-PASEP já encontra-se cadastrado na base de dados! Favor informe outro');</script>";
            $erroCheck = 1;
        }

        # verifica se Login ja existe, se ja existe não deixa cadastrar e exibe a mensagem de login existente
        # Prepara SELECT para ser serializado com os valores
        $this->objParaSerializar = SQL_LOGIN_CAD;

        # Executa o SELECT com o objeto serializado
        $query = $this->bd->prepare( $this->montarSql("", $this->prepararInsert() ) );
        $query->execute();
        if($query->rowCount() > 0){
            echo "<script>alert('Este Login já encontra-se cadastrado na base de dados! Favor informe outro Login');</script>";
            $erroCheck = 1;
        }


        if($erroCheck == 0){

            # Cadastra o registro da pessoa
            # Prepara INSERT para ser serializado com os valores
            $this->objParaSerializar = SQL_INSERT_PESSOA;

            # Executa o insert com o objeto serializado
            $query    = $this->bd->prepare( $this->montarSql("", $this->prepararInsert() ) );
            $inserir1 = $query->execute( $this->dadosTmp );

            # Consulta último registropessoa inserido
            $query = $this->bd->prepare(SQL_MAX_PESSOA);
            $query->execute();
            if ($query->rowCount() > 0) {
                foreach ($query as $rs) {
                    $idregistropessoaMax = $rs["id"];
                }
            }

            # quantidadeacesso, idregistropessoa
            # Verifica se ocorreu o cadastro da pessoa
            if(isset($idregistropessoaMax) && $idregistropessoaMax > 0){

                # Função para capturar a data
                date_default_timezone_set('America/Sao_Paulo');
                $dataCadastro = date('d/m/Y');

                # Quantidade Acesso Colaborador, sempre iniciará com 1
                ###############################################################################################################
                # DICA - Não alterar os valores fixos sem antes consultar quem desenvolveu.                                   #
                # DATA = 01/03/2017                                                                                           #
                # AUTOR = Marciano Luis Cadore                                                                                #
                # CORREÇÃO                                                                                                    #
                # Quantidade Acesso Colaborador, sempre iniciará com 0 (ZERO), existe outros fluxos para validar esse acesso. #
                # No Atualizar ele será incrementado cada vez que alterar o registro                                          #
                ###############################################################################################################
                
                # ------------------------------------------------------------------
                # Serializa os objetos para serem inseridos no banco
                # ------------------------------------------------------------------
                $this->colaboradores['quantidadeacesso']        = 0;
                $this->colaboradores['datacadastrocolaborador'] = $dataCadastro;
                $this->colaboradores['idregistropessoa']        = $idregistropessoaMax;
                
                # Prepara INSERT para ser serializado com os valores
                $this->objParaSerializar = SQL_INSERT_COLABORADORES;

                # Executa o insert com o objeto serializado
                $sql = $this->montarSql("", $this->prepararInsert() ); 
                
                $query = $this->bd->prepare($sql);
                $query->execute( $this->dadosTmp );

                # Busca o último registro do colaborador, para inserir na tabela documentacaocolaborador
                $query = $this->bd->prepare(SQL_MAX_COLABORADORES);
                $query->execute();
                if ($query->rowCount() > 0) {
                    foreach ($query as $rs) {
                        $idcolaboradorMax = $rs["id"];
                    }
                }

                # Esse método insere os registros na tabela documentação colaborador
                # A Tabela documentacaocolaborador é referente a carteira de trabalho do colaborador
                # Data Criação: 31/12/2016
                # Autor: Marciano Luis Cadore
                # ------------------------------------------------------------------
                # Serializa os objetos para serem inseridos no banco
                # ------------------------------------------------------------------
                $this->colaboradores['idcolaborador'] = $idcolaboradorMax;

                # Prepara INSERT para ser serializado com os valores
                $this->objParaSerializar = SQL_INSERT_DOC_COLABORADOR;

                # Executa o insert com o objeto serializado
                $sql   = $this->montarSql("", $this->prepararInsert() ); 
                $query = $this->bd->prepare($sql);
                return $query->execute( $this->dadosTmp );
            }
        }
    }

    public function buscarTodos() {
        $query = $this->bd->query(SQL_BUSCAR_TODOS);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $query = $this->bd->prepare(SQL_BUSCAR);
        $query->execute(array('id' => $id));
        return $query->fetch();
    }

    public function buscarUltimo() {
        $res   = $this->bd->prepare(SQL_BUSCAR_ULTIMO);
        $res->execute();
        if ($res->rowCount() > 0) {
            foreach ($res as $rs) {
                $idcolaborador = $rs["id"];
            }
        }

        return $idcolaborador;
    }

    public function atualizar(array $dados) {
        $this->serializar($dados);
        unset($dados);
        $this->operacao = 'UPDATE';
        

        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');

        #Verifica o registro da pessoa para realizar alteraração
        $sql   = $this->montarSql( $this->colaboradores['id'], SQL_VERIFICA_PESSOA); 
        
        $query = $this->bd->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0) {
            foreach ($query as $rs) {
                $idpessoa = $rs["idregistropessoa"];
            }
        }
        # Verifica se ocorreu o UPDATE da pessoa
        if(isset($idpessoa) && $idpessoa !== '' || $idpessoa > 0){
            # Função para capturar a data
            date_default_timezone_set('America/Sao_Paulo');
            $dataCadastro = date('d/m/Y');

            # Verifica a quantidade de acesso
            $res = $this->bd->prepare(SQL_QUANTIDADE_ACESSO);
            $res->execute();
            foreach ($res as $rs) {
                $quantidadeacesso = $rs["quantidadeacesso"];
                $tipocolaborador  = $rs["tipocolaborador"];
            }
            
            # As quantidades abaixo serve para conseguir fazer a forma de acesso conforme documentado na Análise
            # Marciano Luis Cadore
            $novaquantidade = $quantidadeacesso + 1;
            # ------------------------------------------------------------------
            # Serializa os objetos para serem inseridos no banco
            # ------------------------------------------------------------------
            $this->colaboradores['quantidadeacesso']        = $novaquantidade;
            $this->colaboradores['datacadastrocolaborador'] = $dataCadastro;
            $this->colaboradores['idregistropessoa']        = $idpessoa;
                     
            # Atualiza colaboradores
            # Prepara UPDATE para ser serializado com os valores
            $this->objParaSerializar = SQL_ATUALIZA_COLABORADORES;

            # Executa o UPDATE com o objeto serializado
            $idpessoa = $this->colaboradores['id'];
            $sql      = $this->montarSql($this->colaboradores['id'], $this->prepararInsert() );
            $res      = $this->bd->prepare($sql);
            $count    = $res->execute($this->dadosTmp);
           
            if($count > 0){
                
                # Verifica se existe documentacao do colaborador
                # Caso nao exista ...
                # Deve-se inserir a documentacao
                # Caso exista ...
                # Deve-se atualizar
                $sql   = $this->montarSql($this->colaboradores['id'], SQL_DOC_EXISTE); 
                $query = $this->bd->prepare($sql);
             
                $query->execute();
                
                foreach ($query as $rs) {
                    $idDoc = $rs["id"];
                }

                if(isset($idDoc)){
                    $this->operacao = 'UPDATE';
                    # Atualiza documentação dos colaboradores
                    # Prepara UPDATE para ser serializado com os valores
                    $this->objParaSerializar = SQL_ATUALIZA_DOC_COLABORADOR;
                    # Executa o UPDATE com o objeto serializado
                    $sql = $this->montarSql($this->colaboradores['id'], $this->prepararInsert() );
                }else{
                    $this->operacao = 'INSERT';
                    $this->colaboradores['idcolaborador'] = $this->colaboradores['id'];
                    # Insere documentação dos colaboradores
                    # Prepara INSERT para ser serializado com os valores
                    $this->objParaSerializar = SQL_INSERT_DOC_COLABORADOR;
                    # Executa o INSERT com o objeto serializado
                    $sql   = $this->montarSql("", $this->prepararInsert() ); 
                }

                $res = $this->bd->prepare($sql);
                return $res->execute($this->dadosTmp);
            }
        }
    }

        public function atualizardocumentacao(array $dados) {
        $idcolaboradoresregistropessoa = $_POST['id'];
        
        //Consultar ID do RegistroPessoa para realizar a alteração
        $consultacolaboradorpessoa = "select reg.id as idregistropessoa
                                        from colaboradores col
                                       inner join registropessoa reg
                                          on col.idregistropessoa = reg.id
                                       where col.id = $idcolaboradoresregistropessoa";
        $sqlcolaboradoresregistropessoa = $this->bd->prepare($consultacolaboradorpessoa);
        $sqlcolaboradoresregistropessoa->execute();
        if ($sqlcolaboradoresregistropessoa->rowCount() > 0) {
            foreach ($sqlcolaboradoresregistropessoa as $rs) {
                $idcolaboradoresconsultaregistropessoa = $rs["idregistropessoa"];
            }
        }
        
        $idcolaboradordocumentacao = $_POST['id'];
        $pispasep = $_POST['pispasep'];
        $numerocarteiratrabalho = $_POST['numerocarteiratrabalho'];
        $serie = $_POST['serie'];
        $cpf = $_POST['cpf'];
        $rg = $_POST['rg'];
        $idestadocarteiratrabalho = $_POST['idestadocarteiratrabalho'];
        
        $sql = "UPDATE documentacaocolaborador 
                   SET pispasep = '$pispasep',
                       numero   = '$numerocarteiratrabalho',
                       serie    = '$serie',
                       cpf      = '$cpf',
                       rg       = '$rg',
                       idestado = $idestadocarteiratrabalho
                 WHERE idcolaborador = $idcolaboradordocumentacao";
        unset($dados['pispasep']); unset($dados['numero']); unset($dados['serie']); unset($dados['cpf']);
        unset($dados['rg']); unset($dados['idloja']); unset($dados['id']);
        unset($dados['nome']); unset($dados['tipocolaborador']); unset($dados['idsetor']); unset($dados['idcargo']);
        unset($dados['datanascimento']); unset($dados['numerocarteiratrabalho']); unset($dados['idestadocarteiratrabalho']);
        unset($dados['login']); unset($dados['senha']); unset($dados['endereco']); unset($dados['bairro']);
        unset($dados['cidade']); unset($dados['idestado']); unset($dados['cep']); unset($dados['complemento']);
        unset($dados['email']); unset($dados['telefone']);
        $query = $this->bd->prepare($sql);
        $query->execute($dados);
        
        
        //Atualiza o proximo
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $numero = $_POST['numero'];
        $cidade = $_POST['cidade'];
        $idestado = $_POST['idestado'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        
        
        $sqlatualizacolaboradoresregistropessoa = "UPDATE registropessoa 
                                 SET endereco = '$endereco',
                                     bairro   = '$bairro',
                                     numero   = $numero,
                                     cidade   = '$cidade',
                                     idestado = $idestado,
                                     cep      = '$cep',
                                     complemento = '$complemento',
                                     email    = '$email',
                                     telefone = '$telefone'
                               WHERE id = $idcolaboradoresconsultaregistropessoa";
        $queryatualizacolaboradoresregistropessoa = $this->bd->prepare($sqlatualizacolaboradoresregistropessoa);
        return $queryatualizacolaboradoresregistropessoa->execute($dados);
    }

    public function excluir($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        // Busca o idcolaborador
        $sql            = "SELECT c.idregistropessoa FROM colaboradores c INNER JOIN registropessoa r ON c.idregistropessoa = r.id WHERE c.id = $id";
        $query = $this->bd->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0) {
            foreach ($query as $rs) {
                $idregistropessoa = $rs["idregistropessoa"];
            }
        }
        
        //Exclui por primeiro a documentação do colaborador
        //Tabela documentacaocolaborador
        $docColaborador = "DELETE FROM documentacaocolaborador WHERE idcolaborador = $id";
        $sqldocumentacaocolaborador  = $this->bd->prepare($docColaborador);
        $excluir1       = $sqldocumentacaocolaborador->execute();
        

        //Exclui informacoes adicionais do colaborador
        //Tabela informacao_adicional_colaborador
        $iAcolaborador  = "DELETE FROM informacaoadicionalcolaborador WHERE idcolaborador = $id";
        $sqlinformacao  = $this->bd->prepare($iAcolaborador);
        $excluir2       = $sqlinformacao->execute();
        
        //A Exclusão do Registor Pessoa deve ser antes que o colaborador devido a herança
        //Tabela registropessoa
        $sqlregistrop = "DELETE FROM registropessoa WHERE id = $idregistropessoa";
        $sqlregistropessoa    = $this->bd->prepare($sqlregistrop);
        $excluir3       = $sqlregistropessoa->execute();
        
        //E por último exclui o colaborador, o registropessoa deve ser excluido antes
        //Rever conceito de herança
        //Tabela colaboradores
        $colaborador    = "DELETE FROM colaboradores WHERE id = $id";
        $sqlcolaborador            = $this->bd->prepare($colaborador);
        $excluir4       = $sqlcolaborador->execute();
        
        $msg            = "";

        if(!$excluir1){
            $msg = "Não foi possível excluir documentação do colaborador!";
        }else if(!$excluir3 || !$excluir4){
            $msg = "Não foi possível excluir colaborador!";
        }else if(!$excluir2){
            $msg = "Não foi possível excluir informações adicionais do colaborador!";
        }else if($excluir1 && $excluir2 && $excluir3 && $excluir4){
            $msg = 1;
        }

        return $msg;
    }
}