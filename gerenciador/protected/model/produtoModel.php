<?php

/**
 * @author Felipe Müller - <felipe@camul.com.br>
 * -----------------------------------------------------------------------------
 * Este arquivo é parte do projeto Gerenciador Atlanta Jeans mantido pela Camul
 * www.camul.com.br
 * -----------------------------------------------------------------------------
 * Camada model, contém SQL de: INSERT, UPDATE, SELECT e EXCLUIR
 * -----------------------------------------------------------------------------
 * Funções internas
 * -----------------------------------------------------------------------------
 * @param inserir($dados)           = Insere os $dados (contém o post que veio do frontend)
 * @param buscarTodos()             = Busca todos os dados do BD
 * @param buscar($id)               = Busca os dados passando como parametro um ID
 * @param atualizar($dados)         = Faz um UPDADE no BD com os $dados
 * @param excluir($id)              = Exclui um registro referenciado pelo ID
 * ------------------------------------------------------------------------------
 */

class ProdutoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        
        $aplicacao = $_SERVER['SCRIPT_NAME']; 
        $removercaracteres = str_replace("/","",$aplicacao);
        $nomeaplicacao = str_replace("index.php","",$removercaracteres);
                            
        //Pegar o diretorio do projeto
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/' . $nomeaplicacao .  '/protected/imagens/uploads/';
        
        $caminhodiretorio = $nomeaplicacao . '/protected/imagens/uploads/';
        
        $arquivofoto = $_FILES['arquivo']['name'];
        $nomefotoaleatorio = $arquivofoto;
        if($arquivofoto == ''){
            $nomefoto = '';
            $caminhofoto = '';
        }
        //str_shuffle gera um nome aleatório
        $nomealeatorio = str_shuffle($nomefotoaleatorio);
        //se houver algum ponto ou virgula remover por nada
        $nomefoto = str_replace("." , "" , $nomealeatorio); // Primeiro tira os pontos
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        //Pega extensão da imagem
        $extensaoimagem = substr($arquivofoto, -4);
        $destino = $diretorio . $nomefoto . $extensaoimagem;
        //alterar extensão de imagem para minúscula
        $res = strtolower($extensaoimagem);
        
        if($res != ''){
            if($res == '.jpg' || $res == '.png'){
                //$arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                move_uploaded_file($arquivo_tmp, $destino);
                //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
                $caminhofoto = $caminhodiretorio;
                $nomefotoaleatorio = $nomealeatorio;
                $nomefotoaleatoriosemponto = str_replace("." , "" , $nomefotoaleatorio);
                $nomefoto = $nomefotoaleatoriosemponto . $res;
            }else{
                echo '<div style="color: red;">A imagem não pode ser gravada, pois sua extensão não é válida. Extensões válidas (<b>.jpg, .png</b>)</div></br>';
                $caminhofoto = '';
                $nomefoto = '';
            }
        }
        
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valor']));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        $valor   = $formatavalor;

        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');

        //Busca a seção do usuário 
        $logincolaborador = $_SESSION['login'];
        $login = "select id as idcolaborador from colaboradores where login = '$logincolaborador'";
        $sqllogin = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaboradorgrava = $rs["idcolaborador"];
            }
        }
        
        $referencia = $_POST['referencia'];
        $nomeproduto = $_POST['nomeproduto'];
        $numeroproduto = $_POST['numeroproduto'];
        if($numeroproduto != ''){
           $numeroprod = $numeroproduto;
        }else{
           $numeroprod = "null";
        }
        $cor = $_POST['cor'];
        $tamanho = $_POST['tamanho'];
        $sexo = $_POST['sexo'];
        $quantidade = $_POST['quantidade'];
        $idgrupo = $_POST['idgrupo'];
        
        $sql = "INSERT INTO produto(referencia, nomeproduto, numeroproduto, cor, tamanho, sexo, quantidade, valor, datacadastro, idcolaborador, idgrupo, nomefoto, caminhofoto) "
                . " VALUES('$referencia', '$nomeproduto', $numeroprod, '$cor', '$tamanho', '$sexo', $quantidade, $valor, '$dataCadastro', $idcolaboradorgrava, $idgrupo, '$nomefoto', '$caminhofoto')";
        unset($dados['id']);
        unset($dados['referencia']);
        unset($dados['nomeproduto']);
        unset($dados['numeroproduto']);
        unset($dados['cor']);
        unset($dados['tamanho']);
        unset($dados['sexo']);
        unset($dados['quantidade']);
        unset($dados['valor']);
        unset($dados['datacadastro']);
        unset($dados['idcolaborador']);
        unset($dados['idgrupo']);
        unset($dados['caminhofoto']);
        unset($dados['nomefoto']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos($idvenda=0) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        //URL BASE
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "SELECT 
                    p.id, 
                    p.referencia, 
                    p.nomeproduto, 
                    g.descricao as descricaogrupo, 
                    p.numeroproduto, 
                    p.cor, 
                    p.tamanho, 
                    COALESCE((SELECT 1 FROM itensproduto WHERE idproduto = p.id AND idvenda = $idvenda), 0) AS status,
                    CASE WHEN p.sexo='M' THEN 'Masculino'
                              ELSE 'Feminino'
                       END as sexo,  
                    to_char(p.quantidade - COALESCE((SELECT SUM(i.quantidade) as quantidade FROM itensproduto i WHERE i.idproduto = p.id),0), '9999999G990D999') as quantidade, 
                    p.quantidade as quantidadeproduto,
                    to_char(p.valor, 'L9G999G990D99') as valor,
                    to_char(p.datacadastro,'dd/MM/yyyy') as datacadastro,
                    p.nomefoto,
                    p.caminhofoto,
                    ('$urlbase' || '' || p.caminhofoto || '' || p.nomefoto) as fotoproduto   
                FROM 
                    produto p 
                INNER JOIN 
                    grupo g ON 
                    p.idgrupo = g.id 
                ORDER BY 
                p.nomeproduto ASC;";

        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarTodosProdutosAjuste() {
      
        $sql = "SELECT 
                    p.id, 
                    p.referencia, 
                    p.nomeproduto, 
                    p.numeroproduto, 
                    p.tamanho, 
                    CASE WHEN p.sexo='M' THEN 'Masculino'
                              ELSE 'Feminino'
                       END as sexo,  
                    p.quantidade as quantidade 
               FROM produto p 
           ORDER BY p.referencia, p.nomeproduto ASC;";

        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        //URL BASE
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT p.id, 
                       p.referencia, 
                       p.nomeproduto, 
                       p.numeroproduto, 
                       p.cor, 
                       p.tamanho, 
                       p.sexo,
                       p.quantidade, 
                       p.valor as valor_default,
                       to_char(p.valor, 'L9G999G990D99') as valor,
                       to_char(p.datacadastro,'dd/MM/yyyy') as datacadastro, 
                       ('$urlbase' || '' || p.caminhofoto || '' || p.nomefoto) as fotoproduto,
                       p.idgrupo
                  FROM produto p 
                 WHERE p.id = :id;";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $aplicacao = $_SERVER['SCRIPT_NAME']; 
        $removercaracteres = str_replace("/","",$aplicacao);
        $nomeaplicacao = str_replace("index.php","",$removercaracteres);
        
        //Pegar o diretorio do projeto
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/' . $nomeaplicacao .  '/protected/imagens/uploads/';
        
        $caminhodiretorio = $nomeaplicacao . '/protected/imagens/uploads/';
        
        $arquivofoto = $_FILES['arquivo']['name'];
        $nomefotoaleatorio = $arquivofoto;
        if($arquivofoto == ''){
            $caminhofoto = '';
            $nomefoto = '';
        }
        //str_shuffle gera um nome aleatório
        $nomealeatorio = str_shuffle($nomefotoaleatorio);
        //se houver algum ponto ou virgula remover por nada
        $nomefoto = str_replace("." , "" , $nomealeatorio); // Primeiro tira os pontos
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        //Pega extensão da imagem
        $extensaoimagem = substr($arquivofoto, -4);
        
        $destino = $diretorio . $nomefoto . $extensaoimagem;
        //alterar extensão de imagem para minúscula
        $res = strtolower($extensaoimagem);
        
        if($res != ''){
            if($res == '.jpg' || $res == '.png'){
                //$arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                move_uploaded_file($arquivo_tmp, $destino);
                //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
                $caminhofoto = $caminhodiretorio;
                $nomefotoaleatorio = $nomealeatorio;
                $nomefotoaleatoriosemponto = str_replace("." , "" , $nomefotoaleatorio);
                $nomefoto = $nomefotoaleatoriosemponto . $res;
            }else{
                echo '<div style="color: red;">A imagem não pode ser gravada, pois sua extensão não é válida. Extensões válidas (<b>.jpg, .png</b>)</div></br>';
                $caminhofoto = '';
                $nomefoto = '';
            }
        }
        
        $numeroproduto = $_POST['numeroproduto'];
        if($numeroproduto != ''){
           $numeroprod = $numeroproduto;
        }else{
           $numeroprod = "null";
        }
        
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valor']));
        //Valor formatação
        $valorproduto = $formata;
        $verificavalorproduto = substr($valorproduto, -3, 1);
        if($verificavalorproduto == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorprodutoformatado   = $formatavalor;
        }else{
            $valorproduto = $dados['valor'];
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $removevirgula = str_replace(",", "", $formata);
            $valorprodutoformatado = $removevirgula;
        }

        //Função para capturar a data
        date_default_timezone_set('America/Sao_Paulo');
        $dataCadastro = date('d/m/Y');

        //Busca a seção do usuário 
        $login      = "SELECT id as idcolaborador FROM colaboradores WHERE login = '" . $_SESSION['login'] . "'";
        $sqllogin   = $this->bd->prepare($login);
        $sqllogin->execute();
        if ($sqllogin->rowCount() > 0) {
            foreach ($sqllogin as $rs) {
                $idcolaborador = $rs["idcolaborador"];
            }
        }

        $sql = "UPDATE 
                    produto 
                SET 
                    referencia      = :referencia, 
                    nomeproduto     = :nomeproduto, 
                    numeroproduto   = $numeroprod, 
                    cor             = :cor, 
                    tamanho         = :tamanho, 
                    sexo            = :sexo, 
                    quantidade      = :quantidade, 
                    valor           = $valorprodutoformatado,
                    datacadastro    = '$dataCadastro',
                    idcolaborador   = $idcolaborador,
                    nomefoto        = '$nomefoto',
                    caminhofoto     = '$caminhofoto',
                    idgrupo         = :idgrupo  
                WHERE 
                    id = :id";

        unset($dados['numeroproduto']);
        unset($dados['datacadastro']);
        unset($dados['idcolaborador']);
        unset($dados['nomefoto']);
        unset($dados['caminhofoto']);
        unset($dados['valor']);

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $aplicacao = $_SERVER['SCRIPT_NAME']; 
        $removercaracteres = str_replace("/","",$aplicacao);
        $nomeaplicacao = str_replace("index.php","",$removercaracteres);
        
        //Pegar o diretorio do projeto
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/' . $nomeaplicacao .  '/protected/imagens/uploads/';
        
        $consultaimagem = "select nomefoto from produto where id = " . $_GET['id'];
        $sqlconsultaimagem = $this->bd->prepare($consultaimagem);
        $sqlconsultaimagem->execute();
        
        //Exclusão de foto
        if ($sqlconsultaimagem->rowCount() > 0) {
            
            foreach ($sqlconsultaimagem as $rs){
                $nomeimagemproduto = $rs["nomefoto"];
                if($nomeimagemproduto != ''){
                    //Pega o caminho da imagem
                    $apagararquivo = $diretorio . $nomeimagemproduto;

                    //Remover arquivo de imagem
                    if(!unlink($apagararquivo)){
                      echo ("<div style='color:red'><b>Não foi possível deletar a imagem! </b></div>");
                    }else{
                      echo ("<div style='color:green'><b>Imagem deletada com sucesso!</b></div>");
                    }
                }
            }
        }
        
        $sql = "DELETE FROM produto WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $id));
    }

}