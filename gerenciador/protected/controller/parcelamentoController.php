<?php

class ParcelamentoController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new ParcelamentoModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/parcelamento/listParcelamento.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $parcelamento = $this->model->buscar($id);
        $acao = 'index.php?controle=parcelamentoController&acao=atualizar';
        require './protected/view/parcelamento/formParcelamento.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
}