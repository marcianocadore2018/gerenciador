<?php

class GerenciamentofinanceiroentradaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new GerenciamentofinanceiroentradaModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=gerenciamentofinanceiroentradaController&acao=inserir';
        require './protected/view/gerenciamentofinanceiroentrada/formGerenciamentofinanceiroentrada.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        $listaDadosValoresEntrada = $this->model->buscarTodosValoresEntrada();
        $listaDadosValoresSaida = $this->model->buscarTodosValoresSaida();
        require './protected/view/gerenciamentofinanceiroentrada/listGerenciamentofinanceiroentrada.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $gerenciamentofinanceiroentrada = $this->model->buscar($id);
        $acao = 'index.php?controle=gerenciamentofinanceiroentradaController&acao=atualizar';
        require './protected/view/gerenciamentofinanceiroentrada/formGerenciamentofinanceiroentrada.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Gerenciamento Financeiro de Entrada pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}