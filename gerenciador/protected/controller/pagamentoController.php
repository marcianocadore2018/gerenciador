<?php

class PagamentoController {
    private $bd, $model;
    public $dados;
    
    function __construct() {
        $this->model = new PagamentoModel();
        $this->modelClientes = new ClienteModel();
    }
    
    public function novo() {
       
        $listaClientes      = $this->modelClientes->buscarClientevenda();
        $acao = 'index.php?controle=pagamentoController&acao=filtrarpagamento';
        require './protected/view/pagamento/formPagamento.php';
    }
    
    public function filtrarpagamento() {
        $acao = "index.php?controle=pagamentoController&acao=filtrarpagamento";
        if(isset($_GET['idparcela'])){
           $idparcelapagamento = $_GET['idparcela'];
           $cpf = $_GET['cpf'];
           $this->model->pagar($idparcelapagamento);
           if((isset($_GET['idparcela']) != null)) {
                $idparcela = $_GET['idparcela'];
                $listaDados = $this->model->filtrarpagamentoparcela($idparcela);
                $listaClientes      = $this->modelClientes->buscarClientevenda();
                require './protected/view/pagamento/formPagamento.php';
            }
        }else{
            if((isset($_POST['idcliente']) != null) || (isset($_POST['cpf']) != null) || (isset($_POST['idvenda']) != null)) {
                $idcliente = $_POST['idcliente'];
                $cpf = $_POST['cpf'];
                $idvenda = $_POST['idvenda'];
                $listaDados = $this->model->filtrarpagamento($idcliente, $cpf, $idvenda);
                $listaClientes      = $this->modelClientes->buscarClientevenda();
                require './protected/view/pagamento/formPagamento.php';
            }
        }
    }
    
    /*public function filtroPagamentoparcelas(array $dados){
            $dados['fazBusca'] = true;
            $this->dados = $dados;
            $this->filtrarpagamento();
            
    }*/
}