<?php

class PagamentodiferencatrocaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new PagamentodiferencatrocaModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=pagamentodiferencatrocaController&acao=inserir';
        require './protected/view/pagamentodiferencatroca/formPagamentodiferencatroca.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/pagamentodiferencatroca/listPagamentodiferencatroca.php';
    }
}