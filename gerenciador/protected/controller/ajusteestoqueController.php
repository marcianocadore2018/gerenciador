<?php

class AjusteestoqueController {
    private $bd, $model;
    private $produtoModel;
    
    function __construct() {
        $this->model = new AjusteestoqueModel();
        $this->modelProdutos = new ProdutoModel();
    }
    
    public function novo() {
        $listaAjusteProdutos = $this->modelProdutos->buscarTodosProdutosAjuste();
        $acao = 'index.php?controle=ajusteestoqueController&acao=inserir';
        require './protected/view/ajusteestoque/formAjusteestoque.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar os dados.
                  </div>';
        }
        $this->novo();
    }
}