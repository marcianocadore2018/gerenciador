<?php

class PagamentosController {
    private $bd, $model, $modelClientes;
    
    function __construct() {
        $this->model        = new PagamentosModel();
        $this->modelClientes = new ClienteModel();
    }
    
    public function novo() {
        $listaClientes = $this->modelClientes->buscarClientevenda();
        $acao = 'index.php?controle=pagamentosController&acao=inserir';
        require './protected/view/pagamentos/formPagamentos.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/pagamentos/listPagamentos.php';
    }
    
    public function buscar($id) {
        $listaClientes = $this->modelClientes->buscarClientevenda();
        $pagamentos  = $this->model->buscar($id);
        $acao         = 'index.php?controle=pagamentosController&acao=atualizar';
        require './protected/view/pagamentos/formPagamentos.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Pagamento do Cliente pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}