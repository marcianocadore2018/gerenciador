<?php

class TrocaprodutoController {
    private $bd, $model;
    public $dados;
    
    function __construct() {
        $this->model = new TrocaprodutoModel();
        $this->modelClientes = new ClienteModel();
        $this->modelTrocaprodutos = new TrocaprodutoModel();
    }
    
    public function novo() {
        $listaClientes      = $this->modelClientes->buscarClientevenda();
        $acao = 'index.php?controle=trocaprodutoController&acao=filtrartrocaproduto';
        require './protected/view/trocaproduto/formTrocaproduto.php';
    }
    
    public function filtrartrocaproduto() {
        $acao = "index.php?controle=trocaprodutoController&acao=filtrartrocaproduto";
        if((isset($_POST['idcliente']) != null) || (isset($_POST['cpf']) != null) || (isset($_POST['idvenda']) != null)) {
            $idcliente = $_POST['idcliente'];
            $cpf = $_POST['cpf'];
            $idvenda = $_POST['idvenda'];
            $listaDados = $this->model->filtrartrocaproduto($idcliente, $cpf, $idvenda);
            $listaClientes = $this->modelClientes->buscarClientevenda();
            require './protected/view/trocaproduto/formTrocaproduto.php';
        }
    }
    
    public function trocarproduto() {
        if((isset($_GET['id']) != null) && (isset($_GET['idproduto']) != null) && (isset($_GET['iditemproduto']) != null) && (isset($_GET['idvendatroca']) != null)) {
            $id = $_GET['id'];
            $idproduto = $_GET['idproduto'];
            $iditemproduto = $_GET['iditemproduto'];
            $idvendatroca = $_GET['idvendatroca'];
            //Direciona para o Model
            $this->model->trocarproduto($id, $idproduto, $iditemproduto, $idvendatroca);
        }
    }
}