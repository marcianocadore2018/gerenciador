<?php

class PorcentagemcrediarioclienteController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new PorcentagemcrediarioclienteModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=porcentagemcrediarioclienteController&acao=inserir';
        require './protected/view/porcentagemcrediariocliente/formPorcentagemcrediariocliente.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível gravar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/porcentagemcrediariocliente/listPorcentagemcrediariocliente.php';
    }
    
    public function buscar($id) {
        $porcentagemcrediariocliente = $this->model->buscar($id);
        $acao = 'index.php?controle=porcentagemcrediarioclienteController&acao=atualizar';
        require './protected/view/porcentagemcrediariocliente/formPorcentagemcrediariocliente.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Porcentagem do Crediário pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}