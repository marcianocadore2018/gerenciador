<?php

class LojaController {
    private $bd, $model;
    private $estadoModel;
    
    function __construct() {
        $this->model = new LojaModel();
        $this->estadoModel = new EstadoModel();
    }
    
    public function novo() {
        $listaEstados = $this->estadoModel->buscarTodos();
        $acao = 'index.php?controle=lojaController&acao=inserir';
        require './protected/view/loja/formLoja.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/loja/listLoja.php';
    }
    
    public function buscar($id) {
        $listaEstados = $this->estadoModel->buscarTodos();
        $loja = $this->model->buscar($id);
        $acao = 'index.php?controle=lojaController&acao=atualizar';
        require './protected/view/loja/formLoja.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                   Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Cargo pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}