<?php

class ClienteinadimplenteController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new ClienteinadimplenteModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/clienteinadimplente/listClienteinadimplente.php';
    }
}