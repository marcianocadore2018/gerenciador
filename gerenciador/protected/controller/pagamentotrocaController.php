<?php

class PagamentotrocaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new PagamentotrocaModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=pagamentotrocaController&acao=inserir';
        require './protected/view/pagamentotroca/formPagamentotroca.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não é possível realizar o pagamento, pois o mesmo já encontra-se pago.
                  </div>';
        }
        $listaDados = $this->model->buscarTodos();
        require './protected/view/pagamentotroca/listPagamentotroca.php';
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/pagamentotroca/listPagamentotroca.php';
    }
    
    public function buscar($id) {
        $cargo = $this->model->buscar($id);
        $acao = 'index.php?controle=pagamentotrocaController&acao=atualizar';
        require './protected/view/pagamentotroca/formPagamentotroca.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
}