<?php

    class ColaboradoresController {

        private $bd, $model;
        private $lojaModel;
        private $setorModel;
        private $cargoModel;
        private $estadoModel;
        private $informacaoadicionalcolaboradorModel;
        
        function __construct() {
            
            $this->lojaModel                            = new LojaModel();
            $this->setorModel                           = new SetorModel();
            $this->cargoModel                           = new CargoModel();
            $this->estadoModel                          = new EstadoModel();
            $this->model                                = new ColaboradoresModel();
            $this->informacaoadicionalcolaboradorModel  = new informacaoadicionalcolaboradorModel();
            
        }
        
        public function novo() {

            $listaLojas      = $this->lojaModel->buscarTodos();
            $listaSetores    = $this->setorModel->buscarTodos();
            $listaCargos     = $this->cargoModel->buscarTodos();
            $listaEstados    = $this->estadoModel->buscarTodos();

            $acao              = 'index.php?controle=colaboradoresController&acao=inserir';                   # Ação padrão para o colaborador
            $acaoIAColaborador = 'index.php?controle=colaboradoresController&acao=inserirIAColaborador';      # Ação informações adicionais

            require './protected/view/colaboradores/formColaboradores.php';

        }
        
        public function inserir(array $dados) {

            $acaoIAColaborador = 'index.php?controle=colaboradoresController&acao=inserirIAColaborador';

            $r = $this->model->inserir($dados);

            if((int)$r>0){
                echo '<div class="alert alert-success">
                        Dados cadastrados com <strong>Sucesso</strong>.
                      </div>';
                if($_GET['acao'] != 'listar'){
                    $this->listar();
                }else{
                    $colaboradores = $this->model->buscar( $this->model->buscarUltimo() );

                    $listaLojas    = $this->lojaModel->buscarTodos();
                    $listaSetores  = $this->setorModel->buscarTodos(); 
                    $listaCargos   = $this->cargoModel->buscarTodos();
                    $listaEstados  = $this->estadoModel->buscarTodos();
                    $iAColaborador = $this->informacaoadicionalcolaboradorModel->buscarToIAColaborador($colaboradores['id']);

                    $acao = 'index.php?controle=colaboradoresController&acao=atualizar';
                    require './protected/view/colaboradores/listColaboradores.php'; 
                }
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao cadastrar os dados: '.$r.'
                      </div>';
                $colaboradores = $dados;

                $listaLojas    = $this->lojaModel->buscarTodos();
                $listaSetores  = $this->setorModel->buscarTodos(); 
                $listaCargos   = $this->cargoModel->buscarTodos();
                $listaEstados  = $this->estadoModel->buscarTodos();
                $iAColaborador = $this->informacaoadicionalcolaboradorModel->buscarToIAColaborador($colaboradores['id']);

                $acao = 'index.php?controle=colaboradoresController&acao=inserir';
                require './protected/view/colaboradores/formColaboradores.php';
            }
        }

        public function inserirIAColaborador(array $dados){

            # Pega o objeto serializado pelo front-end
            # Objeto contem o colaborador
            # Pode ser acessado todos os tributos do colaborador
            $deserialize = unserialize(
                                gzinflate(
                                    base64_decode(
                                        $dados['colaborador']
                                )
                            )
                        );

            # Objeto colaborador não é mais necessário, então damos um clean nele (:
            unset($dados['colaborador']);

            # Quando estiver editando esse ID vai existir
            if(isset($_GET['id']))
                $id = $_GET['id'];
            else
                $id = $this->model->buscarUltimo();

            # Busca colaboradores
            $colaboradores = $this->model->buscar( $id );

            # Verifica se ja existe informações de col. para este colaborador
            $res = $this->informacaoadicionalcolaboradorModel->buscarToIAColaborador($id);

            # Caso ele exista ($idinfo)
            # Deve atualizar a tabela de informacoes adicionais do colaborador
            if($res){

                $res = $this->informacaoadicionalcolaboradorModel->atualizar($dados);

                if($res){
                    echo '<div class="alert alert-success">
                            Informações adicionais do colaborador atualizado com <strong>Sucesso</strong>.
                          </div>';
                }else{
                    echo '<div class="alert alert-danger">
                            Não foi possível realizar atualização das informações adicionais:
                          </div>';
                }
            }else{

                $res = $this->informacaoadicionalcolaboradorModel->inserir($dados);

                if($res){
                    echo '<div class="alert alert-success">
                            Informações adicionais do colaborador cadastrado com <strong>Sucesso</strong>.
                            <br/>
                            Aguarde... <br/>
                            Estamos direcionando você para a Relação de Colaboradores
                          </div>';
                          echo "<script>
                                    setTimeout(function () {
                                        window.location.href = 'http://atlantajeans.com.br/gerenciador/index.php?controle=colaboradoresController&acao=listar';
                                     }, 10000);
                                </script>";
                          exit;
                }else{    
                    echo '<div class="alert alert-danger">
                            Não foi possível realizar o cadastro das informações adicionais.
                          </div>';
                }
            }

            $listaLojas    = $this->lojaModel->buscarTodos();
            $listaSetores  = $this->setorModel->buscarTodos(); 
            $listaCargos   = $this->cargoModel->buscarTodos();
            $listaEstados  = $this->estadoModel->buscarTodos();
            $iAColaborador = $this->informacaoadicionalcolaboradorModel->buscarToIAColaborador($deserialize['id']);

            $acao = $dados['acaoBack'];
            # Ação para informações adicionais de colaboradores
            $acaoIAColaborador = 'index.php?controle=colaboradoresController&acao=inserirIAColaborador&id='.$deserialize['id'];
            require './protected/view/colaboradores/formColaboradores.php';
        }
        
        public function listar() {
            $listaDados = $this->model->buscarTodos();
            require './protected/view/colaboradores/listColaboradores.php';
        }
        
        public function buscar($id) {
            $listaLojas    = $this->lojaModel->buscarTodos();
            $listaSetores  = $this->setorModel->buscarTodos(); 
            $listaCargos   = $this->cargoModel->buscarTodos();
            $listaEstados  = $this->estadoModel->buscarTodos();
            $colaboradores = $this->model->buscar($id);   

            $iAColaborador = $this->informacaoadicionalcolaboradorModel->buscarToIAColaborador($colaboradores['id']); 
            $acao          = 'index.php?controle=colaboradoresController&acao=atualizar';
            ///Acao para informações adicionais de colaboradores
            $acaoIAColaborador  
                           = 'index.php?controle=colaboradoresController&acao=inserirIAColaborador&id='.$colaboradores['id'];
            require './protected/view/colaboradores/formColaboradores.php';
        }

        public function buscarUltimo() {
            $sql   = "SELECT MAX(id) as id FROM colaboradores;";
            $res   = $this->bd->prepare($sql);
            $res->execute();
            if ($res->rowCount() > 0) {
                foreach ($res as $rs) {
                    $idcol = $rs["id"];
                }
            }

            return $idcol;
        }
        
        public function atualizar(array $dados) {

            $r = $this->model->atualizar($dados);
            $rdocumentacao = $this->model->atualizardocumentacao($dados);
            //atualiza o colaborador
            if($r){
                echo '<div class="alert alert-success">
                        Dados atualizados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao atualizar os dados.
                      </div>';
            }
            $this->listar();
        }
        
        public function excluir($id){
            $r = $this->model->excluir($id);
            if($r>0){
                echo '<div class="alert alert-success">
                        Dados Removidos com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        '.$r.'
                      </div>';
            }
            $this->listar();
        }
    }