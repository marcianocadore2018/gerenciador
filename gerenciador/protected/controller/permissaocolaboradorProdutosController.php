<?php

class PermissaocolaboradorProdutosController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new PermissaocolaboradorprodutosModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=permissaocolaboradorProdutosController&acao=inserir';
        require './protected/view/permissaocolaboradorprodutos/formPermissaocolaboradorprodutos.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/permissaocolaboradorprodutos/listPermissaocolaboradorprodutos.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $permissaocolaboradorproduto = $this->model->buscar($id);
        $acao = 'index.php?controle=permissaocolaboradorProdutosController&acao=atualizar';
        require './protected/view/permissaocolaboradorprodutos/formPermissaocolaboradorprodutos.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Permissão do Colaborador pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}