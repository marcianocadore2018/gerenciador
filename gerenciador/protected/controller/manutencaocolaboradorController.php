<?php

class ManutencaocolaboradorController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new ManutencaocolaboradorModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/manutencaocolaborador/listManutencaocolaborador.php';
    }
    
    public function restaurar($id) {
        $acao = 'index.php?controle=manutencaocolaboradorController&acao=atualizarrestaurar';
        require './protected/view/manutencaocolaborador/formManutencaocolaborador.php';
    }
    
    public function atualizarrestaurar(array $dados) {
        $r = $this->model->atualizarrestaurar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
}