<?php

class DescontomaximoController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new DescontomaximoModel();
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível gravar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/descontomaximo/listDescontomaximo.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $descontomaximo = $this->model->buscar($id);
        $acao = 'index.php?controle=descontomaximoController&acao=atualizar';
        require './protected/view/descontomaximo/formDescontomaximo.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
}