<?php

class DocumentacaoController {
    private $bd, $model;
    private $ColaboradorModel;
    private $EstadoModel;
    
    function __construct() {
        
        $this->colaboradorModel = new ColaboradoresModel();
        $this->estadoModel = new EstadoModel();
        $this->model        = new DocumentacaoModel();
    }
    
    public function novo() {
        $listaColaboradores = $this->colaboradorModel->buscarTodos();
        $listaEstados = $this->estadoModel->buscarTodos();
        $acao = 'index.php?controle=documentacaoController&acao=inserir';
        require './protected/view/documentacao/formDocumentacao.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/documentacao/listDocumentacao.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaColaboradores = $this->colaboradorModel->buscarTodos();
        $listaEstados = $this->estadoModel->buscarTodos();
        $documentacao  = $this->model->buscar($id);
        $acao         = 'index.php?controle=documentacaoController&acao=atualizar';
        require './protected/view/documentacao/formDocumentacao.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover); 

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Documentacao do Colaborador pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}