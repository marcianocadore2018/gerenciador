<?php

class RetiradaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new RetiradaModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=retiradaController&acao=inserir';
        require './protected/view/retirada/formRetirada.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível gravar os cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/retirada/listRetirada.php';
    }
    
    public function buscar($id) {
        $retirada = $this->model->buscar($id);
        $acao = 'index.php?controle=retiradaController&acao=atualizar';
        require './protected/view/retirada/formRetirada.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Retirada pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}