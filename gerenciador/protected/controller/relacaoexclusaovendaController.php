<?php

class RelacaoexclusaovendaController {
    private $bd, $model;
    private $vendaModel;
    
    function __construct() {
        $this->model = new RelacaoexclusaovendaModel();
        $this->modelVendas = new VendaModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/ajusteexclusaovenda/relacaoAjusteexclusaovenda.php';
    }
}