<?php

class EmailaniversarianteController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new EmailaniversarianteModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/emailaniversariante/listEmailaniversariante.php';
    }
}