<?php

class DiferencabonustrocaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new DiferencabonustrocaModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/diferencabonustroca/listDiferencabonustroca.php';
    }
}