<?php

class AjusteexclusaovendaController {
    private $bd, $model;
    private $vendaModel;
    
    function __construct() {
        $this->model = new AjusteexclusaovendaModel();
        $this->modelVendas = new VendaModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/ajusteexclusaovenda/listAjusteexclusaovenda.php';
    }
    public function buscar($id) {
        $ajusteexclusaovenda = $this->model->buscar($id);
        $acao = 'index.php?controle=ajusteexclusaovendaController&acao=atualizar';
        require './protected/view/ajusteexclusaovenda/formAjusteexclusaovenda.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-warning">
                    Não foi possível excluir esta venda, pois a mesma já encontra-se excluída.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Venda possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}