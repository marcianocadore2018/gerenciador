<?php

class GerenciamentofinanceirosaidaController {
    private $bd, $model;
    private $ColaboradorModel;
    
    function __construct() {

        $this->colaboradorModel = new ColaboradoresModel();
        $this->model = new GerenciamentofinanceirosaidaModel();
    }
    
    public function novo() {
        $listaColaboradores = $this->colaboradorModel->buscarTodos();
        $acao = 'index.php?controle=gerenciamentofinanceirosaidaController&acao=inserir';
        require './protected/view/gerenciamentofinanceirosaida/formGerenciamentofinanceirosaida.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/gerenciamentofinanceirosaida/listGerenciamentofinanceirosaida.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaColaboradores = $this->colaboradorModel->buscarTodos();
        $gerenciamentofinanceirosaida = $this->model->buscar($id);
        $acao = 'index.php?controle=gerenciamentofinanceirosaidaController&acao=atualizar';
        require './protected/view/gerenciamentofinanceirosaida/formGerenciamentofinanceirosaida.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Gerenciamento Financeiro de Saída pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}