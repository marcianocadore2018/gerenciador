<?php

class EstoquecontroleController {

    private $bd, $model;
    private $grupoModel;
    public $controleSql;
    public $dados;

    function __construct() {
        $this->model = new EstoquecontroleModel();
        $this->grupoModel = new GrupoModel();
    }
    
    public function listar() {
        $acao = "index.php?controle=estoquecontroleController&acao=filtroEstoque";
        # Executa uma busca pelo filtro de situacao
        if(isset($this->dados['fazBusca'])){
            if($this->controleSql){
                $checked1 = "checked=''";
                $checked2 = "";
                $listaDados = $this->model->buscarFiltroEntrada($this->dados['idgrupo']);
            }else{
                $checked1 = "";
                $checked2 = "checked=''";
                $listaDados = $this->model->buscarFiltroSaida($this->dados['idgrupo']);
            }
        }else{
            $checked1 = "checked=''";
            $checked2 = "";
            // Somente executa a primera vez
            $listaDados = $this->model->buscarTodos();
        }
        $listaGrupos = $this->grupoModel->buscarTodos();
        require './protected/view/estoque/listEstoquecontrole.php';
    }
    
    public function filtroEstoque(array $dados){
        if($dados['situacao']==='E'){
            $this->controleSql = true;
        }else{
            $this->controleSql = false;
        }
        
        $dados['fazBusca'] = true;
        
        $this->dados = $dados;
        
        $this->listar();
    }
}