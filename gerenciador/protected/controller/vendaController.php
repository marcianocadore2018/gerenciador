<?php

class VendaController {
    private $bd, $model, $modelClientes, $modelColaboradores;
    
    function __construct() {

        $this->model              = new VendaModel();
        $this->modelClientes      = new ClienteModel();
        $this->modelColaboradores = new ColaboradoresModel();
        $this->modelParcelamento  = new ParcelamentoModel();
        $this->modelProdutos      = new ProdutoModel();
        $this->modelFormaPgto     = new TipoPagamentoModel();
        $this->modelDescontoVenda = new DescontoVendaModel();
    }
    
    public function novo() {
        $listaClientes      = $this->modelClientes->buscarClientevenda();
        $listaColaboradores = $this->modelColaboradores->buscarTodos();
        $listaParcelamentos = $this->modelParcelamento->buscarTodos();
        $listaProdutos      = $this->modelProdutos->buscarTodos();
        $listaFormas        = $this->modelFormaPgto->buscarTodos();
        $listaDescontos     = $this->modelDescontoVenda->buscarTodos();
        $acao = 'index.php?controle=vendaController&acao=inserir';
        require './protected/view/venda/formVenda.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/venda/listVenda.php';
    }
    
    public function buscar($id) {
        $listaClientes = $this->modelClientes->buscarClientevenda();
        $listaColaboradores = $this->modelColaboradores->buscarTodos();
        $listaProdutos      = $this->modelProdutos->buscarTodos();
        $listaFormas        = $this->modelFormaPgto->buscarTodos();
        $listaDescontos     = $this->modelDescontoVenda->buscarTodos();
        $venda  = $this->model->buscar($id);
        $acao         = 'index.php?controle=vendaController&acao=atualizar';
        require './protected/view/venda/formVenda.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Venda excluída com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Venda pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}