<?php

class ProdutoController {
    private $bd, $model;
    private $grupoModel;
    
    function __construct() {
        $this->grupoModel = new GrupoModel();
        $this->model      = new ProdutoModel();
    }
    
    public function novo() {
        $listaGrupos = $this->grupoModel->buscarTodos();
        $acao = 'index.php?controle=produtoController&acao=inserir';
        require './protected/view/produto/formProduto.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
           $info = 'Dados cadastrados com sucesso.';
        }else{
            echo '<div id="dialoginserirproduto" title="Erro ao salvar o Produto">
                        <p><b>Não foi possível gravar o registro! Por favor informe a Foto do Produto novamente ou então entre em contato com o administrador do gerenciador.</b></p>
                    </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/produto/listProduto.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaGrupos = $this->grupoModel->buscarTodos();
        $produto = $this->model->buscar($id);
        $acao = 'index.php?controle=produtoController&acao=atualizar';
        require './protected/view/produto/formProduto.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Produto pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}