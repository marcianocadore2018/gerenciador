<?php

class ManutencaotrocaprodutoController {

    private $bd, $model;

    function __construct() {
        $this->model = new ManutencaotrocaprodutoModel();
        
    }

    public function trocarproduto(array $dados) {
        $idprodutodevolvido = $_POST['idprodutodevolvido'];
        $idprodutotrocado = $_POST['idprodutotrocado'];
        $quantidadetrocada = $_POST['quantidade'];
        $quantidadedevolvida = $_POST['quantidadedevolvida'];
        $idvendatroca = $_POST['idvendatroca'];
        
        //VERIFICAR SE O TIPO DO CLIENTE, (CLIENTE OU CLIENTE RÁPIDO)
        $sqlverificartipocliente = pg_query("select v.idcliente
                                               from venda v
                                              where v.id = $idvendatroca");
        
        $rsverificartipocliente = pg_fetch_array($sqlverificartipocliente);
        $tipocliente = $rsverificartipocliente['idcliente'];
        if ($tipocliente >= 100000) {
            //CONSULTA VALOR DO PRODUTO TROCADO
            $sqlvalorprodutotrocado = pg_query("select pro.valor as valorproduto
                                                  from produto pro
                                                 where pro.id = $idprodutotrocado");
            $rsvalorprodutotrocado = pg_fetch_array($sqlvalorprodutotrocado);
            $valorproduto = $rsvalorprodutotrocado['valorproduto'];
            $removerpontovalorprodutotrocado = number_format($valorproduto, 2, '', '.');
            //CONSULTAR ID DO CLIENTE ATRAVÉS DO ID DA TRANSACAO -idtransacao
            $sqlcliente = pg_query("select idcliente
                                      from venda 
                                     where id = $idvendatroca");
            $rscliente = pg_fetch_array($sqlcliente);
            $idcliente = $rscliente['idcliente'];
            
            //Se o produto devolvido é igual ao produto trocado
            if ($idprodutodevolvido == $idprodutotrocado) {
                if (isset($_SERVER['HTTPS'])) {
                    $prefixo = 'https://';
                } else {
                    $prefixo = 'http://';
                }

                $app = $_SERVER['HTTP_REFERER'];
                if (strpos($app, 'gerenciadorlocal') == true) {
                    $aplicacao = 'gerenciadorlocal';
                } else if (strpos($app, 'gerenciadortest') == true) {
                    $aplicacao = 'gerenciadortest';
                } else if (strpos($app, 'gerenciador') == true) {
                    $aplicacao = 'gerenciador';
                }
                //URL BASE
                $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
                //codificarget
                $string = "error=errortrocaigual";
                $encriptografa = base64_encode($string);
                echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errortroca=$encriptografa'</script>";
            } else if ($quantidadetrocada != $quantidadedevolvida) {
                if (isset($_SERVER['HTTPS'])) {
                    $prefixo = 'https://';
                } else {
                    $prefixo = 'http://';
                }

                $app = $_SERVER['HTTP_REFERER'];
                if (strpos($app, 'gerenciadorlocal') == true) {
                    $aplicacao = 'gerenciadorlocal';
                } else if (strpos($app, 'gerenciadortest') == true) {
                    $aplicacao = 'gerenciadortest';
                } else if (strpos($app, 'gerenciador') == true) {
                    $aplicacao = 'gerenciador';
                }
                //URL BASE
                $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
                //codificarget
                $string = "error=quantidadediferente";
                $encriptografa = base64_encode($string);
                echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errorqtddif=$encriptografa'</script>";
            } else {
                $r = $this->model->realizartrocaproduto($dados);
                if ($r) {
                    echo '<div class="alert alert-success">
                                Produto Alterado com <strong>Sucesso</strong>.
                              </div>';
                } else {
                    echo '<div class="alert alert-success">
                                Produto Alterado com <strong>Sucesso</strong>.
                              </div>';
                }
                $this->redirecionarbonus();
            }
        } else if ($tipocliente < 100000) {
            //CONSULTA VALOR DO PRODUTO TROCADO
            $sqlvalorprodutotrocado = pg_query("select pro.valor as valorproduto
                                                  from produto pro
                                                 where pro.id = $idprodutotrocado");
            $rsvalorprodutotrocado = pg_fetch_array($sqlvalorprodutotrocado);
            $valorproduto = $rsvalorprodutotrocado['valorproduto'];
            $removerpontovalorprodutotrocado = number_format($valorproduto, 2, '', '.');

            //CONSULTAR ID DO CLIENTE ATRAVÉS DO ID DA TRANSACAO -idtransacao
            $sqlcliente = pg_query("select idcliente
                                      from venda 
                                     where id = $idvendatroca");
            $rscliente = pg_fetch_array($sqlcliente);
            $idcliente = $rscliente['idcliente'];
            
            //CONSULTA VALOR DO PRODUTO TROCADO E VERIFICA SE O CLIENTE POSSUI SALDO DISPONÍVEL PARA REALIZAR A TROCA
            $sqlvalordisponivel = pg_query("SELECT ( total_renda - total_itens + total_pago ) AS limite_disponivel
                                              FROM   (SELECT COALESCE(Sum(p.valorpago), 0.00) AS total_pago 
                                                        FROM   pagamentos p 
                                                       WHERE  p.idcliente = $idcliente) a, 
                                                     (SELECT COALESCE(( Sum(rc.valorrenda) * (SELECT porcentagem 
                                                                                                FROM porcentagemcrediariocliente) / 100 ), 
                                                                      0.00) AS total_renda 
                                                       FROM   rendacliente rc 
                                                       WHERE  rc.idcliente = $idcliente) b, 
                                                      (SELECT COALESCE(Sum(item.valortotal - v.valor_entrada), 0.00) AS 
                                                              total_compra_itens 
                                                       FROM   itensproduto item, 
                                                              venda v 
                                                       WHERE  v.idcliente = $idcliente 
                                                              AND item.idvenda = v.id) c, 
                                                      (SELECT COALESCE(Sum(item.valortotal), 0.00) AS total_itens 
                                                       FROM   itensproduto item, 
                                                              venda v, 
                                                              tipopagamento tp 
                                                       WHERE  v.idcliente = $idcliente 
                                                              AND item.idvenda = v.id 
                                                              AND v.formapgto = tp.id 
                                                              AND tp.tipo = 'R' 
                                                              AND ( v.tipopagamento = '1' )) d;");
            $rsvalordisponivel = pg_fetch_array($sqlvalordisponivel);
            $valordisponivel = $rsvalordisponivel['limite_disponivel'];
            $formatarvalordisponivel = number_format($valordisponivel, 2, '', '');
            
            //VERIFICA SE O VALOR DA TROCA DO PRODUTO É MAIOR QUE O VALOR DISPONIVEL
            $verificavalordisponiveltroca = ($formatarvalordisponivel - $removerpontovalorprodutotrocado);
            
            //SE O VALOR DISPONIVEL FOR MAIOR QUE ZERO CONTINUA O FLUXO, CASO IRÁ CAIR NO ELSE
            if ($verificavalordisponiveltroca > 0) {
                if ($idprodutodevolvido == $idprodutotrocado) {
                    if (isset($_SERVER['HTTPS'])) {
                        $prefixo = 'https://';
                    } else {
                        $prefixo = 'http://';
                    }

                    $app = $_SERVER['HTTP_REFERER'];
                    if (strpos($app, 'gerenciadorlocal') == true) {
                        $aplicacao = 'gerenciadorlocal';
                    } else if (strpos($app, 'gerenciadortest') == true) {
                        $aplicacao = 'gerenciadortest';
                    } else if (strpos($app, 'gerenciador') == true) {
                        $aplicacao = 'gerenciador';
                    }
                    //URL BASE
                    $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
                    //codificarget
                    $string = "error=errortrocaigual";
                    $encriptografa = base64_encode($string);
                    echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errortroca=$encriptografa'</script>";
                } else if ($quantidadetrocada != $quantidadedevolvida) {
                    if (isset($_SERVER['HTTPS'])) {
                        $prefixo = 'https://';
                    } else {
                        $prefixo = 'http://';
                    }

                    $app = $_SERVER['HTTP_REFERER'];
                    if (strpos($app, 'gerenciadorlocal') == true) {
                        $aplicacao = 'gerenciadorlocal';
                    } else if (strpos($app, 'gerenciadortest') == true) {
                        $aplicacao = 'gerenciadortest';
                    } else if (strpos($app, 'gerenciador') == true) {
                        $aplicacao = 'gerenciador';
                    }
                    //URL BASE
                    $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
                    //codificarget
                    $string = "error=quantidadediferente";
                    $encriptografa = base64_encode($string);
                    echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&errorqtddif=$encriptografa'</script>";
                } else {
                    $r = $this->model->realizartrocaproduto($dados);
                    if ($r) {
                        echo '<div class="alert alert-success">
                                Produto Alterado com <strong>Sucesso</strong>.
                              </div>';
                    } else {
                        echo '<div class="alert alert-success">
                                Produto Alterado com <strong>Sucesso</strong>.
                              </div>';
                    }
                    $this->redirecionarbonus();
                }
            } else {
                echo 'aqui 2'; exit;
                if (isset($_SERVER['HTTPS'])) {
                    $prefixo = 'https://';
                } else {
                    $prefixo = 'http://';
                }

                $app = $_SERVER['HTTP_REFERER'];
                if (strpos($app, 'gerenciadorlocal') == true) {
                    $aplicacao = 'gerenciadorlocal';
                } else if (strpos($app, 'gerenciadortest') == true) {
                    $aplicacao = 'gerenciadortest';
                } else if (strpos($app, 'gerenciador') == true) {
                    $aplicacao = 'gerenciador';
                }
                //URL BASE
                $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/' . '' . $aplicacao . '/';
                //codificarget
                $string = "error=valordisponiveldiferente";
                $encriptografa = base64_encode($string);
                echo "<script>window.location.href='$urlbase/index.php?controle=trocaprodutoController&acao=novo&vldisperr=$encriptografa'</script>";
            }
        }
    }

    public function redirecionarbonus() {
        $listaDados = $this->model->buscarBonus();
        require './protected/view/manutencaotrocaproduto/listManutencaotrocaproduto.php';
    }

    public function listar() {
        $listaDados = $this->model->buscarBonus();
    }

    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if ($r) {
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        } else {
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }

}
