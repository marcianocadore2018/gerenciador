<?php

class FotoprodutoController {
    private $bd, $model;
    private $produtoModel;
    
    function __construct() {
        $this->model = new FotoprodutoModel();
        $this->modelProdutos = new ProdutoModel();
    }
    
    public function novo() {
        $listaProdutos = $this->modelProdutos->buscarTodos();
        $acao = 'index.php?controle=fotoprodutoController&acao=inserir';
        require './protected/view/fotoproduto/formFotoProduto.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/fotoproduto/listFotoProduto.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaProdutos = $this->modelProdutos->buscarTodos();
        $fotoproduto = $this->model->buscar($id);
        $acao = 'index.php?controle=fotoprodutoController&acao=atualizar';
        require './protected/view/fotoproduto/formFotoProduto.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Foto do Produto pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
    
}