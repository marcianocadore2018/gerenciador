<?php

class DescontotipopagamentoController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new DescontotipopagamentoModel();
        $this->modelTipopagamentos = new TipopagamentoModel();
        $this->modelColaboradores = new ColaboradoresModel();
    }
    
    public function novo() {
        $listaTipopagamentos = $this->modelTipopagamentos->buscarTodos();
        $listaColaboradores = $this->modelColaboradores->buscarTodos();
        $acao = 'index.php?controle=descontotipopagamentoController&acao=inserir';
        require './protected/view/descontotipopagamento/formDescontotipopagamento.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não é possível inserir, pois este Tipo de Pagamento já encontra-se cadastrado. Por favor informe outro Tipo de Pagamento!
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/descontotipopagamento/listDescontotipopagamento.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaTipopagamentos = $this->modelTipopagamentos->buscarTodos();
        $descontotipopagamento = $this->model->buscar($id);
        $acao = 'index.php?controle=descontotipopagamentoController&acao=atualizar';
        require './protected/view/descontotipopagamento/formDescontotipopagamento.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Desconto Tipo de Pagamento pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}