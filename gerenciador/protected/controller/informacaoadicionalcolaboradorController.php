<?php

class InformacaoadicionalcolaboradorController {
    private $bd, $model;
    private $modelColaborador;
    private $modelCargo;
    private $modelSetor;
    
    function __construct() {
        $this->model               = new InformacaoadicionalcolaboradorModel();
        $this->modelColaborador    = new ColaboradoresModel();
        $this->modelCargo          = new CargoModel();
        $this->modelSetor          = new SetorModel();
    }
    
    public function novo() {
        $listaColaboradores = $this->modelColaborador->buscarTodos();
        $listaSetores = $this->modelSetor->buscarTodos();
        $listaCargos = $this->modelCargo->buscarTodos();
        $acao = 'index.php?controle=informacaoadicionalcolaboradorController&acao=inserir';
        require './protected/view/informacaoadicionalcolaborador/formInformacaoadicionalcolaborador.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/informacaoadicionalcolaborador/listInformacaoadicionalcolaborador.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaColaboradores = $this->modelColaborador->buscarTodos();
        $listaCargos = $this->modelCargo->buscarTodos();
        $listaSetores = $this->modelSetor->buscarTodos();
        $informacaoadicionalcolaborador  = $this->model->buscar($id);
        $acao         = 'index.php?controle=informacaoadicionalcolaboradorController&acao=atualizar';
        require './protected/view/informacaoadicionalcolaborador/formInformacaoadicionalcolaborador.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Informação Adicional do Colaborador pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}