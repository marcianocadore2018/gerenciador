<?php

class CargotesteController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new CargotesteModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=cargotesteController&acao=inserir';
        require './protected/view/cargoteste/formCargoTeste.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/cargoteste/listCargoTeste.php';
    }
    
    public function buscar($id) {
        $cargo = $this->model->buscar($id);
        $acao = 'index.php?controle=cargotesteController&acao=atualizar';
        require './protected/view/cargoteste/formCargoTeste.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Porcentagem do Crediário do Cliente pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}