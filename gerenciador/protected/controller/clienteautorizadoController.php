<?php

class ClienteautorizadoController {
    private $bd, $model;
    private $clienteModel;
    
    function __construct() {
        $this->model = new ClienteautorizadoModel();
        $this->modelClientes = new ClienteModel();
    }
    
    public function novo() {
        $listaClientes = $this->modelClientes->buscarTodos();
        $acao = 'index.php?controle=clienteautorizadoController&acao=inserir';
        require './protected/view/clienteautorizado/formClienteautorizado.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/clienteautorizado/listClienteautorizado.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaClientes = $this->modelClientes->buscarTodos();
        $clienteautorizado = $this->model->buscar($id);
        $acao = 'index.php?controle=clienteautorizadoController&acao=atualizar';
        require './protected/view/clienteautorizado/formClienteautorizado.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Cliente Autorizado pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
    
}