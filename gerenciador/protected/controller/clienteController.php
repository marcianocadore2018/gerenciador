<?php

class ClienteController {
    private $bd, $model, $modelEstados;
    
    function __construct() {
        $this->model        = new ClienteModel();
        $this->modelEstados = new EstadoModel();
        $this->modelRendas  = new RendaModel();
    }
    
    public function novo() {
        $listaEstados = $this->modelEstados->buscarTodos();
        ///Ação padrão para cliente
        $acao       = 'index.php?controle=clienteController&acao=inserir';
        ///Acao para cadastros de rendas
        $acaoRenda  = 'index.php?controle=clienteController&acao=inserirRenda';
        require './protected/view/cliente/formCliente.php';
    }
    
    public function inserir(array $dados) {

        $acaoRenda  = 'index.php?controle=clienteController&acao=inserirRenda';

        $r = $this->model->inserir($dados);
        if((int)$r>0){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>'; 
            if($_GET['acao'] != 'inserir'){
                $this->listar();
            }else{

                $listaEstados = $this->modelEstados->buscarTodos();
                $cliente      = $this->model->buscar( $this->model->buscarUltimo() );
                $acao = 'index.php?controle=clienteController&acao=atualizar';
                require './protected/view/cliente/formCliente.php'; 
            }
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados: '.$r.'
                  </div>';
            $listaEstados = $this->modelEstados->buscarTodos();
            $dados['cpf'] = NULL;
            $cliente      = $dados;
            $acao = 'index.php?controle=clienteController&acao=inserir';
            require './protected/view/cliente/formCliente.php'; 
        }
       
    }


    public function inserirRenda(array $dados){

        //Pega o cliente do objecto serializado no frontend
        $cliente = unserialize(
                        gzinflate(
                            base64_decode(
                                $dados['cliente']
                            )
                        )
                    );

        unset($dados['cliente']);

        $r = $this->modelRendas->inserir($dados);

        if($r){
            echo '<div class="alert alert-success">
                    Renda cadastrada com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível cadastrar renda.
                  </div>';
        }

        $listaEstados = $this->modelEstados->buscarTodos();
        $listaRendas  = $this->modelRendas->buscarToCliente($cliente['id']);
        $acao = $dados['acaoBack'];
        require './protected/view/cliente/formCliente.php';
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        $listaRendas  = $this->modelRendas->buscarToCliente(3);
        require './protected/view/cliente/listCliente.php';
    }
    
    public function listarClientevenda() {
        $listaDados   = $this->model->buscarClientevenda();
        $listaRendas  = $this->modelRendas->buscarToCliente(3);
        require './protected/view/cliente/listCliente.php';
    }

    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaEstados = $this->modelEstados->buscarTodos();
        $cliente      = $this->model->buscar($id);
        $listaRendas  = $this->modelRendas->buscarToCliente($id);
        $acao         = 'index.php?controle=clienteController&acao=atualizar';
        ///Acao para cadastros de rendas
        $acaoRenda    = 'index.php?controle=clienteController&acao=inserirRenda';
        require './protected/view/cliente/formCliente.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Cliente pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}