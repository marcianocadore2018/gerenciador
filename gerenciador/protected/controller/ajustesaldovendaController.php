<?php

class AjustesaldovendaController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new CargoModel();
    }
    
    public function novo() {
        $acao = 'index.php?controle=cargoController&acao=inserir';
        require './protected/view/cargo/formCargo.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/cargo/listCargo.php';
    }
    
    public function buscar($id) {
        $cargo = $this->model->buscar($id);
        $acao = 'index.php?controle=cargoController&acao=atualizar';
        require './protected/view/cargo/formCargo.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-success">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Porcentagem do Crediário do Cliente pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}