<?php

class RendaController {
    private $bd, $model, $modelClientes;
    
    function __construct() {
        $this->model        = new RendaModel();
        $this->modelClientes = new ClienteModel();
    }
    
    public function novo() {
        $listaClientes = $this->modelClientes->buscarTodos();
        $acao = 'index.php?controle=rendaController&acao=inserir';
        require './protected/view/renda/formRenda.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados cadastrados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/renda/listRenda.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $listaClientes = $this->modelClientes->buscarTodos();
        $renda  = $this->model->buscar($id);
        $acao         = 'index.php?controle=rendaController&acao=atualizar';
        require './protected/view/renda/formRenda.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);

        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Renda do Cliente pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}