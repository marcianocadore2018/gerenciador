<?php

//Verifica ambiente, Test ou Prod
//Data alteração: 24/02/2018
//Author: Marciano L. Cadore
$indicesServer = array('REQUEST_URI');

foreach ($indicesServer as $arg) {
    if (isset($_SERVER[$arg])) {
        $retornoambiente = $_SERVER[$arg];
    }
}

$buscaambiente = explode("/", $retornoambiente);
$testambiente = $buscaambiente[1];

if($testambiente == 'gerenciadortest'){
    $title = 'Gerenciador Test';
    $navtitle = 'Gerenciador Test Atlanta Jeans';
    $sairaplicacao = '/gerenciadortest/controlelogin/sair.php';
    $redireciona = 'http://atlantajeans.com.br/gerenciadortest/controlelogin/login.php';
    $config_bd_host = '104.236.114.2';
    $config_bd_nome = 'gerenciadortest';
    $config_bd_tipo = 'pgsql';
    $config_bd_user = 'postgres';
    $config_bd_senha = 'C4mu1M4st3r';
    $config_port_rel = '5432';
}else if($testambiente == 'gerenciador'){
    $title = 'Gerenciador';
    $navtitle = 'Gerenciador Atlanta Jeans';
    $sairaplicacao = '/gerenciador/controlelogin/sair.php';
    $redireciona = 'http://atlantajeans.com.br/gerenciador/controlelogin/login.php';
    $config_bd_host = '104.236.114.2';
    $config_bd_nome = 'gerenciador';
    $config_bd_tipo = 'pgsql';
    $config_bd_user = 'postgres';
    $config_bd_senha = 'C4mu1M4st3r';
    $config_port_rel = '5432';
}else if($testambiente == 'gerenciadorlocal'){
    $title = 'Gerenciador Local';
    $navtitle = 'Gerenciador Local Atlanta Jeans';
    $sairaplicacao = '/gerenciador/controlelogin/sair.php';
    $redireciona = 'localhost/gerenciador/controlelogin/login.php';
    $config_bd_host = 'localhost';
    $config_bd_nome = 'gerenciador';
    $config_bd_tipo = 'pgsql';
    $config_bd_user = 'postgres';
    $config_bd_senha = 'java2010';
    $config_port_rel = '5432';
}
?>