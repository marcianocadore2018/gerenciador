<?php

Class Controlador{
   
    function __construct() {
        
        if(isset($_GET['controle'])){
            $ctrlNome = $_GET['controle'];
            
            $arquivo  = './protected/controller/'.$ctrlNome.'.php';
            
            if(file_exists($arquivo)){ ?>
                <?php
                require $arquivo;
                
                $controle = new $ctrlNome();                
                    
                if($_GET['acao']=="novo" || $_GET['acao']=="listar"){
                    $controle->{$_GET['acao']}();

                }else if($_GET['acao']=="inserir" || $_GET['acao']=="atualizar" || $_GET['acao']=="filtrarpagamento" 
                        || $_GET['acao']=="filtrartrocaproduto" || $_GET['acao']=="trocarproduto" || $_GET['acao']=="restaurar" || $_GET['acao']=="atualizarrestaurar"){
                    $controle->{$_GET['acao']}($_POST);

                }else if($_GET['acao']=="buscar" || $_GET['acao']=="excluir"){
                    $controle->{$_GET['acao']}($_GET['id']);

                }else if($_GET['acao']=="buscarToCliente"){
                    $controle->{$_GET['acao']}($_GET['id']);

                }else if($_GET['acao']=="inserirRenda"){
                    $controle->{$_GET['acao']}($_POST);

                }else if($_GET['acao']=="buscarToIAColaborador"){
                    $controle->{$_GET['acao']}($_GET['id']);
                    
                }else if($_GET['acao']=="inserirIAColaborador"){
                    $controle->{$_GET['acao']}($_POST);

                }else if($_GET['acao']=="filtroEstoque"){
                    $controle->{$_GET['acao']}($_POST);

                }

            }
        }else{ ?>
        <div id="conteudo">
        	<style>

        	</style>
        	<center>
	        	<table style="margin-top: 15%">
	        		<tr>
	        		<!-- Verifica qual é o tipo do usuário para permissão nos acessos rápidos-->
	        		<?php
		        		$secao = $_SESSION['login'];
						$sql   = pg_query("SELECT tipocolaborador FROM colaboradores WHERE login = '$secao';");
						$res   = pg_fetch_array ($sql);
						$colaboradortipo = $res['tipocolaborador'];
					?>
					<?php if($colaboradortipo == 'C' || $colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D'){?>
						<th>
					    	<a href="index.php?controle=clienteController&acao=listar">
						    	<img style="margin-left: 10px; height: 120px" src="includes/img/clientes.png" class="img-responsive">
						    		<label style="margin-left: 32%">Clientes</label>
						    	</img>
						    </a>
					    </th>
					<?php } ?>

					<?php if($colaboradortipo == 'M' || $colaboradortipo == 'G' || $colaboradortipo == 'D'){?>
					    <th>
					    	<a href="index.php?controle=colaboradoresController&acao=listar">
						    	<img style="margin-left: 50px; height: 120px" src="includes/img/colaboradores.png" class="img-responsive">
							    	<label style="margin-left: 38%">Colaboradores</label>
							    </img>
							</a>
						</th> 
					<?php } ?>

					<?php if($colaboradortipo == 'M' || $colaboradortipo == 'G' || $colaboradortipo == 'C' || $colaboradortipo == 'D'){?>
					    <th>
					    	<a href="index.php?controle=vendaController&acao=listar">
						    	<img style="margin-left: 50px; height: 120px" src="includes/img/vendas.png" class="img-responsive">
						    		<label style="margin-left: 52%">Vendas</label>
						    	</img>
						    </a>
					    </th>
					<?php } ?>

					<?php if($colaboradortipo == 'G' || $colaboradortipo == 'M' || $colaboradortipo == 'D'){?>
					    <th>
					    	<a href="index.php?controle=estoquecontroleController&acao=listar">
						    	<img style="margin-left: 50px; height: 120px" src="includes/img/estoque.png" class="img-responsive">
						    		<label style="margin-left: 50%">Estoque</label>
						    	</img>
						    </a>
					    </th>
					<?php } ?>
				  </tr>
	        	</table>
	        </center>
	        <br><br><br><br>
	        <center>
	        	<table>
				  <?php 
				  	 	$clienteModel = new ClienteModel();
    					$resultado    = $clienteModel->buscaAniversariantes("true"); //indica que deve buscar por aniversariantes que ja foram enviados nesse mes

    					$qtd = $resultado->rowCount();
				  ?>

				  <tr>
				  		<th>
				  		<h4 id="qtd_email">
				  			<?php echo $qtd . ' aniversariantes receberam as felicitações da Atlanta Jeans neste mês.' ?></h4>
				  		</th>
				  </tr>
	        	</table>
	        </center>
        </div>
		<div class="footer">
			<div style="margin-top: 5px; margin-bottom: 5px; text-align: center; color: white">Sistema Gerenciador Desenvolvido por Camul Soluções Web - 2017 <br/>
			Gerenciador sem fins Fiscais</div>
		</div>
                

                <div class="modal fade" id="modalPagamento" role="dialog">
            <!-- RELATÓRIO DE PAGAMENTOS -->
            <form name="filtrorelatoriopagamento" id="filtrorelatoriopagamento" action="protected/view/relatoriopagamento/rel_relatoriopagamento.php" method="post">
                <div class="modal-dialog">
                    <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header" style="background-color: #A44160">
                          <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                      <h4 class="modal-title" style="color: white">Relatório de Pagamentos</h4>
                    </div>
                        <div class="modal-body">
                            <div class="form-group">
                            <label>Colaborador</label>
                            <select name="idcolaborador" id="idcolaborador" class="form-control">
                                <?php
                                $sqlColaborador = "select distinct col.id as idcolaborador,
                                                          col.nome as nomecolaborador
                                                     from pagamentos pg
                                                    inner join colaboradores col
                                                       on pg.idcolaborador = col.id
                                                 order by col.nome asc;";
                                $sqlColaboradorResult = pg_query($sqlColaborador);
                                echo '<option value="">
                                        Selecione...
                                    </option>';
                                while ($sqlColaboradorResultFim = pg_fetch_assoc($sqlColaboradorResult)) {
                                    ?>
                                    <option value="<?php echo $sqlColaboradorResultFim["idcolaborador"] ?>">
                                        <?php echo $sqlColaboradorResultFim["nomecolaborador"] ?>
                                    </option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <br/><br/>
                        <div class="row">
                            <div class="col-xs-5">
                                <label for="datapagamentoinicio">Data Pagamento >= </label>
                                <input class="form-control" id="datapagamentoinicio" name="datapagamentoinicio"/>
                            </div>
                            <div class="col-xs-5">
                                <label for="datapagamentofim"><= Data Pagamento</label>
                                <input class="form-control" id="datapagamentofim" name="datapagamentofim"/>
                            </div>
                        </div>            
                    </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Gerar Relatório</button>
                        </div>
                  </div>
                </div>
            </form>
        </div>
        
        <div class="modal fade" id="modalProdutos" role="dialog">
            <!-- RELATÓRIO DE PRODUTOS -->
            <form name="filtrorelatorioprodutos" id="filtrorelatorioprodutos" action="protected/view/relatorioprodutos/rel_relatorioprodutos.php" method="post">
                <div class="modal-dialog">
                    <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header" style="background-color: #A44160">
                          <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                      <h4 class="modal-title" style="color: white">Relatório de Produtos</h4>
                    </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Grupo</label>
                                <select name="idgrupo" id="idgrupo" class="form-control">
                                    <?php
                                    $sqlGrupo = "select g.id as idgrupo, 
                                                        g.descricao 
                                                   from grupo g 
                                                  where id in (select p.idgrupo as idgrupo
                                                                 from produto p)
                                                 order by g.descricao asc;";
                                    $sqlGrupoResult = pg_query($sqlGrupo);
                                    echo '<option value="">
                                            Selecione...
                                        </option>';
                                    while ($sqlGrupoResultFim = pg_fetch_assoc($sqlGrupoResult)) {
                                        ?>
                                        <option value="<?php echo $sqlGrupoResultFim["idgrupo"] ?>">
                                            <?php echo $sqlGrupoResultFim["descricao"] ?>
                                        </option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-xs-5">
                                    <label for="referenciainicio">Referência Produto Inícial >= </label>
                                    <input class="form-control" id="referenciainicio" name="referenciainicio"/>
                                </div>
                                <div class="col-xs-5">
                                    <label for="referenciafim"><= Referência Produto Final</label>
                                    <input class="form-control" id="referenciafim" name="referenciafim"/>
                                </div>
                            </div>            
                            <br/>
                            <div class="row">
                                <div class="col-xs-5">
                                    <label for="periodocadastroinicio">Data Cadastro Inicial >= </label>
                                    <input class="form-control" id="periodocadastroinicio" name="periodocadastroinicio"/>
                                </div>
                                <div class="col-xs-5">
                                    <label for="periodocadastrofim"><= Data Cadastro Final</label>
                                    <input class="form-control" id="periodocadastrofim" name="periodocadastrofim"/>
                                </div>
                            </div>
                            <br/>    
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-success">Gerar Relatório</button>
                        </div>
                  </div>
                </div>
            </form>
        </div>
 
        <div class="modal fade" id="modalVendas" role="dialog">
            <!-- RELATÓRIO DE VENDAS -->
            <form name="filtrorelatoriovendas" id="filtrorelatoriovendas" action="protected/view/relatoriovendas/rel_relatoriovendas.php" method="post">
                <div class="modal-dialog">
                    <!-- Modal content-->
                  <div class="modal-content">
                      <div class="modal-header" style="background-color: #A44160">
                          <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                      <h4 class="modal-title" style="color: white">Relatório de Vendas</h4>
                    </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Colaborador Vendedor</label>
                                <select name="idcolaboradorvendedor" id="idcolaboradorvendedor" class="form-control">
                                    <?php
                                    $sqlColaboradorVendedor = "select distinct c.id as idcolaboradorvendedor, 
                                                                      c.nome as nomevendedor 
                                                                 from venda v 
                                                                inner join colaboradores c 
                                                                   on c.id = v.idcolaboradorvendedor 
                                                                order by c.nome asc;";
                                    $sqlColaboradorVendedorResult = pg_query($sqlColaboradorVendedor);
                                    echo '<option value="">
                                            Selecione...
                                        </option>';
                                    while ($sqlColaboradorVendedorResultFim = pg_fetch_assoc($sqlColaboradorVendedorResult)) {
                                        ?>
                                        <option value="<?php echo $sqlColaboradorVendedorResultFim["idcolaboradorvendedor"] ?>">
                                            <?php echo $sqlColaboradorVendedorResultFim["nomevendedor"] ?>
                                        </option>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <label for="referencia">Referência Produto</label>
                                    <input class="form-control" id="referencia" name="referencia"/>
                                </div>
                            </div>            
                            <br/>
                            <div class="row">
                                <div class="col-xs-5">
                                    <label for="periodovendainicio">Período Venda Inicio >= </label>
                                    <input class="form-control" id="periodovendainicio" name="periodovendainicio"/>
                                </div>
                                <div class="col-xs-5">
                                    <label for="periodovendafim"><= Período Venda Fim</label>
                                    <input class="form-control" id="periodovendafim" name="periodovendafim"/>
                                </div>
                            </div>
                            <br/>    
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-success">Gerar Relatório</button>
                        </div>
                  </div>
                </div>
            </form>
        </div>
                
		<?php }
    }

    
    
} ?>