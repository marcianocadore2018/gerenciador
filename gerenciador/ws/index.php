<?php

    #Const dinamic variables
    const ID                        = ':id';

    # Routes: buscas
    const BUSCA_TOTAL_VENDA         = 'buscaTotalVenda';
    const BUSCA_PRECO_PRODUTO       = 'buscaPrecoProduto';
    const BUSCA_TOTAL_PAGAMENTO     = 'buscaTotalPgto';
    const BUSCA_PARCELAMENTO        = 'buscaParcelamento';
    const BUSCA_PARCELAMENTO_PGTO   = 'buscaParcelamentoPgto';

    # Routes: ações
    const REMOVE_ITEM_VENDA         = 'removeProduto';
    const PAGAR                     = 'pagar_';
    const ADD_ITEM_VENDA            = 'adicionarItemVenda';
    const FINALIZAR_VENDA           = 'finalizarVenda';
    const SALVAR_VENDA_CAPA         = 'salvaVendaCapa';
    const SALVAR_PGTO_CAPA          = 'salvaPgtoCapa';
    const VERIFICA_CONSISTENCIA     = 'verificaConsistencia';
    const RECALCULAR_VALORES        = 'recalcularValores';

    # Routes: calculos  
    const CALCULA_PRECO_PRODUTO     = 'calculaPrecoProduto';
    const CALCULA_LIMITE_DISPONIVEL = 'calculaLimiteDisponivel';
    const APLICAR_DESCONTO          = 'aplicarDesconto';
    const TOTAL_ABERTO              = 'totalAberto';
   
    # Routes: Grids
    const GRID_TABLE                = 'gridTable';

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim();

    # Create models instances to use in webservice
    $app->modelVenda         = new VendaModel();
    $app->modelPgto          = new PagamentosModel();
    $app->modelCliente       = new ClienteModel();
    $app->modelProduto       = new ProdutoModel();
    $app->modelTipoPgto      = new TipoPagamentoModel();
    $app->modelDescontoVenda = new DescontoVendaModel();

    # Only includes, not use to implement methods, OK?
    include ( 'routes/acoes.php' );
    include ( 'routes/buscas.php' );
    include ( 'routes/calculos.php' );
    include ( 'routes/grid_table.php' );

    include ( 'functions/index.php' );

    $app->run();