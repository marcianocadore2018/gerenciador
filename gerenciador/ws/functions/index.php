<?php

	function gridTableItem($venda, $produto){

	     $str = "<tr class=\"footable-even\" style=\"display: table-row;\">
	                <td class=\"footable-visible footable-first-column\"><span class=\"footable-toggle\"></span>
	                    ".$produto['referencia']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['nomeproduto']."
	                </td>
	                <td class=\"footable-visible\" style='text-align: center'>
	                    ".$produto['numeroproduto']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['valor']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['desconto_linha']."%
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['valor_total']."
	                </td> 
	                <td class=\"footable-visible\">
	                    ".$produto['quantidade']."
	                </td>";
	    if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
	        $str .= "
	                <td class=\"text-right footable-visible footable-last-column\">
	                    <div class=\"btn-group\">
	                        
	                        ";
	                        if($produto['status'] == 1){
	                            $str .= "<button type=\"button\" class=\"btn-white btn btn-xs glyphicon glyphicon-remove\" onclick=\"removeProduto(".$produto['id'].")\"></button>";
	                        }else{
	                            $str .= "<button type=\"button\"  class=\"btn-white btn btn-xs glyphicon glyphicon-shopping-cart\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"changeProduto(".$produto['id'].", '".$produto['nomeproduto']."')\"></button>";
	                        }

	                        $str .= "
	                    </div>
	                </td>
	            </tr>";
	    }else{
	        $str .= "
	                <td class=\"text-right footable-visible footable-last-column\">
	                    <div class=\"btn-group\">
	                        <button type=\"button\"  class=\"btn-white btn btn-xs glyphicon glyphicon-eye-open\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"viewProduto(".$produto['id'].", '".$produto['nomeproduto']."', ".$produto['quantidade'].")\"></button>
	                    </div>
	                </td>
	            </tr>";
	    }

	    return $str;
	}

	function gridTable($venda, $produto){
	    $str = "<tr class=\"footable-even\" style=\"display: table-row;\">
	                <td class=\"footable-visible footable-first-column\"><span class=\"footable-toggle\"></span>
	                    ".$produto['referencia']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['nomeproduto']."
	                </td>
	                <td class=\"footable-visible\" style='text-align: center'>
	                    ".$produto['numeroproduto']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['valor']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['datacadastro']."
	                </td>
	                <td class=\"footable-visible\">
	                    ".$produto['quantidade']."
	                </td>";

	            if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
	                if($produto['status'] == 1){
	                    $str .= "<td class=\"footable-visible\">
	                                <span class=\"label label-success\">Sim</span>
	                            </td>";
	                }else{
	                    $str .= "<td class=\"footable-visible\">
	                                <span class=\"label label-primary\">Não</span>
	                            </td>";
	                }
	            }
	    
	    if($venda['tipopagamento'] == '' || $venda['tipopagamento'] == ' '){
	        $str .= "
	                <td class=\"text-right footable-visible footable-last-column\">
	                    <div class=\"btn-group\">
	                        
	                        ";
	                        if($produto['status'] == 1){
	                            $str .= "<button type=\"button\" class=\"btn-white btn btn-xs glyphicon glyphicon-remove\" onclick=\"removeProduto(".$produto['id'].")\"></button>";
	                        }else{
	                            $str .= "<button type=\"button\"  class=\"btn-white btn btn-xs glyphicon glyphicon-shopping-cart\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"changeProduto(".$produto['id'].", '".$produto['nomeproduto']."')\"></button>";
	                        }

	                        $str .= "
	                    </div>
	                </td>
	            </tr>";
	    }else{
	        $str .= "
	                <td class=\"text-right footable-visible footable-last-column\">
	                    <div class=\"btn-group\">
	                        <button type=\"button\"  class=\"btn-white btn btn-xs glyphicon glyphicon-eye-open\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"viewProduto(".$produto['id'].", '".$produto['nomeproduto']."', ".$produto['quantidade'].")\"></button>
	                    </div>
	                </td>
	            </tr>";
	    }

	    return $str;
	}


	# Funções criadas para realizar os cálculos

	function getTotalVenda($app, $idvenda, $idcliente, $valor_entrada){

        $limiteGasto  = $app->modelVenda->calculaLimiteDisponivel($idvenda, $idcliente);

        $calculado    = $limiteGasto['total_compra_itens'] - $valor_entrada;

        if($calculado<0){
            $calculado = 0.00;
        }

        $total        = 'R$' . number_format($calculado, 2);

        return $total;
	}

	function getParcelamento($app, $idvenda, $idcliente, $valor_entrada){

		$limiteGasto  = $app->modelVenda->calculaLimiteDisponivel($idvenda, $idcliente);

        $max = 10;

        $str = "";

        for($i=1; $i<=$max; $i++){

            $limiteGasto  = $app->modelVenda->calculaLimiteDisponivel($idvenda, $idcliente);

            $calculado    = $limiteGasto['total_compra_itens'] - $valor_entrada;

            if($calculado<0){
                $calculado = 0.00;
            }

            $total_parcelado = ( $calculado / $i );

            $str .= "<option value=\"$i;$total_parcelado\">$i vezes de <i>" . "R$ " . number_format($total_parcelado, 2). "</i></option>";
        }

        return $str;
	}
	
	function calculaDesconto($venda,$desconto){
	    // ---- Calcula desconto da venda
	    $valorTmp = $venda['total_sem_formatacao'] * ($desconto['porcentagemdescontovenda'] / 100);
	    $valor    = 'R$ ' . number_format($venda['total_sem_formatacao'] - $valorTmp, 2);

	    return $valor;
	}