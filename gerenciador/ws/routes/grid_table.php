<?php

    # Create routes:

     $app->post( '/' . CALCULA_PRECO_PRODUTO . '/:id/:quantidade' , function ($id, $quantidade) use ($app) {

        $produto      = $produtoModel->buscar($id);
        
        $preco        =  str_replace("R$", "", $produto['valor']);      # Remove R$ da formatacao monetario
        $preco        =  str_replace(".", "", $preco);                  # Remove .  da formatacao monetario
        $preco        =  str_replace(",", ".", $preco);                 # Remove ,  da formatacao monetario
        $preco        =  trim($preco);

        if($quantidade==0) $quantidade = 1;

        $soma         = ( $quantidade * $preco );

        $soma        = 'R$ ' . number_format($soma, 2);                 # Formata via codigo o valor do limite idisponivel (duas casas decimais apos a virgula)
        $soma        = str_replace(".", ",", $soma);

        echo $app->response->write( $soma );


        exit;
    });

    $app->post( '/' . GRID_TABLE . '/:idvenda/:query', function($idvenda, $query) use ($app){

        $venda        = $app->modelVenda->buscar($idvenda);

        #Verifica se a pesquisa contém uma expressão de busca requisitada pelo usuário
        if($query == '0'){
            $produtos  = $app->modelProduto->buscarTodos($idvenda);
        }else if($query != 'gridTableItem'){
            $produtos  = $app->modelVenda->buscarProdutoVenda($idvenda, $query);
        }else if($query == 'gridTableItem'){
            $produtos  = $app->modelVenda->buscarItemVenda($idvenda);
        }

        $str = '';

        foreach ($produtos as $produto) {
            
            if($query=='gridTableItem'){
                $str .= gridTableItem($venda, $produto);
            }else{
                $str .= gridTable($venda, $produto);
            }
        }

        $str .= "<script></script>";

        echo $app->response->write( $str );

        exit;
    });