<?php

    # Create routes:

    $app->post( '/' . REMOVE_ITEM_VENDA . '/:id/:idvenda' , function ( $id, $idvenda ) use ( $app ) {

        $excluiu      = $app->modelVenda->excluirItemVenda($id, $idvenda);

        if($excluiu){
            $msg = "Item excluido da venda!";
        }else{
            $msg = "Não foi possível excluir o item da venda!";
        }

        echo $app->response->write($msg);

        exit;
    });

    $app->post( '/' . PAGAR . '/:tipoPagamento/:param', function ( $tipoPagamento, $param ) use ( $app ) {
    
        if(isset($_POST['datavencimento'])){
            $param = $_POST['datavencimento'];
        }

        $pagou      = $app->modelPgto->pagar($tipoPagamento, $_POST['idpgto'], $param);
        
        $target     = "<script>";
        $target     .= "alert('Pagamento Concluída!');";
        $target     .= "window.location.href='http://'+window.location.hostname + window.location.pathname + '?controle=pagamentosController&acao=listar'";
        $target     .= "</script>";

        echo $app->response->write($target);

        exit;
    });

    $app->post( '/' . ADD_ITEM_VENDA . '/:idvenda' , function ( $idvenda ) use ( $app ) {


        # Valida algumas coisas primeiramente
        $qtd = $_POST['qtd'];
        //if($qtd==0) $qtd = 1;

        #Verifica se qtd é maior que zero
        if($qtd<=0){
            $ret = "<script>alert('Quantidade item não pode ser menor/igual a zero (0).');</script>";
            echo $app->response->write( $ret );
            exit;
        }

        #Verifica se existe estoque para produto (item)
        if(!$app->modelVenda->verificaEstoqueItem($_POST['idproduto'],$_POST['qtd'])){
            echo $app->response->write( "<script>alert('Item com estoque inferior ou insuficiente!');</script>" );
            exit;
        }

        #VERIFICA QUAL O TIPO DE PAGAMENTO ASSOCIADO PARA ESSA VENDA
        $venda         = $app->modelVenda->buscar($idvenda);
        $tipo          = $app->modelTipoPgto->buscar($venda['formapgto']);

        ///////////////////////////////////////////////////////////////////////////////////////
        // Explito: SEGUIR_FLUXO
        // Quando o tipo de pagamento for R ou seja: Exigir Renda
        // Continua o fluxo para validacao do limite disponivel
        ///////////////////////////////////////////////////////////////////////////////////////
        // Explito: IGNORAR_TUDO
        // Quando o tipo de pagamento for diferente de R ou seja: Não Exigir Renda
        // Ignora o fluxo para validacao do limite disponivel e permite a inclusao de itens normalmente
        if($tipo['tipo']=='R')
            goto SEGUIR_FLUXO;
        else
            goto IGNORAR_TUDO;
        
        SEGUIR_FLUXO:

        $produto           = $app->modelProduto->buscar($_POST['idproduto']);
        $limiteDisponivel  = $app->modelVenda->calculaLimiteDisponivel($venda['id'], $venda['idcliente']);

        if( ($limiteDisponivel['limite_disponivel'] > 0 ) && ( $limiteDisponivel['limite_disponivel'] >= ( $qtd * $produto['valor_default'] ) ) ){
            
            IGNORAR_TUDO:

            if(isset($_POST['desconto_linha'])){
                $desconto = $_POST['desconto_linha'];
            }else{
                $desconto = 0;
            }

            $dados      = array('idvenda'=>$_POST['idvenda'], 'idproduto'=>$_POST['idproduto'], 'qtd'=>$_POST['qtd'], 'desconto_linha'=>$desconto);
            $ret        = $app->modelVenda->adicionarItemVenda($dados);

            if($ret){
                $ret =  "<script>
                        rebuildGrid(true);
                        $('#gridTable').html(requestHTTP('gridTable', ".$_POST['idvenda'].", pesquisa, null));
                        rebuildGrid(false);
                        if(pesquisa=='0') pesquisa = '';
                        $('#txt-pesquisar').val(pesquisa);

                        $('#gridTableItem').html(requestHTTP('gridTable', ".$_POST['idvenda'].", 'gridTableItem', null));
                        $('#quantidade').val(null);
                        $('#myModal').modal('hide');
                        $('#modal-pesquisar-item').modal('show');
                        calculaLimiteDisponivel();
                        alert('Item adicionado com sucesso!' );
                        </script>";
            }else{
                $ret = "<script>
                        $('#gridTable').html(requestHTTP('gridTable', ".$_POST['idvenda'].", null, null));
                        $('#quantidade').val(null);
                        calculaLimiteDisponivel();
                        alert('Erro ao adicionar item');
                        </script>";
            }
        }else{
            $ret = "<script>
                    alert('Limite da compra excedida!');
                    </script>";
        }

        echo $app->response->write( $ret );

        exit;
    });

    $app->post('/' . VERIFICA_CONSISTENCIA . '/:idcliente/:idvenda', function ($idcliente, $idvenda) use ($app) {

        $dados    = $app->modelVenda->verificaStep2($idcliente, $idvenda);

        if($dados['possui_itens']==0){
            $str  = "<script>";
            $str .= "$('#opt3').addClass('disabled');";
            $str .= "$('#opt3').addClass('active');";
            $str .= "</script>";
        }else{
            $str = "<script>";

            $str .= "$('#opt3').removeClass('disabled');";
            $str .= "$('#opt3').removeClass('active');";

            $str .= "var total=requestHTTP('buscaTotalVenda', $idvenda, clienteSelecionado, null);";
            $str .= "var parcelamento = requestHTTP('buscaParcelamento', $idvenda, clienteSelecionado, null);";
            $str .= "$('#money-total').html(total);";
            $str .= "$('#idparcelamento').html(parcelamento);";
            $str .= "$('#total-parcelado').html('1 vezes de '+total);";
            $str .= "$('#boleto-bancario').html(total);";

            $str .= "</script>";
        }

        echo $app->response->write($str);

        exit;
    });

    $app->post( '/' . FINALIZAR_VENDA , function () use ($app) {
        # PEGA VALORES DO POST
        $id_venda      = $_POST['idvenda'];
        $dataVenc      = $_POST['datavencimento'];
        $qtd           = $_POST['qtd'];
        $forma         = $_POST['forma'];
        $idcliente     = $_POST['cliente'];
        $id_desconto   = $_POST['desconto'];
        $valor_entrada = $_POST['valor_entrada'];
        $valor_parcela = $_POST['valor_parcela'];
        $valordescontofinal = $_POST['descontofinal'];
        //Formatar Desconto Final
        $formata      = str_replace("R$", "", str_replace("", "", $valordescontofinal));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalorsemponto = str_replace(",", ".", str_replace("", "", $formatavalor));
        $descontofinal   = $formatavalorsemponto;
        
        $formaPgto     = $app->modelTipoPgto->buscar($forma);

        $excedeuLimite = FALSE;

        # --------------------------------------------------------
        # VERIFICAR REGRAS DE NEGÓCIO
        # --------------------------------------------------------
        if($formaPgto['tipo'] == 'R'){

            #Verifica limite disponivel do cliente
            
            $limiteDisponivel  = $app->modelVenda->calculaLimiteDisponivel($id_venda, $idcliente);
            
            if( ($limiteDisponivel['limite_disponivel'] < 0.01 ) ){
                $target         = "<script>";
                $target         .= "alert('Limite da compra excedido!');";
                $target         .= "</script>";
                $excedeuLimite  = TRUE;
            }

        }

        $dados          = array(
                                "id"=>$id_venda, 
                                "data_vencimento"=>$dataVenc, 
                                "quantidade_parcela"=>$qtd, 
                                "forma_pgto"=>$formaPgto, 
                                "id_desconto"=>$id_desconto, 
                                "valor_entrada"=>$valor_entrada, 
                                "id_cliente"=>$idcliente,
                                "valor_parcela"=>$valor_parcela,
                                "descontofinal"=>$descontofinal
                        );

        if( $excedeuLimite == FALSE ){
            
            $str = $app->modelVenda->finalizarVenda($dados);
                
            if($str){

                $target     = "<script>";
                $target     .= "alert('Venda Concluída!');";
                $target     .= "window.location.href='http://'+window.location.hostname + window.location.pathname + '?controle=vendaController&acao=listar'";
                $target     .= "</script>";
            }else{
                $target = "<script>";
                $target .= "alert('$str');";
                $target .= "</script>";
            }

        }

        echo $app->response->write($target);

        exit;
    });

    $app->post( '/' . SALVAR_VENDA_CAPA , function () use ($app) {
        

        if($_POST['cliente'] != 0){

            $dados      = array('idvenda'=>$_POST['idvenda'], 
                                'cliente'=>$_POST['cliente'], 
                                'colaborador'=>$_POST['colaborador'], 
                                'data'=>$_POST['data'], 
                                'formaPgto'=>$_POST['formaPgto'],
                                'obs'=>$_POST['obs'],
                                'melhordiapagamentocarne'=>$_POST['melhordiapagamentocarne']);
            $venda_id   = $app->modelVenda->salvarCapa($dados);
        }else{
            $venda_id   = $_POST['idvenda'];
        }

        $str = 
            "<script>
                $('#order_id').val($venda_id);
                $('#opt2').removeClass('disabled');
                $('#opt1').removeClass('active');
                $('#opt2').addClass('active');
                if(checkCapa()){
                    $('#main-title').html('Itens da venda');
                    $('#form-step1').hide();
                    $('#form-step2').show();
                    $('#form-step3').hide();
                    $('#btn-salva-capa').hide();
                    $('#btn-pesquisa-item').show();
                    $('#btn-finalizar').hide();
                }
                if(formaObject[1]=='D' && formaObject[2]=='N' && formaObject[3]=='N')
                    $('#section-desconto-linha').show();
                else{
                    $('#section-desconto-linha').hide();
                }
                calculaLimiteDisponivel();

            </script>";

        echo $app->response->write( $str );

        exit;
    });

    $app->post( '/' . RECALCULAR_VALORES . '/:idvenda/:valor_entrada' , function ($idvenda, $valor_entrada) use ($app) {

        $valor_entrada        = str_replace("R$ ", "", $valor_entrada);
        $valor_entrada        = str_replace(".", "",  $valor_entrada);
        $valor_entrada        = str_replace(",", ".", $valor_entrada);
        $valor_entrada        = preg_replace("/\s+/", "",  $valor_entrada);

        $idcliente            = $_POST['idcliente'];

        /*$str = "<script>";

        $str .= "var total = requestHTTP('buscaTotalVenda', $idvenda, clienteSelecionado, 'valor_entrada=$valor_entrada');";
        $str .= "var parcelamento = requestHTTP('buscaParcelamento', $idvenda, clienteSelecionado, 'valor_entrada=$valor_entrada');";
        $str .= "$('#money-total').html(total);";
        $str .= "$('#idparcelamento').html(parcelamento);";
        $str .= "$('#total-parcelado').html('1 vezes de '+total);";
        $str .= "$('#boleto-bancario').html(total);";

        $str .= "</script>";*/

        $total        = getTotalVenda($app, $idvenda, $idcliente, $valor_entrada); 
        $parcelamento = getParcelamento($app, $idvenda, $idcliente, $valor_entrada);

        $str    = '<script>';
        $str   .= "$('#money-total').html('".$total."');";
        $str   .= "$('#idparcelamento').html('".$parcelamento."');";
        $str   .= "$('#total-parcelado').html('1 vezes de + ".$total."');";
        $str   .= "$('#boleto-bancario').html('".$total."');";
        $str   .= "</script>";

        echo $app->response->write($str);

        exit;
    });

    $app->post( '/' . SALVAR_PGTO_CAPA , function () use ($app) {

        //Valor formatação
        $valor        = urldecode($_POST['valor']);
        
        $valor        = str_replace("R$ ", "", $valor);
        $valor        = str_replace(".", "",  $valor);
        $valor        = str_replace(",", ".", $valor);
        $valor        = preg_replace("/\s+/", "",  $valor);

        #Verifica se valor a pagar é menor ou igual ao valor em aberto para essa fatura
        $totais         = $app->modelPgto->totalEmAberto($_POST['cliente']);
        $total_aberto   = $totais['total_aberto'];

        if($valor <= $total_aberto){

            $str = "";

            if( $valor > 0){
                if($_POST['cliente'] != 0){
                    $dados      = array(
                                    'id'=>$_POST['id'],
                                    'cliente'=>$_POST['cliente'], 
                                    'datapagamento'=>$_POST['data'], 
                                    'valorpago'=>$valor
                                );

                    $pgto_id    = $app->modelPgto->salvaPgtoCapa($dados);
                }else{
                    $pgto_id    = $_POST['id'];
                }

                $clienteSelecionado = $_POST['cliente'];

                $str = 
                    "   <script>
                            $('#idpagamento').val($pgto_id);
                            $('#form-step1').hide('slide', { direction: 'left' }, timeEffect);
                            setTimeout(function(){
                                $('#form-step2').show('slide', { direction: 'right' }, timeEffect);
                            }, timeWait);";

                $str .= "  var total=requestHTTP('buscaTotalPgto', $clienteSelecionado, $pgto_id, null);";
                $str .= "  var parcelamento = requestHTTP('buscaParcelamentoPgto', $clienteSelecionado, $pgto_id, null);";
                $str .= "  if(total) $('#money-total').html(total);";
                $str .= "  if(parcelamento) $('#idparcelamento').html(parcelamento);";
                $str .= "  if(parcelamento) $('#total-parcelado').html('1 vezes de '+total);";
                $str .= "  if(total) $('#boleto-bancario').html(total);";

                $str .= "</script>";
            }else{
                $str =  "<script>";
                $str .= "alert('Valor não pode ser 0');";
                $str .= "</script>";
            }
        }else{

            $str  = "<script>";
            $str .= "alert('Valor total em aberto não disponível para o pagamento informado. Favor verificar o valor do pagamento e tentar novamente!');";
            $str .= "</script>";
        }

        echo $app->response->write( $str );

        exit;
    });