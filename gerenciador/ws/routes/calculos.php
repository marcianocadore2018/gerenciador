<?php

    # Create routes:
     
    $app->post( '/' . CALCULA_PRECO_PRODUTO . '/:id' , function ($id) use ($app) {

        $produto      = $app->modelProduto->buscar($id);
        
        $preco        =  str_replace("R$", "", $produto['valor']);      # Remove R$ da formatacao monetario
        $preco        =  str_replace(".", "", $preco);                  # Remove .  da formatacao monetario
        $preco        =  str_replace(",", ".", $preco);                 # Remove ,  da formatacao monetario
        $preco        =  trim($preco);

        $quantidade = 0;

        $soma         = ( $quantidade * $preco );

        $soma        = 'R$ ' . number_format($soma, 2);                 # Formata via codigo o valor do limite idisponivel (duas casas decimais apos a virgula)
        $soma        = str_replace(".", ",", $soma);

        echo $app->response->write( $soma );


        exit;
    });

     $app->post( '/' . CALCULA_PRECO_PRODUTO . '/:id/:quantidade' , function ($id, $quantidade) use ($app) {

        $produto      = $app->modelProduto->buscar($id);
        
        $preco        =  str_replace("R$", "", $produto['valor']);      # Remove R$ da formatacao monetario
        $preco        =  str_replace(".", "", $preco);                  # Remove .  da formatacao monetario
        $preco        =  str_replace(",", ".", $preco);                 # Remove ,  da formatacao monetario
        $preco        =  trim($preco);

        if(isset($_POST['desconto_linha'])){
            $desconto_linha = $_POST['desconto_linha'];
        }else{
            $desconto_linha = 0;
        } 

        if($quantidade==0) $quantidade = 1;

        $soma         = ( $quantidade * $preco );

        # Calculo desconto da linha
        if($desconto_linha>0 && $desconto_linha != ''){
            $desconto     = $soma * ($desconto_linha / 100);
        }else{
            $desconto = 0.00;
        }

        if($desconto < 0)
            $desconto = 0;

        # Valida desconto
        $soma        = $soma - $desconto;

        if($soma < 0)
            $soma = 0.00;

        $soma        = 'R$ ' . number_format($soma, 2);                 # Formata via codigo o valor do limite idisponivel (duas casas decimais apos a virgula)
        $soma        = str_replace(".", ",", $soma);

        echo $app->response->write( $soma );


        exit;
    });

     $app->post( '/' . CALCULA_LIMITE_DISPONIVEL . '/:idvenda/:idcliente', function ($idvenda, $idcliente) use ($app) {

        #----------------------------------
        # Retorno do $limiteDisponivel
        #----------------------------------
        # @param total_renda                  total da renda do cliente
        # @param total_compra_itens           total da compra
        # @param limite_disponivel            limite disponivel para esse cliente
        # @param limite_disponivel_formatado  limite disponivel para impressao na view
        #----------------------------------

        #Verifica limite disponivel do cliente
        $limiteDisponivel  = $app->modelVenda->calculaLimiteDisponivel($idvenda, $idcliente);

        if($limiteDisponivel['limite_disponivel'] < 0.01){
            $limite_disponivel = 0.00;
            $limite_disponivel = 'R$' . number_format($limite_disponivel, 2);
            $limite_disponivel = str_replace(".", ",", $limite_disponivel);
        }else{
            $limite_disponivel = $limiteDisponivel['limite_disponivel_formatado'];
            $limite_disponivel = str_replace(' ', '', $limite_disponivel);
        }
        



        echo $app->response->write( $limite_disponivel );

        exit;
    });

    $app->post( '/' . APLICAR_DESCONTO . '/:idVenda/:idDesconto', function ($idVenda, $idDesconto) use ($app) {

        $venda             = $app->modelVenda->buscar($idVenda);

        if($idDesconto > 0){
            $desconto      = $app->modelDescontoVenda->buscar($idDesconto);

            $submit_value  = calculaDesconto($venda,$desconto);    
        }else{
            $submit_value  = $venda['total'];
        }

        echo $app->response->write( $submit_value );

        exit;
    });

    $app->post( '/' . TOTAL_ABERTO . '/:idcliente' , function ($idCliente) use ($app) {

        $total        = $app->modelPgto->totalEmAberto($idCliente);
        
        $total        = $total['total_aberto'];
        //$qtdde        = $total['quantidade_parcelas'];

        $total        = 'R$' . number_format($total, 2);
        //$qtdde        = 'R$' . number_format($qtdde, 2);
        
        $str          = "<script>";
        
        $str         .= "$('#total-aberto').val('$total');";
        //$str         .= "$('#valor').val('$qtdde');";

        $str         .= "var tmp = $('#total-aberto').val().replace('R$', '').replace(',','');";
        $str         .= "if(tmp > 0){
                            $('#total-aberto').css('color', 'red');
                        }else{
                            $('#total-aberto').css('color', 'blue');
                        }";
        $str         .= "</script>";
        
        echo $app->response->write( $str );

        exit;
    });