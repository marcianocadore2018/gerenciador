<?php

    # Create routes:

    $app->post( '/' . BUSCA_PRECO_PRODUTO . '/:idproduto' , function ( $idproduto ) use ( $app ) {

        $produto      = $app->modelProduto->buscar($idproduto);

        $valor        = str_replace(" ", "", $produto['valor']); 
        $valor        = str_replace("R$", "R$ ", $valor);

        echo $app->response->write($valor);

        exit;
    });

    $app->post( '/' . BUSCA_TOTAL_VENDA . '/:idvenda/:idcliente', function ( $idvenda, $idcliente ) use ( $app ) {
        
        if(isset($_POST['valor_entrada']))
            $valor_entrada = $_POST['valor_entrada'];
        else
            $valor_entrada = 0.00;

        echo $app->response->write( getTotalVenda($app, $idvenda, $idcliente, $valor_entrada) );

        exit;
    });

    $app->post( '/' . BUSCA_TOTAL_PAGAMENTO . '/:idcliente/:idvenda', function ($idcliente, $idvenda) use ( $app ) {
    
        $valor       = $app->modelPgto->buscaValorPgto($idcliente, $idvenda);

        $total        = 'R$' . number_format($valor, 2);

        echo $app->response->write($total);

        exit;
    });

    $app->post( '/' . BUSCA_PARCELAMENTO . '/:idvenda/:idcliente' , function ( $idvenda, $idcliente ) use ( $app ) {

        if(isset($_POST['valor_entrada']))
                $valor_entrada = $_POST['valor_entrada'];
            else
                $valor_entrada = 0.00;
            
        echo $app->response->write( getParcelamento($app, $idvenda, $idcliente, $valor_entrada) );

        exit;
    });

    $app->post( '/' . BUSCA_PARCELAMENTO_PGTO . '/:idcliente/:idpgto' , function ( $idcliente, $idpgto ) use ( $app ) {

        $total        = $app->modelPgto->buscaValorPgto($idcliente, $idpgto);

        $max = 10;

        $str = "0";

       for($i=1; $i<=$max; $i++){

            $total_parcelado = ( $total / $i );

            $str .= "<option value='$i'>$i vezes de <i>" . 'R$' . number_format($total_parcelado, 2)."</i></option>";
        }

        echo $app->response->write($str);

        exit;
    });