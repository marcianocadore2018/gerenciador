//Função para máscara de valor
$(document).ready(function ($) {
    $(".monetary").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#valor").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#valorcomissao").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#valoritemroupa").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#valordisponivelproduto").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#valorlimitevendaproduto").maskMoney({symbol: 'R$ ',showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    $("#cpf_atualizado").mask("999.999.999-99");
    $("#rg").mask("9999999999");
    $("#cnpj").mask("99.999.999/9999-99");
    $("#telefone").mask("(99)9999-9999");
    $("#celular").mask("(99)99999-9999");
    $("#cep").mask("99999-999");
    $("#data").mask("99/99/9999");
    $("#pispasep").mask("999.99999.99-9");
    $("#serie").mask("999-9");
    $("#melhordiapagamentocarne").mask("99/99/999");
    $("#datapagamentoinicio").mask("99/99/9999");
    $("#datapagamentofim").mask("99/99/9999");
    $("#periodocadastroinicio").mask("99/99/9999");
    $("#periodocadastrofim").mask("99/99/9999");
    $("#periodovendainicio").mask("99/99/9999");
    $("#periodovendafim").mask("99/99/9999");

    //Data Calendar
    $("#data, #datademissao, #admissao, #datainiciorenda, #datavenda, #melhordiapagamentocarne").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    
    $("#datapagamentoinicio").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    $("#datapagamentofim").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    
    $("#periodocadastroinicio").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    $("#periodocadastrofim").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });

    $("#periodovendainicio").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    $("#periodovendafim").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    $("#data, #data, #datademissao, #admissao, #datainiciorenda, #datavenda").on("change",function(){
        if(typeof verificaIdade == 'function'){
            var selected = $(this).val();
            verificaIdade(selected);
        }
    });
});

//Função para máscara de porcentagem
$(document).ready(function ($) {
    $("#porcentagem").maskMoney({symbol: '%', thousands: '.', decimal: '.', symbolStay: true});
});

//Validar CPF
//Valida o CPF digitado
//Atualização da máscara de CPF, foi alterado pois um CPF não passou no teste da formatação
//Foi testado o cpf 36032034020 e a validação anterior informava que o CPF era inválido, quando na verdade não era
//Author da modificação da validação do CPF: Marciano Luis Cadore
//                             Data Criação: 05/01/2016
//Begin formatação CPF
function verifica_cpf_atualizado(valor) {
    valor = valor.toString();
    valor = valor.replace(/[^0-9]/g, '');
    if ( valor.length === 11 ) {
        return 'CPF';
    } else {
        return false;
    }
    
} 

function calc_digitos_posicoes(digitos, posicoes = 10, soma_digitos = 0) {
    digitos = digitos.toString();
    for (var i = 0; i < digitos.length; i++) {
        soma_digitos = soma_digitos + (digitos[i] * posicoes);
        posicoes--;
        if (posicoes < 2) {
            posicoes = 9;
        }
    }
    soma_digitos = soma_digitos % 11;
    if (soma_digitos < 2) {
        soma_digitos = 0;
    } else {
        soma_digitos = 11 - soma_digitos;
    }

    var cpf = digitos + soma_digitos;
    return cpf;
    
}

function valida_cpf(valor) {
    valor = valor.toString();
    valor = valor.replace(/[^0-9]/g, '');
    var digitos = valor.substr(0, 9);
    var novo_cpf = calc_digitos_posicoes(digitos);
    var novo_cpf = calc_digitos_posicoes(novo_cpf, 11);
    if (novo_cpf === valor) {
        return true;
    } else {
        return false;
    }
}

function valida_cpf_atualizado(valor) {
    var valida = verifica_cpf_atualizado(valor);
    valor = valor.toString();
    valor = valor.replace(/[^0-9]/g, '');
    if ( valida === 'CPF' ) {
        return valida_cpf(valor);
    }
    else {
        return false;
    }  
}

function formata_cpf_atualizado(valor) {
    var formatado = false;
    var valida = verifica_cpf_atualizado(valor);
    valor = valor.toString();
    valor = valor.replace(/[^0-9]/g, '');

    if (valida === 'CPF') {
        if ( valida_cpf( valor ) ) {
            formatado  = valor.substr( 0, 3 ) + '.';
            formatado += valor.substr( 3, 3 ) + '.';
            formatado += valor.substr( 6, 3 ) + '-';
            formatado += valor.substr( 9, 2 ) + '';
        }
    }
    return formatado;
}

$(function(){
    $('#cpf_atualizado').blur(function(){
        //Primeiramente é capturado os valores verificando se os números informados no campo CPF são iguais.
        //Se os valores do campo CPF forem iguais é feito a validação abaixo e exibida a mensagem de cpf inválido e o campo é limpado
        //Foi acrescentado esse teste na validação do CPF pois a máscara não previa todos os números iguais, então foi necessário criar esse teste
        var valorvariavel = document.getElementById('cpf_atualizado').value;
        if(valorvariavel === '000.000.000-00' ||
           valorvariavel === '111.111.111-11' || 
           valorvariavel === '222.222.222-22' || 
           valorvariavel === '333.333.333-33' || 
           valorvariavel === '444.444.444-44' || 
           valorvariavel === '555.555.555-55' || 
           valorvariavel === '666.666.666-66' || 
           valorvariavel === '777.777.777-77' || 
           valorvariavel === '888.888.888-88' || 
           valorvariavel === '999.999.999-99'){

            alert('CPF Inválido por favor digite novamente!');
            document.getElementById('cpf_atualizado').value='';
            document.getElementById('cpf_atualizado').focus();
        }
        var cpf_atualizado = $(this).val();
        if ( formata_cpf_atualizado(cpf_atualizado)) {
            $(this).val( formata_cpf_atualizado(cpf_atualizado));
        } else {
            //Se os números informados no campo CPF não forem todos iguais será executado esse teste abaixo
            var valorvariavel = document.getElementById('cpf_atualizado').value;
            if(valorvariavel !== ''){
                alert('CPF Inválido por favor digite novamente!');
                document.getElementById('cpf_atualizado').value='';
                document.getElementById('cpf_atualizado').focus();
            }
            
        } 
    });
});
//End formatação CPF

//Validar CNPJ
//valida o CNPJ digitado
function ValidarCNPJ(ObjCnpj) {
    var cnpj = ObjCnpj.value;
    var valida = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
    var dig1 = new Number;
    var dig2 = new Number;

    exp = /\.|\-|\//g
    cnpj = cnpj.toString().replace(exp, "");
    var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

    for (i = 0; i < valida.length; i++) {
        dig1 += (i > 0 ? (cnpj.charAt(i - 1) * valida[i]) : 0);
        dig2 += cnpj.charAt(i) * valida[i];
    }
    dig1 = (((dig1 % 11) < 2) ? 0 : (11 - (dig1 % 11)));
    dig2 = (((dig2 % 11) < 2) ? 0 : (11 - (dig2 % 11)));

    if (((dig1 * 10) + dig2) != digito){
        alert('CNPJ Invalido, favor informe novamente!');
        document.getElementById('cnpj').value='';
    }
}

//Função somente Números
function Onlynumbers(e)
{
    var tecla = new Number();
    if (window.event) {
        tecla = e.keyCode;
    }
    else if (e.which) {
        tecla = e.which;
    }
    else {
        return true;
    }
    if ((tecla >= "97") && (tecla <= "122")) {
        return false;
    }
}

//Função Somente Letras
function Onlychars(e)
{
    var tecla = new Number();
    if (window.event) {
        tecla = e.keyCode;
    }
    else if (e.which) {
        tecla = e.which;
    }
    else {
        return true;
    }
    if ((tecla >= "48") && (tecla <= "57")) {
        return false;
    }
}

//Máscara de Telefone Nono Dígito
$(document).ready(function ($) {
    function inputHandler(masks, max, event) {
        var c = event.target;
        var v = c.value.replace(/\D/g, '');
        var m = c.value.length > max ? 1 : 0;
        VMasker(c).unMask();
        VMasker(c).maskPattern(masks[m]);
        c.value = VMasker.toPattern(v, masks[m]);
    }
    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('input[attrname=telephone1]');
    if(tel){
        VMasker(tel).maskPattern(telMask[0]); 
        tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);
    }

    var docMask = ['999.999.999-999', '99.999.999/9999-99'];
    var doc = document.querySelector('#doc');
    if(doc){
        VMasker(doc).maskPattern(docMask[0]);
        doc.addEventListener('input', inputHandler.bind(undefined, docMask, 14), false);
    }
});